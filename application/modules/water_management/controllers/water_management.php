<?php
class Water_management extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('water_management_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/users_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('real_estate_administration/tenants_model');
		$this->load->model('administration/personnel_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
	}
	
	public function index()
	{
		$where = 'property_invoice.property_id = property.property_id';
		$table = 'property,property_invoice';

		$accounts_search = $this->session->userdata('all_water_property');
		
		if(!empty($accounts_search))
		{
			$where .= $accounts_search;	
			
		}

		$segment = 2;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'property-readings';
		$config['total_rows'] = $this->water_management_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();

		$query = $this->water_management_model->get_all_water_records($table, $where, $config["per_page"], $page);

		$data['title'] = $this->site_model->display_page_title();
		$v_data['properties'] = $this->property_model->get_active_property();
		$v_data['invoice_types'] = $this->water_management_model->get_invoice_types();
		// $v_data['property_invoices'] = $this->water_management_model->get_property_invoices();
		$v_data['title'] = $data['title'];
		$v_data['page'] = $page;
		$v_data['property_invoices'] = $query;
		$data['content'] = $this->load->view('water_reporting', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	public function import_water_readings_template()
	{
		$this->water_management_model->import_template();
	}
	function do_water_readings_import()
	{
		$this->form_validation->set_rules('property_invoice_id', 'Invoice', 'required|xss_clean');
		//$this->form_validation->set_rules('invoice_type_id', 'Type', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if(isset($_FILES['import_csv']))
			{
				// var_dump($message_category_id); die();
				if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
				{
					//import products from excel 
	
					$response = $this->water_management_model->import_csv_charges($this->csv_path);
					
					
					if($response == FALSE)
					{
	
					}
					
					else
					{
						if($response['check'])
						{
							$v_data['import_response'] = $response['response'];
						}
						
						else
						{
							$v_data['import_response_error'] = $response['response'];
						}
					}
				}
				
				else
				{
					$v_data['import_response_error'] = 'Please select a file to import.';
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('cash-office/property-readings');
	}
	
	public function print_water_readings($document_number)
	{
		$data['document_number'] = $document_number;
		$data['document_details'] = $this->water_management_model->get_document_details($document_number);
		$data['rental_units'] = $this->water_management_model->get_rental_units();
		$data['invoice_types'] = $this->water_management_model->get_invoice_types();
		$data['contacts'] = $this->site_model->get_contacts();
		$this->load->view('reading_reports', $data);
	}
	
	public function send_invoices($record_id)
    {
    	if($this->water_management_model->send_invoices($record_id))
    	{
			$this->session->set_userdata('success_message', 'Reminder was send successfully successfully');	
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry something has gone wrong. PLease try again to send the message');
		}
		redirect('cash-office/property-readings');
	}
	
	public function add_property_invoice() 
	{
		//form validation rules
		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_type_id', 'Invoice Type', 'required|xss_clean');
		$this->form_validation->set_rules('property_invoice_amount', 'Amount', 'required|xss_clean');
		$this->form_validation->set_rules('property_invoice_units', 'Units', 'required|xss_clean');
		$this->form_validation->set_rules('property_invoice_date', 'Date', 'required|xss_clean');
		$this->form_validation->set_rules('property_invoice_number', 'Invoice Number', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$response = $this->water_management_model->add_property_invoice();
			if($response)
			{
				$this->session->set_userdata("success_message", "Property invoice added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message", "Could not add property invoice. Please try again");
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('cash-office/property-readings');
	}
	
	public function edit_property_invoice($property_invoice_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_type_id', 'Invoice Type', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_amount', 'Amount', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_units', 'Units', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_date', 'Date', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$response = $this->water_management_model->edit_property_invoice($property_invoice_id);
			if($response)
			{
				$this->session->set_userdata("success_message", "Property invoice updated successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message", "Could not update property invoice. Please try again");
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('cash-office/property-readings');
	}
	
	public function delete_property_invoice($property_invoice_id) 
	{
		if($this->water_management_model->delete_roperty_invoice($property_invoice_id))
		{
			$this->session->set_userdata("success_message", "Property invoice deleted successfully");
		}
		
		else
		{
			$this->session->set_userdata("error_message", "Could not delete property invoice. Please try again");
		}
		redirect('cash-office/property-readings');
	}

	public function generate_report()
	{


		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');
		$this->form_validation->set_rules('year', 'Year', 'required|xss_clean');
		$this->form_validation->set_rules('month', 'Month', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$response = $this->water_management_model->generate_property_reading_report($property_invoice_id);
			if($response)
			{
				$this->session->set_userdata("success_message", "Property invoice updated successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message", "Could not update property invoice. Please try again");
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('property-readings');
	}


	public function water_tenants($property_invoice_id, $property_idd) 
	{
			
		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_idd;
		$table = 'tenants,tenant_unit,rental_unit,leases,property';		


		$accounts_search = $this->session->userdata('all_water_tenants_search');
		
		if(!empty($accounts_search))
		{
			$where .= $accounts_search;	
			
		}
		$segment = 4;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'view-reading-details/'.$property_invoice_id.'/'.$property_idd;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->water_management_model->get_all_water_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		$rs8 = $properties->result();
		$property_list = '';
		$property_names = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;
			if($property_idd == $property_id)
			{
				$property_names = $property_name;
				$property_list .="<option value='".$property_id."' selected='selected'>".$property_name." Location: ".$property_location."</option>";
			}
		endforeach;

		$v_data['property_list'] = $property_list;
		$v_data['property_invoice_id'] = $property_invoice_id;
		$data['title'] = $property_names.' Units';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['property_id'] = $property_idd;
		$data['content'] = $this->load->view('water_tenants', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function update_invoice($tenant_unit_id,$lease_id,$rental_unit_id)
	{
		$this->form_validation->set_rules('current_reading'.$lease_id, 'current reading', 'trim|required|xss_clean');
		$this->form_validation->set_rules('previous_reading'.$lease_id, 'previous reading', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_charges'.$lease_id, 'Water Charge', 'trim|required|xss_clean');

		// var_dump($_POST);die();
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$current_reading = $this->input->post('current_reading'.$lease_id);
			$previous_reading = $this->input->post('previous_reading'.$lease_id);
			$water_charges = $this->input->post('water_charges'.$lease_id);

			if($current_reading < $previous_reading)
			{
				$this->session->set_userdata("error_message", "Sorry you the current reading is not accurate");
			}

			else
			{
				if($this->water_management_model->update_water_invoice($lease_id,$rental_unit_id))
				{

					$this->session->set_userdata("success_message", "You have successfully billed for water");
				}
				
				else
				{
					$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");

				}
			}
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function water_tenant_payments($tenant_unit_id,$lease_id)
	{
		$v_data = array('tenant_unit_id'=>$tenant_unit_id,'lease_id'=>$lease_id);
		$v_data['title'] = 'Tenant payments';
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		// get lease payments for this year
		$v_data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);
		$v_data['lease_pardons'] = $this->accounts_model->get_lease_pardons($lease_id);

		$data['content'] = $this->load->view('water_tenant_payments', $v_data, true);
		
		$data['title'] = 'Tenant payments';
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function close_property_invoice($property_invoice_id,$property_id,$property_invoice_status)
	{
		$data['property_invoice_status'] = $property_invoice_status;
		$this->db->where('property_invoice_id',$property_invoice_id);
		$this->db->update('property_invoice',$data);
		redirect('property-readings');
	}

	public function print_water_bills($property_invoice_id,$property_id)
	{

		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id.' AND property_invoice.property_id = property.property_id AND property_invoice.property_invoice_id = water_management.property_invoice_id AND property_invoice.property_invoice_id = '.$property_invoice_id;
		$table = 'tenants,tenant_unit,rental_unit,leases,property,property_invoice,water_management';	


		$this->db->where($where);
		$this->db->order_by('rental_unit.rental_unit_name','ASC');
		$query = $this->db->get($table);
		// var_dump($query);die();
		$data['rental_units'] = $this->water_management_model->get_rental_units();
		$data['contacts'] = $this->site_model->get_contacts();
		$data['query'] = $query;
		$this->load->view('reading_reports', $data);

	}

	public function search_water_tenants()
	{
		$property_id = $this->input->post('property_id');
		$tenant_name = $this->input->post('tenant_name');
		$rental_unit_name = $this->input->post('rental_unit_name');
		
		$search_title = '';
		
		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}

			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';
			
		}
		
		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name.' ';
			$tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';

			
		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';
			
		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $property_id.$tenant_name.$rental_unit_name;
		$property_search = $property_id;
		// var_dump($search); die();
		
		// $this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('all_water_tenants_search', $search);
		$this->session->set_userdata('accounts_search_title', $search_title);

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function close_accounts_search($property_invoice_id,$property_id)
	{
		$this->session->unset_userdata('all_water_tenants_search');
		$this->session->unset_userdata('accounts_search_title');	
		
		redirect('view-reading-details/'.$property_invoice_id.'/'.$property_id);
	}

	public function search_water_property()
	{
		$property_id = $this->input->post('property_id');		
		$search_title = '';
		
		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}

			$property_id = ' AND property_invoice.property_id = '.$property_id.' ';
			
		}
		
	


		$property_search = $property_id;
		// var_dump($search); die();
		
		// $this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('all_water_property', $property_search);
		$this->session->set_userdata('accounts_search_title', $search_title);

		redirect('property-readings');
	}
	public function close_water_search()
	{
		$this->session->unset_userdata('all_water_property');
		$this->session->unset_userdata('accounts_search_title');

		redirect('property-readings');
	}

	public function export_readings($property_invoice_id,$property_id)
	{
		$this->water_management_model->export_readings($property_invoice_id,$property_id);
	}

	public function import_water_reading_view($property_invoice_id,$property_id)
	{

		$v_data['title'] = 'Import Water Readings';
		$v_data['property_invoice_id'] = $property_invoice_id;
		$v_data['property_id'] = $property_id;
		$v_data['property_invoices'] = $this->water_management_model->get_property_invoices();
		$data['content'] = $this->load->view('import_readings', $v_data, true);
		
		$data['title'] = 'Import water readings';
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function import_readings_template()
	{
		$this->water_management_model->import_water_template();
	}
	function do_readings_import($property_invoice_id,$property_id)
	{
		$this->form_validation->set_rules('property_invoice_id', 'Invoice', 'required|xss_clean');
		//$this->form_validation->set_rules('invoice_type_id', 'Type', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if(isset($_FILES['import_csv']))
			{
				// var_dump($message_category_id); die();
				if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
				{
					//import products from excel 
	
					$response = $this->water_management_model->import_csv_charges_water($this->csv_path);
					
					
					if($response == FALSE)
					{
	
					}
					
					else
					{
						if($response['check'])
						{
							$v_data['import_response'] = $response['response'];
						}
						
						else
						{
							$v_data['import_response_error'] = $response['response'];
						}
					}
				}
				
				else
				{
					$v_data['import_response_error'] = 'Please select a file to import.';
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('view-reading-details/'.$property_invoice_id.'/'.$property_id);
	}

}
?>