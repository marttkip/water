<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
    	<h2 class="panel-title pull-right"></h2>
    	<h2 class="panel-title">Search</h2>
    		<a  href="<?php echo site_url();?>property-readings"  style="margin-top:-24px" class="btn btn-sm btn-success pull-right"> <i class="fa fa-arrow-left"></i> Back to water report </a>
			<!-- <a href="<?php echo site_url();?>accounts/send_notifications" style="margin-top:-24px;margin-right:5px;" class="btn btn-sm btn-default pull-right" onclick="return confirm('Do you want to send notifications for  ?')"> <i class="fa fa-outbox"></i> Send Bulk Notifications</a> -->
    </header>             

  <!-- Widget content -->
        <div class="panel-body">
	<?php
    echo form_open("water_management/search_water_tenants", array("class" => "form-horizontal"));
    ?>
    <div class="row">
     <div class="col-md-3">
            <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
            <div class="form-group">
                <label class="col-lg-4 control-label">Tenant Name: </label>
               							            
	            <div class="col-lg-8">
	            	<input type="text" class="form-control" name="tenant_name" placeholder="Tenant Name" >
	            </div>
            </div>
            
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="col-lg-4 control-label">Unit Name: </label>
                
                <div class="col-lg-8">
                	<input type="text" class="form-control" name="rental_unit_name" placeholder="Unit Name i.e. ASHT001" >
                </div>
            </div>
        </div>
        
       
        
        <div class="col-md-4">
            
            <div class="form-group">
                <label class="col-lg-3 control-label"> Property</label>
                
                <div class="col-lg-9">
                   <select  name='property_id' class='form-control'>
                      <!-- <option value=''>None - Please Select a property</option> -->
                      <?php echo $property_list;?>
                    </select>
                </div>
            </div>
            
            
        </div>
        <div class="col-md-2">
        	<div class="form-group">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="center-align">
                        <button type="submit" class="btn btn-info">SEARCH LIST</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
    
    
    <?php
    echo form_close();
    ?>
  </div>
</section>