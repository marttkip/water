<?php

$month = $this->water_management_model->get_months();
$months_list = '<option value="0">Select a Type</option>';
foreach($month->result() as $res)
{
  $month_id = $res->month_id;
  $month_name = $res->month_name;
  if($month_id < 10)
  {
    $month_id = '0'.$month_id;
  }
  $month = date('M');

  if($month == $month_name)
  {
    $months_list .= '<option value="'.$month_id.'" selected>'.$month_name.'</option>';
  }
  else {
    $months_list .= '<option value="'.$month_id.'">'.$month_name.'</option>';
  }



}


$start = 2015;
$end_year = 2030;
$year_list = '<option value="0">Select a Type</option>';
for ($i=$start; $i < $end_year; $i++) {
  // code...
  $year= date('Y');

  if($year == $i)
  {
    $year_list .= '<option value="'.$i.'" selected>'.$i.'</option>';
  }
  else {
    $year_list .= '<option value="'.$i.'">'.$i.'</option>';
  }
}


?>
<div class="row">
	<div class="col-md-4"> 
	<section class="panel panel-featured panel-featured-info">
	    <header class="panel-heading">
	    	<h2 class="panel-title pull-right"></h2>
	    	<h2 class="panel-title">Search</h2>
	    </header>             

	  <!-- Widget content -->
	        <div class="panel-body">
		<?php
	    echo form_open("search-water-properties", array("class" => "form-horizontal"));
	    ?>
	    <div class="row">
	    	<div class="col-md-12">            
	            <div class="form-group">                
	                <div class="col-lg-12">
	                   <select  name='property_id' class='form-control' required="required">
	                      <option value=''>None - Please Select a property</option>
	                       <?php
		                     if($properties->num_rows() > 0)
		                     {
		                     	foreach ($properties->result() as $key => $value) {
		                     		# code...
		                     		$property_id = $value->property_id;
		                     		$property_name = $value->property_name;
		                     		echo '<option value="'.$property_id.'">'.$property_name.'</option>';
		                     	}
		                     }

		                     ?>
	                    </select>
	                </div>
	            </div>   
	        </div>
	    </div>
	     	<br>
	    <div class="row">
	        <div class="col-md-12">
	        	<div class="form-group">
		            <div class="col-lg-8 col-lg-offset-2">
		                <div class="center-align">
		                    <button type="submit" class="btn btn-info">SEARCH</button>
		                </div>
		            </div>
		        </div>
	        </div>
	       
	    </div>
	   
	    
	    
	    <?php
	    echo form_close();
	    ?>
	  </div>
	</section>
</div>
<div class="col-md-8">
	<section class="panel panel-featured panel-featured-info">
	    <header class="panel-heading">
	    	<h2 class="panel-title pull-right"></h2>
	    	<h2 class="panel-title">Search</h2>
	    </header>             

	  <!-- Widget content -->
	        <div class="panel-body">
		<?php
	    echo form_open("generate-water-report", array("class" => "form-horizontal"));
	    ?>
	    <div class="row">
	    	<div class="col-md-12">
		    	<div class="col-md-4">            
		            <div class="form-group">   
		            <!-- <label class="col-lg-4 control-label">Property: </label>                -->
		                <div class="col-lg-12">
		                   <select  name='property_id' class='form-control' required="required">
		                      <option value=''>None - Please Select a property</option>
		                     <?php
		                     if($properties->num_rows() > 0)
		                     {
		                     	foreach ($properties->result() as $key => $value) {
		                     		# code...
		                     		$property_id = $value->property_id;
		                     		$property_name = $value->property_name;
		                     		echo '<option value="'.$property_id.'">'.$property_name.'</option>';
		                     	}
		                     }

		                     ?>
		                    </select>
		                </div>
		            </div>   
		        </div>
		     	<div class="col-md-4">
		            <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
		             <div class="form-group" id="payment_method">
                        <label class="col-md-4 control-label">Month: </label>

                        <div class="col-md-8">
                          <select class="form-control select2" name="month"   required>
                            <?php echo $months_list;?>
                          </select>
                          </div>
                      </div>
		            
		        </div>
		         <div class="col-md-4">
		             <div class="form-group" id="payment_method">
                        <label class="col-md-4 control-label">Year: </label>
                        <div class="col-md-8">
                          <select class="form-control select2" name="year"   required>

                            <?php echo $year_list;?>
                          </select>
                          </div>
                    </div>
		        </div>
		    </div>
	    </div>
	    <br>
	    <div class="row">
	    	<div class="col-md-12">
		        <div class="col-md-6">
		              <div class="form-group">
                        <label class="col-lg-4 control-label">Invoice Date: </label>                       
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="invoice_date" placeholder="Invoice Date" required="required">
                            </div>
                        </div>
                    </div>
		        </div>
		        <div class="col-md-6">
		        	<div class="form-group">
			            <div class="col-lg-8 col-lg-offset-2">
			                <div class="center-align">
			                    <button type="submit" class="btn btn-info" onclick="return confirm('You are about to generate a water report. Do you want to continue ?')">GENERATE</button>
			                </div>
			            </div>
			        </div>
		        </div>
		    </div>
	       
	    </div>
	   
	    
	    
	    <?php
	    echo form_close();
	    ?>
	  </div>
	</section>
</div>
</div>
	