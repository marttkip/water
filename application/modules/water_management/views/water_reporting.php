<?php echo $this->load->view('generate_water_invoice','', true); ?>
<?php
	$result = '';
	$action_point = '';
	//if users exist display them
	
	
	$result2 = '';
	//if users exist display them
	if ($property_invoices->num_rows() > 0)
	{
		$count = 0;
		
		$result2 .= 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Invoice Date</th>
					<th>Property</th>
					<th>Invoice Number</th>
					<th>For</th>
					<th>Status</th>
					<th colspan="3">Actions</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		
		foreach ($property_invoices->result() as $row)
		{
			$property_invoice_id = $row->property_invoice_id;
			$property_name = $row->property_name;
			$property_invoice_number = $row->property_invoice_number;
			$property_invoice_date = date('jS M Y',strtotime($row->property_invoice_date));
			$property_invoice_amount = $row->property_invoice_amount;
			$property_invoice_units = $row->property_invoice_units;
			$property_invoice_status = $row->property_invoice_status;
			$property_id = $row->property_id;
			$month = $row->month;
			$year = $row->year;
			$created = date('F Y',strtotime($year.'-'.$month));

			if($property_invoice_status == 1)
			{
				$buttons = '<td><a href="'.site_url().'view-reading-details/'.$property_invoice_id.'/'.$property_id.'"  class="btn btn-sm btn-success" ><i class="fa fa-folder-open"></i> View</a></td>
						<td><a href="'.site_url().'close-property-invoice/'.$property_invoice_id.'/'.$property_id.'/2"  class="btn btn-sm btn-warning" onclick="return confirm(\' You are about to mark this invoice as completed note that you will not be able to adjust any invoices for this particular month. Do you want to proceed ? \')"><i class="fa fa-folder-closed" ></i>Close Invoice</a></td>
						<td><a href="'.site_url().'close-property-invoice/'.$property_invoice_id.'/'.$property_id.'/3"  class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this invoice\')"><i class="fa fa-trash"></i> Delete</a></td>';
				$status = 'success';
				$status_name = 'Open';
			}
			else if($property_invoice_status == 2)
			{
				$buttons = '<td><a href="'.site_url().'view-reading-details/'.$property_invoice_id.'/'.$property_id.'"  class="btn btn-sm btn-success" ><i class="fa fa-folder-open"></i> View</a></td>
					<td><a href="'.site_url().'print-reading-details/'.$property_invoice_id.'/'.$property_id.'"  class="btn btn-sm btn-info" target="_blank"><i class="fa fa-folder-open" ></i> Print Report </a></td>

					<td><a href="'.site_url().'export-reading-details/'.$property_invoice_id.'/'.$property_id.'"  class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-folder-open"></i> Export Report </a></td>';
				$status = 'warning';
				$status_name = 'Closed';
			}

			else
			{
				$buttons = '<td><a href="'.site_url().'view-reading-details/'.$property_invoice_id.'/'.$property_id.'"  class="btn btn-sm btn-success" ><i class="fa fa-folder-open"></i> View</a></td>';
				$status = '';
				$status_name = 'success';
			}
			
			$count++;
			$result2 .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td class="'.$status.'">'.$property_invoice_date.'</td>
					<td class="'.$status.'">'.$property_name.'</td>
					<td class="'.$status.'">'.$property_invoice_number.'</td>
					<td class="'.$status.'">'.$created.'</td>
					<td class="'.$status.'">'.$status_name.'</td>
					'.$buttons.'
	
				</tr> 
			';
		}
		
		$result2 .= 
		'
					  </tbody>
					</table>
		';
	}
	
	else
	{
		$result2 .= "There are no invoices";
	}
?>

<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title">Readings</h2>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
            	<?php
					$success = $this->session->userdata('success_message');
				
					if(!empty($success))
					{
						echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
						$this->session->unset_userdata('success_message');
					}
				
					$error = $this->session->userdata('error_message');
				
					if(!empty($error))
					{
						echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
						$this->session->unset_userdata('error_message');
					}

					$search =  $this->session->userdata('all_water_property');
					if(!empty($search))
					{
						echo '<a href="'.site_url().'water_management/close_water_search" class="btn btn-sm btn-warning">Close Search</a>';
					}
				?>
				
				<?php
					if(isset($import_response))
					{
						if(!empty($import_response))
						{
							echo $import_response;
						}
					}
					
					if(isset($import_response_error))
					{
						if(!empty($import_response_error))
						{
							echo '<div class="center-align alert alert-danger">'.$import_response_error.'</div>';
						}
					}
				?>
               <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <?php echo $result2;?>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
				    <?php if(isset($links)){echo $links;}?>
				</div>
                                                
            </div>
        </div>
    </div>
</section>