<?php echo $this->load->view('search/tenants_search','', true); ?>
<?php
// var_dump($property_id);die();
$result = '';
		
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th><a>Unit Name</a></th>
				<th><a>Tenant Name</a></th>
				<th><a>Previous Reading</a></th>
				<th><a>Current Reading</a></th>
				<th><a>Units Consumed</a></th>
				
				<th><a>Rate</a></th>
				<th><a>Total Due</a></th>
				<th colspan="3">Actions</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	
	
	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_id = $leases_row->tenant_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$rental_unit_id = $leases_row->rental_unit_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$tenant_email = $leases_row->tenant_email;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$initial_reading = $leases_row->initial_reading;

		// var_dump($initial_reading);die();
		$rent_calculation = $leases_row->rent_calculation;
		// $property_id = $leases_row->property_id;
		$deposit = $leases_row->deposit;
		$water_charges = $leases_row->billing_amount;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$points = 0;//$leases_row->points;
		$created = $leases_row->created;

	
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

		$invoice_id= $this->accounts_model->get_max_invoice_id($lease_id);
		// var_dump($invoice_id); die();
		$prev_reading = '';
		$current_reading = 0;
		$units_consumed = 0;
		$arrears_bf = 0;
		$property_invoice_status = 0;
		if(!empty($invoice_id))
		{
			$water_readings = $this->accounts_model->get_water_readings($invoice_id);
			if($water_readings->num_rows() > 0)
			{
				foreach ($water_readings->result() as $key) {
					# code...
					$prev_reading = $key->prev_reading;
					// $current_reading = $key->current_reading;
					$arrears_bf = $key->arrears_bf;
					// var_dump($arrears_bf);die();
					// $units_consumed = $current_reading - $prev_reading;
				}
			}
		}

		if(empty($arrears_bf))
		{
			$prev_reading = $initial_reading;
		}
		else
		{
			$prev_reading = $arrears_bf;

		}


	

		$water_readings_current = $this->accounts_model->get_current_reading($lease_id,$property_invoice_id);
		$current_reading = 0;
		
		if($water_readings_current->num_rows() > 0)
		{
			foreach ($water_readings_current->result() as $key) {
				# code...
				$prev_reading = $key->prev_reading;
				$current_reading = $key->current_reading;
				$arrears_bf = $key->arrears_bf;
				$property_invoice_status = $key->property_invoice_status;
				// var_dump($arrears_bf);die();
				$units_consumed = $current_reading - $prev_reading;
			}
		}
		if($current_reading > 0)
		{
			$current_reading = $current_reading;
		}
		else
		{
			$current_reading = 0;
		}
		


		// $property_invoice_status = $this->water_management_model->get_invoice_status($property_invoice_id);
		// var_dump($arrears_bf);die();

		if($property_invoice_status == 1 OR $property_invoice_status == 0)
		{
			$button = '<td><button  class="btn btn-sm btn-primary" onclick="return confirm(\' You are about to update the reading. Do you want to continue ?\')" >Update</button>
				</td>';

			$current = '';
		}
		else
		{

			$current = 'readonly';
			$button = '';
		}
		$count++;
		$string  = 'Do you want to update invoice  ?';
		$result .= 
		'
			'.form_open("update-water-invoice/".$tenant_unit_id."/".$lease_id.'/'.$rental_unit_id, array("class" => "form-horizontal")).'
			<input type="hidden"  class="form-control" value="'.$property_id.'" name="property_id'.$lease_id.'" readonly>
			<input type="hidden"  class="form-control" value="'.$property_invoice_id.'" name="property_invoice_id'.$lease_id.'" readonly>
			<input type="hidden" name="redirect_url" value="'.$this->uri->uri_string().'">
			<tr>
				<td>'.$count.'</td>
				<td>'.$rental_unit_name.'</td>
				<td>'.$tenant_name.'</td>
				<input type="hidden" class="form-control" value="'.$water_charges.'" name="water_charges'.$lease_id.'" >
				<td><input type="text"  class="form-control" value="'.$prev_reading.'" name="previous_reading'.$lease_id.'" readonly></td>
				<td><input type="number"  class="form-control" value="'.$current_reading.'" name="current_reading'.$lease_id.'" '.$current.' required="required"></td>
				<td>'.$units_consumed.'</td>
				<td>112</td>						
				<td>'.number_format($units_consumed * 112 ,2).'</td>
				'.$button.'
				
			</tr> 
			'.form_close().'
		';
		
	}
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}

// var_dump($property_invoice_id);die();
// $property_invoice_id = $property_invoice_id;
$accounts_search_title = $this->session->userdata('all_water_tenants_search');

$property_invoice_status = $this->water_management_model->get_invoice_status($property_invoice_id);
?>  
<!-- href="<?php echo site_url();?>accounts/update_invoices" -->
<section class="panel">
		<header class="panel-heading">						
			<h2 class="panel-title"><?php echo $title;?></h2>
			<?php
			if($property_invoice_status == 1)
			{
				?>
				<a  href="<?php echo site_url();?>import-water-readings/<?php echo $property_invoice_id;?>/<?php echo $property_id;?>"  style="margin-top:-24px" class="btn btn-sm btn-success pull-right"> <i class="fa fa-arrow-down"></i> Import Readings </a>
				<a href="<?php echo site_url().'close-property-invoice/'.$property_invoice_id.'/'.$property_id.'/2';?>"  style="margin-top:-24px;margin-right:5px;" class="btn btn-sm btn-danger pull-right" onclick="return confirm(' You are about to mark this invoice as completed note that you will not be able to adjust any invoices for this particular month. Do you want to proceed ? ')"><i class="fa fa-folder-closed" ></i>Close Invoice</a>
				<?php
			}
			else if($property_invoice_status == 2)
			{
				?>
				<a href="<?php echo site_url().'print-reading-details/'.$property_invoice_id.'/'.$property_id;?>" style="margin-top:-24px" class="btn btn-sm btn-info pull-right" target="_blank"><i class="fa fa-folder-open" ></i> Print Report </a>
				<a href="<?php echo site_url().'export-reading-details/'.$property_invoice_id.'/'.$property_id?>"  style="margin-top:-24px;margin-right:5px;" class="btn btn-sm btn-primary pull-right" target="_blank"><i class="fa fa-folder-open"></i> Export Report </a></td>
				<?php
			}
			else
			{

			}
			?>
			

		</header>
		<div class="panel-body">
        	<?php
            $success = $this->session->userdata('success_message');

			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}
			
			$error = $this->session->userdata('error_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			$search =  $this->session->userdata('all_water_tenants_search');
			if(!empty($search))
			{
				echo '<a href="'.site_url().'water_management/close_accounts_search/'.$property_invoice_id.'/'.$property_id.'" class="btn btn-sm btn-warning">Close Search</a>';
			}
					
			?>

        	
			<div class="table-responsive">
            	
				<?php echo $result;?>
		
            </div>
		</div>
        <div class="panel-footer">
        	<?php if(isset($links)){echo $links;}?>
        </div>
	</section>

	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>