 <section class="panel">
	<header class="panel-heading">						
	    <h2 class="panel-title"><?php echo $title;?></h2>
	</header>
	<div class="panel-body">
	    <?php echo form_open_multipart("import-readings/".$property_invoice_id."/".$property_id, array("class" => "form-horizontal","role" => "form"));?>
	        <div class="row">
	            <div class="col-md-6">
	                <ul>
	                    <li>Download the import template <a href="<?php echo site_url().'import-water-readings-template';?>">here.</a></li>
	                    
	                    <li>Save your file as a <strong>csv</strong> file before importing</li>
	                    <li>Select the Property </li>
	                    <li>Set the invoice date (Note : This is the date that will appear as the invoiced date) </li>
	                    <li>Set the total total units consumed for the month</li>

	                </ul>
	            </div>
	            <div class="col-md-6">
	                <div class="form-group">
	                    <label class="col-lg-5 control-label">Invoice: </label>
	                    <div class="col-lg-7">
	                        <select class="form-control" name="property_invoice_id">
	                            <?php
	                            if($property_invoices->num_rows() > 0)
	                            {
	                                foreach ($property_invoices->result() as $row) {
	                                    # code...
										$property_invoice_date = $row->property_invoice_date;
										$property_name = $row->property_name;
	                                    $property_invoice_idd = $row->property_invoice_id;
										$property_invoice_number = $row->property_invoice_number;
									
										if($property_invoice_id == $property_invoice_idd )
										{
											 ?>
		                                    <option value="<?php echo $property_invoice_id;?>"><?php echo $property_name;?> - <?php echo $property_invoice_number;?> - <?php echo $property_invoice_date;?></option>
		                                    <?php
										}
	                                   
	                                }
	                            }
	                            ?>
	                        </select>
	                    </div>
	                </div>
	              
	                <!-- <div class="fileUpload btn btn-primary"> -->
	                <div class="form-group">
	                    <label class="col-lg-5 control-label">Import Rental Arreas List</label>
	                    <div class="col-lg-7">
	                        <input type="file" class="upload" onChange="this.form.submit();" name="import_csv" />
	                    </div>
	                </div>
	            </div>
	        </div>
	    <?php echo form_close();?>
	   
	</div>
	
</section>