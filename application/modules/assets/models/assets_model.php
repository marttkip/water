<?php
    class Assets_model extends CI_Model 
    {	

 public function get_all_asset($table, $where, $config, $page, $order, $order_method = 'ASC')
    	
     {
    		//retrieve all users
    		$this->db->select('*');
    		$this->db->where($where);
    		$this->db->order_by($order, $order_method);
    		$query = $this->db->get($table, $config, $page);
    		
    		return $query;
	  }		

 public function add_asset_details()
	{
		$data = array(
				'asset_name'=>ucwords(strtolower($this->input->post('asset_name'))),
				'asset_id'=>$this->input->post('asset_id'),
				'asset_serial_no'=>$this->input->post('asset_serial_no'),
				'asset_model_no'=>$this->input->post('asset_model_no'),
				'asset_description'=>$this->input->post('asset_description'),
				'asset_pd_period'=>$this->input->post('asset_pd_period'),
				'ldl_type'=>$this->input->post('ldl_type'),
				'ldl_date'=>$this->input->post('ldl_date'),
				'asset_supplier_no'=>$this->input->post('asset_supplier_no'),
				'asset_project_no'=>$this->input->post('asset_project_no'),
				'asset_owner_name'=>$this->input->post('asset_owner_name'),
				'asset_inservice_period'=>$this->input->post('asset_inserivce_period'),
				'asset_disposal_period'=>$this->input->post('asset_disposal_period'),
				'created'=>date('Y-m-d H:i:s')
				
			);
			
		if($this->db->insert('assets_details', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
     }

    public function update_asset($asset_id)
	{
		$data = array(
				'asset_name'=>$this->input->post('asset_name'),
				'asset_model_no'=>$this->input->post('asset_model_no'),
				'asset_serial_no'=>$this->input->post('asset_serial_no'),
				'asset_description'=>$this->input->post('asset_description'),
				'asset_pd_period'=>$this->input->post('asset_pd_period'),
				'ldl_type'=>$this->input->post('ldl_type'),
				'ldl_date'=>$this->input->post('ldl_date'),
				'asset_supplier_no'=>$this->input->post('asset_supplier_no'),
				'asset_project_no'=>$this->input->post('asset_project_no'),
				'asset_owner_name'=>$this->input->post('asset_owner_name'),
				'asset_inservice_period'=>$this->input->post('asset_inserivce_period'),
				'asset_disposal_period'=>$this->input->post('asset_disposal_period'),
				'created'=>date('Y-m-d H:i:s')
				
			);
			
		$this->db->where('asset_id', $asset_id);
		if($this->db->update('assets_details', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

 public function get_asset($asset_id)
	
	  {
		//retrieve all users
		$this->db->from('assets_details');
		$this->db->select('*');
		$this->db->where('asset_id = '.$asset_id);
		$query = $this->db->get();
		
		return $query;    	
 
     }	

   public function delete_asset($asset_id)
	{
		if($this->db->delete('assets_details', array('asset_id' => $asset_id)))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

  }


   public function activate_asset($asset_id)
	 {
		$data = array(
				'asset_status' => 1
			);
		$this->db->where('asset_id', $asset_id);
		
		if($this->db->update('assets_details', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	 }
public function deactivate_asset($asset_id)
	{
		$data = array(
				'asset_status' => 0
			);
		$this->db->where('asset_id', $asset_id);
		
		if($this->db->update('assets_details', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}	 
	  

}	

?>