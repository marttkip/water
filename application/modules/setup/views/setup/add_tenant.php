<?php
//personnel data
$customer_surname = set_value('customer_surname');
$customer_first_name = set_value('customer_first_name');
$customer_phone = set_value('customer_phone');
$customer_email = set_value('customer_email');
$customer_post_code = set_value('customer_post_code');
$customer_address = set_value('customer_address');

?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>setup/tenant" class="btn btn-info pull-right">Back to Tenants</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">First Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_first_name" placeholder="First Name" value="<?php echo $customer_first_name;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">SurName: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_surname" placeholder="Surname" value="<?php echo $customer_surname;?>">
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Phone Number: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_phone" placeholder="Phone Number" value="<?php echo $customer_phone;?>">
            </div>
        </div>
        
        
    </div>
    
    <div class="col-md-6">
        
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Email Address: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_email" placeholder="Email Address" value="<?php echo $customer_email;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Postal Address: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_post_code" placeholder="Postal Address" value="<?php echo $customer_post_code;?>">
            </div>
        </div>
        
       
        <div class="form-group">
            <label class="col-lg-5 control-label">National ID: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="customer_address" placeholder="National ID" value="<?php echo $customer_address;?>">
            </div>
        </div>
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Tenants
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>