<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Property Name</th>
						<th>Rental Unit Name</th>
						<th>Tenant Name</th>
						<th>Registration Date</th>
						<th>Size</th>
						<th>Status</th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$rental_unit_id = $row->rental_unit_id;
				$tenant_name = $this->setup_model->get_all_unit_tenant($rental_unit_id);
				if($tenant_name == FALSE){
					$tenant_name_sent ='<a href="'.site_url().'setup/add_lease/'.$rental_unit_id.'" class="btn btn-sm btn-warning"><i class="fa fa-user"></i> Allocate Tenant</a>';
				}else{
					$tenant_name_sent = $this->setup_model->get_all_unit_tenant($rental_unit_id);
				}

				$rental_unit_name = $row->rental_unit_name;
				$rental_unit_size = $row->rental_unit_size;
				$created = $row->created;
				$property_id = $row->property_id;
				$property_name = $row->property_name;
				$rental_unit_status = $row->rental_unit_status;
				
				//status
				if($rental_unit_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($rental_unit_status == 0)
				{
					
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'setup/activate_rental_unit/'.$rental_unit_id.'" onclick="return confirm(\'Do you want to activate '.$rental_unit_name.'?\');" title="Activate '.$rental_unit_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				    $button_sent = ' ';	
				    $button_edit = ' ';
				    
				}
				//create activated status display
				else
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'setup/deactivate_rental_unit/'.$rental_unit_id.'" onclick="return confirm(\'Do you want to deactivate '.$rental_unit_name.'?\');" title="Deactivate '.$rental_unit_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				    $button_sent = '<a href="'.site_url().'setup/meters/'.$property_id.'/'.$rental_unit_id.'" class="btn btn-sm btn-info pull-right"> Meter Data</a>';
			        $button_edit ='<a href="'.site_url().'setup/edit_rental_unit/'.$rental_unit_id.'" class="btn btn-sm btn-success" title="Profile '.$rental_unit_name.'"><i class="fa fa-pencil"></i> Edit</a>';
				 }
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$property_name.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$tenant_name_sent.'</td>
						<td>'.date('jS M Y H:i a',strtotime($row->created)).'</td>
						<td>'.$rental_unit_size.'</td>
						<td>'.$status.'</td>
						<td>'.$button_sent.'</td>
						<td>'.$button_edit.'</td>
						<td>'.$button.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Rental Units";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">  
                                    <div class="col-lg-2 col-lg-offset-10">
                                    	<a href="<?php echo site_url();?>setup/add_unit/<?php echo $property_id?>" class="btn btn-sm btn-info pull-right">Add unit</a>
                                    </div>
                                </div>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

