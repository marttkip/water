<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						
						<th>Invoice Type Code</th>
						<th>Invoice Type Name</th>
						<th>Status</th>
						
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$invoice_type_id = $row->invoice_type_id;
				$invoice_type_name = $row->invoice_type_name;
				$invoice_type_code = $row->invoice_type_code;
				$invoice_type_status = $row->invoice_type_status;
				
				//status
				if($invoice_type_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($invoice_type_status == 0)
				{
					
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'setup/activate_rental_unit/'.$invoice_type_id.'" onclick="return confirm(\'Do you want to activate '.$invoice_type_name.'?\');" title="Activate '.$invoice_type_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				    $button_sent = ' ';	
				    $button_edit = ' ';
				}
				//create activated status display
				else
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'setup/deactivate_rental_unit/'.$invoice_type_id.'" onclick="return confirm(\'Do you want to deactivate '.$invoice_type_name.'?\');" title="Deactivate '.$invoice_type_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				    $button_sent = '<a href="setup/meters/'.$invoice_type_id.'/'.$invoice_type_id.'" class="btn btn-sm btn-info pull-right"> Meter Data</a>';
			        $button_edit ='<a href="'.site_url().'setup/edit_rental_unit/'.$invoice_type_id.'" class="btn btn-sm btn-success" title="Profile '.$invoice_type_name.'"><i class="fa fa-pencil"></i> Edit</a>';
				   
				}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$invoice_type_code.'</td>
						<td>'.$invoice_type_name.'</td>
						<td>'.$status.'</td>
						
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Invoice Types Registered";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">  
                                    <div class="col-lg-2 col-lg-offset-10">
                                    	<a href="<?php echo site_url();?>setup/add-invoice-type" class="btn btn-sm btn-info pull-right">Add Invoice Type</a>
                                    </div>
                                </div>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

