<?php

class Customers_model extends CI_Model 
{	
	/*
	*	Retrieve all customers
	*
	*/
	public function all_customers()
	{
		$this->db->where('customer_status = 1');
		$query = $this->db->get('customer');
		
		return $query;
	}
	
	/*
	*	Retrieve all ride types
	*
	*/
	public function get_ride_types()
	{
		$this->db->order_by('ride_type_name');
		$query = $this->db->get('ride_type');
		
		return $query;
	}
	
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_customers($table, $where, $per_page, $page, $order = 'customer_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_yard_customers($table, $where, $per_page, $page, $order = 'customer_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	
	public function get_all_customer_contacts($table, $where, $per_page, $page, $order = 'customer_contacts_first_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	

	/*
	*	Add a new customer
	*	@param string $image_name
	*
	*/
	public function add_company_contact($company_id)
	{
		$data = array(
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				'customer_id'=>$company_id
				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Add a new customer
	*	@param string $image_name
	*
	*/
	public function add_customer($is_company)
	{
		if($is_company == 0){
			$one = '0';
		}else{
			$one = '1';
		}
		$data = array(

				'customer_surname'=>$this->input->post('customer_surname'),
				'customer_first_name'=>$this->input->post('customer_first_name'),
				'customer_phone'=>$this->input->post('customer_phone'),
				'customer_email'=>$this->input->post('customer_email'),
				'customer_post_code'=>$this->input->post('customer_post_code'),
				'customer_address'=>$this->input->post('customer_address'),
				'customer_created'=>date('Y-m-d H:i:s'),
				'company_status'=>$one


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function add_yard_customer($is_company)
	{
		if($is_company == 0){
			$one = '0';
		}else{
			$one = '1';
		}
		$data = array(

				'customer_surname'=>$this->input->post('customer_surname'),
				'customer_first_name'=>$this->input->post('customer_first_name'),
				'customer_phone'=>$this->input->post('customer_phone'),
				'customer_email'=>$this->input->post('customer_email'),
				'customer_post_code'=>$this->input->post('customer_post_code'),
				'customer_address'=>$this->input->post('customer_address'),
				'customer_created'=>date('Y-m-d H:i:s'),
				'company_status'=>$one


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('yard_customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	
	/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_customer($customer_id)
	{
		$data = array(

				'customer_surname'=>$this->input->post('customer_surname'),
				'customer_first_name'=>$this->input->post('customer_first_name'),
				'customer_phone'=>$this->input->post('customer_phone'),
				'customer_email'=>$this->input->post('customer_email'),
				'customer_post_code'=>$this->input->post('customer_post_code'),
				'customer_address'=>$this->input->post('customer_address'),
				
			);
			
		$this->db->where('customer_id', $customer_id);
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function update_yard_customer($customer_id)
	{
		$data = array(

				'customer_surname'=>$this->input->post('customer_surname'),
				'customer_first_name'=>$this->input->post('customer_first_name'),
				'customer_phone'=>$this->input->post('customer_phone'),
				'customer_email'=>$this->input->post('customer_email'),
				'customer_post_code'=>$this->input->post('customer_post_code'),
				'customer_address'=>$this->input->post('customer_address'),
				
			);
			
		$this->db->where('customer_id', $customer_id);
		if($this->db->update('yard_customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer($customer_id)
	{
		//retrieve all users
		$this->db->from('customer');
		$this->db->select('*');
		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get();
		
		return $query;
	}
	





/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_company_contact($customer_contact_id) 
	{

		$data = array(
			
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				
				
			);
			
		$this->db->where('customer_contacts_id', $customer_contact_id);
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer_contact($customer_contact_id)
	{
		//retrieve all users
		$this->db->from('customer_contacts');
		$this->db->select('*');
		$this->db->where('customer_contacts_id = '.$customer_contact_id);
		$query = $this->db->get();
		
		return $query;
	}
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 1
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 0
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Delete an existing customer
	*	@param int $customer_id
	*
	*/
	public function delete_customer($customer_id)
	{
		if($this->db->delete('customer', array('customer_id' => $customer_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 1
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 0
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Retrieve personnel
	*
	*/
	public function get_personnel_type($personnel_type_id)
	{
		$this->db->where(array('personnel_status' => 1, 'personnel_type_id' => $personnel_type_id));
		$query = $this->db->get('personnel');
		
		return $query;
	}
}
?>