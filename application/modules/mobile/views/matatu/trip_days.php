<?php
$today = date('Y-m-d');
$total_amount = 0;
$trip_data = '';

$trips = '<div class="list-block media-list" id="current_trips">';
	$trips .= '<div class="content-block-title center-align">'.date('jS M, Y',strtotime($today)).'</div>';
	
if($query->num_rows() > 0)
{
	foreach ($query->result() as $row)
	{
		$trip_name = $row->trip_name;
		$trip_id = $row->trip_id;
		$trip_count = $row->trip_count;
		$trip_amount = $this->matatu_model->get_total_trip_amount($trip_id);
		$total_amount += $trip_amount;
		
		$trip_data .= '<li class="list-table">';
			$trip_data .= '<a href="dist/bus_collection/trip-collection.html" onclick="get_trip_routes('.$trip_id.')" class="item-link item-content">';
				$trip_data .= '<div class="item-inner row">';
					$trip_data .= '<div class="row">';
						$trip_data .= '<div class="col-30 pull-right">';
							$trip_data .= '<div class="item-text">Trip '.$trip_count.' '.$trip_name.'</div>';
						$trip_data .= '</div>';
						$trip_data .= '<div class="border-item"></div>';
						$trip_data .= '<div class="col-30 pull-left">';
						$trip_data .= '<div class="item-text">Ksh. '.number_format($trip_amount, 2).'</div>';
					$trip_data .= '</div>';
				$trip_data .= '</div>';
			$trip_data .= '</div>';
			$trip_data .= '</a>';
		$trip_data .= '</li>';
	}
}

	$trips .= '<div class="row text-title">';
		$trips .= '<div class="col-50 center-align">';
			$trips .= '<strong>TOTAL COLLECTION</strong>';
		$trips .= '</div>';
		$trips .= '<div class="border-item"></div>';
		$trips .= '<div class="col-50 center-align">';
			$trips .= '<strong>Ksh. '.number_format($total_amount, 2).'</strong>';
		$trips .= '</div>';
	$trips .= '</div>';
	$trips .= '<ul>';
		$trips .= $trip_data;
	$trips .= '</ul>';
$trips .= '</div>';

echo $trips;
?>