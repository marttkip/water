<?php
$routes = '';
if($active_leases->num_rows() > 0)
{
	foreach ($active_leases->result() as $key) {
		# code...
		$tenant_name = $key->tenant_name;
		$tenant_phone_number = $key->tenant_phone_number;
		$created = $key->created;
		$property_name = $key->property_name;
		$rental_unit_name = $key->rental_unit_name;
		$lease_id = $key->lease_id;
		
		$this->load->model('accounts/accounts_model');
		$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
		$total_arrears = $tenants_response['total_arrears'];
		$total_invoice_balance = $tenants_response['total_invoice_balance'];
		$invoice_date = $tenants_response['invoice_date'];

		$tenant_name = str_replace('%20', ' ', $tenant_name);
		$routes .= '
					<li class="list-benefit">
				      <a class="item-link item-content" >
				       	<div class="item-inner">
				          <div class="item-title-row">
				            <div class="item-title"><span><i class="fa fa-user"></i></span> '.strtoupper($tenant_name).' '.$rental_unit_name.'</div>					            
				            <div class="item-after"> Bal Ksh. '.number_format($total_arrears,2).'</div>
				          </div>
				          <div class="item-text">
				          	<span><i class="fa fa-phone"></i></span> '.$tenant_phone_number.'
				          	 <span><i class="fa fa-bank"></i></span> '.$property_name.' 
				          </div>
				        </div>
				      </a>
				    </li>
				   ';
	}
}
echo $routes;
?>