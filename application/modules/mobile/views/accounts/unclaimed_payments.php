<?php
$routes = '';
if($unclaimed_payments->num_rows() > 0)
{
	foreach ($unclaimed_payments->result() as $key) {
		# code...
		$sender_name = $key->sender_name;
		$sender_phone = $key->sender_phone;
		$amount = $key->amount;
		$created = $key->created;
		$serial_number = $key->serial_number;
		$mpesa_id = $key->mpesa_id;
		$sender_name = str_replace('%20', ' ', $sender_name);
		$routes .= '
					<li class="list-benefit" onclick="get_menu_items('.$mpesa_id.')">
				      <a class="item-link item-content" >
				       	<div class="item-inner">
				          <div class="item-title-row">
				            <div class="item-title"><span><i class="fa fa-user"></i></span> '.strtoupper($sender_name).'</div>					            
				            <div class="item-after">KES. '.number_format($amount,2).'</div>
				          </div>
				          <div class="item-text">
				          	<span><i class="fa fa-phone"></i></span> '.$sender_phone.'
				          	 <span>CODE:</span> '.$serial_number.' <span><i class="fa fa-calendar"></i></span> '.$created.' 
				          </div>
				        </div>
				      </a>
				    </li>
				   ';
	}
}
echo $routes;
?>