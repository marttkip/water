<?php

$tenant_response = $this->accounts_model->get_rent_and_service_charge($lease_id);

$grand_rent_bill = $this->accounts_model->get_cummulated_balance($lease_id,1);
$grand_water_bill = $this->accounts_model->get_cummulated_balance($lease_id,2);
$grand_electricity_bill = $this->accounts_model->get_cummulated_balance($lease_id,3);
$grand_service_charge_bill = $this->accounts_model->get_cummulated_balance($lease_id,4);
$grand_penalty_bill = $this->accounts_model->get_cummulated_balance($lease_id,5);

//  interface  for making payments
// use the property id and also the type of the person
// var_dump($property_id); die();
$property_invoices = $this->accounts_model->get_property_invoice_types($lease_id,1,1);
$inputs = '';
if($property_invoices->num_rows() > 0)
{
	// var_dump($property_invoices->result()); die();
	foreach ($property_invoices->result() as $key_types) {
		# code...
		$property_invoice_type_id = $key_types->invoice_type_id;

		if($property_invoice_type_id == 1)
		{
			$inputs .='	<li>
                          <div class="item-content item-input">
                            <div class="item-inner">
                              <div class="item-input-wrap">
                                <input type="text" name="rent_amount" placeholder="Rent Amount" autocomplete="off">
                              </div>
                            </div>
                          </div>
                        </li>
							
					   ';
		}
		else if($property_invoice_type_id == 2)
		{
			$inputs .='<li>
                          <div class="item-content item-input">
                            <div class="item-inner">
                              <div class="item-input-wrap">
                                <input type="text" name="water_amount" placeholder="Water Amount" autocomplete="off">
                              </div>
                            </div>
                          </div>
                        </li>';
		}
		else if($property_invoice_type_id == 3)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 4)
		{
			$inputs .='<li>
                          <div class="item-content item-input">
                            <div class="item-inner">
                              <div class="item-input-wrap">
                                <input type="text" name="service_charge_amount" placeholder="Service Charge Amount" autocomplete="off">
                              </div>
                            </div>
                          </div>
                        </li>';
		}
		else if($property_invoice_type_id == 5)
		{
			$inputs .='<li>
                          <div class="item-content item-input">
                            <div class="item-inner">
                              <div class="item-input-wrap">
                                <input type="text" name="pentalty_fee" placeholder="Penalty Fee" autocomplete="off">
                              </div>
                            </div>
                          </div>
                        </li>';
		}
		else if($property_invoice_type_id == 6)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 7)
		{
			$inputs .='<li>
                          <div class="item-content item-input">
                            <div class="item-inner">
                              <div class="item-input-wrap">
                                <input type="text" name="painting_charge" placeholder="Painting Charge" autocomplete="off">
                              </div>
                            </div>
                          </div>
                        </li>';
		}
		else if($property_invoice_type_id == 8)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Sinking Funds: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="sinking_funds" placeholder="" autocomplete="off">
							</div>
						</div>
					';
		}
		else if($property_invoice_type_id == 9)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Insurance: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="insurance" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 12)
		{
			$inputs .='<li>
                          <div class="item-content item-input">
                            <div class="item-inner">
                              <div class="item-input-wrap">
                                <input type="text" name="fixed_charge" placeholder="Fixed Charge" autocomplete="off">
                              </div>
                            </div>
                          </div>
                        </li>';
		}
		else if($property_invoice_type_id == 10)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Bought Water: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="bought_water" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 13)
		{
			$inputs .='<li>
                          <div class="item-content item-input">
                            <div class="item-inner">
                              <div class="item-input-wrap">
                                <input type="text" name="deposit_charge" placeholder="Deposit Amount" autocomplete="off">
                              </div>
                            </div>
                          </div>
                        </li>';
		}
		else if($property_invoice_type_id == 17)
		{
			$inputs .='<li>
                          <div class="item-content item-input">
                            <div class="item-inner">
                              <div class="item-input-wrap">
                                <input type="text" name="legal_fees" placeholder="Legal Fee" autocomplete="off">
                              </div>
                            </div>
                          </div>
                        </li>';
		}

		else if($property_invoice_type_id == 11)
		{
			$inputs .='';
		}
	}
}

echo $inputs;