<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
	}
	
	public function get_client_profile($member_id)
	{
		$v_data['job_seeker_details'] = $this->profile_model->get_profile_details($member_id);

		$response['message'] = 'success';
		$v_data['member_id'] = $member_id;
		$response['result'] = $this->load->view('job_seeker/seekers_profile', $v_data, true);

		echo json_encode($response);
	}
	
	public function reset_password()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('email_address', 'Email', 'trim|valid_email|required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->profile_model->reset_password())
			{
				$response['message'] = 'success';
			 	$response['result'] = 'Password reset to 123456';
			}
			else
			{
				$response['message'] = 'fail';
			 	$response['result'] = 'Something went wrong. Please try again';
			}

		}
		else
		{
			$validation_errors = validation_errors();
			
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				$response['message'] = 'fail';
			 	$response['result'] = $validation_errors;
			}
			
			//populate form data on initial load of page
			else
			{
				$response['message'] = 'fail';
				$response['result'] = 'Ensure that you have entered all the values in the form provided';
			}
		}
		echo json_encode($response);
	}
	
	public function add_registration_id($customer_id = NULL, $registration_id = NULL)
	{
		if(($customer_id != NULL) && ($customer_id != 'null') && ($registration_id != NULL) && ($registration_id != 'null'))
		{
			//check if exists
			$data = array('customer_id' => $customer_id, 'app_registration_id' => $registration_id);
			$this->db->where($data);
			$query = $this->db->get('customer');
			
			//save if not exists
			if($query->num_rows() == 0)
			{
				$where = array('customer_id' => $customer_id);
				$data2 = array('app_registration_id' => $registration_id);
				
				$this->db->where($where);
				$this->db->update('customer', $data);
				$response['message'] = 'success';
			}
			
			else
			{
				$response['message'] = 'success';
			}
		}
			
		else
		{
			$response['message'] = 'success';
		}
		
		echo json_encode($response);
	}
}