<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rides extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		
		$this->load->model('rides_model');
		$this->load->model('riders_model');
		
	}
	
	public function request_ride()
	{
		$this->form_validation->set_rules('from_route', 'Pick up location', 'required|xss_clean');
		$this->form_validation->set_rules('to_route', 'Destination', 'required|xss_clean');
		$this->form_validation->set_rules('distance', 'Distance', 'xss_clean');
		$this->form_validation->set_rules('duration', 'Duration', 'xss_clean');
		$this->form_validation->set_rules('request_lat', 'Request Latitude', 'xss_clean');
		$this->form_validation->set_rules('request_lng', 'Request Longitude', 'xss_clean');
		$this->form_validation->set_rules('destination_lat', 'Destination Latitude', 'xss_clean');
		$this->form_validation->set_rules('destination_lng', 'Destination Longitude', 'xss_clean');
		$this->form_validation->set_rules('customer_id', 'Customer ID', 'required|xss_clean');
		$this->form_validation->set_rules('ride_type_id', 'Request type', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$ride_id = $this->rides_model->save_ride();
			if($ride_id > 0)
			{
				$ride_type_id = $this->input->post('ride_type_id');
				$from_route = $this->input->post('from_route');
				$to_route = $this->input->post('to_route');
				
				if($ride_type_id == 1)
				{
					$title = 'New ride requested';
				}
				
				else
				{
					$title = 'New delivery requested';
				}
				$result['message'] = 'success';
				$result['result']['message'] = 'Your request was placed successfully. We are searching for a rider.';
				$result['result']['ride_id'] = $ride_id;
				
				$message = 'A new request has come from '.$from_route.' to '.$to_route;
				$this->riders_model->notify_riders($title, $message);
			}
			
			else
			{
				$result['message'] = 'error';
				$result['result'] = 'Unable to request ride. Please try again';
			}
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = validation_errors();
		}
		
		echo json_encode($result);
	}
	
	public function get_all_requests()
	{
		$requests = $this->rides_model->get_all_requests();
		$rides = array();
		if($requests->num_rows() > 0)
		{
			foreach($requests->result() as $res)
			{
				$ride_data = array(
					'from_route' => $res->from_route,
					'to_route' => $res->to_route,
					'distance' => $res->distance,
					'duration' => $res->duration,
					'request_lat' => $res->request_lat,
					'request_lng' => $res->request_lng,
					'destination_lat' => $res->destination_lat,
					'destination_lng' => $res->destination_lng,
					'customer_first_name' => $res->customer_first_name,
					'customer_phone' => $res->customer_phone,
					'ride_type_id' => $res->ride_type_id,
					'ride_id' => $res->ride_id
				);
				
				array_push($rides, $ride_data);
			}
			$response['message'] = 'success';
			$response['result'] = $rides;
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'There are no available rides';
		}
		
		echo json_encode($response);
	}
	
	public function get_customer($ride_id)
	{
		$requests = $this->rides_model->get_customer($ride_id);
		$rides = array();
		if($requests->num_rows() > 0)
		{
			foreach($requests->result() as $res)
			{
				$ride_data = array(
					'from_route' => $res->from_route,
					'to_route' => $res->to_route,
					'distance' => $res->distance,
					'duration' => $res->duration,
					'request_lat' => $res->request_lat,
					'request_lng' => $res->request_lng,
					'destination_lat' => $res->destination_lat,
					'destination_lng' => $res->destination_lng,
					'customer_first_name' => $res->customer_first_name,
					'customer_phone' => $res->customer_phone,
					'ride_type_id' => $res->ride_type_id,
					'ride_id' => $res->ride_id
				);
				
				array_push($rides, $ride_data);
			}
			$response['message'] = 'success';
			$response['result'] = $rides;
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'There are no available rides';
		}
		
		echo json_encode($response);
	}
	
	public function accpet_request($personnel_id = NULL, $ride_id = NULL)
	{
		//if form has been submitted
		if ($ride_id != NULL)
		{
			if($this->rides_model->accpet_ride($ride_id, $personnel_id))
			{
				//get personnel data
				$this->db->where('personnel_id', $personnel_id);
				$query = $this->db->get('personnel');
				
				$title = 'You have been allocated a rider';
				$message = 'Your rider is en route';
				$source = json_encode($query->row());
				
				$this->rides_model->notify_customer($ride_id, $title, $message, $source);
				
				$result['message'] = 'success';
				$result['result'] = 'Ride accepted successfully. You can now pick up the customer';
			}
			
			else
			{
				$result['message'] = 'error';
				$result['result'] = 'Unable to request ride. Please try again';
			}
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = 'Ride not selected';
		}
		
		echo json_encode($result);
	}
	
	public function reverse_geocode($latitude, $longitude)
	{
		$response = $this->rides_model->reverse_geocode($latitude, $longitude);
		//var_dump($response);// json_decode($response);
		
		$decoded = json_decode($response, true);
		$result['message'] = 'success';
		$result['result'] = $decoded['results'][0]["formatted_address"];
		echo json_encode($result);
	}
	
	public function set_rider_location($latitude, $longitude, $ride_id)
	{
		if($this->rides_model->set_rider_location($latitude, $longitude, $ride_id))
		{
			$result['message'] = 'success';
			$result['result'] = 'Rider location set successfully';
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = 'Rider location not set';
		}
		
		echo json_encode($result);
	}
	
	public function begin_ride($ride_id, $latitude, $longitude)
	{
		if($this->rides_model->begin_ride($ride_id))
		{
			$title = 'You are in transit';
			$message = 'Your ride has begun';
			$source = 'begin_ride';
			
			$this->rides_model->notify_customer($ride_id, $title, $message, $source);
			
			$result['message'] = 'success';
			$result['result'] = 'Ride begun successfully';
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = 'Ride not begun';
		}
		
		echo json_encode($result);
	}
	
	public function end_ride($ride_id, $start_latitude, $start_longitude, $end_latitude, $end_longitude)
	{
		if($this->rides_model->end_ride($ride_id, $start_latitude, $start_longitude, $end_latitude, $end_longitude))
		{
			$response = $this->rides_model->reverse_geocode($start_latitude, $start_longitude);
			$decoded = json_decode($response, true);
			//var_dump($decoded);
			$location['start_location'] = $decoded['results'][0]["formatted_address"];
			
			$response = $this->rides_model->reverse_geocode($end_latitude, $end_longitude);
			$decoded = json_decode($response, true);
			$location['end_location'] = $decoded['results'][0]["formatted_address"];
			
			$title = 'You have reached your destination';
			$message = 'Thank you for using Ubiker';
			$source = 'end_ride';
			
			//$cost = $this->rides_model->calculate_ride_cost($ride_id);
			
			$this->rides_model->notify_customer($ride_id, $title, $message, $source);
			
			$result['message'] = 'success';
			$result['result'] = $location;
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = 'Ride not ended successfully';
		}
		
		echo json_encode($result);
	}
	
	public function get_rider_location($ride_id)
	{
		$requests = $this->rides_model->get_ride($ride_id);
		$rides = array();
		if($requests->num_rows() > 0)
		{
			$res = $requests->row();
			$ride_data = array(
				'rider_latitude' => $res->rider_latitude,
				'rider_longitude' => $res->rider_longitude
			);
			
			array_push($rides, $ride_data);
			
			$response['message'] = 'success';
			$response['result'] = $rides;
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'There are no available rides';
		}
		
		echo json_encode($response);
	}
}