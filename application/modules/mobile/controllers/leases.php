<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leases extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		
		$this->load->model('leases_model');
		$this->load->model('site/site_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('admin/admin_model');
	
		$this->load->model('email_model');
	}
	
	public function get_all_unclaimed_payments()
	{
		$v_data['unclaimed_payments'] = $this->leases_model->unclaimed_payments();

		$response['message'] = 'success';
		$response['result'] = $this->load->view('accounts/unclaimed_payments', $v_data, true);

		echo json_encode($response);
	}
	public function get_property_list()
	{
		$v_data['all_properties'] = $this->leases_model->get_all_properties();

		$response['message'] = 'success';
		$response['result'] = $this->load->view('accounts/properties', $v_data, true);

		echo json_encode($response);
	}
	public function get_all_active_leases()
	{
		$v_data['unclaimed_payments'] = $this->leases_model->active_leases();

		$response['message'] = 'success';
		$response['result'] = $this->load->view('accounts/all_leases', $v_data, true);

		echo json_encode($response);
	}

	public function get_all_property_leases($property_id)
	{
		$v_data['active_leases'] = $this->leases_model->property_active_leases($property_id);

		$response['message'] = 'success';
		$response['result'] = $this->load->view('accounts/all_property_leases', $v_data, true);

		echo json_encode($response);
	}


	public function get_lease_payment_form($lease_id)
	{
		$v_data['lease_id'] = $lease_id;

		$response['message'] = 'success';
		$response['result'] = $this->load->view('accounts/payments', $v_data, true);

		echo json_encode($response);
	}
	public function get_mpesa_confirmation($mpesa_id)
	{
		$this->db->where('mpesa_id',$mpesa_id);
		$query = $this->db->get('mpesa_transactions');
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$sender_name = $value->sender_name;
				$response['sender_phone'] = $sender_phone =  $value->sender_phone;
				$response['amount'] = $amount =  $value->amount;
				$response['created'] = $created =  $value->created;
				$response['serial_number'] = $serial_number =  $value->serial_number;
				$response['account_number'] = $account_number =  $value->account_number;
				$created =  $value->created;

				$date_exploded = explode(" ", $created);
				$response['created'] = $date_exploded[0];
				$response['mpesa_id'] =  $value->mpesa_id;
				$response['sender_name'] = $name =  str_replace('%20', ' ', $sender_name);
				$response['confirmation'] = $account_number.' for Ksh. '.$amount.' sent by '.$name.' for '.$account_number;
				$response['message'] = 'success';

			}
		}
		else
		{
			$response['message'] = 'fail';
		}
		echo json_encode($response);

	}

	public function get_lease_information($lease_id)
	{
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND leases.lease_status = 1 AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND property.property_id = rental_unit.property_id AND leases.lease_id = '.$lease_id);
		$query = $this->db->get('leases,tenant_unit,rental_unit,tenants,property');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$tenant_name = $value->tenant_name;
				$response['rental_unit_name'] = $rental_unit_name =  $value->rental_unit_name;
				$response['property_name'] = $property_name =  $value->property_name;
				$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
				$total_invoice_balance = $tenants_response['total_invoice_balance'];


				$response['tenant_name'] = $name =  str_replace('%20', ' ', $tenant_name);
				$response['confirmation'] = 'Do you want to proceed receipting '.$rental_unit_name.' account of '.$tenant_name.' with current balance of Ksh. '.$total_invoice_balance;
				$response['message'] = 'success';

			}
		}
		else
		{
			$response['message'] = 'fail';
		}
		echo json_encode($response);

	}
	public function submit_payment_information()
	{
		// $this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_amount', 'Water amount', 'trim|xss_clean');
		$this->form_validation->set_rules('service_charge_amount', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('penalty_fee', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('rent_amount', 'Rent amount', 'trim|xss_clean');
		


		$payment_method = $this->input->post('payment_method');
		$water_amount = $this->input->post('water_amount');
		$rent_amount = $this->input->post('rent_amount');
		$service_charge_amount = $this->input->post('service_charge_amount');
		$penalty_fee = $this->input->post('penalty_fee');
		$amount_paid = $this->input->post('amount_paid');
		$fixed_charge = $this->input->post('fixed_charge');
		$deposit_charge = $this->input->post('deposit_charge');

		$insurance = $this->input->post('insurance');
		$sinking_funds = $this->input->post('sinking_funds');
		$bought_water = $this->input->post('bought_water');
		$painting_charge = $this->input->post('painting_charge');
		$legal_fees = $this->input->post('legal_fees');
		$lease_id = $this->input->post('lease_id');
		$mpesa_id = $this->input->post('mpesa_id');


		if(empty($water_amount))
		{
			$water_amount = 0;
		}
		if(empty($service_charge_amount))
		{
			$service_charge_amount = 0;
		}
		if(empty($rent_amount))
		{
			$rent_amount = 0;
		}
		if(empty($penalty_fee))
		{
			$penalty_fee = 0;
		}
		if(empty($fixed_charge))
		{
			$fixed_charge = 0;
		}

		if(empty($deposit_charge))
		{
			$deposit_charge = 0;
		}
		if(empty($insurance))
		{
			$insurance = 0;
		}
		if(empty($sinking_funds))
		{
			$sinking_funds = 0;
		}
		if(empty($bought_water))
		{
			$bought_water = 0;
		}
		if(empty($painting_charge))
		{
			$painting_charge = 0;
		}
		if(empty($legal_fees))
		{
			$legal_fees = 0;
		}
		//  add all this items 
		$total = $service_charge_amount + $rent_amount + $water_amount + $penalty_fee + $fixed_charge + $deposit_charge + $insurance + $sinking_funds + $bought_water + $painting_charge+ $legal_fees;

		// var_dump($total); die();
		// Normal
		
		if(!empty($payment_method))
		{
			if($payment_method == 1)
			{
				// check for cheque number if inserted
				$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');
			
			}
			else if($payment_method == 5)
			{
				//  check for mpesa code if inserted
				$this->form_validation->set_rules('mpesa_code', 'Amount', 'is_unique[payments.transaction_code]|trim|required|xss_clean');
			}
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			if($amount_paid == $total)
			{
				$payment_id = $this->leases_model->receipt_payment($lease_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
				$update_array['mpesa_status'] = 1;
				$update_array['approved'] = date('Y-m-d H:i:s');
				$update_array['lease_id'] = $lease_id;
				$this->db->where('mpesa_id',$mpesa_id);
				$this->db->update('mpesa_transactions',$update_array);
				$response['message'] = 'success';
				$response['payment_id'] = $payment_id;
				$response['result'] = 'Payment successfully added';
			}
			else
			{
				$this->session->set_userdata("error_message", 'The amounts of payment do not add up to the total amount paid');
				$response['message'] = 'fail';
				$response['result'] = 'The amounts of payment do not add up to the total amount paid';
			}
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['message'] = 'fail';
			$response['result'] = 'Ensure that you have filled in all the values';
			
		}

		echo json_encode($response);

	}
	public function send_receipt_payment($lease_id,$payment_id)
	{

		$data = array('payment_id' => $payment_id,'lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $lease_payments = $this->accounts_model->get_lease_payments($lease_id);
		
		$data['payment_details'] = $payment_details = $this->accounts_model->get_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_lease_detail_mobile($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];


		

		if($payment_details->num_rows() > 0)
		{
			$y = 0;
			foreach ($payment_details->result() as $key) 
			{
				$payment_id = $key->payment_id;
				$receipt_number = $key->receipt_number;
				$amount_paid = $key->amount_paid;
				$paid_by = $key->paid_by;
				$payment_date = $date_of_payment = $key->payment_date;
				$payment_created = $key->payment_created;
				$payment_created_by = $key->payment_created_by;
				$transaction_code = $key->transaction_code;
				$invoice_month_number = $key->month;
				
				$payment_date = date('jS M Y',strtotime($payment_date));
				$payment_created = date('jS M Y',strtotime($payment_created));
				$y++;
			}
		}
	
		$tenant_phone_number = 254720465220;
		$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
		$total_arrears = $tenants_response['total_arrears'];
		// var_dump($tenant_phone_number);die();
		if(!empty($tenant_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			// $tenant_array = explode(' ', $tenant_name);
			// $tenant_name = $tenant_array[0];

			
			$message = 'Dear '.$tenant_name.', Your payment of  Ksh. '.$amount_paid.' for '.$rental_unit_name.' has been successfully received. You current balance is Ksh. '.$total_arrears;
			$this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);

			// save the message sent out 
			$insert_array = array('phone_number'=>$tenant_phone_number,'client_name'=>$tenant_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'sms_type'=>2);
			$this->db->insert('sms',$insert_array);
			// save the message sent out
			
		}
		$response['message'] ='success';
		echo json_encode($response);
	}
	
}
