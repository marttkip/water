<?php

class Rides_model extends CI_Model 
{
	public function save_ride()
	{
		$data = array
		(
			'from_route' => $this->input->post('from_route'),
			'to_route' => $this->input->post('to_route'),
			'distance' => $this->input->post('distance'),
			'duration' => $this->input->post('duration'),
			'request_lat' => $this->input->post('request_lat'),
			'request_lng' => $this->input->post('request_lng'),
			'destination_lat' => $this->input->post('destination_lat'),
			'destination_lng' => $this->input->post('destination_lng'),
			'customer_id' => $this->input->post('customer_id'),
			'ride_type_id' => $this->input->post('ride_type_id'),
			'ride_status_id' => 1
		);
		if($this->db->insert('ride', $data))
		{
			return $this->db->insert_id();
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function get_all_requests()
	{
		$this->db->where('ride_status_id = 1 AND ride.customer_id = customer.customer_id');
		$rides = $this->db->get('ride, customer');
		
		return $rides;
	}
	
	public function get_customer($ride_id)
	{
		$this->db->where('ride.customer_id = customer.customer_id AND ride.ride_id = '.$ride_id);
		$rides = $this->db->get('ride, customer');
		
		return $rides;
	}
	
	public function accpet_ride($ride_id, $personnel_id)
	{
		$data = array
		(
			'personnel_id' => $personnel_id,
			'time_accepted' => date('Y-m-d H:i:s'),
			'ride_status_id' => 2
		);
		$this->db->where('ride_id', $ride_id);
		if($this->db->update('ride', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function notify_customer($ride_id, $message_title = 'Ubiker', $message = NULL, $source = NULL)
	{
		if($message != NULL)
		{
			//get riders
			$where = 'ride.customer_id = customer.customer_id AND ride.ride_id = '.$ride_id;
			$this->db->where($where);
			$query = $this->db->get('customer, ride');
			
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $res)
				{
					$to = $res->app_registration_id;
					$title = $message_title;
					$result = $this->riders_model->send_push_notification($to, $title, $message, $source);
				}
			}
		}
	}
	
	public function reverse_geocode($latitude, $longitude)
	{
		$maps_api_key = 'AIzaSyCRL4A7M9ZGM7GIPaZqbfv67xtcPFLc2xc';
		$request = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&key='.$maps_api_key;
		
		$json = file_get_contents($request);
		
		return $json;
	}
	
	public function set_rider_location($latitude, $longitude, $ride_id)
	{
		$data = array
		(
			'rider_latitude' => $latitude,
			'rider_longitude' => $longitude,
			'time_changed' => date('Y-m-d H:i:s')
		);
		$this->db->where('ride_id', $ride_id);
		if($this->db->update('ride', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function begin_ride($ride_id)
	{
		$data = array
		(
			'ride_status_id' => 3,
			'ride_start_time' => date('Y-m-d H:i:s')
		);
		$this->db->where('ride_id', $ride_id);
		if($this->db->update('ride', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function end_ride($ride_id, $start_latitude, $start_longitude, $end_latitude, $end_longitude)
	{
		$data = array
		(
			'begin_latitude' => $start_latitude,
			'begin_longitude' => $start_longitude,
			'end_latitude' => $end_latitude,
			'end_longitude' => $end_longitude,
			'ride_status_id' => 4,
			'ride_end_time' => date('Y-m-d H:i:s')
		);
		$this->db->where('ride_id', $ride_id);
		if($this->db->update('ride', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function calculate_ride_cost($ride_id)
	{
		$this->db->where('ride_id', $ride_id);
		$query = $this->db->get('ride');
		
		$cost = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$distance = $query->distance;
			$ride_type_id = $query->ride_type_id;
			
			if($ride_type_id == 1)
			{
				$cost = $distance * $this->config->item('ride_cost');
			}
			
			else
			{
				$cost = $distance * $this->config->item('delivery_cost');
			}
		}
		
		if($cost < 100)
		{
			$cost = 100;
		}
		
		return $cost;
	}
	
	public function get_ride($ride_id)
	{
		$this->db->where('ride.ride_id = '.$ride_id);
		$rides = $this->db->get('ride');
		
		return $rides;
	}
}
?>