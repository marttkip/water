<?php
class Leases_model extends CI_Model
{
	public function unclaimed_payments()
	{
		$this->db->where('mpesa_status', 0);
		$this->db->group_by('serial_number');
		return $this->db->get('mpesa_transactions');
	}
	public function get_all_properties()
	{
		$this->db->where('property_id > 0');
		return $this->db->get('property');
	}
	public function active_leases()
	{
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND leases.lease_status = 1 AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND property.property_id = rental_unit.property_id');
		$this->db->order_by('rental_unit.rental_unit_name','ASC');
		$check_query = $this->db->get('leases,tenant_unit,rental_unit,tenants,property');
		
		return $check_query;
	}
	public function property_active_leases($property_id)
	{
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND leases.lease_status = 1 AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND property.property_id = rental_unit.property_id AND property.property_id ='.$property_id);
		$this->db->order_by('rental_unit.rental_unit_name','ASC');
		$check_query = $this->db->get('leases,tenant_unit,rental_unit,tenants,property');
		
		return $check_query;
	}

	/*
	*	Retrieve a single lease
	*	@param int $lease_id
	*
	*/
	public function get_lease_detail_mobile($lease_id)
	{
		//retrieve all leases
		$this->db->from('leases,rental_unit,tenant_unit,tenants,property');
		$this->db->select('*');
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND lease_id = '.$lease_id);
		$query = $this->db->get();
		
		return $query;
	}

	public function receipt_payment($lease_id,$personnel_id = NULL){
		$amount = $this->input->post('amount_paid');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');
		
		
		
		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}

		// calculate the points to get 
		$payment_date = $this->input->post('payment_date');


		// $this->get_points_to_award($lease_id,$payment_date);
		// end of point calculation
		$date_check = explode('-', $payment_date);
		$month = $date_check[1];
		$year = $date_check[0];

		if($type_of_account == 1)
		{
			$receipt_number = $this->create_receipt_number();
		}
		else
		{
			$receipt_number = $this->create_owners_receipt_number();
		}
		
		$data = array(
			'payment_method_id'=>$payment_method,
			'amount_paid'=>$amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$this->input->post('payment_date'),
			'receipt_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_created'=>date("Y-m-d"),
			'year'=>$year,
			'month'=>$month,
			'confirm_number'=> $receipt_number,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		if($type_of_account == 1)
		{
			$data['lease_id'] = $lease_id;

			if($this->db->insert('payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($lease_id,$confirm_number,$payment_id,$amount);

				$service_charge_amount = $this->input->post('service_charge_amount');
				$water_amount = $this->input->post('water_amount');
				$rent_amount = $this->input->post('rent_amount');
				$penalty_fee = $this->input->post('penalty_fee');
				$fixed_charge = $this->input->post('fixed_charge');
				$bought_water = $this->input->post('bought_water');
				$insurance = $this->input->post('insurance');
				$sinking_funds = $this->input->post('sinking_funds');
				$painting_charge = $this->input->post('painting_charge');
				$fixed_charge = $this->input->post('fixed_charge');
				$deposit_charge = $this->input->post('deposit_charge');
				$legal_fees = $this->input->post('legal_fees');

				// $invoice_number = $this->get_invoice_number(); 

				if(!empty($service_charge_amount))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $service_charge_amount,
										'invoice_type_id' => 4,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}

				if(!empty($water_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $water_amount,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
					// update the invoice table 
					// using invoice_type_id
					$invoice_id = $this->get_last_invoice_id(2);

					

				}
				if(!empty($rent_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $rent_amount,
										'invoice_type_id' => 1,
										'lease_id' => $lease_id,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);

					
				}
				if(!empty($penalty_fee))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $penalty_fee,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($fixed_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $fixed_charge,
										'invoice_type_id' => 12,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($deposit_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $deposit_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 13,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}

				if(!empty($bought_water))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $bought_water,
										'lease_id' => $lease_id,
										'invoice_type_id' => 10,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($sinking_funds))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $sinking_funds,
										'invoice_type_id' => 8,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($legal_fees))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $legal_fees,
										'lease_id' => $lease_id,
										'invoice_type_id' => 17,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}


				return $payment_id;
			}
			else{
				return FALSE;
			}
		}
		else if($type_of_account == 0)
		{
			$data['rental_unit_id'] = $lease_id;

			if($this->db->insert('home_owners_payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($lease_id,$confirm_number,$payment_id,$amount);

				$service_charge_amount = $this->input->post('service_charge_amount');
				$water_amount = $this->input->post('water_amount');
				$rent_amount = $this->input->post('rent_amount');
				$penalty_fee = $this->input->post('penalty_fee');

				// $invoice_number = $this->get_invoice_number(); 

				if(!empty($service_charge_amount))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $service_charge_amount,
										'invoice_type_id' => 4,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}

				if(!empty($water_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $water_amount,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
					// update the invoice table 
					// using invoice_type_id
					$invoice_id = $this->get_last_invoice_id(2);

					

				}
				if(!empty($rent_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $rent_amount,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);

					
				}
				if(!empty($penalty_fee))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $penalty_fee,
										'invoice_type_id' => 5,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($bought_water))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $bought_water,
										'invoice_type_id' => 10,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($sinking_funds))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $sinking_funds,
										'invoice_type_id' => 8,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($insurance))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $insurance,
										'invoice_type_id' => 9,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($fixed_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $fixed_charge,
										'invoice_type_id' => 9,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				return $payment_id;
			}
			else{
				return FALSE;
			}
		}
	}
	function create_owners_receipt_number()
	{
		//select product code
		$preffix = "AW-RO-";
		$this->db->from('home_owners_payments');
		$this->db->where("receipt_number LIKE '".$preffix."%'  AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	function create_receipt_number()
	{
		//select product code
		$preffix = "AW-RT-";
		$this->db->from('payments');
		$this->db->where("receipt_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}

	
	
}
?>