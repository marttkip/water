<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounting extends MX_Controller 
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('customers/customers_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		$this->load->model('products/products_model');
		$this->load->model('accounting_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('sms/sms_model');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index($order = 'customer_surname', $order_method = 'ASC') 
	{

		$where = 'customer.customer_id > 0';
		$table = 'customer';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_customers($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Customers';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('accounting/accounting/all_company_accounting', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	/*
	*
	*	Add a new customer
	*
	*/
	public function sale_company($customer_id) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('product_id', 'product_id', 'required|xss_clean');
		$this->form_validation->set_rules('sale_quantity', 'Quantity', 'required|xss_clean');
		$this->form_validation->set_rules('sale_price', 'Sale Price', 'required|xss_clean');
		$this->form_validation->set_rules('sale_description', 'Sale Description', 'required|xss_clean');
		$this->form_validation->set_rules('personnel_vehicle_id', 'Driven Vehicle', 'required|xss_clean');
		$this->form_validation->set_rules('destination', 'Destination', 'required|xss_clean');

		

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$is_company = 0;
			$result = $this->accounting_model->add_sale($customer_id);


			if($result["status"])
			{
				$this->session->set_userdata('success_message', 'Sale added successfully');
				
				

				redirect('accounting');
			}
			
			else
			{
				$this->session->set_userdata('error_message', $result['message']);
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Add Sales Based On Customer';
		$v_data['title'] = $data['title'];
		$v_data['products'] = $this->accounting_model->all_products();
		$v_data['sales'] = $this->accounting_model->all_sales($customer_id);
		$v_data['vehicle'] = $this->accounting_model->get_personnel_vehicle();
	



		
		$data['content'] = $this->load->view('accounting/accounting/add_sale', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	

	public function payment_company($customer_id) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('paid_by', 'Paid By', 'required|xss_clean');
		
		if(!empty($payment_method))
		{
			if($payment_method == 1)
			{
				// check for cheque number if inserted
				$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');
			
			}
			else if($payment_method == 5)
			{
				//  check for mpesa code if inserted
				$this->form_validation->set_rules('mpesa_code', 'Amount', 'is_unique[payments.transaction_code]|trim|required|xss_clean');
			}
		}

		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			
				$this->accounting_model->receipt_payment($customer_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
			
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		
		//open the add new Customers
		
		$data['title'] = 'Add Payments Based On Customer';
		$v_data['title'] = $data['title'];
		$v_data['products'] = $this->accounting_model->all_products();
		$v_data['sales'] = $this->accounting_model->all_sales($customer_id);
		$v_data['query'] = $this->accounting_model->one_customer($customer_id);
		$v_data['payment_methods'] = $this->accounting_model->get_payment_methods();
		$v_data['payments'] = $this->accounting_model->get_customer_payments($customer_id);
	



		
		$data['content'] = $this->load->view('accounting/accounting/add_payment', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

   /*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function edit_customer($customer_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('customer_surname', 'Surname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_first_name', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_phone', 'Phone Number', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean');
		$this->form_validation->set_rules('customer_post_code', 'Postal Code', 'required|xss_clean');
		$this->form_validation->set_rules('customer_address', 'Address', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->customers_model->update_customer($customer_id))
			{
				$this->session->set_userdata('success_message', 'Customer updated successfully');
				redirect('customers');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update Customer. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit Customer';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->customers_model->get_customer($customer_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['customer'] = $query->result();
			$data['content'] = $this->load->view('customers/customers/edit_customer', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Customer does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Deactivate an existing Customer
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_customer($customer_id)
	{
		if($this->customers_model->deactivate_customer($customer_id))
		{
			$this->session->set_userdata('success_message', 'Customer disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Customer could not be disabled. Please try again');
		}
		redirect('customers');
	}
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_customer($customer_id)
	{
		if($this->customers_model->activate_customer($customer_id))
		{
			$this->session->set_userdata('success_message', 'Customer activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Customer could not be activated. Please try again');
		}
		redirect('customers');
	}



	/*
	*
	*	Add a new company
	*
	*/
	public function add_company() 
	{
		
		//form validation rules
		$this->form_validation->set_rules('customer_surname', 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('customer_first_name', 'Company Registration', 'required|xss_clean');
		$this->form_validation->set_rules('customer_phone', 'Phone Number', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean');
		$this->form_validation->set_rules('customer_post_code', 'Postal Code', 'required|xss_clean');
		$this->form_validation->set_rules('customer_address', 'Address', 'xss_clean');

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$is_company = 1;
			
			if($this->customers_model->add_customer($is_company))
			{
				$this->session->set_userdata('success_message', 'Company added successfully');
				redirect('accounting');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Campany. Please try again');
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Add Company Customers';
		$v_data['title'] = $data['title'];
	
		$data['content'] = $this->load->view('customers/customers/add_company', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Edit an existing Company
	*	@param int $vehicle_id
	*
	*/
	public function edit_company($customer_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('customer_surname', 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('customer_first_name', 'Company Registration', 'required|xss_clean');
		$this->form_validation->set_rules('customer_phone', 'Phone Number', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean');
		$this->form_validation->set_rules('customer_post_code', 'Postal Code', 'required|xss_clean');
		$this->form_validation->set_rules('customer_address', 'Address', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->customers_model->update_customer($customer_id))
			{
				$this->session->set_userdata('success_message', 'Company updated successfully');
				redirect('customers');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update Company. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit Company';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->customers_model->get_customer($customer_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['customer'] = $query->result();
			$data['content'] = $this->load->view('customers/customers/edit_company', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Company does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}



	/*
	*
	*	Add a new company_contacts
	*
	*/
	public function add_company_contact($company_id) 

	{
			
		//form validation rules
		$this->form_validation->set_rules('customer_contacts_sur_name', 'Surname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_first_name', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_phone', 'Phone Number', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_email', 'Email', 'required|xss_clean');
		

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->customers_model->add_company_contact($company_id))
			{
				$this->session->set_userdata('success_message', 'Company Contact added successfully');
				redirect('customers/add_person/'.$company_id);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Company Contact. Please try again');
			}
		}
		

        $order = 'customer_contacts_first_name';
		$order_method = 'ASC';
		$where = 'customer_id = '.$company_id;
		$table = 'customer_contacts';
		//pagination
		$segment = 4;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'customers/add_person/'.$company_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query_sent = $this->customers_model->get_all_customer_contacts($table, $where, $config["per_page"], $page, $order, $order_method);
		//var_dump($query_sent); die();
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		
		
		//open the add new Customers
		
		 $data['title'] = 'Add Company Contact Persons';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query_sent;
		$v_data['page'] = $page;
	
		$data['content'] = $this->load->view('customers/customers/add_company_contact', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
    /*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function edit_company_contact($customer_contact_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('customer_contacts_sur_name', 'Surname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_first_name', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_phone', 'Phone Number', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_email', 'Email', 'required|xss_clean');
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->customers_model->update_company_contact($customer_contact_id))
			{
				$this->session->set_userdata('success_message', 'Company Contact updated successfully');
				redirect('customers');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update Company Contact. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit Company Contact';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->customers_model->get_customer_contact($customer_contact_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['customer_contacts'] = $query->result();
			$data['content'] = $this->load->view('customers/customers/edit_company_contact', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Customer Contact does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Deactivate an existing Customer
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_customer_contact($customer_contact_id)
	{
		if($this->customers_model->deactivate_customer_contact($customer_contact_id))
		{
			$this->session->set_userdata('success_message', 'Customer Contact disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Customer could not be disabled. Please try again');
		}
		redirect('customers');
	}
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_customer_contact($customer_contact_id)
	{
		if($this->customers_model->activate_customer_contact($customer_contact_id))
		{
			$this->session->set_userdata('success_message', 'Customer Contact activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Customer Contact could not be activated. Please try again');
		}
		redirect('customers');
	}









	/*
	*
	*	Add a new customer
	*
	*/
	public function add_vehicle() 
	{
		//form validation rules
		$this->form_validation->set_rules('ride_type_id', 'Type', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_plate', 'Licence Plate', 'is_unique[vehicle.vehicle_plate]|required|xss_clean');
		$this->form_validation->set_rules('vehicle_status', 'Vehicle Status', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_owner', 'Vehicle Owner', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_driver', 'Vehicle Driver', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_description', 'Vehicle Description', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->customers_model->add_vehicle())
			{
				$this->session->set_userdata('success_message', 'Vehicle added successfully');
				redirect('bike/customer');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add vehicle. Please try again');
			}
		}
		
		//open the add new vehicle
		
		$data['title'] = 'Add vehicle';
		$v_data['title'] = $data['title'];
		$v_data['ride_types'] = $this->customers_model->get_ride_types();
		$v_data['owners'] = $this->customers_model->get_personnel_type(4);
		$v_data['drivers'] = $this->customers_model->get_personnel_type(3);
		$data['content'] = $this->load->view('customer/add_vehicle', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function edit_vehicle($vehicle_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('ride_type_id', 'Type', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_plate', 'Licence Plate', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_status', 'Vehicle Status', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_owner', 'Vehicle Owner', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_driver', 'Vehicle Driver', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_description', 'Vehicle Description', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->customers_model->update_vehicle($vehicle_id))
			{
				$this->session->set_userdata('success_message', 'Vehicle updated successfully');
				redirect('bike/customer');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update vehicle. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit vehicle';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->customers_model->get_vehicle($vehicle_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['vehicle'] = $query->result();
			$v_data['ride_types'] = $this->customers_model->get_ride_types();
			$v_data['owners'] = $this->customers_model->get_personnel_type(4);
			$v_data['drivers'] = $this->customers_model->get_personnel_type(3);
			
			$data['content'] = $this->load->view('customer/edit_vehicle', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Vehicle does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function delete_vehicle($vehicle_id)
	{
		if($this->customers_model->delete_vehicle($vehicle_id))
		{
			$this->session->set_userdata('success_message', 'Vehicle has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Vehicle could not be deleted. Please try again');
		}
		redirect('bike/customer');
	}
    
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_vehicle($vehicle_id)
	{
		if($this->customers_model->activate_vehicle($vehicle_id))
		{
			$this->session->set_userdata('success_message', 'Vehicle activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Vehicle could not be activated. Please try again');
		}
		redirect('bike/customer');
	}
    
	/*
	*
	*	Deactivate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_vehicle($vehicle_id)
	{
		if($this->customers_model->deactivate_vehicle($vehicle_id))
		{
			$this->session->set_userdata('success_message', 'Vehicle disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Vehicle could not be disabled. Please try again');
		}
		redirect('bike/customer');
	}
}
?>