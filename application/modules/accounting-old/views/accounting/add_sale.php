<?php
//personnel data
$sale_quantity = set_value('sale_quantity');
$product_id = set_value('product_id');
$sale_price = set_value('sale_price');
$sale_description = set_value('sale_description');
$personnel_vehicle_id = set_value('personnel_vehicle_id');
$destination = set_value('destination');






?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>accounting" class="btn btn-info pull-right">Back to Accounting</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Sale Quantity </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="sale_quantity" placeholder="Sale Quantity" value="<?php echo $sale_quantity;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Product Name: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="product_id">
                    <option value="">--Select Product--</option>
                    <?php
                        if($products->num_rows() > 0)
                        {
                            $veh = $products->result();
                            
                            foreach($veh as $res)
                            {
                                $product_id = $res->product_id;
                                $product_name = $res->product_name;
                                
                                if($product_id == $product_name)
                                {
                                    echo '<option value="'.$product_id.'" selected>'.$product_name.'</option>';
                                }
                                
                                else
                                {
                                    echo '<option value="'.$product_id.'">'.$product_name.'</option>';
                                }
                            }
                        }
                    ?>
                </select>
             </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Driven Vehicle: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="personnel_vehicle_id">
                    <option value="">--Select Vehicle and Driver--</option>
                    <?php
                        if($vehicle->num_rows() > 0)
                        {
                            $veh = $vehicle->result();
                            
                            foreach($veh as $res)
                            {
                                $vehicle_plate = $res->vehicle_plate;
                                $personnel_vehicle_id = $res->personnel_vehicle_id;
                                $personnel_fname = $res->personnel_fname;
                                $personnel_oname = $res->personnel_onames;
                                $sr = $personnel_oname.' '.$personnel_fname.' '.$vehicle_plate;
                                
                                
                                echo '<option value="'.$personnel_vehicle_id.'">'.$sr.'</option>';
                                
                            }
                        }
                    ?>
                </select>
             </div>
        </div>
        
        
        
    </div>
    
    <div class="col-md-6">
        
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Sales Price: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="sale_price" placeholder="Sales Price" value="<?php echo $sale_price;?>">
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Sales Description: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="sale_description" placeholder="Sales Description" value="<?php echo $sale_description;?>">
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Trip Destination: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="destination" placeholder="Trip Destination" value="<?php echo $destination;?>">
            </div>
        </div>
        
        
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Sale
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>

                <?php
        
        $result = '';
        
        //if users exist display them
       
        if ($sales->num_rows() > 0)
        {
            $count = 0;
            
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                         <th>Sale Date</th>
                        <th>Sale Quantity</th>
                        <th>Sale Amount</th>
                        <th>Product Name</th>
                        <th>Trip Destination</th>
                        <th>Status</th>
                        
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
            //get all administrators
            $administrators = $this->users_model->get_active_users();
            if ($administrators->num_rows() > 0)
            {
                $admins = $administrators->result();
            }
            
            else
            {
                $admins = NULL;
            }
            
            foreach ($sales->result() as $row)
            {

  
                $sale_id = $row->sale_id;
                $sale_description = $row->sale_description;
                $sale_quantity = $row->sale_quantity;
                $sale_price = $row->sale_price;
                $sale_amount = $sale_quantity * $sale_price;
                $product_id = $row->product_id;
                $product_name = $this->accounting_model->get_product_name($product_id);
                
                $sale_date = $row->sale_date;
                $sale_destination = $row->sale_destination;
                $sale_status = $row->sale_status;
                
              
                
             
               
                
                //status
                if($sale_status == 1)
                {
                    $status = 'Active';
                }
                else
                {
                    $status = 'Disabled';
                }
                
                
                //create deactivated status display
                if($sale_status == 0)
                {
                    $status = '<span class="label label-default">Deactivated</span>';
                    
                }
                //create activated status display
                else if($sale_status == 1)
                {
                    $status = '<span class="label label-success">Active</span>';
                   
                }
                
                
                
                
                $count++;
                $result .= 
                '
                 
                    <tr>
                        <td>'.$count.'</td>
                        <td>'.$sale_date.'</td>
                        <td>'.$sale_quantity.'</td>
                        <td>'.$sale_amount.'</td>
                        
                        
                        <td>'.$product_name.'</td>
                         <td>'.$sale_destination.'</td>
                        
                         <td>'.$status.'</td>
                        
                        
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no Sales";
        }
        echo $result;
?>
 
 </div>
            </section>
