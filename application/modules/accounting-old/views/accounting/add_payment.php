<?php
if($query->num_rows() > 0){
    foreach ($query->result() as $row)
    {
                $customer_id = $row->customer_id;
                $customer_first_name = $row->customer_first_name;
                $customer_surname = $row->customer_surname;
                $customer_phone = $row->customer_phone;
                $customer_email = $row->customer_email;
                $customer_status = $row->customer_status;
                $customer_created = $row->customer_created;
                $company_status = $row->company_status;
                $customer_name = $customer_first_name.' '.$customer_surname;

      
        //create deactivated status display
        if($customer_status == 0)
        {
            $status = '<span class="label label-default"> Deactivated</span>';

            $button = '';
            $delete_button = '';
        }
        //create activated status display
        else if($customer_status == 1)
        {
            $status = '<span class="label label-success">Active</span>';
            $button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$customer_id.'" onclick="return confirm(\'Do you want to deactivate '.$customer_name.'?\');" title="Deactivate '.$customer_name.'"><i class="fa fa-thumbs-down"></i></a></td>';
            $delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$customer_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$customer_name.'?\');" title="Delete '.$customer_name.'"><i class="fa fa-trash"></i></a></td>';

        }

        
    }
    $tenant_name = $customer_first_name.' '.$customer_surname;
    $lease_id = $customer_id;
}




?>



 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?> <a href="<?php echo site_url();?>accounting" class="btn btn-sm btn-success pull-right"  style="margin-top:-5px;">Back to Accounting</a> </h2>
    </header>
    
    <!-- Widget content -->
    
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
            <?php
                $error = $this->session->userdata('error_message');
                $success = $this->session->userdata('success_message');
                
                if(!empty($error))
                {
                  echo '<div class="alert alert-danger">'.$error.'</div>';
                  $this->session->unset_userdata('error_message');
                }
                
                if(!empty($success))
                {
                  echo '<div class="alert alert-success">'.$success.'</div>';
                  $this->session->unset_userdata('success_message');
                }
             ?>
            </div>
        </div>
        
        
        
        
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-featured panel-featured-info">
                                <header class="panel-heading">
                                    
                                    <h2 class="panel-title">Customer's Details</h2>
                                </header>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                          </div>
                                    </div>



                                    <table class="table table-hover table-bordered col-md-12">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Detail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr><td><span>Customer Name :</span></td><td><?php echo $customer_name;?></td></tr>
                                            <tr><td><span>Customer Phone :</span></td><td><?php echo $customer_phone;?></td></tr>
                                            <tr><td><span>Created On:</span></td><td><?php echo $customer_created;?></td></tr>
                                           
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <!-- END OF THE SPAN 7 -->
                 <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
                <!-- START OF THE SPAN 5 -->
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel panel-featured panel-featured-info">
                                
                                
                                <div class="panel-body">

                                    <div class="tabs">
                                        <ul class="nav nav-tabs nav-justified">
                                            <li class="active">
                                                <a class="text-center" data-toggle="tab" href="#general"><i class="fa fa-user"></i> Payments</a>
                                            </li>
                                           
                                    
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="general">
                                                <?php echo form_open("accounting/payment-company/".$customer_id, array("class" => "form-horizontal"));?>
                                                    <input type="hidden" name="type_of_account" value="1">
                                                    <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                                                    <div class="form-group" id="payment_method">
                                                        <label class="col-md-4 control-label">Payment Method: </label>
                                                          
                                                        <div class="col-md-7">
                                                            <select class="form-control" name="payment_method" onchange="check_payment_type(this.value)" required>
                                                                <option value="0">Select a payment method</option>
                                                                <?php
                                                                  $method_rs = $this->accounting_model->get_payment_methods();
                                                                    
                                                                    foreach($method_rs->result() as $res)
                                                                    {
                                                                      $payment_method_id = $res->payment_method_id;
                                                                      $payment_method = $res->payment_method;
                                                                      
                                                                        echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
                                                                      
                                                                    }
                                                                  
                                                              ?>
                                                            </select>
                                                          </div>
                                                    </div>
                                                    <div id="mpesa_div" class="form-group" style="display:none;" >
                                                        <label class="col-md-4 control-label"> Mpesa TX Code: </label>

                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control" name="mpesa_code" placeholder="">
                                                        </div>
                                                    </div>
                                                  
                                                  
                                                    <div id="cheque_div" class="form-group" style="display:none;" >
                                                        <label class="col-md-4 control-label"> Bank Name: </label>
                                                      
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control" name="bank_name" placeholder="Barclays">
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Total Amount: </label>
                                                      
                                                        <div class="col-md-7">
                                                            <input type="number" class="form-control" name="amount_paid" placeholder="" autocomplete="off" required>
                                                        </div>
                                                    </div>

                                                    <!-- where you put all the item that are to be paid for in this property -->
                                                    <?php //echo $inputs;?>
                                                    <!-- end of the items to be paid for in this property -->
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Paid in By: </label>
                                                      
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control" name="paid_by" placeholder="<?php echo $tenant_name;?>" autocomplete="off" >
                                                        </div>
                                                    </div>

                                                    
                                                    
                                                    <div class="center-align">
                                                        <button class="btn btn-info btn-sm" type="submit">Add Payment Information</button>
                                                    </div>
                                                    <?php echo form_close();?>
                                            </div>
                                            <div class="tab-pane" id="account">
                                            <?php echo form_open("accounts/create_pardon/".$customer_id, array("class" => "form-horizontal"));?>
                                                    <input type="hidden" name="type_of_account" value="1">
                                                    <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Pardon Amount: </label>
                                                      
                                                        <div class="col-md-7">
                                                            <input type="number" class="form-control" name="pardon_amount" placeholder="" autocomplete="off" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Pardon Reason: </label>
                                                      
                                                        <div class="col-md-7">
                                                            <textarea class="form-control" name="pardon_reason" autocomplete="off"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Pardon Date: </label>
                                                      
                                                        <div class="col-md-7">
                                                             <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </span>
                                                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="pardon_date" placeholder="Pardon Date" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="center-align">
                                                        <button class="btn btn-info btn-sm" type="submit">Add Pardon Information</button>
                                                    </div>
                                                    <?php echo form_close();?>
                                            
                                            </div>
                                        
                                        </div>
                                    </div>

                                    
                                </div>
                            </section>
                        </div>
                    </div>

                </div>
                <!-- END OF THE SPAN 5 -->
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">






<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Jopet Individual Account Statement</h2>
    </header>
    <div class="panel-body">
        <!-- Adding Errors -->
            <?php
            $last_date = '';
            $payments = $this->accounting_model->get_customer_payments($customer_id);
            $current_year='2017';
            $outstanding_loan = 0;
            
            $result = '';
            $count = 1;
            $total_debit = 0;
            $total_credit = 0;
            $running_balance = 0;
            $disbursments = $this->accounting_model->all_sales($customer_id);
            $total_disbursments = $disbursments->num_rows();
            $disbursments_count = 0;
            $customer_fname = $customer_first_name;
            
            if($total_disbursments > 0)
            {
                foreach ($disbursments->result() as $row)
                {
                    $disbursement_date = $row->sale_date;
                    $sale_quantity = $row->sale_quantity;
                    $sale_price = $row->sale_price;
                    $cheque_amount = $sale_price * $sale_quantity;
                    $cheque_number = $row->sale_description;
                    //$disbursed_date = date('jS d M Y',strtotime($disbursement_date));
                    $disbursments_count++;
                    
                    //get all loan deductions before date
                    if($payments->num_rows() > 0)
                    {
                        foreach ($payments->result() as $row2)
                        {
                            $loan_payment_id = $row2->payment_id;
                            $personnel_fname = $customer_fname;
                            $personnel_onames = $customer_surname;
                            $payment_amount = $row2->amount;
                            $payment_interest = 0;
                            $created = date('jS M Y H:i:s',strtotime($row2->payment_created));
                            $payment_date = $row2->payment_created;
                            $split = explode('-', $payment_date);
                            
                            if(is_array($split))
                            {
                                $year = $split[0];
                            }
                            else
                            {
                                $year = 0;
                            }
                            
                            if(($payment_date <= $disbursement_date) && ($payment_date > $last_date) && ($payment_amount > 0))
                            {
                                $count++;
                                $running_balance -= $payment_amount;
                            
                                if($year >= $current_year)
                                {
                                    $result .= 
                                    '
                                        <tr>
                                            <td>'.date('d M Y',strtotime($payment_date)).' </td>
                                            <td>Loan repayment</td>
                                            <td></td>
                                            <td>'.number_format($payment_amount, 2).'</td>
                                            <td>'.number_format($payment_interest, 2).'</td>
                                            <td>'.number_format(($payment_amount + $payment_interest), 2).'</td>
                                            <td>'.number_format($running_balance, 2).'</td>
                                        </tr> 
                                    ';
                                }
                                
                                else
                                {
                                    $outstanding_loan -= $payment_amount;
                                }
                                $total_credit += $payment_amount;
                            }
                        }
                    }
                    
                    //display disbursment if cheque amount > 0
                    if($cheque_amount > 0)
                    {
                        $running_balance += $cheque_amount;
                        $total_debit += $cheque_amount;
                        
                        $split = explode('-', $disbursement_date);
                            
                        if(is_array($split))
                        {
                            $year = $split[0];
                        }
                        else
                        {
                            $year = 0;
                        }
                        
                        $count++;
                            
                        if($year >= $current_year)
                        {
                            $result .= 
                            '
                                <tr>
                                    <td>'.date('d M Y',strtotime($disbursement_date)).' </td>
                                    <td>Invoiced Sale: '.$cheque_number.'</td>
                                    <td>'.number_format($cheque_amount, 2).'</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>'.number_format($running_balance, 2).'</td>
                                </tr> 
                            ';
                        }
                        
                        else
                        {
                            $outstanding_loan += $cheque_amount;
                        }
                    }
                    
                    //check if there are any more payments
                    if($total_disbursments == $disbursments_count)
                    {
                        //get all loan deductions before date
                        if($payments->num_rows() > 0)
                        {
                            foreach ($payments->result() as $row2)
                            {
                                $loan_payment_id = $row2->payment_id;
                            $personnel_fname = $customer_fname;
                            $personnel_onames = $customer_surname;
                            $payment_amount = $row2->amount;
                            $payment_interest = 0;
                            $created = date('jS M Y H:i:s',strtotime($row2->payment_created));
                            $payment_date = $row2->payment_created;
                                $split = explode('-', $payment_date);
                                
                                if(is_array($split))
                                {
                                    $year = $split[0];
                                }
                                else
                                {
                                    $year = 0;
                                }
                                
                                if(($payment_date > $disbursement_date) && ($payment_amount > 0))
                                {
                                    $count++;
                                    $running_balance -= $payment_amount;
                            
                                    if($year >= $current_year)
                                    {
                                        $result .= 
                                        '
                                            <tr>
                                                <td>'.date('d M Y',strtotime($payment_date)).' </td>
                                                <td>Payment</td>
                                                <td></td>
                                                <td>'.number_format($payment_amount, 2).'</td>
                                                <td>'.number_format($payment_interest, 2).'</td>
                                                <td>'.number_format(($payment_amount + $payment_interest), 2).'</td>
                                                <td>'.number_format($running_balance, 2).'</td>
                                            </tr> 
                                        ';
                                    }
                                    
                                    else
                                    {
                                        $outstanding_loan -= $payment_amount;
                                    }
                                    $total_credit += $payment_amount;
                                }
                            }
                        }
                    }
                    $last_date = $disbursement_date;
                }
            }
            
            else
            {
                //get all loan deductions before date
                if($payments->num_rows() > 0)
                {
                    foreach ($payments->result() as $row2)
                    {
                        $loan_payment_id = $row2->payment_id;
                            $personnel_fname = $customer_fname;
                            $personnel_onames = $customer_surname;
                            $payment_amount = $row2->amount;
                            $payment_interest = 0;
                            $created = date('jS M Y H:i:s',strtotime($row2->payment_created));
                            $payment_date = $row2->payment_created;
                        $running_balance -= $payment_amount;
                        $split = explode('-', $payment_date);
                        
                        if(is_array($split))
                        {
                            $year = $split[0];
                        }
                        else
                        {
                            $year = 0;
                        }
                        
                        $count++;
                        if($payment_amount > 0)
                        {
                            if($year >= $current_year)
                            {
                                $result .= 
                                '
                                    <tr>
                                        <td>'.date('d M Y',strtotime($payment_date)).' </td>
                                        <td>Loan repayment</td>
                                        <td></td>
                                        <td>'.number_format($payment_amount, 2).'</td>
                                        <td>'.number_format($payment_interest, 2).'</td>
                                        <td>'.number_format(($payment_amount + $payment_interest), 2).'</td>
                                        <td>'.number_format($running_balance, 2).'</td>
                                    </tr> 
                                ';
                            }
                            
                            else
                            {
                                $outstanding_loan -= $payment_amount;
                            }
                            $total_credit += $payment_amount;
                        }
                    }
                }
            }
                    
            //display loan
            $result .= 
            '
                <tr>
                    <th colspan="2">Total</th>
                    <th>'.number_format($total_debit, 2).'</th>
                    <th></th>
                    <th></th>
                    <th>'.number_format($total_credit, 2).'</th>
                    <th></th>
                </tr> 
                <tr>
                    <th colspan="5">Balance</th>
                    <th></th>
                    <th>'.number_format($total_debit - $total_credit, 2).'</th>
                </tr> 
            ';
            
            
            ?>
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
                    <th style="text-align:center" rowspan=3>Date</th>
                    <th rowspan=2>Description</th>
                    <th colspan=5 style="text-align:center;">Amount</th>
                </tr>
                <tr>
                    <th rowspan=2 style="text-align:left">Debit</th>
                    <th colspan=6 style="text-align:center">Credit</th>
                </tr>
                <tr>
                    <th colspan=2 style="text-align:left"></th>
                    <th style="text-align:left">Principal Repayment</th>
                    <th style="text-align:left">Interest Payment</th>
                    <th style="text-align:left">Repayment</th>
                    <th style="text-align:left">Running Balance</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td>Account balance b/f</td>
                    <td><?php echo number_format($outstanding_loan, 2);?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr> 
                <?php echo $result;?>
            </tbody>
        </table>
    </div>
</section>
  <!-- END OF ROW -->
<script type="text/javascript">
  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");
        
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }
</script>