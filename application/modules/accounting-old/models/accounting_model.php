<?php

class Accounting_model extends CI_Model 
{	
	/*
	*	Retrieve all customers
	*
	*/

	function get_payment_methods()
	{
		$table = "payment_method";
		$where = "payment_method_id > 0";
		$items = "*";
		$order = "payment_method";
		
		$this->db->order_by($order);
		$result = $this->db->get($table);
		
		return $result;
	}
	

	public function get_customer_payments($customer_id)
	{

		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get('jopet_payments');
		
		return $query;
	}
	public function get_customer_yard_payments($customer_id)
	{

		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get('yard_jopet_payments');
		
		return $query;
	}
	public function all_customers()
	{
		$this->db->where('customer_status = 1');
		$query = $this->db->get('customer');
		
		return $query;
	}
	public function one_customer($customer_id)
	{
		$this->db->where('customer_status = 1 AND customer_id = '.$customer_id.'');
		$query = $this->db->get('customer');
		
		return $query;
	}
	public function get_product_name($product_id)
	{
		$product_name='0';
		$this->db->select('product_name');
		$this->db->where('product_id = '.$product_id);
		$query = $this->db->get('products');
		$total = $query->row();
		$product_name = $total->product_name;
		return $product_name;
	}
	public function all_products()
	{
		$this->db->where('product_status = 1');
		$query = $this->db->get('products');
		
		return $query;
	}
	public function all_sales($customer_id)
	{
		$this->db->where('sale_status = 1 AND customer_id = '.$customer_id.'');
		$query = $this->db->get('sales');
		
		return $query;
	}
	public function all_sales_yard($customer_id)
	{
		$this->db->where('sale_status = 1 AND customer_id = '.$customer_id.'');
		$query = $this->db->get('yard_sales');
		
		return $query;
	}
	public function get_total_batch()
	{
		$total_sales='0';
		$this->db->select('SUM(sales.sale_quantity) AS total_sales');
		$this->db->where('sales.sale_status = 1 AND sales.product_id = 1');
		$query2 = $this->db->get('sales');
		$total2 = $query2->row();
		$total_sales = $total2->total_sales;
		
		return $total_sales;
	}
	
	/*
	*	Retrieve all ride types
	*
	*/
	public function get_ride_types()
	{
		$this->db->order_by('ride_type_name');
		$query = $this->db->get('ride_type');
		
		return $query;
	}
	
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_customers($table, $where, $per_page, $page, $order = 'customer_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	
	public function get_all_customer_contacts($table, $where, $per_page, $page, $order = 'customer_contacts_first_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	function create_receipt_number()
	{
		//select product code
		$preffix = "JOP-RN_";
		$this->db->from('jopet_payments');
		$this->db->where("reciept_number LIKE '".$preffix."%' AND status = 1");
		$this->db->select('MAX(reciept_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	function create_receipt_number_yard()
	{
		//select product code
		$preffix = "JOP-Y-RN_";
		$this->db->from('yard_jopet_payments');
		$this->db->where("reciept_number LIKE '".$preffix."%' AND status = 1");
		$this->db->select('MAX(reciept_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	function create_cat_number()
	{
		//select product code
		$preffix = "JOP-CAT_";
		$this->db->from('cash_at_hand');
		$this->db->where("reciept_number LIKE '".$preffix."%' AND cat_status = 1");
		$this->db->select('MAX(reciept_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	
	

	/*
	*	Add a new customer
	*	@param string $image_name
	*
	*/
	public function receipt_payment($customer_id)
	{
		$amount = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');
		
		
		
		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}
		$receipt_number = $this->accounting_model->create_receipt_number();

		$data = array(
			'payment_method_id'=>$payment_method,
			'amount'=>$amount,
			'transaction_code'=>$transaction_code,
			'reciept_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'status'=>1,
			'customer_id'=>$customer_id
			
		);
		if ($this->db->insert('jopet_payments',$data)){


				
				$result["status"] = True;
				$result["message"] = "Payments Records Saved";
				$this->create_message_payment($customer_id, $amount, $receipt_number);

				
			}
			else{
				return FALSE;
			}
	}
	public function receipt_payment_yard($customer_id)
	{
		$amount = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');
		
		
		
		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}
		$receipt_number = $this->accounting_model->create_receipt_number_yard();

		$data = array(
			'payment_method_id'=>$payment_method,
			'amount'=>$amount,
			'transaction_code'=>$transaction_code,
			'reciept_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'status'=>1,
			'customer_id'=>$customer_id
			
		);
		if ($this->db->insert('yard_jopet_payments',$data)){


				
				return TRUE;
			}
			else{
				return FALSE;
			}
	}
	public function add_company_contact($company_id)
	{
		$data = array(
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				'customer_id'=>$company_id
				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function add_sale($customer_id)
	{
		$personnel_vehicle_id= $this->input->post('personnel_vehicle_id');


		$total_batch='0';
		$this->db->select('SUM(batches.batch_quantity) AS total_batch');
		$this->db->where('batches.batch_status = 1 AND batches.product_id = 1');
		$query = $this->db->get('batches');
		$total = $query->row();
		$total_batch = $total->total_batch;


		$total_sales='0';
		$this->db->select('SUM(sales.sale_quantity) AS total_sales');
		$this->db->where('sales.sale_status = 1 AND sales.product_id = 1');
		$query2 = $this->db->get('sales');
		$total2 = $query2->row();
		$total_sales = $total2->total_sales;



    
		$data = array(
				
				'sale_price'=>$this->input->post('sale_price'),
				'sale_quantity'=>$this->input->post('sale_quantity'),
				'sale_description'=>$this->input->post('sale_description'),
				'product_id'=>$this->input->post('product_id'),
				'personnel_vehicle_id'=>$this->input->post('personnel_vehicle_id'),
				'sale_destination'=>$this->input->post('destination'),
				'customer_id'=>$customer_id,
				'sale_status'=>1
				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);

		$product_id = $this->input->post('product_id');
		$sale_quantity = $this->input->post('sale_quantity');

		$total_batch='0';
		$this->db->select('SUM(batches.batch_quantity) AS total_batch');
		$this->db->where('batches.batch_status = 1 AND batches.product_id = '.$product_id.'');
		$query = $this->db->get('batches');
		$total = $query->row();
		$total_batch = $total->total_batch;


		$total_sales='0';
		$this->db->select('SUM(sales.sale_quantity) AS total_sales');
		$this->db->where('sales.sale_status = 1 AND sales.product_id = '.$product_id.'');
		$query2 = $this->db->get('sales');
		$total2 = $query2->row();
		$total_sales = $total2->total_sales;

		$total_account = $total_batch-$total_sales-$sale_quantity;
		
			
		if($this->db->insert('sales', $data))
		{
			$result["status"] = True;
			$result["message"] = "Sales Records Saved";
			
			$this->create_message($personnel_vehicle_id,$customer_id);
			

			
		}
		else{
			$result["status"] = FALSE;
			$result["message"] = "Sales Records Could not be saved";
			
		}
		
		return $result;
	}
	
	
	/*
	*	Add a new add_sale
	*/
	public function add_customer($is_company)
	{
		if($is_company == 0){
			$one = '0';
		}else{
			$one = '1';
		}
		$data = array(

				'customer_surname'=>$this->input->post('customer_surname'),
				'customer_first_name'=>$this->input->post('customer_first_name'),
				'customer_phone'=>$this->input->post('customer_phone'),
				'customer_email'=>$this->input->post('customer_email'),
				'customer_post_code'=>$this->input->post('customer_post_code'),
				'customer_address'=>$this->input->post('customer_address'),
				'customer_created'=>date('Y-m-d H:i:s'),
				'company_status'=>$one


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	
	/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_customer($customer_id)
	{
		$data = array(

				'customer_surname'=>$this->input->post('customer_surname'),
				'customer_first_name'=>$this->input->post('customer_first_name'),
				'customer_phone'=>$this->input->post('customer_phone'),
				'customer_email'=>$this->input->post('customer_email'),
				'customer_post_code'=>$this->input->post('customer_post_code'),
				'customer_address'=>$this->input->post('customer_address'),
				
			);
			
		$this->db->where('customer_id', $customer_id);
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer($customer_id)
	{
		//retrieve all users
		$this->db->from('customer');
		$this->db->select('*');
		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get();
		
		return $query;
	}
	





/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_company_contact($customer_contact_id) 
	{

		$data = array(
			
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				
				
			);
			
		$this->db->where('customer_contacts_id', $customer_contact_id);
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer_contact($customer_contact_id)
	{
		//retrieve all users
		$this->db->from('customer_contacts');
		$this->db->select('*');
		$this->db->where('customer_contacts_id = '.$customer_contact_id);
		$query = $this->db->get();
		
		return $query;
	}
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 1
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 0
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Delete an existing customer
	*	@param int $customer_id
	*
	*/
	public function delete_customer($customer_id)
	{
		if($this->db->delete('customer', array('customer_id' => $customer_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 1
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 0
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Retrieve personnel
	*
	*/
	public function get_personnel_type($personnel_type_id)
	{
		$this->db->where(array('personnel_status' => 1, 'personnel_type_id' => $personnel_type_id));
		$query = $this->db->get('personnel');
		
		return $query;
	}
	public function get_personnel_vehicle()
	{
		
		$this->db->where('personnel_vehicle.vehicle_id = vehicle.vehicle_id
		AND personnel_vehicle.personnel_id = personnel.personnel_id
		AND personnel_vehicle.personnel_vehicle_status = 1');
		$query = $this->db->get('personnel_vehicle, vehicle, personnel');
		
		return $query;
	}
	public function create_message($personnel_vehicle_id,$customer_id)
	{
		$vehicle_plate='0';
		$this->db->select('vehicle_plate');
		$this->db->where('personnel_vehicle.personnel_vehicle_status = 1 
			AND personnel_vehicle_id = '.$personnel_vehicle_id);
		$query = $this->db->get('personnel_vehicle, vehicle, personnel');
		$total = $query->row();
		$vehicle_plate = $total->vehicle_plate;

		$personnel_fname='0';
		$this->db->select('personnel_fname');
		$this->db->where('personnel_vehicle.personnel_vehicle_status = 1 AND personnel.personnel_id =personnel_vehicle.personnel_id
			AND personnel_vehicle_id = '.$personnel_vehicle_id);
		$query1 = $this->db->get('personnel_vehicle, vehicle, personnel');
		$total1 = $query1->row();
		$personnel_fname = $total1->personnel_fname;
		//var_dump($personnel_fname); die();

		$personnel_phone='0';
		$this->db->select('personnel_phone');
		$this->db->where('personnel_vehicle.personnel_vehicle_status = 1 AND personnel.personnel_id =personnel_vehicle.personnel_id
			AND personnel_vehicle_id = '.$personnel_vehicle_id);
		$query2 = $this->db->get('personnel_vehicle, personnel');
		$total2 = $query2->row();
		$personnel_phone = $total2->personnel_phone;
		//var_dump($personnel_phone); die();


		$personnel_onames='0';
		$this->db->select('personnel_onames');
		$this->db->where('personnel_vehicle.personnel_vehicle_status = 1 AND personnel.personnel_id =personnel_vehicle.personnel_id
			AND personnel_vehicle_id = '.$personnel_vehicle_id);
		$query3 = $this->db->get('personnel_vehicle, vehicle, personnel');
		$total3 = $query3->row();
		$personnel_onames = $total3->personnel_onames;

		$customer_first_name='0';
		$this->db->select('customer_first_name');
		$this->db->where('customer_status = 1 
			AND customer_id = '.$customer_id);
		$query4 = $this->db->get('customer');
		$total4 = $query4->row();
		$customer_first_name = $total4->customer_first_name;

		$customer_surname='0';
		$this->db->select('customer_surname');
		$this->db->where('customer_status = 1 
			AND customer_id = '.$customer_id);
		$query5 = $this->db->get('customer');
		$total5 = $query5->row();
		$customer_surname = $total5->customer_surname;

		$customer_phone='0';
		$this->db->select('customer_phone');
		$this->db->where('customer_status = 1 
			AND customer_id = '.$customer_id);
		$query6 = $this->db->get('customer');
		$total6 = $query6->row();
		$customer_phone = $total6->customer_phone;


		$message = 'Hi '.$customer_surname.' '.$customer_first_name.' Your shipment has just left the yard Driver Details: '.$personnel_fname
		.' '.$personnel_onames.' '.$personnel_phone.' Vehicle Plate: '.$vehicle_plate;
		
		//var_dump($message);die();
		$remix = $this->sms_model->sms($message, 1000, $customer_phone, 1);
				
		
		
	}
	public function create_message_payment($customer_id, $amount, $reciept_number)
	{
		$total_sales='0';
		$this->db->select('SUM(sale_quantity * sale_price) AS total_sales');
		$this->db->where('sales.sale_status = 1 AND sales.customer_id = '.$customer_id);
		$query2 = $this->db->get('sales');
		$total2 = $query2->row();
		$total_sales = $total2->total_sales;

		$payments='0';
		$this->db->select('SUM(amount) AS total_amount');
		$this->db->where('jopet_payments.status = 1 AND jopet_payments.customer_id = '.$customer_id);
		$query3 = $this->db->get('jopet_payments');
		$total3 = $query3->row();
		$payments = $total3->total_amount;
		
		$customer_first_name='0';
		$this->db->select('customer_first_name');
		$this->db->where('customer_status = 1 
			AND customer_id = '.$customer_id);
		$query4 = $this->db->get('customer');
		$total4 = $query4->row();
		$customer_first_name = $total4->customer_first_name;

		$customer_surname='0';
		$this->db->select('customer_surname');
		$this->db->where('customer_status = 1 
			AND customer_id = '.$customer_id);
		$query5 = $this->db->get('customer');
		$total5 = $query5->row();
		$customer_surname = $total5->customer_surname;

		$customer_phone='0';
		$this->db->select('customer_phone');
		$this->db->where('customer_status = 1 
			AND customer_id = '.$customer_id);
		$query6 = $this->db->get('customer');
		$total6 = $query6->row();
		$customer_phone = $total6->customer_phone;

		$balance = $total_sales-$payments;


		$message = 'Hi '.$customer_surname.' '.$customer_first_name.' Your payment Details: kes.'.$amount
		.' Reciept Number: '.$reciept_number.' has been recieved. Your balance: kes.'.$balance;
		
		//var_dump($message);die();
		$remix = $this->sms_model->sms($message, 1000, $customer_phone, 1);
				
		
		//return $remix;
	}
}
?>