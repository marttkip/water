<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/real_estate_administration/controllers/property.php";

class Accounts extends property {
	var $csv_path;
	var $invoice_path;
	var $statement_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('real_estate_administration/tenants_model');
		$this->load->model('real_estate_administration/rental_unit_model');
		$this->load->model('real_estate_administration/leases_model');
		$this->load->model('real_estate_administration/property_owners_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/petty_cash_model');
		$this->load->model('admin/email_model');
		$this->load->model('payroll/payroll_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('administration/reports_model');
		// $this->load->model('accounting/salary_advance_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->invoice_path = realpath(APPPATH . '../invoices/');
		$this->statement_path = realpath(APPPATH . '../statements/');
	}
    
	/*
	*
	*	Default action is to show all the tenants
	*
	*/
	public function index() 
	{
			
		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';		


		$accounts_search = $this->session->userdata('all_accounts_search');
		
		if(!empty($accounts_search))
		{
			$where .= $accounts_search;	
			
		}
		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'tenants-accounts';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		
		// $this->accounts_model->update_invoices();
		

		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Tenants Accounts';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/tenants_list', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	public function water_tenants() 
	{
			
		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';		


		$accounts_search = $this->session->userdata('all_accounts_search');
		
		if(!empty($accounts_search))
		{
			$where .= $accounts_search;	
			
		}
		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'tenants-accounts';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Tenants Accounts';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/water_tenants', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function update_invoices()
	{
		
		$this->accounts_model->update_invoices();

		redirect('tenants-accounts');
	}
	public function update_home_owner_invoices()
	{

		$this->accounts_model->update_home_owner_invoices();

		redirect('cash-office/owners');
	}
	public function search_accounts()
	{
		$property_id = $this->input->post('property_id');
		$tenant_name = $this->input->post('tenant_name');
		$rental_unit_name = $this->input->post('rental_unit_name');
		
		$search_title = '';
		
		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}


			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';
			
			
		}
		
		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name.' ';
			$tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';

			
		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';
			
		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $property_id.$tenant_name.$rental_unit_name;
		$property_search = $property_id;
		// var_dump($search); die();
		
		$this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('all_accounts_search', $search);
		$this->session->set_userdata('accounts_search_title', $search_title);
		
		$this->water_tenants();
	}
	public function search_home_owners()
	{
		$property_id = $this->input->post('property_id');
		$home_owner_name = $this->input->post('home_owner_name');
		$rental_unit_name = $this->input->post('rental_unit_name');
		
		$search_title = '';
		
		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}


			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';
			
			
		}
		
		if(!empty($home_owner_name))
		{
			$search_title .= $home_owner_name.' ';
			$home_owner_name = ' AND home_owners.home_owner_name LIKE \'%'.$home_owner_name.'%\'';
		}
		else
		{
			$home_owner_name = '';
			$search_title .= ' ';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $home_owner_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';
		}
		else
		{
			$rental_unit_name = '';
			$search_title .= ' ';
		}


		$search = $property_id.$home_owner_name.$rental_unit_name;
		$property_search = $property_id;
		// var_dump($search); die();
		
		// $this->session->set_userdata('home_owner_property_search', $property_search);
		$this->session->set_userdata('home_owner_property_search', $search);
		$this->session->set_userdata('owners_search_title', $search_title);
		
		$this->owners();
	}
	public function close_accounts_search()
	{
		$this->session->unset_userdata('all_accounts_search');
		$this->session->unset_userdata('search_title');
		
		
		redirect('setup/water');
	}
	public function close_home_owners_search()
	{
		$this->session->unset_userdata('home_owner_property_search');
		$this->session->unset_userdata('search_title');
		
		
		$this->owners();
	}
	public function make_payments($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_amount', 'Water amount', 'trim|xss_clean');
		$this->form_validation->set_rules('service_charge_amount', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('penalty_fee', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('rent_amount', 'Rent amount', 'trim|xss_clean');
		


		$payment_method = $this->input->post('payment_method');
		$water_amount = $this->input->post('water_amount');
		$rent_amount = $this->input->post('rent_amount');
		$service_charge_amount = $this->input->post('service_charge_amount');
		$penalty_fee = $this->input->post('penalty_fee');
		$amount_paid = $this->input->post('amount_paid');
		$fixed_charge = $this->input->post('fixed_charge');
		$deposit_charge = $this->input->post('deposit_charge');

		$insurance = $this->input->post('insurance');
		$sinking_funds = $this->input->post('sinking_funds');
		$bought_water = $this->input->post('bought_water');
		$painting_charge = $this->input->post('painting_charge');
		$legal_fees = $this->input->post('legal_fees');


		if(empty($water_amount))
		{
			$water_amount = 0;
		}
		if(empty($service_charge_amount))
		{
			$service_charge_amount = 0;
		}
		if(empty($rent_amount))
		{
			$rent_amount = 0;
		}
		if(empty($penalty_fee))
		{
			$penalty_fee = 0;
		}
		if(empty($fixed_charge))
		{
			$fixed_charge = 0;
		}

		if(empty($deposit_charge))
		{
			$deposit_charge = 0;
		}
		if(empty($insurance))
		{
			$insurance = 0;
		}
		if(empty($sinking_funds))
		{
			$sinking_funds = 0;
		}
		if(empty($bought_water))
		{
			$bought_water = 0;
		}
		if(empty($painting_charge))
		{
			$painting_charge = 0;
		}
		if(empty($legal_fees))
		{
			$legal_fees = 0;
		}
		//  add all this items 
		$total = $service_charge_amount + $rent_amount + $water_amount + $penalty_fee + $fixed_charge + $deposit_charge + $insurance + $sinking_funds + $bought_water + $painting_charge+ $legal_fees;

		// var_dump($total); die();
		// Normal
		
		if(!empty($payment_method))
		{
			if($payment_method == 1)
			{
				// check for cheque number if inserted
				$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');
			
			}
			else if($payment_method == 5)
			{
				//  check for mpesa code if inserted
				$this->form_validation->set_rules('mpesa_code', 'Amount', 'is_unique[payments.transaction_code]|trim|required|xss_clean');
			}
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			if($amount_paid == $total)
			{
				$this->accounts_model->receipt_payment($lease_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
			}
			else
			{
				$this->session->set_userdata("error_message", 'The amounts of payment do not add up to the total amount paid');
			}
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}
	public function create_pardon($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('pardon_amount', 'Amount', 'numeric|trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_date', 'Date ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_reason', 'Reason', 'required|trim|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			if($this->accounts_model->receipt_pardon($lease_id))
			{
				$this->session->set_userdata("success_message", 'Pardon created successfully added');
			}
			else
			{
				$this->session->set_userdata("error_message", 'Sorry. Please try again');
			}
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);

	}
	public function create_owner_pardon($home_owner_unit_id , $rental_unit_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('pardon_amount', 'Amount', 'numeric|trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_date', 'Date ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_reason', 'Reason', 'required|trim|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			if($this->accounts_model->receipt_owner_pardon($home_owner_unit_id, $rental_unit_id))
			{
				$this->session->set_userdata("success_message", 'Pardon created successfully added');
			}
			else
			{
				$this->session->set_userdata("error_message", 'Sorry. Please try again');
			}
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);

	}
    public function payments($tenant_unit_id,$lease_id)
	{
		$v_data = array('tenant_unit_id'=>$tenant_unit_id,'lease_id'=>$lease_id);
		$v_data['title'] = 'Tenant payments';
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		// get lease payments for this year
		$v_data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);
		$v_data['lease_pardons'] = $this->accounts_model->get_lease_pardons($lease_id);

		$data['content'] = $this->load->view('cash_office/payments', $v_data, true);
		
		$data['title'] = 'Tenant payments';
		
		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Add a new tenant page
	*
	*/
	public function add_tenant($rental_unit_id = NULL) 
	{
		//form validation rules
		$this->form_validation->set_rules('tenant_email', 'Email', 'xss_clean|is_unique[tenants.tenant_email]|valid_email');
		$this->form_validation->set_rules('tenant_name', 'Tenant name', 'required|xss_clean');
		$this->form_validation->set_rules('tenant_phone_number', 'Tenant Phone Number', 'required|xss_clean|is_unique[tenants.tenant_phone_number]');
		$this->form_validation->set_rules('tenant_national_id', 'National Id', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if tenant has valid login credentials
			if($this->tenants_model->add_tenant())
			{
				$this->session->unset_userdata('tenants_error_message');
				$this->session->set_userdata('success_message', 'Tenant has been successfully added');

				if($rental_unit_id == NULL OR $rental_unit_id == 0)
				{
					redirect('rental-management/tenants');
				}
				else
				{
					redirect('rents/tenants/'.$rental_unit_id);
				}

				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');
				if($rental_unit_id == NULL OR $rental_unit_id == 0)
				{
					redirect('rental-management/tenants');
				}
				else
				{
					redirect('rents/tenants/'.$rental_unit_id);
				}

			}
		}
		
		//open the add new tenant page
	}
	public function allocate_tenant_to_unit($rental_unit_id)
	{
		$this->form_validation->set_rules('tenant_id', 'Tenant id', 'required|trim|xss_clean');

		if ($this->form_validation->run())
		{	
			if($this->tenants_model->add_tenant_to_unit($rental_unit_id))
			{
				$this->session->unset_userdata('lease_error_message');
				$this->session->set_userdata('success_message', 'Tenant has been added successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry please check for errors has been added successfully');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please fill in all the fields');
		}
		redirect('tenants/'.$rental_unit_id);	
	}
	public function allocate_tenant_to_unit_other($rental_unit_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('tenant_id', 'Tenant name', 'required|xss_clean');
		$this->form_validation->set_rules('rental_unit_id', 'Rental Unit', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			// check if another tenant account is active
			$rental_unit_id = $this->input->post('rental_unit_id');
			$checker = $this->tenants_model->check_for_account($rental_unit_id);

			if($checker = TRUE)
			{
				$this->session->set_userdata('error_message', 'Seems Like another lease is still active, Please close the active lease before proceeding');
				redirect('rents/tenants/'.$rental_unit_id);
			}
			else
			{
				//check if tenant has valid login credentials
				if($this->tenants_model->allocate_tenant_to_rental_unit())
				{
					$this->session->unset_userdata('tenants_error_message');
					$this->session->set_userdata('success_message', 'Tenant has been successfully added');
					redirect('rents/tenants/'.$rental_unit_id);
				}
				
				else
				{
					$this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');
					redirect('rents/tenants/'.$rental_unit_id);

				}
			}
				
		}
		
		//open the add new tenant page
	}
    
	/*
	*
	*	Edit an existing tenant page
	*	@param int $tenant_id
	*
	*/
	public function edit_tenant($tenant_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('tenant_email', 'Email', 'xss_clean|exists[tenants.tenant_email]|valid_email');
		$this->form_validation->set_rules('tenant_name', 'Tenant name', 'required|xss_clean');
		$this->form_validation->set_rules('tenant_phone_number', 'Tenant Phone Number', 'required|xss_clean|exists[tenants.tenant_phone_number]');
		$this->form_validation->set_rules('tenant_national_id', 'National Id', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if tenant has valid login credentials
			if($this->tenants_model->edit_tenant($tenant_id))
			{
				$this->session->set_userdata('success_message', 'tenant edited successfully');
				$pwd_update = $this->input->post('admin_tenant');
				if(!empty($pwd_update))
				{
					redirect('admin-profile/'.$tenant_id);
				}
				
				else
				{
					redirect('admin/administrators');
				}
			}
			
			else
			{
				$data['error'] = 'Unable to add tenant. Please try again';
			}
		}
		
		//open the add new tenant page
		$data['title'] = 'Edit administrator';
		$v_data['title'] = $data['title'];
		
		//select the tenant from the database
		$query = $this->tenants_model->get_tenant($tenant_id);
		if ($query->num_rows() > 0)
		{
			$v_data['tenants'] = $query->result();
			$data['content'] = $this->load->view('tenants/edit_tenant', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'tenant does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing tenant page
	*	@param int $tenant_id
	*
	*/
	public function delete_tenant($tenant_id) 
	{
		if($this->tenants_model->delete_tenant($tenant_id))
		{
			$this->session->set_userdata('success_message', 'Administrator has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Administrator could not be deleted');
		}
		
		redirect('admin/administrators');
	}
    
	/*
	*
	*	Activate an existing tenant page
	*	@param int $tenant_id
	*
	*/
	public function activate_tenant($tenant_id) 
	{
		if($this->tenants_model->activate_tenant($tenant_id))
		{
			$this->session->set_userdata('success_message', 'Administrator has been activated');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Administrator could not be activated');
		}
		
		redirect('admin/administrators');
	}
    
	/*
	*
	*	Deactivate an existing tenant page
	*	@param int $tenant_id
	*
	*/
	public function deactivate_tenant($tenant_id) 
	{
		if($this->tenants_model->deactivate_tenant($tenant_id))
		{
			$this->session->set_userdata('success_message', 'Administrator has been disabled');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Administrator could not be disabled');
		}
		
		redirect('admin/administrators');
	}
	
	/*
	*
	*	Reset a tenant's password
	*	@param int $tenant_id
	*
	*/
	public function reset_password($tenant_id)
	{
		$new_password = $this->login_model->reset_password($tenant_id);
		$this->session->set_userdata('success_message', 'New password is <br/><strong>'.$new_password.'</strong>');
		
		redirect('admin/administrators');
	}
	
	/*
	*
	*	Show an administrator's profile
	*	@param int $tenant_id
	*
	*/
	public function admin_profile($tenant_id)
	{
		//open the add new tenant page
		$data['title'] = 'Edit tenant';
		
		//select the tenant from the database
		$query = $this->tenants_model->get_tenant($tenant_id);
		if ($query->num_rows() > 0)
		{
			$v_data['tenants'] = $query->result();
			$v_data['admin_tenant'] = 1;
			$tab_content[0] = $this->load->view('tenants/edit_tenant', $v_data, true);
		}
		
		else
		{
			$data['tab_content'][0] = 'tenant does not exist';
		}
		$tab_name[1] = 'Overview';
		$tab_name[0] = 'Edit Account';
		$tab_content[1] = 'Coming soon';//$this->load->view('account_overview', $v_data, true);
		$data['total_tabs'] = 2;
		$data['content'] = $tab_content;
		$data['tab_name'] = $tab_name;
		
		$this->load->view('templates/tabs', $data);
	}
	public function change_password()
	{
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->input->post('new_password') == $this->input->post('confirm_new_password'))
			{
				if($this->tenants_model->change_password())
				{
					$this->session->set_userdata('success_message', 'Your password has been changed successfully');
				}
				else
				{
					$this->session->set_userdata('error_message', 'Something went wrong, please try again');
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Ensure that the new password match with the confirm password');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check that all the fields have values');
		}
		redirect('dashboard');
	}

	public function import_payments()
	{

		$where = 'import_payment_upload_deleted = 0';
		$table = 'import_payment_upload';		


		$import_payment_search = $this->session->userdata('all_import_payment_search');
		
		if(!empty($import_payment_search))
		{
			$where .= $import_payment_search;	
			
		}
		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'import/payments';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_failed($table, $where, $config["per_page"], $page, $order='property_id', $order_method='ASC');

		

		$v_data['title'] = $data['title'] = 'Import Tenant payments';

		$v_data['page'] = $page;
		$property_order = 'property_id';
		$property_table = 'property';
		$property_where = 'property_id > 0';

		$property_query = $this->property_model->get_active_list($property_table, $property_where, $property_order);
		$rs8 = $property_query->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;

		    $property_list .="<option value='".$property_id."'>".$property_name."</option>";

		endforeach;

		$v_data['property_list'] = $property_list;


		$v_data['query'] = $query;
		$data['content'] = $this->load->view('import/import_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function payments_import_template()
	{
		$this->accounts_model->payments_import_template();
	}

	public function payments_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->accounts_model->import_csv_payroll($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = 'Import Tenant payments';
		
		redirect('import/payments');
		// $data['content'] = $this->load->view('import/import_payments', $v_data, true);
		// $this->load->view('admin/templates/general_page', $data);
	}
	public function send_arrears($lease_id)
	{

		$month = date('m');
		$year = date('Y');

		$this->send_invoices($lease_id,$month,$year);

		$this->session->set_userdata('success_message', 'Message has been send successfully');	

		redirect('setup/water');
	}
	public function print_receipt($payment_id, $tenant_unit_id, $lease_id)
	{
		$data = array('payment_id' => $payment_id, 'tenant_unit_id' => $tenant_unit_id, 'lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);
		
		$data['lease_payments'] = $this->accounts_model->get_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];

		$this->load->view('receipts/tenants_receipt', $data);
	}

	public function send_tenants_receipt($payment_id, $tenant_unit_id, $lease_id)
	{
		$data = array('payment_id' => $payment_id, 'tenant_unit_id' => $tenant_unit_id, 'lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $lease_payments = $this->accounts_model->get_lease_payments($lease_id);
		
		$data['payment_details'] = $this->accounts_model->get_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];


		$html = $this->load->view('receipts/tenants_receipt', $data, TRUE);

		$this->load->library('mpdf');
		
		$document_number = date("ymdhis");
		$receipt_month_date = date('Y-m-d');
		$receipt_month = date('M',strtotime($receipt_month_date));
		$receipt_year = date('Y',strtotime($receipt_month_date));
		$title = $document_number.'-REC.pdf';
		$invoice = $title;
		// echo $invoice;die();
		
		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}

		if($lease_payments->num_rows() > 0)
		{
			$y = 0;
			foreach ($lease_payments->result() as $key) 
			{
				$payment_id = $key->payment_id;
				$receipt_number = $key->receipt_number;
				$amount_paid = $key->amount_paid;
				$paid_by = $key->paid_by;
				$payment_date = $date_of_payment = $key->payment_date;
				$payment_created = $key->payment_created;
				$payment_created_by = $key->payment_created_by;
				$transaction_code = $key->transaction_code;
				$invoice_month_number = $key->month;
				
				$payment_date = date('jS M Y',strtotime($payment_date));
				$payment_created = date('jS M Y',strtotime($payment_created));
				$y++;
			}
		}
		// var_dump($tenant_phone_number);die();
		// $tenant_email = 'marttkip@gmail.com';
		if(!empty($tenant_email))
		{
			$message['subject'] =  $document_number.' Receipt';
			$message['text'] = ' <p>Dear '.$tenant_name.',<br>
									Please find attached receipt
									</p>
								';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email']; 
			
			$button = '';
			$sender['email'] = $contacts['email']; 
			$sender['name'] = $contacts['company_name'];
			$receiver['email'] = $tenant_email;
			$receiver['name'] = $tenant_name;
			$payslip = $title;
			// var_dump($tenant_email);die();
			
			$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
			
			// save the invoice sent out on the database

			$insert_array = array('email'=>$receiver['email'],'client_name'=>$tenant_name,'email_body'=>$message['text'],'type_of_account'=>1,'email_attachment'=>$title,'date_created'=>date('Y-m-d'),'email_type'=>2);
			$this->db->insert('email',$insert_array);
		}
		// $tenant_phone_number = 704808007;
		// var_dump($tenant_phone_number);die();
		if(!empty($tenant_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			// $tenant_array = explode(' ', $tenant_name);
			// $tenant_name = $tenant_array[0];

			
			$message = 'Dear '.$tenant_name.', Your payment of  KES. '.$amount_paid.' for '.$rental_unit_name.' has been successfully received '.$message_prefix;
			$this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);

			// save the message sent out 
			$insert_array = array('phone_number'=>$tenant_phone_number,'client_name'=>$tenant_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'sms_type'=>2);
			$this->db->insert('sms',$insert_array);
			// save the message sent out
			
		}

		redirect('cash-office/payments/'.$tenant_unit_id.'/'.$lease_id.'');
	}

	public function print_owners_receipt($payment_id, $home_owner_unit_id, $rental_unit_id)
	{
		$data = array('payment_id' => $payment_id, 'home_owner_unit_id' => $home_owner_unit_id, 'rental_unit_id' => $rental_unit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		// $data['lease_payments'] = $this->accounts_model->get_lease_payments_owners($rental_unit_id);
		
		$data['lease_payments'] = $this->accounts_model->get_owners_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_owner_unit_detail($rental_unit_id);
		
		
		$contacts = $data['contacts'];

		$this->load->view('receipts/owners_receipt', $data);
	}


	public function send_owners_receipt($payment_id, $home_owner_unit_id, $rental_unit_id)
	{
		$data = array('payment_id' => $payment_id, 'home_owner_unit_id' => $home_owner_unit_id, 'rental_unit_id' => $rental_unit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments_owners($rental_unit_id);
		
		$data['payment_details'] = $this->accounts_model->get_owners_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_owner_unit_detail($rental_unit_id);
		
		
		$contacts = $data['contacts'];

		$html = $this->load->view('receipts/owners_receipt', $data,TRUE);


		$this->load->library('mpdf');
		// var_dump($html);die();
		$document_number = date("ymdhis");
		$invoice_month_date = date('Y-m-d');
		$invoice_month = date('M',strtotime($invoice_month_date));
		$invoice_year = date('Y',strtotime($invoice_month_date));
		$title = $document_number.'-INV.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;
		// echo $invoice;die();
		
		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}
		
		$all_leases = $this->leases_model->get_lease_detail_owners_receipt($home_owner_unit_id);
		// var_dump($all_leases); die();
		foreach ($all_leases->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$home_owner_id = $leases_row->home_owner_id;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_email = $leases_row->home_owner_email;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$message_prefix = $leases_row->message_prefix;
			
		}

		$lease_payments = $this->accounts_model->get_lease_payments_owners($rental_unit_id);
		if($lease_payments->num_rows() > 0)
		{
			$y = 0;
			foreach ($lease_payments->result() as $key) 
			{
				$payment_idd = $key->payment_id;
				$receipt_number = $key->receipt_number;
				$amount_paid = $key->amount_paid;
				$paid_by = $key->paid_by;
				$payment_date = $key->payment_date;
				$payment_created = $key->payment_created;
				$payment_created_by = $key->payment_created_by;
				$transaction_code = $key->transaction_code;
				$invoice_month_number = $key->month;
				
				$payment_date = date('jS M Y',strtotime($payment_date));
				$payment_created = date('jS M Y',strtotime($payment_created));
				$y++;
			}
		}

		// $home_owner_email = 'marttkip@gmail.com';
		if(!empty($home_owner_email))
		{
			$message['subject'] =  $document_number.' Receipt';
			$message['text'] = ' <p>Dear '.$home_owner_name.',<br>
									Please find attached receipt
									</p>
								';
			$sender_email = $contacts['email'];



			$shopping = "";
			$from = $contacts['email']; 
			$home_owner_email = str_replace(';', '/', $home_owner_email);
			$home_owner_email = str_replace(' ', '', $home_owner_email);
			$email_array = explode('/', $home_owner_email);
			$total_rows_email = count($email_array);

			for($x = 0; $x < $total_rows_email; $x++)
			{
				$receiver['email'] = $home_owner_email = $email_array[$x];
				
				$button = '';
				$sender['email'] = $contacts['email']; 
				$sender['name'] = $contacts['company_name'];
				// $receiver['email'] = $home_owner_email;
				$receiver['name'] = $home_owner_name;
				$payslip = $title;
			
			
				$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
				
				// save the invoice sent out on the database

				$insert_array = array('email'=>$receiver['email'],'client_name'=>$home_owner_name,'email_body'=>$message['text'],'type_of_account'=>0,'email_attachment'=>$title,'date_created'=>date('Y-m-d'),'email_type'=>2);
				$this->db->insert('email',$insert_array);
			}

			//  save the item set out
		}
		// var_dump($tenant_phone_number);die();
		// send SMS to the person
		// $home_owner_phone_number = 71654;
		if(!empty($home_owner_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			// $tenant_array = explode(' ', $tenant_name);
			// $tenant_name = $tenant_array[0];
			$phone_number_array = explode('/', $home_owner_phone_number);
			$total_rows_phone_number = count($phone_number_array);

			for($x = 0; $x < $total_rows_phone_number; $x++)
			{
			   $home_owner_phone_number = $phone_number_array[$x];
			
				$message = 'Dear '.$home_owner_name.', Your payment of  KES. '.$amount_paid.' for '.$rental_unit_name.' has been successfully received '.$message_prefix;
				$this->accounts_model->sms($home_owner_phone_number,$message,$home_owner_name);
			}

			// save the message sent out 
			$insert_array = array('phone_number'=>$home_owner_phone_number,'client_name'=>$home_owner_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'sms_type'=>2);
			$this->db->insert('sms',$insert_array);
			// save the message sent out
			
		}

			redirect('owners-payments/'.$home_owner_id.'/'.$rental_unit_id.'');
	}

	public function print_lease_statement($lease_id)
	{
		$data = array('lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);

		
		$this->load->view('cash_office/statement', $data);
	}
	public function print_invoice($lease_id,$invoice_month,$invoice_year)
	{
		$data = array('lease_id' => $lease_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_invoice'] = $this->accounts_model->get_invoices_month($lease_id,$invoice_month,$invoice_year);

		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);
		
		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];

		$invoice_template_name = $this->accounts_model->get_invoice_template(1,$property_id);
		// $this->load->view('cash_office/water_charge_invoice', $data);
		// $this->load->view('cash_office/'.$invoice_template_name.'', $data);
		// if($lease_id == 1 OR $lease_id == 2 OR $lease_id == 3 OR $lease_id == 6 OR  $lease_id == 171 OR $lease_id == 7 OR $lease_id == 13 OR $lease_id == 15 OR $lease_id == 17 OR $lease_id == 19 OR $lease_id == 20 OR $lease_id == 21 OR $lease_id == 22 OR $lease_id == 23 OR $lease_id == 25 OR $lease_id == 29 OR $lease_id == 31 OR $lease_id == 32 OR $lease_id == 34 OR $lease_id == 37 OR $lease_id == 42 OR $lease_id == 47 OR $lease_id == 51 OR $lease_id == 53 OR $lease_id == 54 OR $lease_id == 55 OR $lease_id == 57 OR $lease_id == 58 OR $lease_id == 59 OR $lease_id == 62 OR $lease_id == 63 OR $lease_id == 64 OR $lease_id == 65 OR $lease_id == 67 OR $lease_id == 69 OR $lease_id == 72 OR $lease_id == 172 OR $lease_id == 89 OR $lease_id == 166 OR $lease_id == 75 OR $lease_id == 79 OR $lease_id == 173 OR $lease_id == 170 OR $lease_id == 82 OR $lease_id == 85 OR $lease_id == 86 OR $lease_id == 87 OR $lease_id == 174 OR $lease_id == 175 OR $lease_id == 179 OR $lease_id == 176 )
		// {
		// 	$this->load->view('cash_office/invoice_water_fixed_charge', $data);
		// }
		// else
		// {
			$this->load->view('cash_office/'.$invoice_template_name.'', $data);
		// }

	}
	public function edit_invoice($lease_id,$invoice_month,$invoice_year)
	{
		$data = array('lease_id' => $lease_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_invoice'] = $this->accounts_model->get_invoices_month($lease_id,$invoice_month,$invoice_year);

		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);
		
		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];

		$invoice_template_name = $this->accounts_model->get_invoice_template(1,$property_id);
		// $this->load->view('cash_office/water_charge_invoice', $data);
		$this->load->view('invoices/'.$invoice_template_name.'', $data);

	}
	public function print_owners_invoice($home_owner_id,$rental_unit_id,$invoice_month,$invoice_year)
	{
		$data = array('home_owner_id' => $home_owner_id,'rental_unit_id'=>$rental_unit_id,'invoice_month_parent' => $invoice_month,'invoice_year_parent' => $invoice_year);
		$data['contacts'] = $this->site_model->get_contacts();
		// $data['lease_invoice'] = $this->accounts_model->get_invoices_month_home_owners($home_owner_id,$invoice_month,$invoice_year);

		// $data['lease_payments'] = $this->accounts_model->get_lease_payments_owners($home_owner_id);
		
		$all_leases = $this->leases_model->get_lease_detail_owners($home_owner_id);


		foreach ($all_leases->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_email = $leases_row->home_owner_email;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			
		}
		
		$contacts = $data['contacts'];

		// $html = $this->load->view('cash_office/owner_invoice', $data,TRUE);

		$invoice_template_name = $this->accounts_model->get_invoice_template(0,$property_id);
		// var_dump($invoice_template_name); die();
		// $this->load->view('cash_office/water_charge_invoice', $data);
		$this->load->view('cash_office/'.$invoice_template_name.'', $data);

	}
	
	public function send_sms($payment_id, $tenant_unit_id, $lease_id, $phone = NULL, $message = NULL)
	{
		// $this->load->model('messaging/messaging_model');
		$message = $this->site_model->decode_web_name($message);
		$contacts = $this->site_model->get_contacts();
		$where = 'leases.lease_id = '.$lease_id.' AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id';
		$this->db->from('leases,tenant_unit,tenants,rental_unit');
		$this->db->select('tenant_name,rental_unit_name,tenant_phone_number,tenant_email');
		$this->db->where($where);
		$query = $this->db->get();
		
		$data = array('lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);

		
		$html = $this->load->view('cash_office/statement', $data);

		$this->load->library('mpdf');
		$title = $lease_id.'-'.$rental_unit_name.'-Statement.pdf';
		$statement_path = $this->statement_path;
		$statement = $statement_path.'/'.$title;
		
		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');
		
		if($query->num_rows > 0)
		{
			$date_today = date('Y-m-d');
			$date_exploded = explode('-', $date_today);
			$_year = $date_exploded[0];
			$_month = $date_exploded[1];
			$rows = $query->row();

			$tenant_name = $rows->tenant_name;
			$rental_unit_name = $rows->rental_unit_name;
			$tenant_phone_number = $rows->tenant_phone_number;
			$tenant_email  =$rows->tenant_email;

			$tenant_explode = explode(' ', $tenant_name);
			$name = $tenant_explode[0];

		
			if(!empty($phone))
			{
				//sms for that month,year n unit has been sent
				if($this->check_statement_sms_sent($name,$_year,$_month)==FALSE)
				{
					$status = $this->accounts_model->sms($tenant_phone_number, $message,$name,$title);
					if($status == 'Success')
					{
						
						$this->session->set_userdata('success_message', 'Message sent successfully');
					}
					
					else
					{
						$this->session->set_userdata('error_message', 'Unable to send message'.$status);
					}
				}
			}
				
			else
			{
				$this->session->set_userdata('error_message', 'Please enter tenant\'s phone number to send message');
			}
			if(!empty($tenant_email))
			{
				$message['subject'] =  $document_number.' Statement';
				$message['text'] = ' <p>Dear '.$tenant_name.',<br>
										Please find attached your statement 
										</p>
									';
				$sender_email = $contacts['email'];
				$shopping = "";
				$from = $contacts['email']; 
				
				$button = '';
				$sender['email'] = $contacts['email']; 
				$sender['name'] = $contacts['company_name'];
				$receiver['email'] = $tenant_email;
				$receiver['name'] = $tenant_name;
				$statement_file = $title;
				
				//check if statement was sent for that month,year for that unit
				if($this->statement_email_sent($_year,$_month,$tenant_unit_id,1)==FALSE)
				{
					$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $statement_file);
					
					$statement_insert_array = array('email'=>$receiver['email'],'client_name'=>$tenant_name,'email_body'=>$message['text'],'email_attachment'=>$title,'date_created'=>date('Y-m-d'), 'tenant_unit_id'=>$tenant_unit_id, 'statement_year'=>$_year, 'statement_month'=>$_month, 'statement_email_sent'=>1);
					$this->db->insert('statement_email',$statement_insert_array);
				}	
			}

		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry something went wrong please try again');
		}
		
		
		redirect('cash-office/payments/'.$tenant_unit_id.'/'.$lease_id);
	}
	public function send_notifications()
	{

		//  get all accounts with invoices for the month ... 
		$month = date('m');
		$year = date('Y');

		$leases = $this->accounts_model->get_all_invoices($month,$year);
		// var_dump($leases); die();
		if($leases->num_rows() > 0)
		{
			foreach ($leases->result() as $key) {
				# code...
				$lease_id = $key->lease_id;

				$this->send_invoices($lease_id,$month,$year);
			}
			$this->session->set_userdata('success_message', 'Notifications have been successfully sent');
		}
		else
		{
			$this->session->set_userdata('error_message', 'No notifications to be sent out');
		}
		redirect('tenants-accounts');
	}

	public function send_invoices($lease_id,$invoice_month,$invoice_year)
	{
		$data = array('lease_id' => $lease_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_invoice'] = $this->accounts_model->get_invoices_month($lease_id,$invoice_month,$invoice_year);

		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);
		
		$all_leases = $this->leases_model->get_lease_detail($lease_id);


		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$rental_unit_id = $leases_row->rental_unit_id;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;
			$message_prefix = $leases_row->message_prefix;
			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];

		$data['rental_unit_id'] = $lease_id;

		// $html = $this->load->view('cash_office/water_charge_invoice', $data, TRUE);
		// $html = $this->load->view('cash_office/invoice_three', $data, TRUE);

		$invoice_template_name = $this->accounts_model->get_invoice_template(1,$property_id);
		// $this->load->view('cash_office/water_charge_invoice', $data);

		// if($lease_id == 1 OR $lease_id == 2 OR $lease_id == 3  OR $lease_id == 6 OR  $lease_id == 171 OR $lease_id == 7 OR $lease_id == 13 OR $lease_id == 15 OR $lease_id == 17 OR $lease_id == 19 OR $lease_id == 20 OR $lease_id == 21 OR $lease_id == 22 OR $lease_id == 23 OR $lease_id == 25 OR $lease_id == 29 OR $lease_id == 31 OR $lease_id == 32 OR $lease_id == 34 OR $lease_id == 37 OR $lease_id == 42 OR $lease_id == 47 OR $lease_id == 51 OR $lease_id == 53 OR $lease_id == 54 OR $lease_id == 55 OR $lease_id == 57 OR $lease_id == 58 OR $lease_id == 59 OR $lease_id == 62 OR $lease_id == 63 OR $lease_id == 64 OR $lease_id == 65 OR $lease_id == 67 OR $lease_id == 69 OR $lease_id == 72 OR $lease_id == 172 OR $lease_id == 89 OR $lease_id == 166 OR $lease_id == 75 OR $lease_id == 79 OR $lease_id == 173 OR $lease_id == 170 OR $lease_id == 82 OR $lease_id == 85 OR $lease_id == 86 OR $lease_id == 87 OR $lease_id == 174 OR $lease_id == 175 OR $lease_id == 176 )
		// {
		// 	$html = $this->load->view('cash_office/invoice_water_fixed_charge', $data, TRUE);
		// }
		// else
		// {
			$html = $this->load->view('cash_office/'.$invoice_template_name.'', $data, TRUE);
		// }

		$this->load->library('mpdf');
		// var_dump($html);die();
		// $document_number = date("ymdhis");
		$document_number = 'L'.$lease_id.'-'.$invoice_month.'-'.$invoice_year;


		$invoice_month_date = date('Y-m-d');
		$invoice_month = date('M',strtotime($invoice_month_date));
		$invoice_year = date('Y',strtotime($invoice_month_date));
		$title = $document_number.'-INV.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;
		// echo $invoice;die();
		
		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}
		// var_dump($tenant_phone_number);die();

		$this_month = date('m');
     	$amount_paid = 0;
     	$payments = $this->accounts_model->get_this_months_payment($lease_id,$this_month);
     	$current_items = '<td> -</td>
     			  <td>-</td>'; 
     	$total_paid_amount = 0;
     	if($payments->num_rows() > 0)
     	{
     		$counter = 0;
     		
     		$receipt_counter = '';
     		foreach ($payments->result() as $value) {
     			# code...
     			$receipt_number = $value->receipt_number;
     			$amount_paid = $value->amount_paid;

     			if($counter > 0)
     			{
     				$addition = '#';
     				// $receipt_counter .= $receipt_number.$addition;
     			}
     			else
     			{
     				$addition = ' ';
     				
     			}
     			$receipt_counter .= $receipt_number.$addition;

     			$total_paid_amount = $total_paid_amount + $amount_paid;
     		
     		}
     		$current_items = '<td>'.$receipt_number.'</td>
     					<td>'.number_format($amount_paid,0).'</td>';
     	}
     	else
     	{
     		$current_items = '<td> -</td>
     					<td>-</td>';
     	}
		$todays_date = date('Y-m-d');
		$todays_month = date('m');
		$todays_year = date('Y');



		$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
		$total_arrears = $tenants_response['total_arrears'];
		$invoice_date = $tenants_response['invoice_date'];



		$current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
		$total_current_invoice = $current_invoice_amount + $current_balance - $total_paid_amount;
		// $total_bills = $total_bills + 250;
		
		$date_today = date('Y-m-d');
		$date_exploded = explode('-', $date_today);
		$_year = $date_exploded[0];
		$_month = $date_exploded[1];
		// var_dump($tenant_email); die();
		// $tenant_email = 'martin@omnis.co.ke/accounts@homesandabroad.co.ke';

		// var_dump($tenant_email); die();


		$tenant_name = ucfirst($tenant_name);

		

		// if(!empty($tenant_email))
		// {
		// 	if($property_id == 2)
		// 	{

		// 		$message['subject'] =  'April Water Invoice';	
		// 	}
		// 	else
		// 	{
		// 		$message['subject'] =  $document_number.' Invoice';	
		// 	}
		// 	$message['text'] = ' <p>Dear '.$tenant_name.',<br>
		// 							Please find attached invoice.
		// 							</p>
		// 						';
		// 	$sender_email = $contacts['email'];
		// 	$shopping = "";
		// 	$from = $contacts['email']; 
			
		// 	$button = '';
		// 	$sender['email']= $contacts['email']; 
		// 	$sender['name'] = $contacts['company_name'];
		// 	$receiver['name'] = $tenant_name;
		// 	$payslip = $title;

		// 	$sender_email = 'marttkip@gmail.com';
		// 	$tenant_email .= '/'.$sender_email.'';
		// 	// var_dump($tenant_email); die();
		// 	$email_array = explode('/', $tenant_email);
		// 	$total_rows_email = count($email_array);

		// 	for($x = 0; $x < $total_rows_email; $x++)
		// 	{
		// 		$receiver['email'] = $email_tenant = $email_array[$x];
				
		// 		// if($this->accounts_model->check_if_email_sent($rental_unit_id,$email_tenant,$todays_year,$todays_month,1))
		// 		// {
		// 		// }
		// 		// else
		// 		// {
		// 			$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
		// 			$insert_array = array('email'=>$receiver['email'],'client_name'=>$tenant_name,'email_body'=>$message['text'],'type_of_account'=>1,'email_attachment'=>$title,'date_created'=>date('Y-m-d'), 'rental_unit_id'=>$rental_unit_id, 'invoice_year'=>$_year, 'invoice_month'=>$_month, 'email_sent'=>1);
		// 			$this->db->insert('email',$insert_array);
		// 		// }

		// 	}
		// }
		// var_dump($tenant_phone_number);die();
		// send SMS to the person
		// $tenant_phone_number = 716548104;
		if(!empty($tenant_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			$phone_array = explode('/', $tenant_phone_number);
			$total_rows = count($phone_array);

			for($r = 0; $r < $total_rows; $r++)
			{
				$tenant_phone_number = $phone_array[$r];

				$message = 'Dear '.$tenant_name.', Your current dues upto '.$invoice_month.' '.$invoice_year.'  is KES. '.number_format($total_arrears).' '.$message_prefix.' ';
				// if($this->accounts_model->check_if_sms_sent($rental_unit_id,$tenant_phone_number,$todays_year,$todays_month,1))
				// {
				// }
				// else
				// {
					$this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);
					$insert_array = array('phone_number'=>$tenant_phone_number,'client_name'=>$tenant_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'rental_unit_id'=>$rental_unit_id, 'sms_invoice_year'=>$_year, 'sms_invoice_month'=>$_month, 'sms_sent'=>1);
					$this->db->insert('sms',$insert_array);
				// }

				// save the message sent out 
				
			}
			
			
			
			// save the message sent out
			
		}

	}


	public function send_notifications_home_owners()
	{
		
		// $leases = $this->accounts_model->get_all_invoices_home_owners($month,$year);
		$leases = $this->accounts_model->get_all_owners_lists();
		// var_dump($leases); die();
		if($leases->num_rows() > 0)
		{
			foreach ($leases->result() as $key) {
				# code...
				$rental_unit_id = $key->rental_unit_id;
				$home_owner_id = $key->home_owner_id;
				$home_owner_unit_id = $key->home_owner_unit_id;

				$latest_month = $this->accounts_model->get_client_latest_invoice($home_owner_id,$rental_unit_id);
				if(!empty($latest_month))
				{
				   $latest_month_explode = explode('-', $latest_month);
				   $month = $latest_month_explode[1];
				   $year = $latest_month_explode[0];
				   // var_dump($month); die();
				   $this->send_invoices_home_owners($home_owner_id,$rental_unit_id,$month,$year,$latest_month);

				}
			}
			$this->session->set_userdata('success_message', 'Notifications have been successfully sent');
		}
		else
		{
			$this->session->set_userdata('error_message', 'No notifications to be sent out');
		}
		redirect('cash-office/owners');
	}
	public function send_invoices_home_owners($home_owner_id,$rental_unit_id,$invoice_month,$invoice_year,$invoice_date_parent=NULL)
	{


		$data = array('home_owner_id' => $home_owner_id,'rental_unit_id' => $rental_unit_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year,'invoice_month_parent'=>$invoice_month,'invoice_year_parent'=>$invoice_year);
		
		$data['contacts'] = $this->site_model->get_contacts();

		
		$all_leases = $this->leases_model->get_lease_detail_owners($home_owner_id);

		$total_bill = 0;
		$rental = '';
		$x = 0;
		foreach ($all_leases->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$home_owner_id = $leases_row->home_owner_id;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_email = $leases_row->home_owner_email;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;



			$rental .= $rental_unit_name;
			$x++;
			if($x < $all_leases->num_rows())
			{
				$rental .= ',';
			}
			$todays_date = date('Y-m-d');
			$todays_month = date('m');
			$todays_year = date('Y');


			$owners_response = $this->accounts_model->get_owners_billings($rental_unit_id,$home_owner_id);
			$total_bills = $owners_response['total_arrears'];
			$invoice_date = $owners_response['invoice_date'];

			$total_bill = $total_bill + $total_bills;
		}
		
		$contacts = $data['contacts'];



		$invoice_template_name = $this->accounts_model->get_invoice_template(0,$property_id);
		// $this->load->view('cash_office/water_charge_invoice', $data);
		$html = $this->load->view('cash_office/'.$invoice_template_name.'', $data, TRUE);

		$this->load->library('mpdf');
		// var_dump($html);die();
		// $document_number = date("ymdhis");
		$document_number = 'U'.$rental_unit_id.'-'.$invoice_month.'-'.$invoice_year;
		if(!empty($invoice_date_parent))
		{
			$invoice_month_date = $invoice_date_parent;
		}
		else
		{
			$invoice_month_date = date('Y-m-d');	
		}

		// var_dump($invoice_month_date);die();
		$invoice_month = date('M',strtotime($invoice_month_date));
		$invoice_year = date('Y',strtotime($invoice_month_date));
		$title = $document_number.'-INV.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;
		// echo $invoice;die();
		
		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);

		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}
		// echo $title; die();
		 
		$date_today = date('Y-m-d');
		$date_exploded = explode('-', $date_today);
		$_year = $date_exploded[0];
		$_month = $date_exploded[1];

		// $home_owner_email = "marttkip@gmail.com/accounts@homesandabroad.co.ke";
		// var_dump($home_owner_email); die();
		$home_owner_name = ucfirst($home_owner_name);
		if(!empty($home_owner_email))
		{
			if($property_id == 2)
			{

				$message['subject'] =  $invoice_month.' Service Charge Invoice';	
			}
			else
			{
				$message['subject'] =  $document_number.' Invoice';	
			}
			$message['subject'] =  $document_number.' Invoice';
			$message['text'] = ' <p>Dear '.$home_owner_name.',<br>
									Please find attached invoice
									</p>
								';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email'];;
			$button = '';
			$sender['email'] = $contacts['email']; 
			$sender['name'] = $contacts['company_name'];
			
			$receiver['name'] = $home_owner_name;
			$payslip = $title;
			
			$sender_email = 'homesandabroad@gmail.com';
			$home_owner_email .= '/'.$sender_email.'';


			$email_array = explode('/', $home_owner_email);
			$total_rows_email = count($email_array);

			for($x = 0; $x < $total_rows_email; $x++)
			{
				$receiver['email'] = $owner_email = $email_array[$x];

				// if($this->accounts_model->check_if_email_sent($rental_unit_id,$owner_email,$todays_year,$todays_month,0))
				// {

				// }
				// else
				// {
					$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);

					$insert_array = array('email'=>$receiver['email'],'client_name'=>$home_owner_name,'email_body'=>$message['text'],'type_of_account'=>0,'email_attachment'=>$title,'date_created'=>date('Y-m-d'),'rental_unit_id'=>$rental_unit_id, 'invoice_year'=>$_year, 'invoice_month'=>$_month, 'email_sent'=>1);
					$this->db->insert('email',$insert_array);
				// }

			}
			
		}

		// send SMS to the person
		// $home_owner_phone_number = 704808007;//724330022;
		if(!empty($home_owner_phone_number))
		{
			$owner_array = explode(' ', $home_owner_email);
			$first = $owner_array[0];
			$second = $owner_array[0];

			$name = $first.' '.$second;
			$name = ucfirst($name);
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			// $home_owner_phone_number = 716548104;//724330022;


			$phone_array = explode('/', $home_owner_phone_number);
			$total_rows = count($phone_array);

			for($r = 0; $r < $total_rows; $r++)
			{
				$home_owner_phone = $phone_array[$r];

				// var_dump($tenant_phone_number); die();
				// if($this->accounts_model->check_if_sms_sent($rental_unit_id,$home_owner_phone,$todays_year,$todays_month,0))
				// {
				// }
				// else
				// {
					$message = 'Dear '.$home_owner_name.', Your current service charge dues for '.$rental.' as at '.$date.' is KES. '.number_format($total_bill).'';
					// $message = 'Dear '.$home_owner_name.', Contribution towards debt repayment for '.$rental.'  is KES. '.number_format(50000).'';
					$this->accounts_model->sms($home_owner_phone,$message,$home_owner_name);

					// save the message sent out 
					$insert_array = array('phone_number'=>$home_owner_phone,'client_name'=>$home_owner_name,'type_of_account'=>0,'message'=>$message,'date_created'=>date('Y-m-d'),'rental_unit_id'=>$rental_unit_id, 'invoice_year'=>$invoice_year, 'invoice_month'=>$invoice_month, 'sms_sent'=>1);
					// $this->db->insert('sms',$insert_array);
				// }
				
			 }
			//
			// save the message sent out
			
		}

	}


	/*
	*
	*	Default action is to show all the tenants
	*
	*/
	public function owners() 
	{
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND  home_owner_unit.home_owner_unit_status = 1';
		$home_owner_property_search = $this->session->userdata('home_owner_property_search');
		// var_dump($home_owner_property_search); die();
		if(!empty($home_owner_property_search))
		{
			$where .= $home_owner_property_search;	
			
		}
		$table = 'rental_unit,property,home_owners,home_owner_unit';	
		
		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'cash-office/owners';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_unit_owners($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		
		// $this->accounts_model->update_invoices();
		

		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Unit Owners';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/unit_owners', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function owners_old() 
	{
		$where = 'property.property_id = rental_unit.property_id AND rental_unit_id > 0';
		$rental_unit_search = $this->session->userdata('all_rental_unit_search');
		
		if(!empty($rental_unit_search))
		{
			$where .= $rental_unit_search;	
			
		}
		$table = 'rental_unit,property';	
		
		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'cash-office/owners';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_unit_owners($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		
		// $this->accounts_model->update_invoices();
		

		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Unit Owners';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/unit_owners', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function send_owners($home_owner_id,$rental_unit_id)
	{
		$latest_month = $this->accounts_model->get_client_latest_invoice($home_owner_id,$rental_unit_id);
		if(!empty($latest_month))
		{
		   $latest_month_explode = explode('-', $latest_month);
		   $month = $latest_month_explode[1];
		   $year = $latest_month_explode[0];

			$this->send_invoices_home_owners($home_owner_id,$rental_unit_id,$month,$year,$latest_month);

			$this->session->set_userdata('success_message', 'Notification have been successfully sent');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Nothing to send as notification');
		}

		redirect('cash-office/owners');
	}
	public function owners_payments($home_owner_id,$rental_unit_id)
	{


		$v_data = array('rental_unit_id'=>$rental_unit_id,'home_owner_id'=>$home_owner_id);
		$v_data['title'] = 'Owner payments';
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		// get lease payments for this year
		$v_data['rental_payments'] = $this->accounts_model->get_lease_payments_owners($rental_unit_id);
		$v_data['lease_pardons'] = $this->accounts_model->get_lease_pardons_owner($rental_unit_id);

		$data['content'] = $this->load->view('cash_office/owner_payments', $v_data, true);
		
		$data['title'] = 'Tenant payments';
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function update_changes()
	{
		$month = date('m');
		$year = date('Y');
		
		$this->db->select("*");
		$this->db->where('invoice_month = "'.$month.'" AND invoice_year= "'.$year.'" AND invoice_type = 2');
		$query = $this->db->get('invoice');
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_id = $key->invoice_id;
				$lease_id = $key->lease_id;
				$invoice_amount = $key->invoice_amount;
				$invoice_month = $key->invoice_month;
				$invoice_year = $key->invoice_year;

				$current_invoice_amount = 1100;
				
				$array = array('invoice_amount' => $current_invoice_amount);

				$this->db->where('invoice_id',$invoice_id);
				$this->db->update('invoice',$array);
			}
		}

		redirect('tenants-accounts');
	}

	public function print_other_invoices()
	{
		$this->db->select('*');
		// $this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id > 2016 AND rental_unit.rental_unit_id < 2026 ');
		$this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id = 2047');
		$query = $this->db->get('rental_unit,property');
		$data['query'] = $query;
		$data['contacts'] = $this->site_model->get_contacts();

		foreach ($query->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;


			$becam_email = 'preet.sen@hotmail.com';
			$becam_phone = 715770467;
			
		}
		
		$contacts = $data['contacts'];

		// $this->load->view('cash_office/other_water_charge_invoice', $data);
		$this->load->view('cash_office/service_charge_invoice', $data);
	}
	public function send_other_invoices()
	{
		$this->db->select('*');
		// $this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id > 2016 AND rental_unit.rental_unit_id < 2026 ');
		// $this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id = 2038 ');
		// $this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id > 2040 AND rental_unit.rental_unit_id < 2043');
		$this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id = 2047');
		// AND  rental_unit.rental_unit_id < 2039 
		$query = $this->db->get('rental_unit,property');
		$data['query'] = $query;
		$data['contacts'] = $this->site_model->get_contacts();
		$total_bill = 0;
		foreach ($query->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$message_prefix = $leases_row->message_prefix;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$home_owner_email = $leases_row->home_owner_email;
			$invoice_month = date('m');
			$invoice_year = date('Y');
			$lease_invoice = $this->accounts_model->get_invoices_month_home_owners($rental_unit_id,$invoice_month,$invoice_year);
			if($lease_invoice->num_rows() > 0)
			{
				
				$service_charge = 0;
				$service_bf = 0;
				$penalty_charge =0;
				$penalty_bf =0;

				foreach ($lease_invoice->result() as $key_invoice) {
					# code...
					$invoice_date = $key_invoice->invoice_date;
					$invoice_id = $key_invoice->invoice_id;
					$invoice_type = $key_invoice->invoice_type;

					
					$invoice_date_date = date('jS F Y',strtotime($invoice_date));
					

					if($invoice_type == 4)
					{
						// service charge
						$service_charge = $key_invoice->invoice_amount;
						$service_bf = $key_invoice->arrears_bf;
					}


					if($invoice_type == 5)
					{
						// penalty

						$penalty_charge = $key_invoice->invoice_amount;
						$penalty_bf = $key_invoice->arrears_bf;
					}
				}
			}


			$total_bill = $total_bill + $service_charge + $penalty_charge;
			
			// $becam_email = 'marttkip@gmail.com';//'oyugierick3@gmail.com';
			
			
		}
		
		$contacts = $data['contacts'];

		$html = $this->load->view('cash_office/service_charge_invoice', $data, TRUE);

		$this->load->library('mpdf');
		// var_dump($html);die();
		$invoice_month_date = date('Y-m-d');
		$invoice_month = date('M',strtotime($invoice_month_date));
		$invoice_year = date('Y',strtotime($invoice_month_date));
		$title = 'Invoice-for-'.$invoice_month.'-'.$invoice_year.'.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;
		// echo $invoice;die();
		
		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}
		if(!empty($home_owner_email))
		{
			$message['subject'] = $invoice_month.' '.$invoice_year.' Invoice';
			$message['text'] = ' <p>Dear '.$home_owner_name.',<br>
									Please find attached invoice for Q4 2016
									</p>
								';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email']; 
			
			$button = '';
			$sender['email'] = $contacts['email']; 
			$sender['name'] = $contacts['company_name'];
			$receiver['email'] = $home_owner_email;
			$receiver['name'] = $home_owner_name;
			$payslip = $title;
			
			$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
		}

		// send SMS to the person
		if(!empty($home_owner_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			$message = 'Dear '.$home_owner_name.', Your current service charge dues for Q4 '.$invoice_year.' is KES. '.number_format($total_bill).'';
			$this->accounts_model->sms($home_owner_phone_number,$message,$home_owner_name);
			
		}
	}

	public function update_payment_item($import_payment_id)
	{

		if($this->accounts_model->update_payment_item($import_payment_id))
		{
			$this->session->set_userdata('success_message', 'Successfully corrected the payment detail');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}
		redirect('import/payments');
	}
	public function delete_payment_item($import_payment_id)
	{
		$update_array = array('import_payment_upload_deleted'=>1);
		$this->db->where('import_payment_id',$import_payment_id);
		if($this->db->update('import_payment_upload',$update_array))
		{
			$this->session->set_userdata('success_message', 'Successfully corrected the record');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}

		redirect('import/payments');
	}

	

	public function tenants_invoices()
	{	

		$where = 'tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id AND invoice.lease_id = leases.lease_id AND invoice.invoice_status = 3';
		$table = 'tenants,tenant_unit,rental_unit,leases,property,invoice';		


		$accounts_search = $this->session->userdata('all_accounts_search');
		
		if(!empty($accounts_search))
		{
			$where .= $accounts_search;	
			
		}
		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'invoices-&-payments/tenants-invoices';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_unapproved_invoices($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		
		// $this->accounts_model->update_invoices();
		

		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'Tenants Invoices ';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('approval/tenants_invoices', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function tenants_payments()
	{
		$where = 'tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id AND payments.lease_id = leases.lease_id AND payments.payment_status = 2 AND payments.payment_id = payment_item.payment_id ' ;
		$table = 'tenants,tenant_unit,rental_unit,leases,property,payments,payment_item';		


		// $accounts_search = $this->session->userdata('all_accounts_search');
		
		// if(!empty($accounts_search))
		// {
		// 	$where .= $accounts_search;	
			
		// }
		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'invoices-&-payments/tenants-payments';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_unapproved_payments($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');
		// var_dump($where); die();
		$properties = $this->property_model->get_active_property();
		
		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'Tenants Payments ';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('approval/tenants_payments', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function update_payment($payment_id,$payment_item_id)
	{

	}

	public function update_payments_receipt()
	{
		$this->db->where('payment_id > 0');
		$query = $this->db->get('payments');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$payment_id = $key->payment_id;

				$receipt_number = $this->accounts_model->create_receipt_number();
				// var_dump($receipt_number);
				$array = array('receipt_number'=>$receipt_number);
				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments',$array);
			}
		}
	}

	public function update_owners_payments_receipt()
	{
		$this->db->where('payment_id > 0');
		$query = $this->db->get('home_owners_payments');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$payment_id = $key->payment_id;

				$receipt_number = $this->accounts_model->create_owners_receipt_number();
			
				$array = array('receipt_number'=>$receipt_number);
				$this->db->where('payment_id',$payment_id);
				$this->db->update('home_owners_payments',$array);
			}
		}
	}
	public function lease_invoices($lease_id)
	{



		$where = 'invoice_status = 1 AND lease_id = '.$lease_id ;
		$table = 'invoice';		


		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'lease-invoices/'.$lease_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_lease_invoices($table, $where, $config["per_page"], $page, $order='invoice.invoice_month', $order_method='DESC');
		// var_dump($where); die();
		$properties = $this->property_model->get_active_property();
		
		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'Lease Invoices ';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;
		$data['content'] = $this->load->view('edits/lease_invoices', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function lease_payments($lease_id)
	{
		$where = 'payment_status = 1 AND lease_id = '.$lease_id ;
		$table = 'payments';		


		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'lease-payments/'.$lease_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_lease_payments($table, $where, $config["per_page"], $page, $order='payments.month,payments.year', $order_method='DESC');
		// var_dump($where); die();
		$properties = $this->property_model->get_active_property();
		
		$rs8 = $properties->result();

		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;


		$v_data['property_list'] = $property_list;
		$data['title'] = 'Lease Payments ';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;
		$data['content'] = $this->load->view('edits/lease_payments', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function update_account_invoice($lease_id,$invoice_type_id,$invoice_month,$invoice_year,$invoice_id)
	{
		if($invoice_type_id == 2)
		{
			// water charge
			$current_reading = $this->input->post('water_curr_reading');
			$previous_reading = $this->input->post('water_prev_reading');

			// get the water invoice id
			$record_id = $this->accounts_model->get_water_record_id($invoice_id);

			//  update the balance on water management

			if(!empty($record_id))
			{
				$rate = 110;
				if($this->accounts_model->update_water_management($record_id,$rate))
				{

					// update the invoice

					$consumption = $current_reading - $previous_reading;

					$invoice_amount = $consumption * $rate;

					$update_array = array('invoice_amount'=>$invoice_amount);
					$this->db->where('invoice_id',$invoice_id);
					if($this->db->update('invoice',$update_array))
					{
						$this->session->set_userdata('success_message', 'Successfully updated water charge');
					}
					else
					{
						$this->session->set_userdata('error_message', 'Please try again to update the record');
					}
				}
				else
				{
					$this->session->set_userdata('error_message', 'Please try again to update the record');
				}
			}


		}
		if($invoice_type_id == 4 || $invoice_type_id == 8 || $invoice_type_id == 7 || $invoice_type_id == 9 || $invoice_type_id == 5)
		{
			// service charge

			$invoice_amount = $this->input->post('amount');
			$update_array = array('invoice_amount'=>$invoice_amount);
			$this->db->where('invoice_id',$invoice_id);
			if($this->db->update('invoice',$update_array))
			{
				$this->session->set_userdata('success_message', 'Successfully updated service charge');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Please try again to update the record');
			}
		}
		if($invoice_type_id == 10)
		{
			$bought_rate = 0.5;
			$invoice_amount = $this->input->post('amount');

			$invoice_amount = $invoice_amount * 0.5;
			// var_dump($invoice_amount); die();
			$update_array = array('invoice_amount'=>$invoice_amount);
			$this->db->where('invoice_id',$invoice_id);
			if($this->db->update('invoice',$update_array))
			{
				$this->session->set_userdata('success_message', 'Successfully updated service charge');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Please try again to update the record');
			}
		}

		redirect('lease-invoices/'.$lease_id);
	}

	public function update_account_payment($lease_id,$payment_idd,$property_id)
	{

		$total_amount = $this->input->post('amount_paid_total');
		$payment_date_from = $this->input->post('payment_date_from');


		$property_invoices = $this->accounts_model->get_property_invoice_types($property_id,1);

		if($property_invoices->num_rows() > 0)
		{
			// var_dump($property_invoices->result()); die();
			foreach ($property_invoices->result() as $key_types) {
				# code...
				$property_invoice_type_id = $key_types->invoice_type_id;

				// update payment item
				$payments_list = $this->accounts_model->get_payments_month_payment($payment_idd,$property_invoice_type_id);
				if($payments_list->num_rows() > 0)
				{
					
					foreach ($payments_list->result() as $key_invoice) {
						$payment_item_id = $key_invoice->payment_item_id;

						$amount_paid = $this->input->post('amount_paid'.$payment_item_id);


						// update value
						$update_array = array('amount_paid'=>$amount_paid);
						$this->db->where('payment_item_id',$payment_item_id);
						$this->db->update('payment_item',$update_array);
						
					}

				}
				else
				{

					// insert for the other item
					$amount_paid = $this->input->post('insert_amount'.$property_invoice_type_id);


					$insert_array = array('amount_paid'=>$amount_paid,'payment_id'=>$payment_idd,'invoice_type_id'=>$property_invoice_type_id,'lease_id'=>$lease_id,'payment_item_created'=>date('Y-m-d'));
					
					$this->db->insert('payment_item',$insert_array);
					
				}
			}
			$this->session->set_userdata('success_message', 'Successfully updated items');

		}
		else
		{
			$this->session->set_userdata('error_message', 'Could not update item');
		}

		redirect('lease-payments/'.$lease_id);
	}


	public function update_owner_account_payment($rental_unit_id,$payment_idd,$property_id)
	{

		$total_amount = $this->input->post('amount_paid_total');
		$payment_date_from = $this->input->post('payment_date_from');


		$property_invoices = $this->accounts_model->get_property_invoice_types($property_id,0);

		if($property_invoices->num_rows() > 0)
		{
			// var_dump($property_invoices->result()); die();
			foreach ($property_invoices->result() as $key_types) {
				# code...
				$property_invoice_type_id = $key_types->invoice_type_id;

				// update payment item
				$payments_list = $this->accounts_model->get_owner_payments_month_payment($payment_idd,$property_invoice_type_id);
				if($payments_list->num_rows() > 0)
				{
					
					foreach ($payments_list->result() as $key_invoice) {
						$payment_item_id = $key_invoice->payment_item_id;

						$amount_paid = $this->input->post('amount_paid'.$payment_item_id);


						// update value
						$update_array = array('amount_paid'=>$amount_paid);
						$this->db->where('payment_item_id',$payment_item_id);
						$this->db->update('home_owner_payment_item',$update_array);
						
					}

				}
				else
				{

					// insert for the other item
					$amount_paid = $this->input->post('insert_amount'.$property_invoice_type_id);


					$insert_array = array('amount_paid'=>$amount_paid,'payment_id'=>$payment_idd,'invoice_type_id'=>$property_invoice_type_id,'rental_unit_id'=>$rental_unit_id,'payment_item_created'=>date('Y-m-d'));
					
					$this->db->insert('home_owner_payment_item',$insert_array);
					
				}
			}
			$this->session->set_userdata('success_message', 'Successfully updated items');

		}
		else
		{
			$this->session->set_userdata('error_message', 'Could not update item');
		}

		redirect('rental-payments/'.$rental_unit_id);
	}

	public function statement_email_sent($_year,$_month,$tenant_unit_id,$statement_sent_status)
	{
		$this->db->select('*');
		$this->db->where('tenant_unit_id = '.$tenant_unit_id. ' AND statement_year = '.$_year.' AND statement_month = "'.$_month.'" AND statement_email_sent = '.$statement_sent_status);
		$statement_email_query_sent = $this->db->get('statement_email');
		if($statement_email_query_sent->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function check_statement_sms_sent($name,$_year,$_month)
	{
		$this->db->select('*');
		$this->db->where('statement_year = '.$_year.' AND statement_month = "'.$_month.'" AND messaging_tenant_name = "'.$name.'"');
		$sms_statement_query_sent = $this->db->get('messaging');
		if($sms_statement_query_sent->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function update_payment_detail($lease_id,$payment_id)
	{

		$amount_paid = $this->input->post('amount_paid_total'.$payment_id);
		$date_of_payment = $this->input->post('payment_date'.$payment_id);
		// var_dump($amount_paid); die();
		$array = array('amount_paid'=>$amount_paid,'payment_date'=>$date_of_payment);
		$this->db->where('payment_id',$payment_id);
		if($this->db->update('payments',$array))
		{
			$this->session->set_userdata('success_message', 'Successfully updated items');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please try again');
		}
		redirect('lease-payments/'.$lease_id);
	}
public function update_owner_payment_detail($rental_unit_id,$payment_id)
	{

		$amount_paid = $this->input->post('amount_paid_total'.$payment_id);
		$date_of_payment = $this->input->post('payment_date'.$payment_id);
		// var_dump($amount_paid); die();
		$array = array('amount_paid'=>$amount_paid,'payment_date'=>$date_of_payment);
		$this->db->where('payment_id',$payment_id);
		if($this->db->update('home_owners_payments',$array))
		{
			$this->session->set_userdata('success_message', 'Successfully updated items');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please try again');
		}
		redirect('rental-payments/'.$rental_unit_id);
	}

	public function cancel_payment($payment_id)
	{
		$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		$redirect_url = $this->input->post('redirect_url');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->accounts_model->cancel_payment($payment_id))
			{
				$this->session->set_userdata("success_message", "Payment action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		redirect($redirect_url);
	}
	public function cancel_owner_payment($payment_id)
	{
		$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		$redirect_url = $this->input->post('redirect_url');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->accounts_model->cancel_owner_payment($payment_id))
			{
				$this->session->set_userdata("success_message", "Payment action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		redirect($redirect_url);
	}

	public function update_payments_to_sept()
	{
		$this->accounts_model->update_payments_to_sept();
	}
	public function rental_payments($rental_unit_id)
	{
		$where = 'payment_status = 1 AND rental_unit_id = '.$rental_unit_id;
		$table = 'home_owners_payments';		


		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'rental-payments/'.$rental_unit_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_owner_payments($table, $where, $config["per_page"], $page, $order='home_owners_payments.month,home_owners_payments.year', $order_method='DESC');
		
		$properties = $this->property_model->get_active_property();
		
		$rs8 = $properties->result();

		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;


		$v_data['property_list'] = $property_list;
		$data['title'] = 'Home Owner Payments ';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['rental_unit_id'] = $rental_unit_id;
		$data['content'] = $this->load->view('edits/lease_owners_payment', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function cancel_pardon($pardon_id)
	{
		$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		$redirect_url = $this->input->post('redirect_url');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->accounts_model->cancel_pardon($pardon_id))
			{
				$this->session->set_userdata("success_message", "Pardon action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		redirect($redirect_url);
	}
	public function cancel_owner_pardon($pardon_id)
	{
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		$redirect_url = $this->input->post('redirect_url');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->accounts_model->cancel_owners_pardon($pardon_id))
			{
				$this->session->set_userdata("success_message", "Pardon action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		redirect($redirect_url);
	}
	public function update_invoice($tenant_unit_id,$lease_id,$rental_unit_id)
	{
		$this->form_validation->set_rules('current_reading'.$lease_id, 'current reading', 'trim|required|xss_clean');
		$this->form_validation->set_rules('previous_reading'.$lease_id, 'previous reading', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_charges'.$lease_id, 'Water Charge', 'trim|required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$current_reading = $this->input->post('current_reading'.$lease_id);
			$previous_reading = $this->input->post('previous_reading'.$lease_id);
			$water_charges = $this->input->post('water_charges'.$lease_id);

			if($current_reading < $previous_reading)
			{
				$this->session->set_userdata("error_message", "Sorry you the current reading is not accurate");
			}

			else
			{
				if($this->accounts_model->update_water_invoice($lease_id,$rental_unit_id))
				{

					$this->session->set_userdata("success_message", "You have successfully billed for water");
				}
				
				else
				{
					$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");

				}
			}
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		redirect('setup/water');
	}

	// Confirmed Payments
	public function activate_payment($payment_id, $lease_id) 
	{
		if($this->accounts_model->activate_payment($payment_id))
		{
			$this->session->set_userdata('success_message', 'Payment has been Confirmed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Payment could not be Confirmed');
		}
		
		redirect('accounts/payments/'.$lease_id.'/'.$lease_id);
	}
    
	/*
	*
	*	Deactivate an existing payment page
	*	@param int $payment_id
	*fsearch
	*/
	public function deactivate_payment($payment_id, $lease_id) 
	{
		if($this->accounts_model->deactivate_payment($payment_id))
		{
			$this->session->set_userdata('success_message', 'Payment has been Unconfirmed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Payment could not be Unconfirmed');
		}
		
		redirect('accounts/payments/'.$lease_id.'/'.$lease_id);
	}
}

?>