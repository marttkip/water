<?php
$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$property_id = $leases_row->property_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Deactivated</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}

		//create deactivated status display
		if($tenant_status == 0)
		{
			$status_tenant = '<span class="label label-default">Deactivated</span>';
		}
		//create activated status display
		else if($tenant_status == 1)
		{
			$status_tenant = '<span class="label label-success">Active</span>';
		}
	}




?>


<?php

$tenant_response = $this->accounts_model->get_rent_and_service_charge($lease_id);

$grand_rent_bill = $this->accounts_model->get_cummulated_balance($lease_id,1);
$grand_water_bill = $this->accounts_model->get_cummulated_balance($lease_id,2);
$grand_electricity_bill = $this->accounts_model->get_cummulated_balance($lease_id,3);
$grand_service_charge_bill = $this->accounts_model->get_cummulated_balance($lease_id,4);
$grand_penalty_bill = $this->accounts_model->get_cummulated_balance($lease_id,5);

//  interface  for making payments
// use the property id and also the type of the person
// var_dump($property_id); die();
$property_invoices = $this->accounts_model->get_property_invoice_types($lease_id,1);
$inputs = '';
if($property_invoices->num_rows() > 0)
{
	// var_dump($property_invoices->result()); die();
	foreach ($property_invoices->result() as $key_types) {
		# code...
		$property_invoice_type_id = $key_types->invoice_type_id;

		if($property_invoice_type_id == 1)
		{
			$inputs .='	
						<div class="form-group">
							<label class="col-md-4 control-label">Rent Amount: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="rent_amount" placeholder="'.$grand_rent_bill.'" value="'.$grand_rent_bill.'" autocomplete="off" >
							</div>
						</div>	
					   ';
		}
		else if($property_invoice_type_id == 2)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">W/C Amount: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="water_amount" placeholder="'.$grand_water_bill.'" autocomplete="off" >
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 3)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 4)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">S/C Amount: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="service_charge_amount" value="'.$grand_service_charge_bill.'" placeholder="'.$grand_service_charge_bill.'" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 5)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Penalty: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="penalty_fee" placeholder="'.$grand_penalty_bill.'" autocomplete="off" >
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 6)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 7)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Painting Charge: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="painting_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 8)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Sinking Funds: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="sinking_funds" placeholder="" autocomplete="off">
							</div>
						</div>
					';
		}
		else if($property_invoice_type_id == 9)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Insurance: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="insurance" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 12)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Fixed Charge: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="fixed_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 10)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Bought Water: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="bought_water" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 13)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Deposit: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="deposit_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 17)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Legal Fees: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="legal_fees" placeholder="" autocomplete="off">
							</div>
						</div>';
		}

		else if($property_invoice_type_id == 11)
		{
			$inputs .='';
		}
	}
}




?>
<div class="row">
	<div class="col-md-12 pull-right">
		<a href="<?php echo site_url();?>accounts/tenants-invoices" class="btn btn-sm btn-warning pull-right"  ><i class="fa fa-arrow-left"></i> Back to leases</a> 
	</div>
</div>
 <section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title"><?php echo $title;?> </h2>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				  echo '<div class="alert alert-danger">'.$error.'</div>';
				  $this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
				  echo '<div class="alert alert-success">'.$success.'</div>';
				  $this->session->unset_userdata('success_message');
				}
			 ?>
			</div>
		</div>
		
		
		
        <!--<div class="row">
        	<div class="col-sm-3 col-sm-offset-3">
            	<a href="<?php echo site_url().'doctor/print_prescription'.$tenant_unit_id;?>" class="btn btn-warning">Print prescription</a>
            </div>
            
        	<div class="col-sm-3">
            	<a href="<?php echo site_url().'doctor/print_lab_tests'.$tenant_unit_id;?>" class="btn btn-danger">Print lab tests</a>
            </div>
        </div>-->
        
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<section class="panel panel-featured panel-featured-info">
								<header class="panel-heading">
									
									<h2 class="panel-title">Tenant's Details</h2>
								</header>
								<div class="panel-body">
									<table class="table table-hover table-bordered col-md-12">
										<thead>
											<tr>
												<th>Title</th>
												<th>Detail</th>
											</tr>
										</thead>
									  	<tbody>
									  		<tr><td><span>Tenant Name :</span></td><td><?php echo $tenant_name;?></td></tr>
									  		<tr><td><span>Tenant Phone :</span></td><td><?php echo $tenant_phone_number;?></td></tr>
									  		<tr><td><span>Tenant National Id :</span></td><td><?php echo $tenant_national_id;?></td></tr>
									  		<tr><td><span>Property Name :</span></td><td><?php echo $property_name.' '.$rental_unit_name;?></td></tr>
									  	
									  		<tr><td><span>Account Status :</span></td><td><?php echo $status_tenant;?></td></tr>
									  		
									  	</tbody>
									</table>
								</div>
							</section>
						</div>
					</div>
				</div>
				<!-- END OF THE SPAN 7 -->
				 <div class="col-md-3">
					<div class="row">
						<div class="col-md-12">
							<section class="panel panel-featured panel-featured-info">
								<header class="panel-heading">
									
									<h2 class="panel-title">Lease Details</h2>
								</header>
								<div class="panel-body">
                                	<div class="row">
                                    	<div class="col-md-12">
                                         </div>
                                    </div>
									<table class="table table-hover table-bordered col-md-12">
										<thead>
											<tr>
												<th>Title</th>
												<th>Detail</th>
											</tr>
										</thead>
									  	<tbody>
									  		<tr><td><span>Lease Number :</span></td><td><?php echo $lease_number;?></td></tr>
									  		<tr><td><span>Lease Start date :</span></td><td><?php echo $lease_start_date;?></td></tr>
									  		<tr><td><span>Lease Expiry date :</span></td><td><?php echo $expiry_date;?></td></tr>
									  										  		
									  	</tbody>
									</table>

									 <div class="col-md-12">
										<div class="form-actions center-align">
								            <a href="<?php echo site_url();?>lease-detail/<?php echo $lease_id?>" class="submit btn btn-warning btn-sm">
								                Lease Detail 
								            </a>
								        </div>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<!-- START OF THE SPAN 5 -->
				<div class="col-md-5">
					<div class="row">
						<div class="col-md-12">
							<section class="panel panel-featured panel-featured-info">
								
                                
								<div class="panel-body">

									<div class="tabs">
										<ul class="nav nav-tabs nav-justified">
											<li class="active">
												<a class="text-center" data-toggle="tab" href="#general"><i class="fa fa-user"></i> Payments</a>
											</li>
											<li>
												<a class="text-center" data-toggle="tab" href="#account"><i class="fa fa-lock"></i> Pardons</a>
											</li>
									
										</ul>
										<div class="tab-content">
											<div class="tab-pane active" id="general">
												<?php echo form_open("accounts/make_payments/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
													<input type="hidden" name="type_of_account" value="1">
													<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
													<div class="form-group" id="payment_method">
														<label class="col-md-4 control-label">Payment Method: </label>
														  
														<div class="col-md-7">
															<select class="form-control" name="payment_method" onchange="check_payment_type(this.value)" required>
																<option value="0">Select a payment method</option>
			                                                	<?php
																  $method_rs = $this->accounts_model->get_payment_methods();
																	
																	foreach($method_rs->result() as $res)
																	{
																	  $payment_method_id = $res->payment_method_id;
																	  $payment_method = $res->payment_method;
																	  
																		echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
																	  
																	}
																  
															  ?>
															</select>
														  </div>
													</div>
													<div id="mpesa_div" class="form-group" style="display:none;" >
														<label class="col-md-4 control-label"> Mpesa TX Code: </label>

														<div class="col-md-7">
															<input type="text" class="form-control" name="mpesa_code" placeholder="">
														</div>
													</div>
												  
												  
													<div id="cheque_div" class="form-group" style="display:none;" >
														<label class="col-md-4 control-label"> Bank Name: </label>
													  
														<div class="col-md-7">
															<input type="text" class="form-control" name="bank_name" placeholder="Barclays">
														</div>
													</div>
													

													<div class="form-group">
														<label class="col-md-4 control-label">Total Amount: </label>
													  
														<div class="col-md-7">
															<input type="number" class="form-control" name="amount_paid" placeholder=""  autocomplete="off" required>
														</div>
													</div>

													<!-- where you put all the item that are to be paid for in this property -->
													<?php echo $inputs;?>
													<!-- end of the items to be paid for in this property -->
													<div class="form-group">
														<label class="col-md-4 control-label">Paid in By: </label>
													  
														<div class="col-md-7">
															<input type="text" class="form-control" name="paid_by" placeholder="<?php echo $tenant_name;?>" value="<?php echo $tenant_name;?>" autocomplete="off" >
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-4 control-label">Payment Date: </label>
													  
														<div class="col-md-7">
															 <div class="input-group">
								                                <span class="input-group-addon">
								                                    <i class="fa fa-calendar"></i>
								                                </span>
								                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Payment Date" required>
								                            </div>
														</div>
													</div>
													
													<div class="center-align">
														<button class="btn btn-info btn-sm" type="submit" onclick="return confirm('Are you sure you want to proceed recording the payment ?')">Add Payment Information</button>
													</div>
													<?php echo form_close();?>
											</div>
											<div class="tab-pane" id="account">
											<?php echo form_open("accounts/create_pardon/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
													<input type="hidden" name="type_of_account" value="1">
													<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
													<div class="form-group">
														<label class="col-md-4 control-label">Pardon Amount: </label>
													  
														<div class="col-md-7">
															<input type="number" class="form-control" name="pardon_amount" placeholder="" autocomplete="off" required>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-4 control-label">Pardon Reason: </label>
													  
														<div class="col-md-7">
															<textarea class="form-control" name="pardon_reason" autocomplete="off"></textarea>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-4 control-label">Pardon Date: </label>
													  
														<div class="col-md-7">
															 <div class="input-group">
								                                <span class="input-group-addon">
								                                    <i class="fa fa-calendar"></i>
								                                </span>
								                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="pardon_date" placeholder="Pardon Date" required>
								                            </div>
														</div>
													</div>
													
													<div class="center-align">
														<button class="btn btn-info btn-sm" type="submit">Add Pardon Information</button>
													</div>
													<?php echo form_close();?>
											
											</div>
										
										</div>
									</div>

									
								</div>
							</section>
						</div>
					</div>

				</div>
				<!-- END OF THE SPAN 5 -->
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<section class="panel panel-featured panel-featured-info">
					<header class="panel-heading">
						
						<h2 class="panel-title">Payment Details <?php echo date('Y');?></h2>
						<!-- <a href="<?php echo site_url();?>lease-payments/<?php echo $lease_id;?>" target="_blank" class="btn btn-sm btn-warning pull-right"  style="margin-top:-25px;"> <i class="fa fa-pencil"></i> Tenant's Payments</a> -->

					</header>
					<div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-12">
                             </div>
                        </div>
						<table class="table table-hover table-bordered col-md-12">
							<thead>
								<tr>
									<th>#</th>
									<th>Payment Date</th>
									<th>Receipt Number</th>
									<th>Amount Paid</th>
									<th>Paid By</th>
									<th>Receipted Date</th>
									<th>Receipted By</th>
									<th>Confirmation Status</th>
									<th colspan="3">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($lease_payments->num_rows() > 0)
								{
									$y = 0;
									foreach ($lease_payments->result() as $key) {
										# code...
										$receipt_number = $key->receipt_number;
										$amount_paid = $key->amount_paid;
										$payment_id = $key->payment_id;
										$paid_by = $key->paid_by;
										$payment_date = $key->payment_date;
										$payment_created = $key->payment_created;
										$payment_created_by = $key->payment_created_by;
										$confirmation_status = $key->confirmation_status;

										if($confirmation_status == 0)
										{
											$confirmation_status_text = "Payment Confirmed";
											$button = '<a class="btn btn-danger" href="'.site_url().'accounts/deactivate_payments/'.$payment_id.'/'.$lease_id.'" onclick="return confirm(\'Do you want to Deactivate this payment Kes.'.$amount_paid.'?\');" title="Deactivate For Company Return'.$amount_paid.'"> Deactivate </a>';
			

										}
										else
										{
											$confirmation_status_text = "Payment Not Confirmed";
											$button = '<a class="btn btn-info" href="'.site_url().'accounts/activate_payments/'.$payment_id.'/'.$lease_id.'" onclick="return confirm(\'Do you want to Activate this payment Kes.'.$amount_paid.'?\');" title="Activate For Company Return'.$amount_paid.'"> Activate</a>';
			

										}
										$payment_explode = explode('-', $payment_date);

										$payment_date = date('jS M Y',strtotime($payment_date));
										$payment_created = date('jS M Y',strtotime($payment_created));
										$y++;
										$message = $this->site_model->create_web_name('Your payment of Ksh. '.number_format($amount_paid, 0).' has been received for Hse No. '.$rental_unit_name);
										
										?>
										<tr>
											<td><?php echo $y?></td>
											<td><?php echo $payment_date;?></td>
											<td><?php echo $receipt_number?></td>
											<td><?php echo number_format($amount_paid,2);?></td>
											<td><?php echo $paid_by;?></td>
											<td><?php echo $payment_created;?></td>
											<td><?php echo $payment_created_by;?></td>
											<td><?php echo $confirmation_status_text;?></td>
											<td><?php echo $button; ?></td>

										
											<td><a href="<?php echo site_url().'cash-office/print-receipt/'.$payment_id.'/'.$tenant_unit_id.'/'.$lease_id;?>" class="btn btn-sm btn-primary" target="_blank">Receipt</a></td>
                                            <td><a href="<?php echo site_url().'send-tenants-receipt/'.$payment_id.'/'.$tenant_unit_id.'/'.$lease_id;?>" class="btn btn-sm btn-success">Send SMS</a></td>
                                            <td>
                                          		<button type="button" class="btn btn-small btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $payment_id;?>"><i class="fa fa-times"></i></button>
                                            	
                                            	<div class="modal fade" id="refund_payment<?php echo $payment_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												    <div class="modal-dialog" role="document">
												        <div class="modal-content">
												            <div class="modal-header">
												            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												            	<h4 class="modal-title" id="myModalLabel">Cancel payment</h4>
												            </div>
												            <div class="modal-body">
												            	<?php echo form_open("accounts/cancel_payment/".$payment_id, array("class" => "form-horizontal"));?>

												            	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
												                <div class="form-group">
												                    <label class="col-md-4 control-label">Action: </label>
												                    
												                    <div class="col-md-8">
												                        <select class="form-control" name="cancel_action_id">
												                        	<option value="">-- Select action --</option>
												                            <?php
												                                if($cancel_actions->num_rows() > 0)
												                                {
												                                    foreach($cancel_actions->result() as $res)
												                                    {
												                                        $cancel_action_id = $res->cancel_action_id;
												                                        $cancel_action_name = $res->cancel_action_name;
												                                        
												                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
												                                    }
												                                }
												                            ?>
												                        </select>
												                    </div>
												                </div>
												                
												                <div class="form-group">
												                    <label class="col-md-4 control-label">Description: </label>
												                    
												                    <div class="col-md-8">
												                        <textarea class="form-control" name="cancel_description"></textarea>
												                    </div>
												                </div>
												                
												                <div class="row">
												                	<div class="col-md-8 col-md-offset-4">
												                    	<div class="center-align">
												                        	<button type="submit" class="btn btn-primary">Save action</button>
												                        </div>
												                    </div>
												                </div>
												                <?php echo form_close();?>
												            </div>
												            <div class="modal-footer">
												                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												            </div>
												        </div>
												    </div>
												</div>
                                            </td>
										</tr>
										<?php

									}
								}
								?>
								
							</tbody>
						</table>
					</div>
				</section>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<section class="panel panel-featured panel-featured-info">
					<header class="panel-heading">
						
						<h2 class="panel-title"> Account Pardons <?php echo date('Y');?></h2>
						

					</header>
					<div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-12">
                             </div>
                        </div>
						<table class="table table-hover table-bordered col-md-12">
							<thead>
								<tr>
									<th>#</th>
									<th>Document number</th>
									<th>Pardoned Date</th>
									<th>Pardoned Amount </th>
									<th>Pardoned By</th>
									<th>Reason</th>
									<th colspan="1">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($lease_pardons->num_rows() > 0)
								{
									$y = 0;
									foreach ($lease_pardons->result() as $key_pardons) {
										# code...
										$document_number = $key_pardons->document_number;
										$pardon_amount = $key_pardons->pardon_amount;
										$pardon_id = $key_pardons->pardon_id;
										$created_by = $key_pardons->created_by;
										$payment_date = $key_pardons->payment_date;
										$pardon_date = $key_pardons->pardon_date;
										$pardon_reason = $key_pardons->pardon_reason;
										$payment_explode = explode('-', $payment_date);

										$pardon_date = date('jS M Y',strtotime($pardon_date));
										$y++;
										$message = $this->site_model->create_web_name('Your payment of Ksh. '.number_format($pardon_amount, 0).' has been received for Hse No. '.$rental_unit_name);
										
										?>
										<tr>
											<td><?php echo $y?></td>
											<td><?php echo $document_number?></td>
											<td><?php echo $pardon_date;?></td>
											<td><?php echo number_format($pardon_amount,2);?></td>
											<td><?php echo $created_by;?></td>
											<td><?php echo $pardon_reason;?></td>
											
                                            <td>
                                          		<button type="button" class="btn btn-small btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $pardon_id;?>"><i class="fa fa-times"></i></button>
                                            	
                                            	<div class="modal fade" id="refund_payment<?php echo $pardon_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												    <div class="modal-dialog" role="document">
												        <div class="modal-content">
												            <div class="modal-header">
												            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												            	<h4 class="modal-title" id="myModalLabel">Cancel Pardons</h4>
												            </div>
												            <div class="modal-body">
												            	<?php echo form_open("accounts/cancel_pardon/".$pardon_id, array("class" => "form-horizontal"));?>

												            	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
												                <div class="form-group">
												                    <label class="col-md-4 control-label">Action: </label>
												                    
												                    <div class="col-md-8">
												                        <select class="form-control" name="cancel_action_id">
												                        	<option value="">-- Select action --</option>
												                            <?php
												                                if($cancel_actions->num_rows() > 0)
												                                {
												                                    foreach($cancel_actions->result() as $res)
												                                    {
												                                        $cancel_action_id = $res->cancel_action_id;
												                                        $cancel_action_name = $res->cancel_action_name;
												                                        
												                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
												                                    }
												                                }
												                            ?>
												                        </select>
												                    </div>
												                </div>
												                
												                <div class="form-group">
												                    <label class="col-md-4 control-label">Description: </label>
												                    
												                    <div class="col-md-8">
												                        <textarea class="form-control" name="cancel_description"></textarea>
												                    </div>
												                </div>
												                
												                <div class="row">
												                	<div class="col-md-8 col-md-offset-4">
												                    	<div class="center-align">
												                        	<button type="submit" class="btn btn-primary">Save action</button>
												                        </div>
												                    </div>
												                </div>
												                <?php echo form_close();?>
												            </div>
												            <div class="modal-footer">
												                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												            </div>
												        </div>
												    </div>
												</div>
                                            </td>
										</tr>
										<?php

									}
								}
								?>
								
							</tbody>
						</table>
					</div>
				</section>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<section class="panel panel-featured panel-featured-info">
					<header class="panel-heading">
						
						<h2 class="panel-title">Payment Statement <?php echo date('Y');?>
							<!-- <a href="<?php echo site_url();?>lease-statement/<?php echo $lease_id;?>" target="_blank" class="btn btn-sm btn-success pull-right"  style="margin-top:-5px;">Statement</a> -->
							

						</h2>
						<a href="<?php echo site_url();?>lease-invoices/<?php echo $lease_id;?>" target="_blank" class="btn btn-sm btn-warning pull-right"  style="margin-top:-25px;"> <i class="fa fa-pencil"></i> Tenant's Invoices</a>

					</header>
					<div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-12">
                             </div>
                        </div>


						<table class="table table-hover table-bordered col-md-12">
							<thead>
								<tr>
									<th>Date</th>
									<th>Description</th>
									<th>Invoices</th>
									<th>Payments</th>
									<th>Arrears</th>
									<th></th>
								</tr>
							</thead>
						  	<tbody>
						  		<?php echo $tenant_response['result'];?>
						  	</tbody>
						</table>
					</div>
				</section>
			</div>
		</div> 
	
		<!-- END OF PADD -->
	</div>
</section>
  <!-- END OF ROW -->
<script type="text/javascript">
  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");
		
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }
</script>