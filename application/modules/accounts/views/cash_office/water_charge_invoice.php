<?php
	$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		// $units_name = $leases_row->units_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arrears_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;
		$message_prefix = $leases_row->message_prefix;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	}

	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
	// get when the lease was active

	// get all the invoiced months
	// var_dump($lease_invoice); die();
	
	if($lease_invoice->num_rows() > 0)
	{
		$electricity_charge = 0;
		$electricity_bf = 0;
		$service_charge = 0;
		$service_bf = 0;
		$water_charge = 0;
		$water_bf = 0;
		$penalty_charge =0;
		$penalty_bf =0;
		$borehole = 0;
		$prev_reading = 0;
		$current_reading =0;
		$elec_prev_reading = 0;
		$elec_current_reading =0;
		$water_bill_lamp = 0;
		$elect_bill_lamp = 0;

		foreach ($lease_invoice->result() as $key_invoice) {
			# code...
			$invoice_date = $key_invoice->invoice_date;
			$invoice_id = $key_invoice->invoice_id;
			$invoice_type = $key_invoice->invoice_type;

			if($invoice_type == 2 OR $invoice_type == 3)
			{


			// $water_bill_lamp = $this->accounts_model->get_total_bill($invoice_id,$invoice_type,$invoice_date);
			// $elect_bill_lamp = $this->accounts_model->get_total_bill($invoice_id,$invoice_type,$invoice_date);
				$water_bill_amount = $key_invoice->invoice_amount;
				$water_bill_bf = $key_invoice->arrears_bf;
			}
			$invoice_date_date = date('jS F Y',strtotime($invoice_date));
			if($invoice_type == 3)
			{
				// electricity
				$electricity_charge = $key_invoice->invoice_amount;
				$electricity_bf = $key_invoice->arrears_bf;

				$elec_readings = $this->accounts_model->get_water_readings($invoice_id);

				if($elec_readings->num_rows() > 0)
				{
					foreach ($elec_readings->result() as $key_elec) {
						# code...
						$elec_prev_reading = $key_elec->prev_reading;
						$elec_current_reading = $key_elec->current_reading;
					}
				}
			}

			if($invoice_type == 4)
			{
				// service charge
				$service_charge = $key_invoice->invoice_amount;
				$service_bf = $key_invoice->arrears_bf;
			}

			if($invoice_type == 2)
			{
				// water
				$water_charge = $key_invoice->invoice_amount;
				$water_bf = $key_invoice->arrears_bf;

				$water_readings = $this->accounts_model->get_water_readings($invoice_id);

				if($water_readings->num_rows() > 0)
				{
					foreach ($water_readings->result() as $key) {
						# code...
						$prev_reading = $key->prev_reading;
						$current_reading = $key->current_reading;
					}
				}
			}

			if($invoice_type == 5)
			{
				// penalty

				$penalty_charge = $key_invoice->invoice_amount;
				$penalty_bf = $key_invoice->arrears_bf;
			}
		}
	}

	$total_service_charge = $water_bf + $water_charge + $borehole;	

	$total_billing_water = 1100;//115 * ($current_reading - $prev_reading);

	$total_billing_electricity = 40 * ($elec_current_reading - $elec_prev_reading);

	$total_billing_borehole = 26 * ($current_reading - $prev_reading);

	// $total_water_billing = $total_billing_water + $total_billing_borehole + $total_billing_electricity;

	$total_bill = number_format($total_billing_water + $penalty_charge);

	$fixed_charge = 250;


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-9  col-xs-offset-1">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;">
		        
		            <div class="col-md-5 pull-left">
		                <h2 class="panel-title">Personal Information</h2>
		                <table class="table table-hover">
		                    <tbody>
		                        <tr><td><span>Name :</span></td><td><?php echo $tenant_name;?></td></tr>
		                        <tr><td><span>Phone :</span></td><td><?php echo $tenant_phone_number;?></td></tr>
		                        <tr><td><span>Property :</span></td><td><?php echo $property_name;?></td></tr>
		                        <tr><td><span>Hse No. :</span></td><td><?php echo $rental_unit_name;?></td></tr>
		                        
		                    </tbody>
		                </table>
		            </div>
		             <div class="col-md-5 pull-right">
		                <h2 class="panel-title">Invoice</h2>
		                <table class="table table-hover">
		                    <tbody>
		                    	<!-- <tr><td><span>Invoice# :</span></td><td><?php echo date('jS M Y',strtotime(date('Y-m-d')));?></td></tr> -->
		                        <tr><td><span>Date :</span></td><td><?php echo $invoice_date_date;?></td></tr>
		                        
		                    </tbody>
		                </table>
		            </div>
		        </div>
		        <div class="row">
			        <div class="col-md-12">
			        	 <div class="row receipt_bottom_border">
				        	<div class="col-xs-12">
				            	<strong>A. WATER DUES</strong>
				            </div>
				        </div>
		               	  <table class="table table-hover" >
				            <tbody>
				              
				               <tr>
				               	<td >Balance B/F</td><td class="pull-right">Kes. <?php echo number_format($water_bill_bf);?> </td>
				               </tr>
				               <tr >
				               	<td >Water Dues  </td><td class="pull-right" style="border-top:2px #000 solid !important;">Kes. <?php echo number_format($water_bill_amount);?></td>
				               </tr>
				            </tbody>
				        </table>
			        </div>
			   	</div>
			   	<div class="row">
			        <div class="col-md-12">
			        	<div class="row receipt_bottom_border">
				        	<div class="col-xs-12">
				            	<strong>B. Penalties</strong>
				            </div>
				        </div>
				       
			        	<table class="table table-hover">
				           
				            <tbody>
				               	<tr>
				               		<td class="col-md-6">Penalty Dues</td><td class="pull-right">Kes. <?php echo number_format($penalty_charge);?></td>
				               	</tr>
				            </tbody>
				        </table>
			        </div>
			     </div>
			    <div class="row">
			    	<div class="col-md-12">
				    	<table class="table table-hover">
				            <tbody>
				               <tr>
				               	<td ><strong> Total Dues A+B </strong> </td><td class="pull-right" style="border-top:2px #000 solid !important;border-bottom:2px #000 solid !important;"><strong> Kes. <?php echo number_format($water_bill_amount);?></strong> </td>
				               </tr>
				            </tbody>
				        </table>
				    </div>
			    </div>
		       
		       
		       <?php

		       	$bills = $this->accounts_model->get_statement_items($lease_id);
				// var_dump($bills); die();
				$payment_result = '';
				if($bills->num_rows() > 0)
				{
					$x =0;
					$total_invoice = 0;
					$total_paid = 0;
					foreach ($bills->result() as $key_result) {
						# code...
						$ptype = $key_result->PTYPE;
						$invoice_type_id = $key_result->invoice_type_id;
						
						if($invoice_type_id > 0)
						{
							$invoice_type_name = $this->accounts_model->get_invoice_type_name($invoice_type_id);
							$description = 'Invoice for '.$invoice_type_name.' Due';
						}
						else{
							# code...
							$description = 'Payment';
						}
						
						
						$date = $key_result->date;
						$invoice_amount = '';
						$paid_amount = '';
						$invoiced = 0;
						$paid = 0;
						if($ptype == 'INVOICE')
						{
							$amount = $key_result->amount;
							$invoice_amount = $amount;
						}
						else
						{
							$amount = $key_result->amount;
							$paid_amount = $amount;
						}

						if($invoice_amount == '')
						{
							$invoice = 0;
						}
						else
						{
							$invoiced = $invoice_amount;
						}
						if($paid_amount == '')
						{
							$paid = 0;
						}
						else
						{
							$paid = $paid_amount;
						}
						$total_invoice = $total_invoice + $invoiced;
						$total_paid = $total_paid + $paid;
						$balance = $total_invoice - $total_paid;
						$x++;
						$payment_result .=
										  '
										  	<tr>
										  		<td>'.$x.'</td>
										  		<td>'.$date.'</td>
										  		<td>'.$description.'</td>
										  		<td>'.$invoice_amount.'</td>
										  		<td>'.$paid_amount.'</td>
										  		<td>'.number_format($balance).'</td>
										  	</tr>
										  ';

					}

				}

		       ?>
		       <div class="row" style="border-top:1px #000 solid;">
		       		<div class="col-xs-6 pull-left">
		       			<strong > <?php echo $message_prefix;?>.</strong>
		            </div>
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>