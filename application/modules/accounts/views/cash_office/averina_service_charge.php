<?php
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$total_penalty =0;
$penalty_charge =0;
$query = $this->leases_model->get_lease_detail_owners($home_owner_id);
// $invoice_month_parent = $invoice_month;
// $invoice_year_parent = $invoice_year;

foreach ($query->result() as $leases_row)
{
	$rental_unit_id = $leases_row->rental_unit_id;
	$rental_unit_name = $leases_row->rental_unit_name;
	$property_name = $leases_row->property_name;
	$message_prefix = $leases_row->message_prefix;
	$home_owner_name = $leases_row->home_owner_name;
	$home_owner_phone_number = $leases_row->home_owner_phone_number;
	$home_owner_name = $leases_row->home_owner_name;
	$home_owner_email = $leases_row->home_owner_email;
	$home_owner_phone_number = $leases_row->home_owner_phone_number;
	$home_owner_email = $leases_row->home_owner_email;
	// $amount = 450;
	// $invoice_month = date('m');
	// $invoice_year = date('Y');
	// $total_amount = $total_amount + $amount;
	$lease_invoice = $this->accounts_model->get_invoices_month_home_owners($rental_unit_id,$invoice_month_parent,$invoice_year_parent);

	
	if($lease_invoice->num_rows() > 0)
	{
		
		$service_charge = 0;
		$service_bf = 0;
		$penalty_charge =0;
		$penalty_bf =0;
		foreach ($lease_invoice->result() as $key_invoice) {
			# code...
			$invoice_date = $key_invoice->invoice_date;
			$invoice_id = $key_invoice->invoice_id;
			$invoice_type = $key_invoice->invoice_type;
			$billing_schedule_quarter = $key_invoice->billing_schedule_quarter;

			
			$invoice_date_date = date('jS F Y',strtotime($invoice_date));
		
			// quater dates
			$quater_details = $this->accounts_model->get_quarter(0);
			$current_quater_start = date('Y-m-d',strtotime($quater_details['start']));
			$current_quater_end = date('Y-m-d',strtotime($quater_details['end']));
			// current quater 
			$quater_details_previous = $this->accounts_model->get_quarter(1);
			$previous_quater_start = date('Y-m-d',strtotime($quater_details_previous['start']));
			$previous_quater_end = date('Y-m-d',strtotime($quater_details_previous['end']));



			if($invoice_type == 4)
			{
				// service charge
				$service_charge = $key_invoice->invoice_amount;
				$month = date('m');
				$year = date('Y');
				// $service_payments = $this->accounts_model->get_total_owners_payments_before($rental_unit_id,$year,$month,4);
				// $service_invoices= $this->accounts_model->get_total_owners_invoices_before($rental_unit_id,$year,$month,4);
				// $service_arrears = $service_invoices - $service_payments;

				$starting_arreas = $arrears_amount =$this->accounts_model->get_all_owners_arrears($rental_unit_id);


				$prev_payments = $this->accounts_model->get_prev_all_owners_quater_payments($rental_unit_id,$previous_quater_start,$invoice_type);

				$prev_invoices = $this->accounts_model->get_prev_all_owners_quater_invoices($rental_unit_id,$previous_quater_start,$invoice_type);


				$prev_arrears = $prev_invoices - $prev_payments + $starting_arreas;




				// the previous quarter things 

				$service_payments = $this->accounts_model->get_prev_owners_quater_payments($rental_unit_id,$previous_quater_start,$previous_quater_end,$invoice_type);

				$service_invoices = $this->accounts_model->get_prev_owners_quater_invoices($rental_unit_id,$previous_quater_start,$previous_quater_end,$invoice_type);


				$previous_quater_arrears = $service_invoices - $service_payments;

				// end of previous quarter things



				// balance brought forward
				$service_arrears = $prev_arrears + $previous_quater_arrears ;
				// end of balance brought forward


				// current and forward 
				$current_service_payments = $this->accounts_model->get_curr_quater_owners_payment_amount($rental_unit_id,$current_quater_start,$current_quater_end,$invoice_type);

				$current_service_invoices = $this->accounts_model->get_curr_owners_quater_invoice_amount($rental_unit_id,$current_quater_start,$current_quater_end,$invoice_type);

				$service_arrears = $service_arrears - $current_service_payments;

				$current_service_amount  = $current_service_invoices ;





				// var_dump($current_service_payments); die();
				$total_service = $current_service_amount + $service_arrears;


				// var_dump($current_service_amount); die();


				$end_invoice_date = $invoice_year_parent.'-'.$invoice_month_parent.'-30';

				//  get the amount changed
				$invoice_amount = $this->accounts_model->get_invoice_brought_forward($rental_unit_id,$invoice_date,$invoice_type);
				$paid_amount = $this->accounts_model->get_paid_brought_forward($rental_unit_id,$invoice_date,$invoice_type);

				$total_brought_forward = $invoice_amount - $paid_amount;
				// end of amount changed
				// var_dump($paid_amount); die();

				// get current invoice
				$current_invoice =  $this->accounts_model->get_invoice_current_forward($rental_unit_id,$invoice_date,$invoice_type);
				$current_payment =  $this->accounts_model->get_paid_current_forward($rental_unit_id,$invoice_date,$invoice_type);
 
				// get current payment


				$service_bf = $key_invoice->arrears_bf;
				$items .= '
							 <div class="row">
					        	<div class="col-xs-2">
					        		'.$rental_unit_name.'
					        	</div>
					        	<div class="col-xs-2">
					        		'.number_format($total_brought_forward).'
					        	</div>
					        	<div class="col-xs-2">
					        		'.number_format($current_invoice) .'
					        	</div>
					        	<div class="col-xs-3 pull-right">
					        		'.number_format($total_service).'
					        	</div>
					        </div>
							';
			}


			if($invoice_type == 5)
			{
				// penalty

				$penalty_charge = $key_invoice->invoice_amount;
				$month = date('m');
				$year = date('Y');
				$penalty_payments = $this->accounts_model->get_total_owners_payments_before($rental_unit_id,$year,$month,4);
				$penalty_invoices= $this->accounts_model->get_total_owners_invoices_before($rental_unit_id,$year,$month,4);
				$penalty_arrears = $penalty_invoices - $penalty_payments;
				$total_penalty = $penalty_arrears + $penalty_charge; 

				$penalty_bf = $key_invoice->arrears_bf;
				$items .= '
							 <div class="row">
					        	<div class="col-xs-2">
					        		'.$rental_unit_name.'
					        	</div>
					        	<div class="col-xs-2">
					        		'.number_format($penalty_invoices - $penalty_payments).'
					        	</div>
					        	<div class="col-xs-2">
					        		'.number_format($penalty_charge) .'
					        	</div>
					        	<div class="col-xs-3 pull-right">
					        		'.number_format($total_penalty).'
					        	</div>
					        </div>
							';
			}
			

			$total_bill = $total_bill + $total_service + $total_penalty;
		}
	}

	
	


	

	
}


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_top_border{border-top: #888888 medium solid; margin-top:1px;}
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-12">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		    	 <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        	<div class="col-xs-6 ">
		                
		                <p><strong> Name </strong> : <?php echo $home_owner_name;?></p>
		                <p><strong> Phone </strong> :<?php echo $home_owner_phone_number;?></p>
		                <p><strong> Property </strong> :<?php echo $property_name;?></p>
		               
		            </div>
		            <div class="col-xs-6">
		             	<p><strong> Invoice # </strong> : <?php echo '';?></p>
		             	<p><strong> Invoice Date </strong> : <?php echo date('jS M Y',strtotime($invoice_date_date));?></p>
		             	<p><strong>Schedule</strong> : <?php echo $billing_schedule_quarter;?></p>
		                
		            </div>
		        </div>
		        
		        <div class="row">
			        <div class="col-xs-12">
			        	 <div class="row receipt_bottom_border">
				        	<div class="col-xs-12" id="title">
				            	<strong>A. SERVICE CHARGE DUES</strong>
				            </div>
				        </div>
				        <div class="row">
				        	<div class="col-xs-2">
				        		<strong>Apartment</strong>
				        	</div>
				        	<div class="col-xs-2">
				        		<strong>B/F KES</strong>
				        	</div>
				        	<div class="col-xs-2">
				        		<strong>Current Dues</strong>
				        	</div>
				        	<div class="col-xs-3 pull-right">
				        		<strong>Total Dues KES</strong>
				        	</div>
				        </div>
				        <?php echo $items;?>
			        </div>
			   	</div>
			   	<div class="row">
			        <div class="col-xs-12">
			        	<div class="row receipt_bottom_border">
				        	<div class="col-xs-12"  id="title">
				            	<strong>B. Penalties</strong>
				            </div>
				        </div>
				       
			        	<table class="table table-hover">
				           
				            <tbody>
				               	<?php echo $items_charge;?>
				            </tbody>
				        </table>
			        </div>
			     </div>
			    <div class="row" >
			    	<div class="col-xs-12" style="padding-top: 10px;padding-bottom: 5px;">
			    		<div class="row receipt_bottom_border">
				        	<div class="col-xs-12"  id="title">
				            	<strong>Total Dues </strong>
				            </div>
				        </div>
				        <div class="row">
				        	<div class="col-xs-8 pull-left">
				        		Total Dues  A+B 
				        	</div>
				        	<div class="col-xs-3 pull-right">
				        	  <strong> <?php echo number_format($total_bill);?> </strong>
				        	</div>
				        </div>
				    </div>
			    </div>
		       
		       <div class="row " style="border-top:1px #000 solid; ">
		       		<div class="col-xs-8 pull-left">
		       			<strong style="text-align:left;"> <?php echo $message_prefix;?></strong>
		       		</div>

		       			
		            
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>