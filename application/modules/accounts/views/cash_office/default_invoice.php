<?php
	$all_leases = $this->leases_model->get_lease_detail($lease_id);
	$items = '';
	$items_charge ='';
	$total_bill = 0;
	$service_charge = 0;
	$penalty_charge =0;
	$water_charge =0;
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		// $units_name = $leases_row->units_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arrears_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;
		$message_prefix = $leases_row->message_prefix;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	}

	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
	// get when the lease was active

	$lease_invoice = $this->accounts_model->get_invoices_month($lease_id,$invoice_month,$invoice_year);
	// get all the invoiced months
	$parent_invoice_date = $this->accounts_model->get_max_invoice_date($lease_id,$invoice_month,$invoice_year);

	if($lease_invoice->num_rows() > 0)
	{
		$water_charge = 0;
		$service_bf = 0;
		$water_charge = 0;
		$water_bf = 0;
		$penalty_charge =0;
		$penalty_bf =0;
		$water_payments = 0;
		$total_penalty = 0;
		$total_water = 0;
		$total_variance = 0;

		foreach ($lease_invoice->result() as $key_invoice) {
			# code...
			$invoice_date = $key_invoice->invoice_date;
			$invoice_id = $key_invoice->invoice_id;
			$invoice_type = $key_invoice->invoice_type;


			
			$invoice_date_date = date('jS F Y',strtotime($invoice_date));
			
			if($invoice_type == 2)
			{
				// service charge
				$invoice_type_name = $key_invoice->invoice_type_name;


				
				$water_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$water_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $water_invoice_amount - $water_paid_amount;
				
				$water_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$water_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_water = $water_current_invoice - $water_current_payment;

				$water_readings = $this->accounts_model->get_water_readings($invoice_id);

				if($water_readings->num_rows() > 0)
				{
					foreach ($water_readings->result() as $key) {
						# code...
						$prev_reading = $key->prev_reading;
						$current_reading = $key->current_reading;
					}
				}

				
			}


			if($invoice_type == 14)
			{
				// service charge
				$invoice_type_name = $key_invoice->invoice_type_name;


				
				$variance_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$variance_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_variance_brought_forward = $variance_invoice_amount - $variance_paid_amount;
				
				$variance_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$variance_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_variance = $variance_current_invoice - $variance_current_payment;

				

				
			}


			if($invoice_type == 5)
			{
				// penalty

				$invoice_type_name = $key_invoice->invoice_type_name;
				$penalty_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$penalty_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $penalty_invoice_amount - $penalty_paid_amount;
				
				$penalty_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$penalty_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_penalty = $penalty_current_invoice - $penalty_current_payment;
			
			}

			if($invoice_type == 12)
			{
				// service charge
				$invoice_type_name = $key_invoice->invoice_type_name;


				$fixed_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$fixed_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $fixed_invoice_amount - $fixed_paid_amount;
				
				$fixed_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$fixed_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_fixed = $fixed_current_invoice - $fixed_current_payment;

			
			}

			
		}
	}

	$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
	$total_pardon_amount = $tenants_response['total_pardon_amount'];


	
	$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date);
	$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date);
	$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date);
	$total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment - $total_pardon_amount;
	// var_dump($account_todays_payment); die();
	// var_dump($parent_invoice_date); die();

	$total_bill = $total_water + $total_penalty + $total_brought_forward_account + $total_fixed + $total_variance;

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_top_border{border-top: #888888 medium solid; margin-top:1px;}
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body >
        <div class="row">
	        <div class="col-xs-12">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> Invoice </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        	<div class="col-xs-6 ">
		                
		                <p><strong> Name </strong> : <?php echo $tenant_name;?></p>
		                <p><strong> Phone </strong> :<?php echo $tenant_phone_number;?></p>
		                <p><strong> Property </strong> :<?php echo $property_name;?></p>
		                 

		               
		            </div>
		            <div class="col-xs-6">
		             	<p><strong> Invoice # </strong> : <?php echo "June Invoice";?></p>
		             	<p><strong> Invoice Date </strong> : <?php echo date('jS M Y',strtotime($invoice_date));?></p>
		                
		            </div>
		        </div>
		      	

		      	<div class="row">
			        <div class="col-xs-12">
				        	<div class="col-xs-4 receipt_bottom_border">
				        		<strong>A. WATER CHARGE </strong> 
				        	</div>
				    </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Balance B/F
			        	</div>
			        	<div class="col-xs-4">
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		Kes. <?php echo number_format($total_brought_forward_account,2);?>
			        	</div>
			        	
			        </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Current Water Charge
			        	</div>
			        	<div class="col-xs-4">
			        		<table class="table " >
					            <tbody>
					            		
					               <tr >
					               	<td >Current Reading </td><td ><?php echo $current_reading;?></td> 
					               </tr>
					               <tr>
					               	<td >Previous Reading</td><td ><?php echo $prev_reading;?> </td>
					               </tr>
					               <tr>
					               	<td >Units Consumed</td><td  style="border-top:2px #000 solid !important;" ><?php echo $current_reading - $prev_reading;?> </td>
					               </tr>
					               <tr>
					               	<td >Rate  </td><td style="border-bottom:2px #000 solid !important;">Kes. <?php echo 112;?></td>
					               	</tr>
					               <tr>
					               
					               </tr>
					              
					            </tbody>
					        </table>
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		Kes.  <?php echo number_format($water_current_invoice,2);?>
			        	</div>
			        	
			        </div>
				        
			   	</div>
			   <!-- 	<div class="row">
			        <div class="col-xs-12">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>B. Fixed Charge </strong> 
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		 Kes. <?php echo number_format($total_fixed,2);?>
			        	</div>
			        </div>
			   	</div> -->
			   	<div class="row">
			        <div class="col-xs-12">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>B. PENALTIES </strong> 
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		 Kes. <?php echo number_format($total_penalty,2);?>
			        	</div>
			        </div>
			   	</div>
			   	
			   <div class="row">
			        <div class="col-xs-12">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>TOTAL DUES  </strong> (Balance B/F + A + B)
			        	</div>
			        	<div class="col-xs-4" >
			        		
			        	</div>
			        	<div class="col-xs-4 pull-right " >
			        		<strong style="border-top:2px #000 solid !important;border-bottom:2px #000 solid !important; margin-bottom:5px;"> Kes. <?php echo number_format($total_bill,2);?></strong>
			        	</div>
			        </div>
			   	</div>
		       <?php
		       	$disc = date('jS M Y', strtotime('+2 weeks', strtotime($invoice_date)));
		       ?>
		       
		       <div class="row " style="border-top:1px #000 solid; ">
		       		<div class="col-xs-8 pull-left">
		       			<strong > NOTE : Payment should be made by <?php echo $disc;?>, failure will lead to water disconnection.
		       			</strong><br>
		       			<strong style="text-align:left;"> <?php echo $message_prefix;?></strong>
		       		</div>

		       			
		            
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>

