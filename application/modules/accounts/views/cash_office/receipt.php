<?php
	$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		// $units_name = $leases_row->units_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arrears_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	}

	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	if($lease_payments->num_rows() > 0)
	{
		$y = 0;
		foreach ($lease_payments->result() as $key) 
		{
			$payment_id = $key->payment_id;
			$receipt_number = $key->receipt_number;
			$amount_paid = $key->amount_paid;
			$paid_by = $key->paid_by;
			$payment_date = $key->payment_date;
			$payment_created = $key->payment_created;
			$payment_created_by = $key->payment_created_by;
			$transaction_code = $key->transaction_code;
			$invoice_month_number = $key->month;
			
			$payment_date = date('jS M Y',strtotime($payment_date));
			$payment_created = date('jS M Y',strtotime($payment_created));
			$y++;
		}
	}



										
		$invoice_month = date('F', mktime(0,0,0,$invoice_month_number, 1, date('Y')));
		// $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
		$this_month = date('m');
		$this_year = date('Y');

		$total_paid = $this->accounts_model->get_months_last_debt($lease_id,$invoice_month_number);
		$last_bal = $this->accounts_model->get_months_last_arrears($invoice_month_number,$this_year,$lease_id);

		$months_invoice_query = $this->accounts_model->get_months_invoices($lease_id,$invoice_month_number);
		$total_bill = 0;
		$total_rental_bill = 0;
		$total_service_charge = 0;
		// var_dump($months_invoice_query->result()); die();
		foreach ($months_invoice_query->result() as $invoice_key) {
			
			$invoice_type = $invoice_key->invoice_type;
			if($invoice_type == 1)
			{
				$rental_invoice_amount = $invoice_key->invoice_amount;
				// rental bill
				 $total_rental_bill = $total_rental_bill + $rental_invoice_amount;
			}
			else
			{
				$service_charge_amount = $invoice_key->invoice_amount;
				// service charge 
				 $total_service_charge = $total_service_charge + $service_charge_amount;
			}

			$total_bill = $total_bill + ($total_rental_bill +$total_service_charge);
		}
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));


$invoice_types = $this->accounts_model->get_payments_invoice_types($payment_idd);
// var_dump($invoice_types); die();
$payment_result = '';
if($invoice_types->num_rows() >0)
{
	$x = 0;
	foreach ($invoice_types->result() as $types) {
		# code...
		$invoice_type_name = $types->invoice_type_name;
		$invoice_type_id = $types->invoice_type_id;
		$amount_paid = $this->accounts_model->get_payments_detail($payment_idd,$invoice_type_id);
		// var_dump($amount_paid);die();
		$invoiced_amount = $this->accounts_model->get_invoiced_amount($invoice_type_id);

		$paid_amount = $this->accounts_model->get_payments_amount($invoice_type_id);
		// -8000 +2000)
		$invoice_amount = $invoiced_amount + $amount_paid;
		// $invoiced_amount = $invoiced_amount - $paid_amount;
		// $total_balance = $invoiced_amount - $amount_paid;


		

		// if($total_balance > 0)
		$x++;
		$payment_result .=
							'
							<tr>
			                    <td>'.$x.'</td>
			                    <td>'.$invoice_type_name.'</td>
			                    <td>'.number_format($invoice_amount,2).'</td>
			                    <td>'.number_format($amount_paid,2).'</td>
			                    <td>'.number_format($invoiced_amount,2).'</td>
			                </tr>

							';

	}
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Receipt</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 8px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				font-size: 13px !important;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
    		<div class="col-md-12">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> RECEIPT</h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;">
		        	<div class="col-md-12">
			            <div class="col-md-5 pull-left">
			                <h2 class="panel-title">Tenant's Details</h2>
			                <table class="table table-hover table-bordered">
			                    <tbody>
			                        <tr><td><span>Tenant Name :</span></td><td><?php echo $tenant_name;?></td></tr>
			                        <tr><td><span>Tenant Phone :</span></td><td><?php echo $tenant_phone_number;?></td></tr>
			                        <tr><td><span>Property Name :</span></td><td><?php echo $property_name;?></td></tr>
			                        <tr><td><span>Hse No. :</span></td><td><?php echo $rental_unit_name;?></td></tr>
			                        
			                    </tbody>
			                </table>
			            </div>
			            <div class="col-md-6 pull-right">
			            	<h2 class="panel-title">Receipt Details</h2>
			                <table class="table table-hover table-bordered">
			                    <tbody>
			                        <tr><td><span>Receipt Number :</span></td><td><?php echo $receipt_number;?></td></tr>
			                        <tr><td><span>Transaction Number:</span></td><td><?php echo $transaction_code;?></td></tr>
			                        <tr><td><span>Payment Date :</span></td><td><?php echo $payment_date;?></td></tr>
			                        <tr><td><span>Paid By:</span></td><td><?php echo $paid_by;?></td></tr>
			                    </tbody>
			                </table>
			            </div>
			        </div>
		            
		        </div>
		        
		    	<div class="row receipt_bottom_border">
		        	<div class="col-md-12 center-align">
		            	<strong>RECEIPTED ITEMS</strong>
		            </div>
		        </div>
		    	<table class="table table-hover table-bordered table-striped">
		            <thead>
		                <tr>
		                  <th>#</th>
		                  <th>Payment for </th>
		                  <th>Arrears (Kes)</th>
		                  <th>Paid Amount(Kes)</th>
		                  <th>Balance (Kes)</th>
		                </tr>
		            </thead>
		            <tbody>
		                <?php echo $payment_result;?>
		            </tbody>
		        </table>
		        <table class="table table-condensed">
		            <tr>
		                <th class="align-right">
		                    <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
		                     E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                </th>
		            </tr>
		        </table>
		</div>
		</div>
        
    </body>
    
</html>