<?php echo $this->load->view('search/tenants_search','', true); ?>
<?php

$result = '';
		
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th><a>Unit Name</a></th>
				<th><a>Tenant Name</a></th>
				<th><a>Phone Number</a></th>
				<th><a>Email</a></th>
				<th><a>Bal BF</a></th>
				<th><a>Last Receipt</a></th>
				<th><a>Amount Paid</a></th>
				<th><a>Curr Bal</a></th>
				<th colspan="2">Actions</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	
	
	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$rental_unit_id = $leases_row->unit_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$tenant_email = $leases_row->tenant_email;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$points = 0;//$leases_row->points;
		$created = $leases_row->created;

// var_dump($rental_unit_id); die();
	
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		

		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Deactivated</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
	     	
	     	$this_month = date('m');
	     	$amount_paid = 0;
		     	$payments = $this->accounts_model->get_this_months_payment($lease_id,$this_month);
		     	$current_items = '<td> -</td>
		     			  <td>-</td>'; 
		     	$total_paid_amount = 0;
		     	if($payments->num_rows() > 0)
		     	{
		     		$counter = 0;
		     		
		     		$receipt_counter = '';
		     		foreach ($payments->result() as $value) {
		     			# code...
		     			$receipt_number = $value->receipt_number;
		     			$amount_paid = $value->amount_paid;

		     			if($counter > 0)
		     			{
		     				$addition = '#';
		     				// $receipt_counter .= $receipt_number.$addition;
		     			}
		     			else
		     			{
		     				$addition = ' ';
		     				
		     			}
		     			$receipt_counter .= $receipt_number.$addition;

		     			$total_paid_amount = $total_paid_amount + $amount_paid;
		     			$counter++;
		     		}
		     		$current_items = '<td>'.$receipt_number.'</td>
		     					<td>'.number_format($amount_paid,0).'</td>';
		     	}
		     	else
		     	{
		     		$current_items = '<td> -</td>
		     					<td>-</td>';
		     	}
				
		     	$todays_date = date('Y-m-d');



		  //    	$datestring=''.$todays_date.' first day of last month';
				// $dt=date_create($datestring);
				// $last_date = $dt->format('Y-m'); //2011-02

				// $last_date_explode = explode('-', $last_date);

				// $last_month = $last_date_explode[1];
				// $last_year = $last_date_explode[0];

				$todays_date = date('Y-m-d');
				$todays_month = date('m');
				$todays_year = date('Y');

				$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
				$total_arrears = $tenants_response['total_arrears'];
				$total_invoice_balance = $tenants_response['total_invoice_balance'];
				$invoice_date = $tenants_response['invoice_date'];

				$current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
				// $total_current_invoice = $current_invoice_amount + $current_balance - $total_paid_amount;

				$count++;
				//check if email for the current_month has been sent for that rental unit
				$all_sms_notifications_sent = $this->accounts_model->check_if_sms_sent($rental_unit_id,$tenant_phone_number,$todays_year,$todays_month,1);
				$all_email_notifications_sent = $this->accounts_model->check_if_email_sent($rental_unit_id,$tenant_email,$todays_year,$todays_month,1);
				if(($all_sms_notifications_sent==FALSE) &&($all_email_notifications_sent==FALSE))
				{
					$send_notification_button = '<a class="btn btn-sm btn-default" href="'.site_url().'send-arrears/'.$lease_id.'" onclick="return confirm(\' Are you sure you want to send notification to tenant ? \')"> Send Message</a>';	
				}
				else
				{
					$send_notification_button = '-';
				}
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$tenant_name.'</td>
						<td>'.$tenant_phone_number.'</td>
						<td>'.$tenant_email.'</td>
						<td>'.number_format($total_invoice_balance,2).'</td>
						'.$current_items.'
						<td>'.number_format($total_arrears ,2).'</td>
						<td>
							<a class="btn btn-sm btn-warning" href="'.site_url().'accounts/payments/'.$tenant_unit_id.'/'.$lease_id.'" > <i class="fa fa-folder"></i> Account Detail
							</a>
						</td>	
						<td>'.$send_notification_button.'</td>	
						
					</tr> 
				';
		$v_data['lease_id'] = $lease_id;
		$v_data['amount_paid'] = $amount_paid;
		$v_data['current_balance'] = $current_balance;
		$v_data['balance_bf'] = $current_balance;
		
	}
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}


$accounts_search_title = $this->session->userdata('accounts_search_title');
?>  
<!-- href="<?php echo site_url();?>accounts/update_invoices" -->
<section class="panel">
		<header class="panel-heading">						
			<h2 class="panel-title"><?php echo $title;?></h2>
			<!-- <a  href="<?php echo site_url();?>accounts/update_invoices"  style="margin-top:-24px" class="btn btn-sm btn-success pull-right" onclick="return confirm('Do you want to update invoices for <?php echo $accounts_search_title;?> ?')"> <i class="fa fa-recycle"></i> Update invoices </a> -->


			<a href="<?php echo site_url();?>accounts/send_notifications" style="margin-top:-24px;margin-right:5px;" class="btn btn-sm btn-default pull-right" onclick="return confirm('Do you want to send notifications for <?php echo $accounts_search_title;?> ?')"> <i class="fa fa-outbox"></i> Send Bulk Notifications</a>


		</header>
		<div class="panel-body">
        	<?php
            $success = $this->session->userdata('success_message');

			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}
			
			$error = $this->session->userdata('error_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			$search =  $this->session->userdata('all_accounts_search');
			if(!empty($search))
			{
				echo '<a href="'.site_url().'close_search_accounts" class="btn btn-sm btn-warning">Close Search</a>';
			}
					
			?>

        	<div class="row" style="margin-bottom:20px;">
                <div class="col-lg-2 col-lg-offset-8">
                    
                </div>
                <div class="col-lg-12">
                </div>
            </div>
			<div class="table-responsive">
            	
				<?php echo $result;?>
		
            </div>
		</div>
        <div class="panel-footer">
        	<?php if(isset($links)){echo $links;}?>
        </div>
	</section>

	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>