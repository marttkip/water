<?php
$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$property_id = $leases_row->property_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Deactivated</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}

		//create deactivated status display
		if($tenant_status == 0)
		{
			$status_tenant = '<span class="label label-default">Deactivated</span>';
		}
		//create activated status display
		else if($tenant_status == 1)
		{
			$status_tenant = '<span class="label label-success">Active</span>';
		}
	}



// get the months 

$invoices_month = '';
$items_check  = '';
$items_check_list  = '';
$items = '';
$counter = 0; 
$items_check_pl = '';
if($query->num_rows() > 0)
{
	// var_dump($query->result()); die();

	foreach ($query->result() as $month_key) {
		# code...

		$month_invoice = $month_key->invoice_month;
		$year_invoice = $month_key->invoice_year;

		// get all in
		$lease_invoice = $this->accounts_model->get_invoices_month($lease_id,$month_invoice,$year_invoice);
		
		if($lease_invoice->num_rows() > 0)
		{
			$electricity_charge = 0;
			$electricity_bf = 0;
			$service_charge = 0;
			$service_bf = 0;
			$water_charge = 0;
			$water_bf = 0;
			$penalty_charge =0;
			$penalty_bf =0;
			$borehole = 0;
			$prev_reading = 0;
			$current_reading =0;
			$elec_prev_reading = 0;
			$elec_current_reading =0;
			$water_bill_lamp = 0;
			$elect_bill_lamp = 0;
			$bought_water_charge = 0;
			$bought_water_bf = 0;
			$common_areas_charge = 0;
			$common_areas_bf = 0;
			$painting_charge = 0;
			$painting_bf = 0;
			$sinking_charge = 0;
			$sinking_bf = 0;
			$insurance_charge = 0;
			$insurance_bf = 0;
			$current_insurance_invoice_amount = 0;
			$current_service_invoice_amount = 0;
			$current_bought_invoice_amount = 0;
			$current_water_invoice_amount = 0;
			$current_painitng_invoice_amount = 0;


			$insurance_arrears  =0;
			$insurance_invoices_bf  =0;
			$total_insurance = 0;

			$common_arrears  =0;
			$common_invoices_bf  =0;
			$total_common = 0;

			$penalty_arrears  =0;
			$penalty_invoices_bf  =0;
			$total_penalty = 0;

			$sinking_arrears  =0;
			$sinking_invoices_bf  =0;
			$total_sinking = 0;

			$water_arrears  =0;
			$water_invoices_bf  =0;
			$total_water = 0;

			$bought_arrears  =0;
			$bought_invoices_bf  =0;
			$total_bought = 0;

			$painting_arrears  =0;
			$painting_invoices_bf  =0;
			$total_painting = 0;

			$service_invoices_bf = 0;
			foreach ($lease_invoice->result() as $key_invoice) {
				# code...
				$invoice_date = $key_invoice->invoice_date;
				$invoice_id = $key_invoice->invoice_id;
				 $invoice_type = $key_invoice->invoice_type;
				$invoice_array = explode('-', $invoice_date);
				$invoice_month = $invoice_array[1];
				$invoice_year = $invoice_array[0];

				if($invoice_type == 2 OR $invoice_type == 3)
				{


				$water_bill_lamp = $this->accounts_model->get_total_bill($invoice_id,$invoice_type,$invoice_date);
				$elect_bill_lamp = $this->accounts_model->get_total_bill($invoice_id,$invoice_type,$invoice_date);
				}
				$invoice_date_date = date('jS F Y',strtotime($invoice_date));
				if($invoice_type == 3)
				{
					// electricity
					$electricity_charge = $key_invoice->invoice_amount;
					$electricity_bf = $key_invoice->arrears_bf;

					$elec_readings = $this->accounts_model->get_water_readings($invoice_id);

					if($elec_readings->num_rows() > 0)
					{
						foreach ($elec_readings->result() as $key_elec) {
							# code...
							$elec_prev_reading = $key_elec->prev_reading;
							$elec_current_reading = $key_elec->current_reading;
						}
					}
				}

				if($invoice_type == 4)
				{
					$invoice_id = $key_invoice->invoice_id;
					// service charge
					$service_charge = $key_invoice->invoice_amount;
					// $service_bf = $key_invoice->arrears_bf;


					$service_payments = $this->accounts_model->get_total_payments_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
					
					$service_invoices= $this->accounts_model->get_total_invoices_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
					$service_invoices_bf= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_year,$invoice_month,$invoice_type);
					// var_dump($service_invoices);die();
					$current_service_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
					$service_arrears = $service_invoices - $service_payments;
					$total_service = $service_arrears + $current_service_invoice_amount; 

					$items_check .= form_open("update-tenant-invoice/".$lease_id."/".$invoice_type."/".$month_invoice."/".$year_invoice."/".$invoice_id, array("class" => "form-horizontal"));
					$items_check .= '
										<div class="row">
									        <div class="col-md-12">
									        	<div class="row receipt_bottom_border">
										        	<div class="col-xs-12">
										            	<strong>SERVICE CHARGE DUES</strong>
										            </div>
										        </div>
										       
									        	<table class="table table-hover">
										           
										            <tbody>
										            	<tr>
										               		<td class="col-md-4">Bal B/F</td>
										               		<td class="col-md-4">Kes. '.number_format($service_invoices_bf).'</td>
										               		<td class="col-md-4"></td>
										               	</tr>

										               	<tr>
										               		<td class="col-md-4">Current Dues Kes.</td>
										               		<td  class="col-md-4">
										               		<input type="number" name="amount" class="form-control" value="'.$service_arrears.'"/> </td>
										               		<td class="col-md-4"></td>

										               	</tr>
										               	<tr>
											               	<td class="col-md-4">Total S/C Billing</td>
											               	<td class="col-md-4"> 
											               	<button class=" col-md-12 btn btn-info btn-sm" type="submit">Update Charge</button>
											               	 </td>
											               	<td  class="col-md-4 pull-right"><strong>Kes. '.number_format($total_service).'</strong> </td>
											               </tr>
										            </tbody>
										        </table>
									        </div>
									     </div>';
					$items_check .= form_close();
				}

				if($invoice_type == 2)
				{
					$invoice_id = $key_invoice->invoice_id;
					// water
					$water_charge = $key_invoice->invoice_amount;
					$water_bf = $key_invoice->arrears_bf;

					$water_payments = $this->accounts_model->get_total_payments_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
					
					$water_invoices= $this->accounts_model->get_total_invoices_end($lease_id,$invoice_year,$invoice_month,$invoice_type);

					$water_invoices_bf= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_year,$invoice_month,$invoice_type);
					// var_dump($water_invoices);die();
					$current_water_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
					$water_arrears = $water_invoices - $water_payments;
					$total_water = $water_arrears + $current_water_invoice_amount; 

					

					$water_readings = $this->accounts_model->get_water_readings($invoice_id);

					if($water_readings->num_rows() > 0)
					{
						foreach ($water_readings->result() as $key) {
							# code...
							$prev_reading = $key->prev_reading;
							$current_reading = $key->current_reading;
						}
					}

					$ncc_rate = 110;
					$water_bill = ($current_reading - $prev_reading) * $ncc_rate;
					$items_check .= form_open("update-tenant-invoice/".$lease_id."/".$invoice_type."/".$month_invoice."/".$year_invoice."/".$invoice_id, array("class" => "form-horizontal"));
					$items_check .=' <div class="row">
								        <div class="col-md-12">
								        	 <div class="row receipt_bottom_border">
									        	<div class="col-xs-12">
									            	<strong>WATER DUES</strong>
									            </div>
									        </div>
									        <strong style="text-decoration:underline;">Billed Water</strong>
							               	  <table class="table table-hover" >
									            <tbody>
									               <tr >
										               	<td class="col-md-4">Current Reading </td>
										               	<td class="col-md-4"><input type="number" name="water_curr_reading" class="form-control" value="'.$current_reading.'"/> </td> 
										               	<td class="col-md-4"></td>
									               </tr>
									               <tr>
										               	<td class="col-md-4">Previous Reading</td>
										               	<td class="col-md-4"> <input type="number" name="water_prev_reading" class="form-control" value="'.$prev_reading.'"/> </td>
										               	<td class="col-md-4"></td>
									               </tr>
									               <tr>
									               		<td class="col-md-4">Units Consumed</td>
									               		<td  class="col-md-4" style="border-top:2px #000 solid !important;" >'.($current_reading - $prev_reading).' </td>
									               		<td class="col-md-4"></td>
									               </tr>
									               <tr >
										               	<td class="col-md-4">Rate  </td>
										               	<td class="col-md-4" style="border-bottom:2px #000 solid !important;">Kes. '.$ncc_rate.'</td>
										               	<td class="col-md-4"></td>
									               </tr>
									               <tr>
									               		<td class="col-md-4">Current Billing <br>(R.per use * units Consumed)</td>
									               		<td class="col-md-4"> 
											               	<button class=" col-md-12 btn btn-info btn-sm" type="submit">Update Charge</button>
											            </td>
									               		<td class="col-md-4 pull-right" colspan="2"><strong> Kes. '.number_format($water_bill).'</strong> </td>
									               </tr>
									              
									            </tbody>
									        </table>
									        </div>
			   							</div>';
			   			$items_check .= form_close();
				}

				if($invoice_type == 10)
				{
					$invoice_id = $key_invoice->invoice_id;
					// water
					$bought_water_charge = $key_invoice->invoice_amount;
					$bought_water_bf = $key_invoice->arrears_bf;
					

					$bought_payments = $this->accounts_model->get_total_payments_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
					
					$bought_invoices= $this->accounts_model->get_total_invoices_end($lease_id,$invoice_year,$invoice_month,$invoice_type);

					$bought_invoices_bf= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_year,$invoice_month,$invoice_type);
					// var_dump($bought_invoices);die();
					$current_bought_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
					$bought_arrears = $bought_invoices - $bought_payments;
					$total_bought = $bought_arrears + $current_bought_invoice_amount; 

					$consumption = $bought_water_charge/0.5;
					$items_check .= form_open("update-tenant-invoice/".$lease_id."/".$invoice_type."/".$month_invoice."/".$year_invoice."/".$invoice_id, array("class" => "form-horizontal"));
					$items_check .= ' <strong style="text-decoration:underline;"> Bought Water</strong>
					               	  <table class="table table-hover" >
							            <tbody>
							               <tr >
								               	<td class="col-md-4" >Litres  <br/> (Split Based on usage of units consumed) </td>
								               	<td class="col-md-4" ><input type="number" name="amount"  class="form-control" value="'.$consumption.'"/></td>
								               	<td class="col-md-4"></td>
							               </tr>
							               <tr>
							               	<td class="col-md-4">Rate  </td>
							               	<td class="col-md-4" style="border-bottom:2px #000 solid !important;">Kes. 0.5</td>
							               	<td class="col-md-4"></td>

							               </tr>
							               <tr>
							               	<td class="col-md-4">Billing <br>(R.per use * units Consumed)</td>
							               	<td class="col-md-4">Kes. '.number_format($bought_water_charge).'</td>
							               	<td class="col-md-4"></td>
							               </tr>
							               
							               <tr>
							               	<td class="col-md-4">Balance B/F  </td>
							               	<td class="col-md-4"></td>
							               	<td class="col-md-4 pull-right">Kes. '.number_format($water_bf).'</td>
							               </tr>
							               <tr>
							               	<td class="col-md-4">Current Billing</td>
							               	<td class="col-md-4"> 
							               	<button class=" col-md-12 btn btn-info btn-sm" type="submit">Update Charge</button>
							               	 </td>
							               	<td class="col-md-4 pull-right"><strong>Kes. '.number_format($total_bought).'</strong> </td>
							               </tr>
							              
							            </tbody>
							        </table>';
						$items_check .= form_close();
			
				}
				if($invoice_type == 7)
				{
					$invoice_id = $key_invoice->invoice_id;
					// water
					$painting_charge = $key_invoice->invoice_amount;
					$painting_bf = $key_invoice->arrears_bf;

					$painting_payments = $this->accounts_model->get_total_payments_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
					
					$painting_invoices= $this->accounts_model->get_total_invoices_end($lease_id,$invoice_year,$invoice_month,$invoice_type);

					$painting_invoices_bf= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_year,$invoice_month,$invoice_type);
					// var_dump($painting_invoices);die();
					$current_painitng_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
					$painting_arrears = $painting_invoices - $painting_payments;
					$total_painting = $painting_arrears + $current_painitng_invoice_amount; 
					$items_check .= form_open("update-tenant-invoice/".$lease_id."/".$invoice_type."/".$month_invoice."/".$year_invoice."/".$invoice_id, array("class" => "form-horizontal"));
					 $items_check .='<div class="row">
								        <div class="col-md-12">
								        	<div class="row receipt_bottom_border">
									        	<div class="col-xs-12">
									            	<strong>PAINTING DUES</strong>
									            </div>
									        </div>
									       
								        	<table class="table table-hover">
									           
									            <tbody>
									            	<tr>
									               		<td class="col-md-4">Bal B/F</td>
									               		<td class="col-md-4"></td>
									               		<td class="col-md-4 pull-right" >Kes. '. number_format($painting_invoices_bf).'</td>
									               	</tr>

									               	<tr>
									               		<td class="col-md-4">Current Charge</td>
									               		<td class="col-md-4">
									               		<input type="number" name="amount"  class="form-control" value="'.$painting_arrears.'"/>
									               		</td>
									               		<td class="col-md-4" ></td>
									               	</tr>

									               	<tr>
									               		<td class="col-md-4">Total Painting Charge</td>
									               		<td class="col-md-4"> 
											               	<button class=" col-md-12 btn btn-info btn-sm" type="submit">Update Charge</button>
											             </td>
									               		<td class="col-md-4 pull-right">
									               		<strong>Kes. '.number_format($total_painting).'</strong> </td>
									               	</tr>
									            </tbody>
									        </table>
								        </div>
								     </div>';
						$items_check .= form_close();

			
				}
				if($invoice_type == 8)
				{
					$invoice_id = $key_invoice->invoice_id;
					// water
					$sinking_charge = $key_invoice->invoice_amount;
					
					$sinking_payments = $this->accounts_model->get_total_payments_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
					
					$sinking_invoices= $this->accounts_model->get_total_invoices_end($lease_id,$invoice_year,$invoice_month,$invoice_type);

					$sinking_invoices_bf= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_year,$invoice_month,$invoice_type);
					// var_dump($sinking_invoices);die();
					$current_sinking_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
					$sinking_arrears = $sinking_invoices - $sinking_payments;
					$total_sinking = $sinking_arrears + $current_sinking_invoice_amount; 
					$current_sinking_amount = $total_sinking - $sinking_invoices_bf;
					$items_check .= form_open("update-tenant-invoice/".$lease_id."/".$invoice_type."/".$month_invoice."/".$year_invoice."/".$invoice_id, array("class" => "form-horizontal"));
					$items_check .='<div class="row">
								        <div class="col-md-12">
								        	<div class="row receipt_bottom_border">
									        	<div class="col-xs-12">
									            	<strong>SINKING FUNDS</strong>
									            </div>
									        </div>
									       
								        	<table class="table table-hover">
									           
									            <tbody>
									            	<tr>
									               		<td class="col-md-4">Bal B/F</td>
									               		<td class="col-md-4"></td>
									               		<td class="col-md-4 pull-right">Kes. '.number_format($sinking_invoices_bf).'</td>
									               	</tr>
									               	<tr>
									               		<td class="col-md-4">Current Dues</td>
									               		<td class="col-md-4">
									               			<input type="number" name="amount"  class="form-control" value="'.$current_sinking_amount.'"/>
									               		</td>
									               		<td class="col-md-4 pull-right">
									               		 </td>
									               	</tr>

									               	<tr>
									               		<td class="col-md-4">Total Dues</td>
									               		<td class="col-md-4">
									               			<button class=" col-md-12 btn btn-info btn-sm" type="submit">Update Charge</button>
									               		</td>
									               		<td class="col-md-4 pull-right">
									               		<strong>Kes. '.number_format($total_sinking).'</strong> </td>
									               	</tr>
									            </tbody>
									        </table>
								        </div>
								     </div>';
						$items_check .= form_close();

			
				}
				if($invoice_type == 9)
				{
					$invoice_id = $key_invoice->invoice_id;
					// water


					$insurance_charge = $key_invoice->invoice_amount;
					$insurance_bf = $key_invoice->arrears_bf;

					$insurance_payments = $this->accounts_model->get_total_payments_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
					
					$insurance_invoices= $this->accounts_model->get_total_invoices_end($lease_id,$invoice_year,$invoice_month,$invoice_type);

					$insurance_invoices_bf= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_year,$invoice_month,$invoice_type);
					// var_dump($insurance_invoices);die();
					$current_insurance_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
					$insurance_arrears = $insurance_invoices - $insurance_payments;

					


					$total_insurance = $insurance_arrears + $current_insurance_invoice_amount; 

					$current_insurance_amount = $total_insurance - $insurance_invoices_bf;

					// var_dump($invoice_type); die();
					$items_check .= form_open("update-tenant-invoice/".$lease_id."/".$invoice_type."/".$month_invoice."/".$year_invoice."/".$invoice_id, array("class" => "form-horizontal"));
					$items_check .='<div class="row">
								        <div class="col-md-12">
								        	<div class="row receipt_bottom_border">
									        	<div class="col-xs-12">
									            	<strong>INSURANCE DUES</strong>
									            </div>
									        </div>
									       
								        	<table class="table table-hover">
									           
									            <tbody>
									            	<tr>
									               		<td class="col-md-4">Bal B/F</td>
									               		<td class="col-md-4"></td>
									               		<td class="col-md-4 pull-right">Kes. '.number_format($insurance_invoices_bf).'
									               		</td>
									               	</tr>

									               	<tr>
									               		<td class="col-md-4">Current Insurance Dues</td>
									               		<td class="col-md-4">
									               		<input type="number" name="amount" class="form-control" value="'.$current_insurance_amount.'"/></td>
									               		<td class=" col-md-4 ">
									               			
									               		</td>

									               	<tr>
									               		<td class="col-md-4">Total Insurance Charge</td>
									               		<td class="col-md-4"> 
											               	<button class=" col-md-12 btn btn-info btn-sm" type="submit">Update Charge</button>
											             </td>
									               		<td class="col-md-4 pull-right">
									               		<strong>Kes. '.number_format($total_insurance).'</strong> </td>
									               	</tr>
									               	</tr>
									            </tbody>
									        </table>
								        </div>
								     </div>';
						$items_check .= form_close();
			
				}
				if($invoice_type == 11)
				{
					$invoice_id = $key_invoice->invoice_id;
					// water
					$common_areas_charge = $key_invoice->invoice_amount;
					$common_areas_bf = $key_invoice->arrears_bf;

					$common_payments = $this->accounts_model->get_total_payments_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
					
					$common_invoices= $this->accounts_model->get_total_invoices_end($lease_id,$invoice_year,$invoice_month,$invoice_type);

					$common_invoices_bf= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_year,$invoice_month,$invoice_type);
					// var_dump($common_invoices);die();
					$current_common_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
					$common_arrears = $common_invoices - $common_payments;
					$total_common = $common_arrears + $current_common_invoice_amount; 

			
				}

				if($invoice_type == 5)
				{
					$invoice_id = $key_invoice->invoice_id;
					// penalty

					$penalty_charge = $key_invoice->invoice_amount;
					$penalty_bf = $key_invoice->arrears_bf;

					$penalty_payments = $this->accounts_model->get_total_payments_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
					
					$penalty_invoices= $this->accounts_model->get_total_invoices_end($lease_id,$invoice_year,$invoice_month,$invoice_type);

					$penalty_invoices_bf= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_year,$invoice_month,$invoice_type);
					// var_dump($penalty_invoices);die();
					$current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
					$penalty_arrears = $penalty_invoices - $penalty_payments;
					$total_penalty = $service_arrears + $current_invoice_amount; 
					$items_check .= form_open("update-tenant-invoice/".$lease_id."/".$invoice_type."/".$month_invoice."/".$year_invoice."/".$invoice_id, array("class" => "form-horizontal"));
					$items_check .= '	<div class="row">
									        <div class="col-md-12">
									        	<div class="row receipt_bottom_border">
										        	<div class="col-xs-12">
										            	<strong>Penalties</strong>
										            </div>
										        </div>
										       
									        	<table class="table table-hover">
										           
										            <tbody>
										               	<tr>
										               		<td class="col-md-6">Penalty Dues</td><td class="pull-right">Kes. '. number_format($total_penalty).'</td>
										               	</tr>
										            </tbody>
										        </table>
									        </div>
									     </div>';
					$items_check .= form_close();
				}

				
			}
			// var_dump($total_insurance); die();
			$total_bill = $total_service +$total_penalty + $total_insurance + $total_painting + $total_sinking + $total_water +$total_bought;
		}
		$ncc_rate = 110;
		// $total_service_charge = $water_bf + $water_charge + $borehole;	

		$total_billing_water = 110 * ($current_reading - $prev_reading);


		$counter++;

		if($counter == 1)
		{

			$items_check_list = '
								<div class="row receipt_bottom_border">
						        	<div class="col-xs-12" id="title">
						            	<strong>'.date('M Y',strtotime($year_invoice.'-'.$month_invoice)).'</strong>
						            </div>
						        </div>
								 '.$items_check.'
								';
			$items_check_list .= '<div class="row">
								        <div class="col-md-12">
								        	<div class="row receipt_bottom_border">
									        	<div class="col-xs-12">
									            	<strong>Totals</strong>
									            </div>
									        </div>
									       
								        	<table class="table table-hover">
								        		<tbody>
													<tr>
														<td class="col-md-4">Total Bill</td>
														<td class="col-md-4"></td>
														<td class="col-md-4 pull-right" style="border-top:2px #000 solid !important;border-bottom:2px #000 solid !important;">
															<strong> Kes. '.number_format($total_bill).'</strong> </td>
													</td>
												</tbody>
											</table>
								        </div>
								     </div>
									
								';
		}
		else
		{
			$invoices_month .= '
							<tr>
								<td>'.date('M Y',strtotime('2016-'.$month_invoice)).' </td>
								<td>Payment</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr> 
							';
		}
		
	}
}

// end of the months thinh


$bills = $this->accounts_model->get_all_invoice_month($lease_id);
$payments = $this->accounts_model->get_all_payments_lease($lease_id);

// var_dump($bills); die();
$x=0;

$bills_result = '';
$last_date = '';
$current_year = date('Y');
$total_invoices = $bills->num_rows();
$invoices_count = 0;
$total_invoice_balance = 0;
$total_arrears = 0;
$total_payment_amount = 0;
$result = '';
if($bills->num_rows() > 0)
{
	foreach ($bills->result() as $key_bills) {
		# code...
		$invoice_month = $key_bills->invoice_month;
	    $invoice_year = $key_bills->invoice_year;
		$invoice_date = $key_bills->invoice_date;
		$invoice_amount = $key_bills->total_invoice;
		$invoices_count++;
		if($payments->num_rows() > 0)
		{
			foreach ($payments->result() as $payments_key) {
				# code...
				$payment_date = $payments_key->payment_date;
				$payment_year = $payments_key->year;
				$payment_month = $payments_key->month;
				$payment_amount = $payments_key->amount_paid;

				if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
				{
					$total_arrears -= $payment_amount;
					if($payment_year >= $current_year)
					{
						$result .= 
						'
							<tr>
								<td>'.date('d M Y',strtotime($payment_date)).' </td>
								<td>Payment</td>
								<td></td>
								<td>'.number_format($payment_amount, 2).'</td>
								<td>'.number_format($total_arrears, 2).'</td>
								<td></td>
							</tr> 
						';
					}
					
					$total_payment_amount += $payment_amount;

				}
			}
		}
		//display disbursment if cheque amount > 0
		if($invoice_amount != 0)
		{
			$total_arrears += $invoice_amount;
			$total_invoice_balance += $invoice_amount;
				
			if($invoice_year >= $current_year)
			{
				$result .= 
				'
					<tr>
						<td>'.date('d M Y',strtotime($invoice_date)).' </td>
						<td>'.$invoice_month.' '.$invoice_year.' Invoice</td>
						<td>'.number_format($invoice_amount, 2).'</td>
						<td></td>
						<td>'.number_format($total_arrears, 2).'</td>
						<td><a href="'.site_url().'invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'" target="_blank" class="btn btn-sm btn-warning">Invoice</a></td>
					</tr> 
				';
			}
		}
				
		//check if there are any more payments
		if($total_invoices == $invoices_count)
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;

					if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						if($payment_year >= $current_year)
						{
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>Payment</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr> 
							';
						}
						
						$total_payment_amount += $payment_amount;

					}
				}
			}
		}
				$last_date = $invoice_date;
	}
}	
else
{
	//get all loan deductions before date
	if($payments->num_rows() > 0)
	{
		foreach ($payments->result() as $payments_key) {
			# code...
			$payment_date = $payments_key->payment_date;
			$payment_year = $payments_key->year;
			$payment_month = $payments_key->month;
			$payment_amount = $payments_key->amount_paid;



			if(($payment_amount > 0))
			{
				$total_arrears -= $payment_amount;
				if($payment_year >= $current_year)
				{
					$result .= 
					'
						<tr>
							<td>'.date('d M Y',strtotime($payment_date)).' </td>
							<td>Payment</td>
							<td></td>
							<td>'.number_format($payment_amount, 2).'</td>
							<td>'.number_format($total_arrears, 2).'</td>
							<td></td>
						</tr> 
					';
				}
				
				$total_payment_amount += $payment_amount;

			}
		}
	}
}
				
//display loan
$result .= 
'
	<tr>
		<th colspan="2">Total</th>
		<th>'.number_format($total_invoice_balance, 2).'</th>
		<th>'.number_format($total_payment_amount, 2).'</th>
		<th>'.number_format($total_arrears, 2).'</th>
		<td></td>
	</tr> 
';

$grand_rent_bill = $this->accounts_model->get_cummulated_balance($lease_id,1);
$grand_water_bill = $this->accounts_model->get_cummulated_balance($lease_id,2);
$grand_electricity_bill = $this->accounts_model->get_cummulated_balance($lease_id,3);
$grand_service_charge_bill = $this->accounts_model->get_cummulated_balance($lease_id,4);
$grand_penalty_bill = $this->accounts_model->get_cummulated_balance($lease_id,5);

//  interface  for making payments
// use the property id and also the type of the person
// var_dump($property_id); die();
$property_invoices = $this->accounts_model->get_property_invoice_types($property_id,1);
$inputs = '';
if($property_invoices->num_rows() > 0)
{
	// var_dump($property_invoices->result()); die();
	foreach ($property_invoices->result() as $key_types) {
		# code...
		$property_invoice_type_id = $key_types->invoice_type_id;

		if($property_invoice_type_id == 1)
		{
			$inputs .='	
						<div class="form-group">
							<label class="col-md-4 control-label">Rent Amount: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="rent_amount" placeholder="'.$grand_rent_bill.'" autocomplete="off" >
							</div>
						</div>	
					   ';
		}
		else if($property_invoice_type_id == 2)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">W/C Amount: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="water_amount" placeholder="'.$grand_water_bill.'" autocomplete="off" >
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 3)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 4)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">S/C Amount: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="service_charge_amount" placeholder="'.$grand_service_charge_bill.'" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 5)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Penalty: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="penalty_fee" placeholder="'.$grand_penalty_bill.'" autocomplete="off" >
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 6)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 7)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Painting Charge: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="painting_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 8)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Sinking Funds: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="sinking_funds" placeholder="" autocomplete="off">
							</div>
						</div>
					';
		}
		else if($property_invoice_type_id == 9)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Insurance: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="insurance" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 12)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Fixed Charge: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="fixed_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 10)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Bought Water: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="bought_water" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 11)
		{
			$inputs .='';
		}
	}
}




?>
 <section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title"><?php echo $title;?> For <?php echo $tenant_name;?> 
		<br>
		<span>Unit Name :</span></td><td><?php echo $rental_unit_name;?> <a href="<?php echo site_url();?>cash-office/accounts" class="btn btn-sm btn-success pull-right"  style="margin-top:-5px;">Back to leases</a> </h2>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				  echo '<div class="alert alert-danger">'.$error.'</div>';
				  $this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
				  echo '<div class="alert alert-success">'.$success.'</div>';
				  $this->session->unset_userdata('success_message');
				}
			 ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<section class="panel panel-featured panel-featured-info">
					<header class="panel-heading">
						
						<h2 class="panel-title">Invoices <?php echo date('Y');?>

						</h2>

					</header>
					<div class="panel-body">
                    	<?php echo $items_check_list;?>


						<table class="table table-hover table-bordered col-md-12">
							<thead>
								<tr>
									<th>Date</th>
									<th>Description</th>
									<th>Invoices</th>
									<th>Payments</th>
									<th>Arrears</th>
									<th></th>
								</tr>
							</thead>
						  	<tbody>
						  		<?php echo $invoices_month;?>
						  	</tbody>
						</table>
					</div>
				</section>
			</div>
		</div> 
	
		<!-- END OF PADD -->
	</div>
</section>
  <!-- END OF ROW -->
<script type="text/javascript">
  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");
		
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }
</script>