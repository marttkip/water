<?php
$all_leases = $this->leases_model->get_owner_unit_detail($rental_unit_id);

foreach ($all_leases->result() as $leases_row)
{
	$rental_unit_id = $leases_row->rental_unit_id;
	$property_id = $leases_row->property_id;
	$home_owner_name = $leases_row->home_owner_name;
	$home_owner_email = $leases_row->home_owner_email;
	$home_owner_phone_number = $leases_row->home_owner_phone_number;
	$rental_unit_name = $leases_row->rental_unit_name;
	$property_name = $leases_row->property_name;
	$home_owner_unit_id = $leases_row->home_owner_unit_id;

}



// get the months 

$invoices_month = '';

$items_check_list  = '';
$items = '';
$counter = 0; 
$items_check_pl = '';
// var_dump($query); die();
if($query->num_rows() > 0)
{
	// var_dump($query->result()); die();

	foreach ($query->result() as $month_key) {
		# code...

		$payment_month = $month_key->month;
		$payment_id = $month_key->payment_id;
		$payment_year = $month_key->year;
		$receipt_no = $month_key->receipt_number;
		$payment_date = $month_key->payment_date;


		// get the payments done on that month

		$payments_done = $this->accounts_model->get_owner_payments_done_month($rental_unit_id,$payment_month,$payment_year);

		if($payments_done->num_rows() > 0)
		{
			foreach ($payments_done->result() as $key_payments) {
				# code...
				$date_of_payment = $key_payments->payment_date;
				$receipt_number = $key_payments->receipt_number;
				$total_amount = $key_payments->amount_paid;
				$payment_idd = $key_payments->payment_id;
				$payments_done_month = $key_payments->month;
				$payments_done_year = $key_payments->year;
				$paid_by = $key_payments->paid_by;

				$items_check = '<div class="row">
							        <div class="col-md-12">
								        <strong style="text-decoration:underline;">Receipt Number: '.$receipt_number.'</strong>
								    </div>
								</div>';

				$items_check .= '	<hr>
									'.form_open("update-owner-payment-detail/".$rental_unit_id."/".$payment_idd, array("class" => "form-horizontal")).'
									<div class="row" >
									<div class="col-md-12">
										<div class="col-md-4">
											<div class="form-group">
												<label class="col-md-4 control-label">Date: </label>
											  
												<div class="col-md-8">
													<div class="input-group">
						                                <span class="input-group-addon">
						                                    <i class="fa fa-calendar"></i>
						                                </span>
														<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date'.$payment_idd.'"   value="'.$date_of_payment.'" >
													</div>
												</div>
											</div>
										</div>
								        <div class="col-md-4 ">
									       
									       	<div class="form-group">
												<label class="col-md-4 control-label">Total Amount: </label>
											  
												<div class="col-md-8">
													<input type="number" class="form-control" name="amount_paid_total'.$payment_idd.'" value="'.$total_amount.'" >
												</div>
											</div>
									    </div>
									    <div class="col-md-4 ">
									    	<button class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i> Update Payment</button>
									    </div>
									</div>

								</div>
								'.form_close().'
								<hr>
								';



				$property_invoices = $this->accounts_model->get_property_invoice_types($property_id,0);
				$inputs = '';
				$items_check .= form_open("update-owner-payment/".$rental_unit_id."/".$payment_idd."/".$property_id, array("class" => "form-horizontal"));
				$items_check  .= '<table class="table table-hover table-bordered col-md-12">
												<thead>
													<tr>
														<th>Payment Item</th>
														<th>Receipt Number</th>
														<th>Date of payment</th>
														<th>Amount Paid</th>
														<th>Paid By</th>
													</tr>
												</thead>
											  	<tbody>';
				
				// var_dump($property_invoices->result()); die();							
				if($property_invoices->num_rows() > 0)
				{
					// var_dump($property_invoices->result()); die();
					$total_sum = 0;
					foreach ($property_invoices->result() as $key_types) {
						# code...
						$property_invoice_type_id = $key_types->invoice_type_id;
						$property_invoice_type_name = $key_types->invoice_type_name;
						// var_dump($property_invoice_type_name); die();
						//  get amount paid on that invoice id
						
						$payments_list = $this->accounts_model->get_owner_payments_month_payment($payment_idd,$property_invoice_type_id);
						// var_dump($payments_list); die();
						
						if($payments_list->num_rows() > 0)
						{
							
							foreach ($payments_list->result() as $key_invoice) {
								# code...
								$invoice_type = $key_invoice->invoice_type;

								$invoice_type_id = $key_invoice->invoice_type_id;
								$amount_paid = $key_invoice->amount_paid;
								$payment_item_id = $key_invoice->payment_item_id;
								
								$items_check .= '
												<tr>

													<td>'.$property_invoice_type_name.'</td>
													<td>'.$receipt_number.'</td>
													<td> <input  type="text" class="form-control" name="payment_date_from" value="'.$payment_date.'" readonly> </td>
													<td><input type="number" class="form-control" name="amount_paid'.$payment_item_id.'" value="'.$amount_paid.'"></td>
													<td><input type="text" class="form-control" name="amount_paid" value="'.$paid_by.'" readonly></td>
													
												</tr>
												';
							}

						}
						else
						{
							$items_check .= '
													<tr>

														<td>'.$property_invoice_type_name.'</td>
														<td>'.$receipt_number.'</td>
														<td> <input  type="text" class="form-control" name="payment_date_from" value="'.$payment_date.'" readonly> </td>
														<td><input type="number" class="form-control" name="insert_amount'.$property_invoice_type_id.'" value=""></td>
														<td><input type="text" class="form-control" name="amount_paid" value="'.$paid_by.'" readonly></td>
														
													</tr>
													';
							

						}


						
							
							// var_dump($total_insurance); die();
					}

					$items_check .= '</tbody>
										</table>';
					$items_check .= '<div class="row">
								        <div class="col-md-12 center-align">
									       <button class="btn btn-sm btn-warning"> <i class="fa fa-pencil"></i> Update Entry</button>
									    </div>
									</div>';
				}
				$items_check .= form_close();
					// get all in


				$counter++;
				$items_check_list .= '
									<div class="row receipt_bottom_border">
							        	<div class="col-xs-12" id="title">
							            	<strong>'.date('M Y',strtotime($payment_year.'-'.$payment_month)).' Receipt No. '.$receipt_no.' </strong>
							            </div>
							        </div>
									 '.$items_check.'
									<hr>
									';

			}
		}	
	}
}


?>
 <section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title"><?php echo $title;?> For <?php echo $tenant_name;?> 
		<br>
		<span>Unit Name :</span></td><td><?php echo $rental_unit_name;?> <a href="<?php echo site_url();?>cash-office/accounts" class="btn btn-sm btn-success pull-right"  style="margin-top:-5px;">Back to leases</a> </h2>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				  echo '<div class="alert alert-danger">'.$error.'</div>';
				  $this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
				  echo '<div class="alert alert-success">'.$success.'</div>';
				  $this->session->unset_userdata('success_message');
				}
			 ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<section class="panel panel-featured panel-featured-info">
					<header class="panel-heading">
						
						<h2 class="panel-title">Payments <?php echo date('Y');?>

						</h2>

					</header>
					<div class="panel-body">
                    	<?php echo $items_check_list;?>
					</div>
				</section>
			</div>
		</div> 
	
		<!-- END OF PADD -->
	</div>
</section>
  <!-- END OF ROW -->
<script type="text/javascript">

	function change_date_value() {
		// body...
		alert('sdasda');
	}


  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");
		
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }
</script>