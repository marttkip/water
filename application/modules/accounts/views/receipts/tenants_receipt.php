<?php
	$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		// $units_name = $leases_row->units_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arrears_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;
		$message_prefix = $leases_row->message_prefix;
		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	}

	$lease_pardons = $this->accounts_model->get_lease_pardons($lease_id);
	$total_pardons = 0;
	if($lease_pardons->num_rows() > 0)
	{
		
		foreach ($lease_pardons->result() as $key_pardons) {
			# code...
			$document_number = $key_pardons->document_number;
			$pardon_amount = $key_pardons->pardon_amount;
			$pardon_id = $key_pardons->pardon_id;
			$created_by = $key_pardons->created_by;
			$payment_date = $key_pardons->payment_date;
			$pardon_date = $key_pardons->pardon_date;
			$pardon_reason = $key_pardons->pardon_reason;

			$total_pardons = $total_pardons + $pardon_amount;
		}
	}




	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	if($lease_payments->num_rows() > 0)
	{
		$y = 0;
		foreach ($lease_payments->result() as $key) 
		{
			$payment_id = $key->payment_id;
			$receipt_number = $key->receipt_number;
			$amount_paid = $key->amount_paid;
			$paid_by = $key->paid_by;
			$payment_date = $date_of_payment = $key->payment_date;
			$payment_created = $key->payment_created;
			$payment_created_by = $key->payment_created_by;
			$transaction_code = $key->transaction_code;
			$invoice_month_number = $key->month;
			
			$payment_date = date('jS M Y',strtotime($payment_date));
			$payment_created = date('jS M Y',strtotime($payment_created));
			$y++;
		}
	}



										
		$invoice_month = date('F', mktime(0,0,0,$invoice_month_number, 1, date('Y')));
		// $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
		$this_month = date('m');
		$this_year = date('Y');

		$total_paid = $this->accounts_model->get_months_last_debt($lease_id,$invoice_month_number);
		$last_bal = $this->accounts_model->get_months_last_arrears($invoice_month_number,$this_year,$lease_id);

		$months_invoice_query = $this->accounts_model->get_months_invoices($lease_id,$invoice_month_number);
		$total_bill = 0;
		$total_rental_bill = 0;
		$total_service_charge = 0;
		// var_dump($months_invoice_query->result()); die();
		foreach ($months_invoice_query->result() as $invoice_key) {
			
			$invoice_type = $invoice_key->invoice_type;
			if($invoice_type == 1)
			{
				$rental_invoice_amount = $invoice_key->invoice_amount;
				// rental bill
				 $total_rental_bill = $total_rental_bill + $rental_invoice_amount;
			}
			else
			{
				$service_charge_amount = $invoice_key->invoice_amount;
				// service charge 
				 $total_service_charge = $total_service_charge + $service_charge_amount;
			}

			$total_bill = $total_bill + ($total_rental_bill +$total_service_charge);
		}
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));


$invoice_types = $this->accounts_model->get_payments_invoice_types($payment_idd);


// var_dump($invoice_types); die();
$payment_result = '<div class="row center-align">';
if($invoice_types->num_rows() >0)
{
	$x = 0;
	$total_balance = 0;
	foreach ($invoice_types->result() as $types) {
		# code...
		$invoice_type_name = $types->invoice_type_name;
		$invoice_type_id = $types->invoice_type_id;
		$amount_paid = $this->accounts_model->get_payments_detail($payment_idd,$invoice_type_id);
		// var_dump($amount_paid);die();
		$invoiced_amount = $this->accounts_model->get_invoiced_amount($invoice_type_id);

		$paid_amount = $this->accounts_model->get_payments_amount($invoice_type_id);
		// -8000 +2000)
		$invoice_amount = $invoiced_amount + $amount_paid;
		// $invoiced_amount = $invoiced_amount - $paid_amount;
		// $total_balance = $invoiced_amount - $amount_paid;

		// $invoiced_amount = $this->accounts_model->get_total_invoices_before_payment($lease_id,$date_of_payment,$invoice_type_id= NULL);

		// $payed_amount = $this->accounts_model->get_total_payments_before_payment_lease($lease_id,$date_of_payment,$invoice_type_id=NULL);
		// $items_balance = $invoiced_amount - $payed_amount;
		// $total_amount = $total_amount + $amount_paid;
		// $balance = $items_balance - $amount_paid;

		// $total_balance = $total_balance + $items_balance;
		$total_amount = $total_amount + $amount_paid;

		// var_dump($date_of_payment); die();
		// if($total_balance > 0)
		$x++;
		$payment_result .=
							'
							<div class="row ">
					        	<div class="col-xs-5">
					        		'.$invoice_type_name.'
					        	</div>
					        	<div class="col-xs-5">
					        		'.number_format($amount_paid,2) .'
					        	</div>
					        </div>

							';

	}

	$invoiced_amount = $this->accounts_model->get_total_invoices_before_payment($lease_id,$date_of_payment,$invoice_type_id= NULL);

	$payed_amount = $this->accounts_model->get_total_payments_before_payment_lease($lease_id,$date_of_payment,$invoice_type_id=NULL);
	$items_balance = $invoiced_amount - $payed_amount;

	$payment_result .=
							'

							<div class="row ">
					        	<div class="col-xs-5">
					        		Total Paid
					        	</div>
					        	<div class="col-xs-5 receipt_top_border">
					        	
					        		'.number_format($total_amount,2) .'
					        	</div>
					        </div>

							';

	$payment_result .=
							'

							<div class="row " style="margin-bottom:5px;">
					        	<div class="col-xs-5">
					        		Balance
					        	</div>
					        	<div class="col-xs-5 receipt_bottom_border receipt_top_border" >
					        		<strong class=""> '.number_format($items_balance - $total_amount - $total_pardons ,2) .'</strong>
					        	</div>
					        </div>

							';
}
$payment_result .= '</div>';

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Receipt</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_top_border{border-top: #888888 medium solid; margin-top:1px;}
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
    		<div class="col-xs-10  col-xs-offset-1">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> RECEIPT</h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        	<div class="col-xs-6 ">
		                
		                <p><strong> Name </strong> : <?php echo $tenant_name;?></p>
		                <p><strong> Phone </strong> :<?php echo $tenant_phone_number;?></p>
		                <p><strong> Property </strong> :<?php echo $property_name;?></p>
		                 

		               
		            </div>
		            <div class="col-xs-6">
		             	<p><strong> Receipt </strong> :<?php echo $receipt_number;?></p>
		             	<p><strong> Payment Date </strong> :<?php echo $payment_date;?></p>
		                
		            </div>
		        </div>
		        <div class="row receipt_bottom_border">
		        	<div class="col-md-12 center-align">
		            	<strong>RECEIPTED ITEMS</strong>
		            </div>
		        </div>
		        <div class="row">
			        <div class="col-xs-12">
				        <div class="row center-align">
				        	<div class="col-xs-5">
				        		<strong>Payment For </strong>
				        	</div>
				        	<div class="col-xs-5">
				        		<strong>Paid Amount (KES)</strong>
				        	</div>
				        </div>
				        <?php echo $payment_result;?>
			        </div>
			   	</div>
		        <div class="row " style="border-top:1px #000 solid; ">
		       		<div class="col-xs-8 pull-left">
		       			<strong style="text-align:left;"> <?php echo $message_prefix;?></strong>
		       		</div>  
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		            
		    </div>
		        
		    	
		</div>
		</div>
        
    </body>
    
</html>