<?php
if($query->num_rows()>0)
{
	foreach($query->result() as $row)
	{
		$post_id = $row->post_id;
		$blog_category_name = $row->blog_category_name;
		$blog_category_id = $row->blog_category_id;
		$post_image = $row->post_image;
		$post_content = $row->post_content;
		$post_title = $row->post_title;
		$post_comments = $row->post_comments;
		$post_status = $row->post_status;
		$post_views = $row->post_views;
		$image = base_url().'assets/images/posts/'.$row->post_image;
		$created_by = $row->created_by;
		$modified_by = $row->modified_by;
		$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
		$created = $row->created;
		$day = date('j',strtotime($created));
		$month = date('M Y',strtotime($created));
		$created_on = date('jS M Y H:i a',strtotime($row->created));
		
		$categories = '';
		$count = 0;
		$post_video = $row->post_video;
		
		if(empty($post_video))
		{
			$image = '<img src="'.$image.'" class="pic" alt=""/>';
		}
		
		else
		{
			$image = '<div class="youtube" id="'.$post_video.'"></div>';
		}
	}
}
//get all administrators
	$administrators = $this->users_model->get_all_administrators();
	if ($administrators->num_rows() > 0)
	{
		$admins = $administrators->result();
		
		if($admins != NULL)
		{
			foreach($admins as $adm)
			{
				$user_id = $adm->user_id;
				
				if($user_id == $created_by)
				{
					$created_by = $adm->first_name;
				}
			}
		}
	}
	
	else
	{
		$admins = NULL;
	}

	foreach($categories_query->result() as $res)
	{
		$count++;
		$category_name = $res->blog_category_name;
		$category_id = $res->blog_category_id;
		
		if($count == $categories_query->num_rows())
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>';
		}
		
		else
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>, ';
		}
	}
	$comments_query = $this->blog_model->get_post_comments($post_id);
	//comments
	$comments = 'No Comments';
	$total_comments = $comments_query->num_rows();
	if($total_comments == 1)
	{
		$title = 'comment';
	}
	else
	{
		$title = 'comments';
	}
	
	if($comments_query->num_rows() > 0)
	{
		$comments = '';
		foreach ($comments_query->result() as $row)
		{
			$post_comment_user = $row->post_comment_user;
			$post_comment_description = $row->post_comment_description;
			$date = date('jS M Y H:i a',strtotime($row->comment_created));
			
			$comments .= 
			'
				<li>
					<div class="avatar"><img src="'.base_url().'assets/images/avatar.png" alt="" />
					</div>
					<div class="comment-des">
						<h4 class="fw600">'.$post_comment_user.'</h4>
						<p class="date">'.$date.'</p>
						<p>'.$post_comment_description.'</p>
					</div>
					<div class="clearfix"></div>
				</li>
			';
		}
	}


?>
			<!-- inner banner -->
            <section class="bg-white inner-banner pages">

                <div class="container pad-container">
                    <div class="col-md-12">
                        <h1>Blog</h1>
                    </div>
                </div>

            </section>
            <section>

                <div class="container pad-container2">

                    <div class="row">

                        <!-- blog posts -->
                        <div class="col-sm-8 content">

                            <!-- blog post full -->
                            <div class="post">
								<div class="post-content nomgr">
                                        <div class="img">
                                            <div class="date"><?php echo $month;?></div>
                                            <?php echo $image;?>
                                        </div>   

                                <div class="post-format nobg">

                                    <h2><a href="#"><?php echo $post_title;?></a></h2>
                                    <div class="meta">
                                        <ul>
                                           
                                           <li><a href="#">Posted by Admin</a></li>
											<?php echo $categories;?>
											<li><a href="#"><?php echo $total_comments;?> <?php echo $title;?></a></li>
                                        </ul>
                                    </div>

                                    <?php echo $description;?>

                                </div>

                            </div>
                            </div>
                            <!-- / blog post full -->

                            <!-- Comments -->
                            <div class="comments-sec t-pad30">
                                <ol class="commentlist">

                                    <?php echo $comments;?>

                                </ol>
                            </div>
                            <!-- /comments -->

                            <!-- post comments -->
                            <div class="comment-form-wrapper">

                                <h3 class="fw600">Post your comments here</h3>
                                <p>Your email address will not be published. Required fields are marked *</p>
								
								<form id="add-comment" class="add-comment" action="<?php echo site_url().'site/blog/add_comment';?>" method="post" >
                                    <fieldset>
                                        <div>
                                            <label>Name:</label>
                                            <input type="text" value="" name="name" required />
                                        </div>
                                        <div>
                                            <label>Email: <span>*</span>
                                            </label>
                                            <input type="text" value="" name="email" required/>
                                        </div>
                                        <div>
                                            <label>Comment: <span>*</span>
                                            </label>
                                            <textarea cols="40" rows="3" name="post_comment_description" required></textarea>
                                        </div>
                                    </fieldset>
                                    <input type="submit" class="btn-submit" value="Post Comment" />
                                    <div class="clearfix"></div>
                                </form>

                            </div>
                            <!-- /post comments -->

                        </div>
                        <!--/ blog posts -->

                        <!-- sidebar -->
                        <div class="col-sm-4 sidebar">
							<?php echo $this->load->view('blog/includes/side_bar', '', TRUE);?>
                        </div>
                        <!--/ sidebar -->
                    </div>

                </div>

            </section>
            
                 