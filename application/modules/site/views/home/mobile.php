		<!-- START SOCIAL SECTION -->
        <section class="section-dark" id="social" data-parallax="scroll" data-image-src="<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/banner_background.jpg">      <!-- Set the URL of your section background image to "data-image-src" attr. -->    
            <div class="section-content">
                <div class="container">

                    <!-- START SECTION HEADER -->
                    <h2 class="reveal reveal-top">We Are Social</h2>
                    <div class="subheading reveal reveal-top" >
                        Follow us, like us, watch us
                    </div>
                    <!-- END SECTION HEADER -->
                    
                    <!-- START SOCIAL LINKS -->
                    <ul class="social-content">

                        <li class="reveal reveal-animate">
                            <a class="social-link" href="index.html#">
                                <div class="social-icon-holder">
                                    <span class="social-icon fa fa-twitter-square"></span>
                                </div>
                                <span class="social-name">Twitter</span>
                            </a>
                        </li>

                        <li class="reveal reveal-animate">
                            <a class="social-link" href="index.html#">
                                <div class="social-icon-holder">
                                    <span class="social-icon social-icon-symbol fa fa-facebook"></span>
                                </div>
                                <span class="social-name">Facebook</span>
                            </a>
                        </li>

                        <li class="reveal reveal-animate">
                            <a class="social-link" href="index.html#">
                                <div class="social-icon-holder">
                                    <span class="social-icon social-icon-symbol fa fa-youtube-square"></span>
                                </div>
                                <span class="social-name">YouTube</span>
                            </a>
                        </li>
                    </ul>
                    <!-- END SOCIAL LINKS -->
                    
                </div>
            </div>
        </section>
        <!-- END SOCIAL SECTION -->
        
        <!-- START UPDATES SECTION -->
        <section class="section-light section-updates" id="updates">
            <div class="container">
                
                <!-- START SECTION HEADER -->
                <h2 class="reveal reveal-top">How It Works</h2>
                <div class="subheading reveal reveal-top">
                    It's as easy as 1, 2, 3
                </div>
                <!-- END SECTION HEADER -->
                
                <!-- START UPDATES STEPS -->
                <div class="updates-content reveal reveal-bottom">

                    <div class="step step-first">
                        <div>
                            <div class="name name-lg">Download The App</div>
                            <p>Download the app from the Google Play Store or the Apple Store.</p>
                        </div>
                        <div class="disk btn-open" data-toggle="modal" data-target=".modal-updates">
                            <svg class="svg-icon play-icon" height="47" width="37" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 47">
                            <path stroke-linejoin="round" stroke-linecap="round" stroke-width="3" stroke="#CF1248" fill="#ffffff" fill-rule="evenodd" d="M 3 3 C 3 3 3 44 3 44 C 3 44 34 22.13 34 22.13 C 34 22.13 3 3 3 3 Z" />
                            </svg>
                        </div>
                    </div>
                    <div class="step step-right">
                        <div class="step-content">
                            <div class="name">Sign Up or Log In</div>
                            <p>Register with your details or log in if you've already registered before.</p>
                        </div>
                        <div class="disk"></div>
                        <div class="step-decoration">
                            <div class="mask line-1"></div>
                            <div class="mask curve-1"></div>
                            <div class="mask line-2"></div>
                        </div>
                    </div>

                    <div class="step step-left">

                        <div class="step-left">
                            <div class="step-content">
                                <div class="name name-lg">Search For Your Location</div>
                                <p>Enter your start location and desired destination.</p>
                            </div>
                            <div class="disk"></div>
                        </div>

                        <div class="step step-center">
                            <div class="step-content">
                                <div class="name">Request For A Biker</div>
                                <p>Choose between a ride and a delivery and request for your biker.</p>
                            </div>
                            <div class="disk"></div>
                        </div>

                        <div class="step step-center step-center-bottom">
                            <div class="step-content">
                                <div class="name">Enjoy Your Ride</div>
                                <p>Thank you for using Ubiker.</p>
                            </div>
                            <div class="disk"></div>
                        </div>
                        
                        <div class="step-decoration">
                            <div class="mask line-3"></div>
                            <div class="mask curve-2"></div>
                            <div class="mask line-4"></div>
                        </div>

                    </div>

                    <div class="step step-right">
                        <div class="step-content">
                            <div class="name">Simple</div>
                        </div>
                        <div class="disk"></div>
                        <div class="step-decoration">
                            <div class="mask line-5"></div>
                            <div class="mask curve-3"></div>
                            <div class="mask line-6"></div>
                        </div>
                    </div>
                    
                    <div class="step step-last">
                        <div class="step-content">
                            <div class="name name-lg">Convenient</div>
                        </div>
                        <div class="disk">
                            <svg class="arrow-icon svg-icon" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 38 47">
                            <path stroke-linejoin="round" stroke-linecap="round" stroke-width="3" stroke="#CF1248" fill="none" fill-rule="evenodd" d="M 19 1 C 19 1 19 45 19 45 M 18 45 C 18 45 1 25 1 25 M 20 45 C 20 45 36 26 36 26" />
                            </svg>
                        </div>
                    </div>

                </div>
                
            </div>
            <!-- END UPDATES STEPS -->
            
        </section>
        <!-- END UPDATES SECTION -->
        