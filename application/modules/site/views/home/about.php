		<section class="section-light" id="features">
            <div class="container">
                
                <!-- START SECTION HEADER -->
                <h2 class="reveal reveal-top">Features</h2>
                <!-- END SECTION HEADER -->
                
                <div class="features">
                    <div class="features-left reveal reveal-left">
                        
                        <div class="feature">
                            <div class="feature-disk">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="feature-name">Mark on the Map</div>
                            <div class="feature-desc">
                                <p>Lorem ipsum dolor sit amet,
                                    consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore</p> 
                            </div>
                        </div>
                        
                        <div class="feature">
                            <div class="feature-disk">
                                <i class="fa fa-inbox"></i>
                            </div>
                            <div class="feature-name">Request Delivery</div>
                            <div class="feature-desc">
                                <p>Duis aute irure dolor in
                                    reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur</p>
                            </div>
                        </div>
                        
                        <div class="feature">
                            <div class="feature-disk">
                                <i class="fa fa-motorcycle"></i>
                            </div>
                            <div class="feature-name">Request Ride</div>
                            <div class="feature-desc">
                                <p>But I must explain to you how 
                                    all this mistaken idea of denouncing 
                                    pleasure and praising</p>
                            </div>
                        </div>
                        
                    </div>
                    
                    <!-- START FEATURES DECORATION-->
                    <div class="features-decoration reveal reveal-bottom" style="background-image: url('<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/phone-black.png')">
                        <div class="image-holder">
                            <!-- Set your decoration image here. -->
                            <img src="<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/features_mobile.jpg" alt="features image"/>
                        </div>
                    </div>
                    <!-- END FEATURES DECORATION-->

                    <div class="features-right reveal reveal-right">
                        <div class="feature">
                            <div class="feature-disk">
                                <i class="fa fa-smile-o"></i>
                            </div>
                            <div class="feature-name">Convenient</div>
                            <div class="feature-desc">
                                <p>Duis aute irure dolor in reprehenderit 
                                    in voluptate velit esse cillum dolore 
                                    eu fugiat nulla pariatur.</p>
                            </div>
                        </div>
                        
                        <div class="feature">
                            <div class="feature-disk">
                                <i class="fa fa-money"></i>
                            </div>
                            <div class="feature-name">Affordable</div>
                            <div class="feature-desc">
                                <p>Lorem ipsum dolor sit amet,
                                    consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore</p>
                            </div>
                        </div>
                        
                        <div class="feature">
                            <div class="feature-disk">
                                <i class="fa fa-lock"></i>
                            </div>
                            <div class="feature-name">Safe</div>
                            <div class="feature-desc">
                                <p>Sed ut perspiciatis unde omnis iste 
                                    natus error sit voluptatem accusantium 
                                    doloremque laudantium</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </section>
		
		<!-- START DOWNLOADS SECTION -->
        <section class="section-dark" id="download" data-parallax="scroll" data-image-src="<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/download_background.jpg">      <!-- Set the URL of your section background image to "data-image-src" attr. -->
            <div class="section-content">
                <div class="container">
                    
                    <!-- START SECTION HEADER -->
                    <h2 class="reveal reveal-top">Download App</h2>
                    <div class="subheading reveal reveal-top" >
                        Get your Ubiker today
                    </div>
                    <!-- END SECTION HEADER -->
                    
                    <!-- START SECTION CONTENT -->
                    <div class="stores clearfix">
                        <a href="index.html#" class="store reveal reveal-left">
                            <span class="store-icon fa fa-apple"></span>
                            <span class="store-name">App Store</span>
                        </a>

                        <a href="index.html#" class="store reveal reveal-bottom">
                            <svg class="store-icon" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 51">
                                <path fill-rule="evenodd" d="M 3 0 C 3 0 36 17 36 17 C 36 17 28 25 28 25 C 28 25 3 0 3 0 Z M 0 1 C 0 1 26 26 26 26 C 26 26 26 27 26 27 C 26 27 1 51 1 51 C 1 51 0 50 0 50 C 0 50 0 1 0 1 Z M 29 29 C 29 29 34 35 34 35 C 34 35 7 50 7 50 C 7 50 6 50 6 50 C 6 50 28 29 28 29 C 28 29 29 29 29 29 M 30 27 C 30 27 36 33 36 33 C 36 33 38 33 38 33 C 38 33 48 27 48 27 C 48 27 48 25 48 25 C 48 25 38 19 38 19 C 38 19 30 27 30 27 Z"/>
                            </svg>
                            <span class="store-name">Google Play</span>
                        </a>

                        <a href="index.html#" class="store reveal reveal-right">
                            <svg class="store-icon" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 52">
                                <path fill-rule="evenodd" d="M 37.49 51.99 C 37.49 51.99 0 44.6 0 44.6 C 0 44.6 0 17.71 0 17.71 C 0 17.71 37.49 10.31 37.49 10.31 C 37.49 10.31 44.98 13 44.98 13 C 44.98 13 44.98 49.3 44.98 49.3 C 44.98 49.3 37.49 51.99 37.49 51.99 Z M 16.38 22.61 C 16.38 22.61 8.06 23.77 8.06 23.77 C 8.06 23.77 8.06 30.04 8.06 30.04 C 8.06 30.04 16.38 30.04 16.38 30.04 C 16.38 30.04 16.38 22.61 16.38 22.61 Z M 16.38 31.07 C 16.38 31.07 8.06 31.07 8.06 31.07 C 8.06 31.07 8.06 37.88 8.06 37.88 C 8.06 37.88 16.38 39.03 16.38 39.03 C 16.38 39.03 16.38 31.07 16.38 31.07 Z M 27.91 21.04 C 27.91 21.04 17.43 22.55 17.43 22.55 C 17.43 22.55 17.43 30.04 17.43 30.04 C 17.43 30.04 27.91 30.04 27.91 30.04 C 27.91 30.04 27.91 21.04 27.91 21.04 Z M 27.91 31.07 C 27.91 31.07 17.43 31.07 17.43 31.07 C 17.43 31.07 17.43 39.1 17.43 39.1 C 17.43 39.1 27.91 40.61 27.91 40.61 C 27.91 40.61 27.91 31.07 27.91 31.07 Z M 29.33 9.18 C 29.33 6.16 29.39 4.71 25.83 5.22 C 25.78 4.73 25.71 4.28 25.62 3.86 C 29.93 3.23 30.67 4.49 30.67 9.18 C 30.67 9.18 30.67 10.03 30.67 10.03 C 30.67 10.03 29.33 10.3 29.33 10.3 C 29.33 10.3 29.33 9.18 29.33 9.18 Z M 17.72 12.59 C 17.72 12.59 17.72 10.53 17.72 10.53 C 17.72 6.24 19.39 5.57 21.66 4.78 C 21.75 5.17 21.8 5.63 21.81 6.18 C 19.82 7.02 19.05 7.63 19.09 10.54 C 19.09 10.54 19.09 12.32 19.09 12.32 C 19.09 12.32 17.72 12.59 17.72 12.59 Z M 23.18 6.28 C 23.18 4.02 22.82 0.48 17.25 1.77 C 13.55 2.62 11.55 3.8 11.59 7.62 C 11.59 7.62 11.59 13.8 11.59 13.8 C 11.59 13.8 10.23 14.07 10.23 14.07 C 10.23 14.07 10.23 7.49 10.23 7.49 C 10.23 1.91 13.64 1.07 16.98 0.32 C 23.18 -1.07 24.53 2.13 24.54 6.39 C 24.54 6.39 24.54 11.24 24.54 11.24 C 24.54 11.24 23.18 11.51 23.18 11.51 C 23.18 11.51 23.18 6.28 23.18 6.28 Z"/>
                            </svg>
                            <span class="store-name">Windows Phone</span>
                        </a>
                    </div>
                    <!-- END SECTION CONTENT -->

                </div>
            </div>
        </section>
        <!-- END DOWNLOADS SECTION -->

        