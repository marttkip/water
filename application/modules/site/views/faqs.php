<?php
    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $facebook = $contacts['facebook'];
        $twitter = $contacts['twitter'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $phone = $contacts['phone'];
        $address = $contacts['address'];
        $post_code = $contacts['post_code'];
        $city = $contacts['city'];
        $building = $contacts['building'];
        $floor = $contacts['floor'];
        $location = $contacts['location'];

        $working_weekday = $contacts['working_weekday'];
        $working_weekend = $contacts['working_weekend'];

        $mission = $contacts['mission'];
        $vision = $contacts['vision'];
        $about = $contacts['about'];
        $core_values = $contacts['core_values'];
    }
?>

			<div role="main" class="main shop">

				<section class="page-header">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">
									<?php echo $this->site_model->get_breadcrumbs();?>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h1><?php echo $title;?></h1>
							</div>
						</div>
					</div>
				</section>

				
				<div class="container">

					<h2>Frequently Asked <strong>Questions</strong></h2>

					<div class="row">
						<div class="col-md-12">

							<div class="toggle toggle-primary" data-plugin-toggle>
								<section class="toggle active">
									<label>What is a Dobi?</label>
									<p>A Dobi is a washer. Specifically someone who washes clothes</p>
								</section>

								<section class="toggle">
									<label>Why should I become a Dobi?</label>
									<h3>Become a Dobi</h3>
                                    <p>Wash with Dobi and earn great money as an independent agent. Get paid weekly for helping people in your neighborhood get their laundry cleaned. Become your own boss and get paid for washing services at your own schedule. There has never been a better side hustle. There is nothing wrong in making some extra cash.</p>
                                    <h3>Make good money</h3>
                                    <p>Got a Washing Machine and dryer? Turn it into a money machine. Everyday, Everywhere clothes are getting dirty and Dobi makes it easy for you to cash in on that right in your neigbourhood. Plus, you've already got everything you need to get started.</p>
                                    <h3>On your own terms</h3>
                                    <p>Need something outside the normal office hours? As an independent contractor with Dobi, you’ve got freedom and flexibility to do laundry whenever you have time. Set your own schedule, so you can be there for all other things that matter to you.</p>
                                    <h3>No office no boss</h3>
                                    <p>Whether you’re supporting your family, saving for something big or just chasing that paper, Dobi gives you the freedom make some money, when you want to, however much you want to.  Choose when you are available, when to wash and when to enjoy your earnings.
</p>
								</section>

								<section class="toggle">
									<label>What do I need to know about becoming a Dobi?</label>
									<ol>
                                    	<li>As a “Dobi”, you may list your laundry services. To create a Listing, you will be asked a variety of questions about the machines to be listed, including, but not limited to, the location, capacity, size, features, and availability.
                                            <ul>
                                                <li>In order to be featured in Listings via the Site, Application and Services, all Dobi’s must have valid physical addresses. </li>
                                                <li>Listings will be made publicly available via the Site, Application and Services.</li>
                                                <li>Other Members will be able to book laundry services via the Site, Application and Services based upon the information provided in your Listing. </li>
                                            </ul>
                                      	</li>
                                        <li>You decided when you are available and when you are not! Turn availability off if you don’t want to do any laundry.</li>
                                        <li>You understand and agree that once a client requests laundry services by you, you may not request them to pay a higher price than in the order confirmation received during the time of booking.</li>
                                        <li>Only wash as many items as shown in the order confirmation email. If a client brings more items, please do not accept and can ask them to book them through the system. Should you make a local agreement with a client and not through us, then our binding terms of service will no longer be protected up should a dispute arise.</li>
                                        <li>You understand and agree that Dobi does not act as an insurer for your machines.</li>
                                        <li>In no way shall Dobi be liable for your machines wear and tear costs, repair costs, water bills, electricity bills and any other monies associated with doing the laundry for clients.</li>
                                        <li>You the Dobi agree to liase with the client regarding pick up and drop off points for the laundry.</li>
                                        <li>You agree to return laundry to the clients, within twelve (12) hours of receipt of the dirty laundry
                                        	<ul>
                                            	<li>Should you be experiencing slight delays, (not more than 4 hours) please inform the client</li>
                                                <li>If experiencing extreme delays (more than 4 hours) please contact the Dobi team on info@dobi.co.ke and we’ll outsource that job for you.</li>
                                                <li>You will not be paid any money for any laundry not done.</li>
                                            </ul>
                                        </li>
                                        <li>We are solely responsible for collecting payments on your behalf and remitting them once weekly to your bank account or mobile phone.</li>
                                        <li>We will also mediate any disputes between you and the clients should any disagreements arise during washing and /or delivery of your clothes</li>
                                        <li>Dobi Payments serves as the limited authorized payment collection agent of the Dobis for the purpose of accepting, on behalf of the Dobi, payments from clients of such amounts stipulated by our site.</li>
                                        <li>The minimum order size is for a wash and fold is KES 1000. For any orders over KES 2000 folding is free. Laundry services over the weekend might be slightly higher on the prices.</li>
                                        <li>Standard extra options are:-
                                        	<ul>
                                            	<li>Folding KES 200; Folding for orders of KES 2000 and above is included free of charge</li>
                                                <li>Ironing KES 600</li>
                                                <li>Delivery within 1 km radius KES 400</li>
                                            </ul>
                                        </li>
                                        <li><p>DAMAGED ITEMS. Taking care of your clients garments should be your number one priority. While you are very cautious to treat all garments carefully, you cannot guarantee against color loss, bleeding or shrinkage of garments. In addition, you do not take responsibility for any deteriorated or flawed garments, which could result in small holes or tears. For any items deemed damaged, Dobi may reimburse the by paying up to five (5) times the charge for cleaning the item regardless of brand, price or condition of the garment. Any damaged items must be reported to support@dobi.co.co.ke and inspected verified by you and the client.</p><p>Dobi’s agreement with the clients protects you should any damage occur to their garments (booked through our site) during washing.</p></li>
                                    </ol>

								</section>
                                
							</div>
                            
                            <p>Not bad terms after all. Don’t let this opportunity pass you by. Sign up today and make some money.</p>
                            
						</div>

					</div>

				</div>

			</div>
			
			<!--new template--!>

<!-- Top -->
<div class="over_header"> 
    <div class="container">
        <div class="row">
            <div class="over_header_ins col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="sotials_top col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<a class="facebook" href="<?php echo $facebook;?>"><img src="images/f_b.png" alt=""/></a>
						<a class="twitter" href="<?php echo $twitter;?>"><img src="images/t_b.png" alt=""/></a><a class="youtube" href="faq.html#"><img src="images/y_b.png" alt=""/></a><a class="google" href="faq.html#"><img src="images/g_b.png" alt=""/></a></div>
                    <div class="text-right col-lg-10 col-md-10 col-sm-11 col-xs-12">
                        <span class="hr_last"><img src="images/phone.png" alt=""/><?php echo $phone;?></span>
                        <span><img src="images/mail.png" alt=""/><?php echo $email;?></span>
                        <span><img src="images/adress.png" alt=""/><?php echo $address;?></span> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Header -->
<header class="header_type_2">
<div class="header-wrapper2 page_header">
<div id="fixblock" class="fixblock">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12"><a class="logo" href="../../webdesign-finder_default.html"><img src="images/logo_2.png" alt=""/></a></div>
                <nav class="menu col-lg-6 col-md-6 col-sm-4 col-xs-12">
                    <div class="menu-button"><i class="fa fa-bars"></i></div>
                        <ul class="flexnav" data-breakpoint="">
                            <li><a href="../../webdesign-finder_default.html">Home</a></li>
                            <li><a class="active" href="faq.html#features">Features</a>
                                 <ul> 
                                    <li> 
                                        <a href="typography.html">Typography</a>
                                        <a href="support.html">Support</a>
                                        <a href="faq.html">FAQ</a>
                                    </li>   
                                </ul> 
                            </li>
                            <li><a href="faq.html#services">Services</a>
                                <ul> 
                                    <li> 
                                        <a href="faq.html#">General Features</a>
                                        <a href="services.html">Indirect Tax</a>
                                        <a href="faq.html#">Inheritance tax</a>
                                        <a href="faq.html#">International Tax</a>
                                        <a href="faq.html#">Large Corporate</a>
                                        <a href="faq.html#">Personal tax</a>
                                    </li>   
                                </ul> 
                            </li>
                            <li><a href="faq.html#pages">Pages</a>
                                <ul> 
                                    <li> 
                                        <a href="about.html">About Us</a>
                                        <a href="team.html">Team</a>
                                        <a href="team-member.html">Team Members</a>
                                        <a href="clients.html">Our Clients</a>
                                        <a href="404.html">404</a>
                                        <a href="pricing.html">Pricing</a>
                                    </li>   
                                </ul> 
                            </li>
                            <li><a href="faq.html#blog">Blog</a>
                                <ul> 
                                    <li> 
                                        <a href="blog.html">our blog</a>
                                        <a href="blog-post.html">single post</a>
                                        <a href="faq.html#">video post</a>
                                    </li>   
                                </ul> 
                            </li>
                            <li><a href="contact_us.html">Contact us</a></li> 
                        </ul> 
                </nav>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12"><a class="button_flat button_flat_menu" href="faq.html#">Find adviser</a></div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="breadcrumbs">  
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="breadcrumb">
						<?php echo $this->site_model->get_breadcrumbs();?>
					</ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header end -->

<!-- Content blocks -->
<section class="faq-section">
    <div class="container">
        <div class="row row-15">
                
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
                
                  <div class="row">
        
            <div class="col-sm-6">

                <div class="panel-group" id="accordion1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion1" href="faq.html#collapse1" >
                                    Lorem ipsum dolor sit amet, consetetur?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion1" href="faq.html#collapse2" class="collapsed">
                                    Ad venison jowl, leberkas fatback porchetta?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                               Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion1" href="faq.html#collapse3" class="collapsed">
                                    Voluptate shankle corned beef, ullamco pork eu?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion1" href="faq.html#collapse4" class="collapsed">
                                    Shoulder minim doner, ea veniam incididunt?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion1" href="faq.html#collapse5" class="collapsed">
                                   Doner nulla biltong, cupidatat beef ribs drumstick?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-sm-6">
                <div class="panel-group" id="accordion2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="faq.html#collapse21" class="collapsed">
                                    Esse salami filet mignon nisi occaeca?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse21" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="faq.html#collapse22" class="collapsed">
                                    Tri-tip in incididunt spare ribs dolor leberkas?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse22" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut. </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="faq.html#collapse23" class="collapsed">
                                    Est velit nostrud laborum. Rump commodo?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse23" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="faq.html#collapse24" class="collapsed">
                                    Velit venison reprehenderit beef ribs porchetta?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse24" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="faq.html#collapse25" class="collapsed">
                                    in nostrud in, do picanha porchetta?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse25" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="faq.html#collapse26" class="collapsed">
                                    Strip steak occaecat incididunt ribeye?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse26" class="panel-collapse collapse">
                            <div class="panel-body">
                                Tail velit fatback, cupim salami nostrud biltong picanha culpa in alcatra all tip. Chuck in magna biltong id 
                         fatback hamburger commodo rump beef a chicken brisket ex frankfurter ut.</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
                
            </div>
             
        </div>
    </div>
</section>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row row-15">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="footer_sec_1 footer-text col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <h6>About us</h6>
                    
                    <p>But because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. 
                    Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because 
                    occasionally circumstances</p>
                    <span><img src="images/adress.png" alt=""/>528 tenth Avenue Boston, BT 58966</span>
                    <span><img src="images/mail.png" alt=""/>Support@gmail.com</span>
                    <span><img src="images/phone.png" alt=""/>1-800-300-2121</span>
                    <a class="first_link facebook" href="faq.html#"><img src="images/f_b.png" alt=""/></a><a  class="twitter" href="faq.html#"><img src="images/t_b.png" alt=""/></a><a class="youtube" href="faq.html#"><img src="images/y_b.png" alt=""/></a><a class="google" href="faq.html#"><img src="images/g_b.png" alt=""/></a>
                </div>   
                <div class="footer_sec_2 footer-text col-lg-4 col-md-3 col-sm-6 col-xs-12">
                    
                    <h6>Our blog</h6>
                    <strong><a href="faq.html#">Generating Traffic Through Search</a></strong>
                    <p>But who has any right to find fault with a man who chooses memories</p>
                    <span>25 / 04 /2016</span>
                    <hr />
                    <strong><a href="faq.html#">Today May Be Just Like Any Other Day!</a></strong>
                    <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae et molestiae non recusandae</p>
                    <span>25 / 04 /2016</span>
                    
                    
                </div>   
                <div class="footer_sec_3 footer-text col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <h6>Get Free Quote</h6>
                    <div class="input input1">
                        <input type="text" id="posName" placeholder="Name"/>
                    </div>
                    <div class="input input2">
                        <input class="input_2" type="text" id="posEmail" placeholder="Email"/>
                    </div>
                    <div class="input input_text">
                        <textarea id="posText" placeholder="Comment"></textarea>
                    </div>
                    <div class="button_footer"><button type="button" class="button_fat" id="send">Submit</button></div>                
                    <div class="span4">
            	    	<div class="lod_bar" id='loadBar'></div>
            	    </div>
                </div>   
            </div>
        </div>
    </div>
</footer>
<div class="copiright"> 
<span>tax adviser -  SEO &#38; WP Marketing Agency &#169; 2016 All Rights Reserved</span>
<a href="faq.html#">Made by <img src="images/mw.png" alt=""/> mw templates</a> 
</div>
    
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Flexnav plugin -->
    <script src="js/flexnav.js"></script>
    <!-- Slider plugin -->
    <script src="js/owl.carousel.js"></script>
    <!-- Custom -->
    <script src="js/custom.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script> 



</body>
</html> 