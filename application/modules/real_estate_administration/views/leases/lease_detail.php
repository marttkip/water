<?php
	
$all_leases = $this->leases_model->get_lease_detail($lease_id);
// var_dump($all_leases->result()); die();
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_id = $leases_row->tenant_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date =$leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_email = $leases_row->tenant_email;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$created = $leases_row->created;
		$expense_amount = $leases_row->expense_amount;
		$closing_end_date = $leases_row->closing_end_date;
		$remarks = $leases_row->remarks;
		$closing_water_reading = $leases_row->closing_water_reading;
		$lease_end_date = $leases_row->lease_end_date;


		// $lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		// $lease_end_date  = date('jS M Y', strtotime($lease_end_date));
		
		$total_due = $rent_amount*12;

		$total_paid = 6000;
		$lease_start = $leases_row->lease_start_date;
		// var_dump($leases_row); die();


		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Inactive Lease</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active Lease</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}
		else if($lease_status == 2)
		{
			$status = '<span class="label label-success">Terminated Lease</span>';
		}

		
	}

	$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
	$total_arrears = $tenants_response['total_arrears'];
	$total_invoice_balance = $tenants_response['total_invoice_balance'];
	$total_payment_amount = $tenants_response['total_payment_amount'];
	$total_pardon_amount = $tenants_response['total_pardon_amount'];

	$accounts_response = $this->accounts_model->get_total_invoices_month($lease_id);

	$total_water = $accounts_response['total_water'];
	$total_rent = $accounts_response['total_rent'];
	$total_deposit = $accounts_response['total_deposit'];
	$total_service_charge = $accounts_response['total_service_charge'];
	$total_bill = $accounts_response['total_bill'];
	$deposit_invoice_amount = $accounts_response['deposit_invoice_amount'];
?>
<!-- <div class="row"> -->
	<div class="col-md-12">
		<a href="<?php echo site_url();?>lease-manager/leases" class="btn btn-sm btn-warning pull-right"  ><i class="fa fa-arrow-left"></i> Back to leases</a> 
	</div>
<!-- </div> -->

<div class="row">
	<div class="col-md-12"> 
		<div class="col-md-4"> 
			<section class="panel">
				<header class="panel-heading">						
					<h2 class="panel-title">Lease details </h2>
				</header>
				<div class="panel-body">
					<div class="table-responsive">
						<?php echo form_open("update-lease-details/".$lease_id, array("class" => "form-horizontal", "role" => "form"));?>
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th style="width: 40%;">Title</th>
									<th style="width: 60%;">Detail</th>
								</tr>
							</thead>
						  	<tbody>
						  		<tr><td><span>Lease Number :</span></td><td><?php echo $lease_number;?></td></tr>
						  		<tr><td><span>Lease Start date :</span></td><td>
			                        <div class="input-group">
			                            <span class="input-group-addon">
			                                <i class="fa fa-calendar"></i>
			                            </span>
			                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="lease_start_date" value="<?php echo $lease_start;?>" placeholder="Report Date From" >
			                        </div>
						  			</td></tr>
						  		<tr><td><span>Lease Expiry date :</span></td>
						  			<td>
						  				<div class="input-group">
				                            <span class="input-group-addon">
				                                <i class="fa fa-calendar"></i>
				                            </span>
				                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="lease_end_date" value="<?php echo $lease_end_date;?>" placeholder="Report Date From" >
				                        </div>	
						  			</td>
						  		</tr>
						  		<tr><td><span>Lease Status :</span></td><td><?php echo $status;?></td></tr>
						  		<tr><td><span>Property Name :</span></td><td><?php echo $property_name;?> - <?php echo $rental_unit_name;?></td></tr>
						  		

						  		
						  	</tbody>
						</table>

						  <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
						  <?php 
						  if($lease_status < 2)
						  {
						  ?>
							<div class="col-md-12">
								<div class="form-actions center-align">
						            <button class="submit btn btn-primary btn-sm" type="submit">
						                UPDATE DETAILS 
						            </button>
						        </div>
							</div>
							<?php
						}
							?>

            			<?php echo form_close();?>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-4"> 
			<section class="panel">
				<header class="panel-heading">						
					<h2 class="panel-title">Tenancy Info </h2>
				</header>
				<div class="panel-body">
					<div class="table-responsive">
						<?php echo form_open("update-tenant-details/".$tenant_id, array("class" => "form-horizontal", "role" => "form"));?>
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th style="width: 40%;">Title</th>
									<th style="width: 60%;">Detail</th>
								</tr>
							</thead>
						  	<tbody>
						  		<tr><td><span>Tenant Name :</span></td><td>
						  			<input type="text" class="form-control" name="tenant_name" value="<?php echo $tenant_name;?>"></td></tr>
						  		<tr><td><span>Tenant Phone :</span></td><td>
						  			<input type="text" class="form-control" name="tenant_phone_number" value="<?php echo $tenant_phone_number;?>"></td></tr>
						  		<tr>
						  			<td><span>Tenant National Id :</span></td><td>
						  			<input type="text" class="form-control" name="tenant_national_id" value="<?php echo $tenant_national_id;?>">
						  			</td>
						  		</tr>
						  		<tr>
						  			<td><span>Tenant Email :</span></td><td>
						  			<input type="text" class="form-control" name="tenant_email" value="<?php echo $tenant_email;?>">
						  			</td>
						  		</tr>
						  		
						  	</tbody>
						  </table>
						  <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
						  <?php 
						  if($lease_status < 2)
						  {
						  ?>
						  <div class="col-md-12">
								<div class="form-actions center-align">
						            <button class="submit btn btn-primary btn-sm" type="submit">
						                UPDATE DETAILS 
						            </button>
						        </div>
							</div>
							<?php
						}
							?>

            			<?php echo form_close();?>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-4"> 
			<section class="panel">
				<header class="panel-heading">						
					<h2 class="panel-title">Current Account State</h2>

				</header>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>Title</th>
									<th>Detail</th>
								</tr>
							</thead>
						  	<tbody>
						  		<tr><td><span>Deposit :</span></td><td><strong>KES. <?php echo number_format(($deposit_invoice_amount),2);?></strong> </td></tr>
						  		<tr><td><span>Rent + SC + Deposit Bal + Legal :</span></td><td><strong>KES. <?php echo number_format(($total_rent + $total_service_charge + $total_deposit),2);?></strong> </td></tr>
						  		<tr><td><span>Water Balance :</span></td><td><strong>KES. <?php echo number_format(($total_water),2);?></strong> </td></tr>						  		
						  		<tr><td><span>Total Pardons :</span></td><td>KES <?php echo number_format($total_pardon_amount,2);?></td></tr>
						  		<tr><td><span>Expenses :</span></td><td><strong>KES. <?php echo number_format(($expense_amount),2);?></strong> </td></tr>
						  		<tr><td><span>Total Balance :</span></td><td><strong>KES. <?php echo number_format($total_arrears+$expense_amount,2);?></strong> </td></tr>


						  	</tbody>
						  </table>
						  <?php 
						  if($lease_status < 2)
						  {
						  ?>
						  <div class="col-md-12">
								<div class="form-actions center-align">
						            <a href="<?php echo site_url()?>update-billing/<?php echo $lease_id?>" class="btn btn-primary btn-sm" onclick="return confirm('Do you really want to update billing?');">
						                UPDATE ACCOUNT 
						            </a>
						        </div>
							</div>
						<?php
						}
						?>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12"> 
		<div class="col-md-7"> 
			<section class="panel">
				<header class="panel-heading">						
					<h2 class="panel-title">Invoicing Detail </h2>
				</header>

				<div class="panel-body">
					<div class="row" style="margin-bottom:20px;">
						<?php 
						  if($lease_status < 2)
						  {
						  ?>
						<?php echo form_open("add-lease-charge/".$lease_id, array("class" => "form-horizontal", "role" => "form"));?>
            			<div class="col-lg-12 col-sm-12 col-md-12">
            				<div class="row">
                				<div class="col-md-12">

                					<div class="row">
                    					<div class="col-md-12">
                        					<div class="form-group ">
									            <label class="col-lg-3 control-label">Invoice type: </label>
									            
									            <div class="col-lg-9">
									            	<select id='invoice_type_id' name='invoice_type_id' class='form-control custom-select '>
								                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
								                      <option value=''>None - Please Select an invoice type</option>
								                      <?php echo $invoice_type_list;?>
								                    </select>
									            </div>
									        </div>
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Billing Schedule: </label>
									            
									            <div class="col-lg-9">
									            	<select id='billing_schedule_id' name='billing_schedule_id' class='form-control custom-select '>
								                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
								                      <option value=''>None - Please Select a billing schedule</option>
								                      <?php echo $billing_schedule_list;?>
								                    </select>
									            </div>
									        </div>
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Amount/Rate : </label>
									            
									            <div class="col-lg-9">
									            	<input type="text" class="form-control" name="amount" value="">
									            	
									            </div>
									        </div>
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Arrears : </label>
									            
									            <div class="col-lg-9">
									            	<input type="text" class="form-control" name="arrears" value="">
									            </div>
									        </div>
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Initial Reading : </label>
									            
									            <div class="col-lg-9">
									            	<input type="text" class="form-control" name="initial_reading" value="">
									            </div>
									        </div>
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Start Date : </label>
									            
									            <div class="col-lg-9">
									            	<div class="input-group">
							                            <span class="input-group-addon">
							                                <i class="fa fa-calendar"></i>
							                            </span>
							                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" value="<?php echo $lease_start;?>" placeholder="Start Date From" >
							                        </div>
									            </div>
									        </div>

									    </div>
									</div>
								    <div class="row" style="margin-top:10px;">
										<div class="col-md-12">
									        <div class="form-actions center-align">
									            <button class="submit btn btn-primary btn-sm" type="submit">
									                Add Billing Detail 
									            </button>
									        </div>
									    </div>
									</div>
                				</div>
                			</div>
            				
            			</div>
            			<?php echo form_close();?>
                				<!-- end of form -->
                		<?php
                		}
                		?>
            			
            		</div>
            		<div class="row">
        				<div class="col-md-12">
        					<table class="table table-bordered table-striped table-condensed">
								<thead>
									<tr>
										<th style="width: 5%;">#</th>
										<th style="width: 20%;">Charge</th>
										<th style="width: 20%;">Schedule</th>
										<th style="width: 10%;">Arrears</th>
										<th style="width: 10%;">Amount/Rate</th>
										<th style="width: 10%;">Initial reading</th>
										<th style="width: 10%;">Date</th>
										<th style="width: 15%;">Action</th>
									</tr>
								</thead>
							  	<tbody>
							  		<?php
							  		if($query_invoice->num_rows() > 0)
							  		{
							  			$x = 0;
							  			foreach ($query_invoice->result() as $key => $value) {
								  			# code...
							  				$billing_schedule_id = $value->billing_schedule_id;
							  				$property_billing_id = $value->property_billing_id;
							  				$invoice_type_id = $value->invoice_type_id;
							  				$invoice_type_name = $value->invoice_type_name;
							  				$billing_schedule_name = $value->billing_schedule_name;
							  				$billing_start_date = $value->start_date;
							  				$arrears = $value->arrears_bf;
							  				$billing_amount = $value->billing_amount;
							  				$initial_water_meter_reading = $value->initial_reading;

										  if($lease_status < 2)
										  {

										  	$buttons = '<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#refund_payment'.$property_billing_id.'"><i class="fa fa-pencil"></i></button>
														<a href="'.site_url().'delete-billing/'.$property_billing_id.'/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete?\');" title="Delete "><i class="fa fa-trash"></i></a>';
										  }
										  else
										  {
										  	$buttons ='';
										  }
							  				$x++;
							  				echo '	<tr>
											  			<td>'.$x.'</td>
											  			<td>'.$invoice_type_name.'</td>
											  			<td>'.$billing_schedule_name.'</td>
											  			<td>'.$arrears.'</td>
											  			<td>'.$billing_amount.'</td>
											  			<td>'.$initial_water_meter_reading.'</td>
											  			<td>'.$billing_start_date.'</td>
											  			<td>
											  			
											  				'.$buttons.'
															<!-- Modal -->
															<div class="modal fade" id="refund_payment'.$property_billing_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															    <div class="modal-dialog" role="document">
															        <div class="modal-content">
															            <div class="modal-header">
															            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															            	<h4 class="modal-title" id="myModalLabel">Edit '.$invoice_type_name.' Billing</h4>
															            </div>
															            <div class="modal-body">
															            	'.form_open("update-billing-item/".$property_billing_id."/".$invoice_type_id, array("class" => "form-horizontal","id"=>"payments-paid-form")).'
															                <div class="row">
										                    					<div class="col-md-12">
										                        					
																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Billing Schedule: </label>
																			            
																			            <div class="col-lg-9">
																			            	<select id="billing_schedule_id" name="billing_schedule_id" class="form-control custom-select ">
																		                    <!-- <select class="form-control custom-select " id="procedure_id" name="procedure_id"> -->
																		                      <option value="">None - Please Select a billing schedule</option>
																		                      '.$billing_schedule_list.'
																		                    </select>
																			            </div>
																			        </div>
																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Amount/Rate : </label>
																			            
																			            <div class="col-lg-9">
																			            	<input type="text" class="form-control" name="amount" value="'.$billing_amount.'">
																			            	
																			            </div>
																			        </div>
																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Arrears : </label>
																			            
																			            <div class="col-lg-9">
																			            	<input type="text" class="form-control" name="arrears" value="'.$arrears.'">
																			            </div>
																			        </div>
																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Initial Reading : </label>
																			            
																			            <div class="col-lg-9">
																			            	<input type="text" class="form-control" name="initial_reading" value="'.$initial_water_meter_reading.'">
																			            </div>
																			        </div>
																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Start Date : </label>
																			            
																			            <div class="col-lg-9">
																			            	<div class="input-group">
																	                            <span class="input-group-addon">
																	                                <i class="fa fa-calendar"></i>
																	                            </span>
																	                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" value="'.$billing_start_date.'" placeholder="Start Date From" >
																	                        </div>
																			            </div>
																			        </div>

																			    </div>
																			</div>
																			 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="'.$this->uri->uri_string().'">
															                
															                <div class="row">
															                	<div class="col-md-8 col-md-offset-4">
															                    	<div class="center-align">
															                        	<button type="submit" class="btn btn-primary">Save action</button>
															                        </div>
															                    </div>
															                </div>
															                '.form_close().'
															            </div>
															            <div class="modal-footer">
															                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
															            </div>
															        </div>
															    </div>
															</div>
											  			</td>
											  		</tr>';
								  		}

							  		}
							  		
							  		?>
							  	
							  		
							  		

							  		
							  	</tbody>
							</table>
        				</div>
        			</div>
				</div>
			</section>
		</div>

		<div class="col-md-5"> 
			<section class="panel">
				<header class="panel-heading">						
					<h2 class="panel-title">Lease Closing </h2>
				</header>

				<div class="panel-body">
					<div class="row" style="margin-bottom:20px;">
						<?php echo form_open("close-lease/".$lease_id.'/'.$tenant_unit_id, array("class" => "form-horizontal", "role" => "form"));?>
            			<div class="col-lg-12 col-sm-12 col-md-12">
            				<div class="row">
                				<div class="col-md-12">
                					<div class="row">
                    					<div class="col-md-12">
                        					<div class="form-group ">
									            <label class="col-lg-3 control-label"> Expenses Amount : </label>
									            
									            <div class="col-lg-9">
									            	<input type="text" class="form-control" name="expense_amount" value="<?php echo $expense_amount;?>">
									            </div>
									        </div>
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Water Reading : </label>
									            
									            <div class="col-lg-9">
									            	<input type="text" class="form-control" name="closing_water_reading" value="<?php echo $closing_water_reading?>">
									            </div>
									        </div>
									         <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Closing Date : </label>
									            
									            <div class="col-lg-9">
									            	<div class="input-group">
							                            <span class="input-group-addon">
							                                <i class="fa fa-calendar"></i>
							                            </span>
							                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="closing_end_date" value="<?php echo $closing_end_date;?>" placeholder="Report Date From" >
							                        </div>
									            </div>
									        </div>
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Remarks : </label>
									            
									            <div class="col-lg-9">
									            	<textarea class="form-control" placeholder="remarks"><?php echo $remarks;?></textarea>
									            	
									            </div>
									        </div>

									    </div>
									</div>
								    <div class="row" style="margin-top:10px;">
										<div class="col-md-12">
									        <div class="form-actions center-align">
									            <button class="submit btn btn-primary btn-sm" type="submit">
									                Close Lease 
									            </button>
									        </div>
									    </div>
									</div>
                				</div>
                			</div>
            				
            			</div>
            			<?php echo form_close();?>
                				<!-- end of form -->

            			
            		</div>
				</div>
			</section>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
	    $("#invoice_type_id").customselect();
	    $("#billing_schedule_id").customselect();
	    
	});
</script>