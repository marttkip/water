<?php

$result = '';
		
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Lease Number</th>
				<th>Property Name</th>
				<th>Unit Name</th>
				<th>Tenant Name</th>
				<th>Lease Start Date</th>
				<th>Lease End Date</th>
				<th>Lease Status</th>
				<th colspan="5">Actions</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	
	
	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$created = $leases_row->created;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		

		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Inactive Lease</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}
		else if($lease_status == 2)
		{
			$status = '<span class="label label-success">Terminated Lease</span>';
			$button = '';
			$delete_button = '';
		}

		if(empty($lease_number))
		{
			$lease_start_date ='';
			$expiry_date = '';
			$status = '<span class="label label-warning">Inactive Lease</span>';
		}
	
		
		$count++;
		$result .= 
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.$lease_number.'</td>
				<td>'.$property_name.'</td>
				<td>'.$rental_unit_name.'</td>
				<td>'.$tenant_name.'</td>
				<td>'.$lease_start_date.'</td>
				<td>'.$expiry_date.'</td>
				<td>'.$status.'</td>
				<td><a  class="btn btn-sm btn-primary" id="open_lease'.$lease_id.'" onclick="get_lease_details('.$lease_id.')" ><i class="fa fa-folder"></i> View Lease Info</a>
					<a  class="btn btn-sm btn-warning" id="close_lease'.$lease_id.'" style="display:none;" onclick="close_lease_details('.$lease_id.')" ><i class="fa fa-folder"></i> Close Lease Info</a>
				</td>
				<td><a href="'.site_url().'lease-detail/'.$lease_id.'" class="btn btn-sm btn-danger">View</a></td>
				'.$button.'
				'.$delete_button.'
			</tr> 
		';
		$v_data['lease_id'] = $lease_id;
		$result .= '<tr id="lease_details'.$lease_id.'" style="display:none;">
						<td colspan="12">
							'.$this->load->view("leases/lease_details", $v_data, TRUE).'
						</td>
					</tr>';
	}
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}
?>  
<section class="panel">
		<header class="panel-heading">						
			<h2 class="panel-title"><?php echo $title;?> 
				<div class="pull-right" >
					<a  class="btn btn-sm btn-primary pull-right" id="assign_new_tenant" onclick="assign_new_tenant();" style="margin-top:-5px">Allocate Tenant to Rental Unit </a>
					<a  class="btn btn-sm btn-default pull-right" id="close_assign_new_tenant" style="display:none; margin-top:-5px;" onclick="close_assign_new_tenant();">Close tenant allocation view</a>
				</div>
			</h2>
		</header>
		<div class="panel-body">
        	<?php
            $success = $this->session->userdata('success_message');

			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}
			
			$error = $this->session->userdata('error_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			?>
        	<div class="row" style="margin-bottom:20px;">
                <div class="col-lg-12">
                	<div style="display:none;" class="col-md-12" id="new_tenant_allocation" >
                    	<section class="panel">
							<header class="panel-heading">
								<div class="panel-actions">
								</div>
								<h2 class="panel-title">Allocate tenant to </h2>
							</header>
							<div class="panel-body">
								<div class="row" style="margin-bottom:20px;">
									<?php echo form_open("add-tenant-unit", array("class" => "form-horizontal", "role" => "form"));?>
                        			<div class="col-lg-12 col-sm-12 col-md-12">
                        				<div class="row">
                            				<div class="col-md-12">
                            					<div class="row">
                                					<div class="col-md-6">
                                    					<div class="form-group center-align">
												            <label class="col-lg-4 control-label">Tenant Name: </label>
												            
												            <div class="col-lg-8">
												            	<select id='tenant_id' name='tenant_id' class='form-control custom-select '>
											                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
											                      <option value=''>None - Please Select a Tenant</option>
											                      <?php echo $tenants_list;?>
											                    </select>
												            </div>
												        </div>
												    </div>
												    <div class="col-md-6">
                                    					<div class="form-group center-align">
												            <label class="col-lg-4 control-label">Rental Unit: </label>
												            
												            <div class="col-lg-8">
												            	<select id='rental_unit_id' name='rental_unit_id' class='form-control custom-select '>
											                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
											                      <option value=''>None - Please Select rental Unit</option>
											                      <?php echo $rental_unit_list;?>
											                    </select>
												            </div>
												        </div>
												    </div>
												</div>
											    <div class="row" style="margin-top:10px;">
													<div class="col-md-12">
												        <div class="form-actions center-align">
												            <button class="submit btn btn-primary btn-sm" type="submit">
												                Allocate tenant to Rental unit
												            </button>
												        </div>
												    </div>
												</div>
                            				</div>
                            			</div>
                        				
                        			</div>
                        			<?php echo form_close();?>
                            				<!-- end of form -->
                        			
                        		</div>
							</div>
						</section>
                    </div>
                </div>
            </div>
			<div class="table-responsive">
            	
				<?php echo $result;?>
		
            </div>
		</div>
        <div class="panel-footer">
        	<?php if(isset($links)){echo $links;}?>
        </div>
	</section>

	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>
  <script type="text/javascript">
	$(function() {
	    $("#tenant_id").customselect();
	    $("#rental_unit_id").customselect();
	});
	$(document).ready(function(){
		$(function() {
			$("#rental_unit_id").customselect();
			$("#tenant_id").customselect();
		});
	});

	function get_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant");
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var button = document.getElementById("open_new_tenant");
		var button2 = document.getElementById("close_new_tenant");

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		button2.style.display = '';
	}
	function close_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant");
		var button = document.getElementById("open_new_tenant");
		var button2 = document.getElementById("close_new_tenant");
		var myTarget3 = document.getElementById("new_tenant_allocation");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		button2.style.display = 'none';
	}


	function assign_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant_allocation");
		var myTarget3 = document.getElementById("new_tenant");
		var button = document.getElementById("assign_new_tenant");
		var button2 = document.getElementById("close_assign_new_tenant");

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		button2.style.display = 'block';
	}
	function close_assign_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant_allocation");
		var button = document.getElementById("assign_new_tenant");
		var myTarget3 = document.getElementById("new_tenant");
		var button2 = document.getElementById("close_assign_new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		button2.style.display = 'none';
	}


	// lease details


	function get_tenant_leases(tenant_id){

		var myTarget2 = document.getElementById("lease_details"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");
		var button = document.getElementById("open_lease_details"+tenant_id);
		var button2 = document.getElementById("close_lease_details"+tenant_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = '';
	}
	function close_tenant_leases(tenant_id){

		var myTarget2 = document.getElementById("lease_details"+tenant_id);
		var button = document.getElementById("open_lease_details"+tenant_id);
		var button2 = document.getElementById("close_lease_details"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = 'none';
	}

	// tenant_info
	function get_tenant_info(tenant_id){

		var myTarget2 = document.getElementById("tenant_info"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");
		var button = document.getElementById("open_tenant_info"+tenant_id);
		var button2 = document.getElementById("close_tenant_info"+tenant_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = '';
	}
	function close_tenant_info(tenant_id){

		var myTarget2 = document.getElementById("tenant_info"+tenant_id);
		var button = document.getElementById("open_tenant_info"+tenant_id);
		var button2 = document.getElementById("close_tenant_info"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = 'none';
	}

  </script>