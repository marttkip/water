<?php

$result = '';
$total_invoice_balance =0;
$total_balance_bf = 0;
$total_rent_payable = 0;

$this_year_item = $this->session->userdata('year');
$this_month_item = $this->session->userdata('month');
$return_date = $this->session->userdata('return_date');
$total_invoice_balance_end =0;
$total_payable_end =0;
$total_arreas_end = 0;
$total_rent_end = 0;

		
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>HOUSE NUMBER</th>
				<th>TENANT NAME</th>
				<th>RENT PAYABLE</th>
				<th>ARREARS B/F</th>
				<th>RENT PAID</th>
				<th>ARREARS</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	
	// var_dump($query->num_rows()); die();
	foreach ($query->result() as $leases_row)
	{
		$rental_unit_name = $leases_row->rental_unit_name;
		$rental_unit_id = $leases_row->rental_unit_id;
		$property_name = $leases_row->property_name;
		$property_id = $leases_row->property_id;

		$todays_month = $this->session->userdata('month');
		$todays_year = $this->session->userdata('year');
		$return_date = $this->session->userdata('return_date');


		if($todays_month < 10)
		{
			$todays_month = '0'.$todays_month;
		}
		$lease_id = $this->reports_model->get_active_lease($property_id,$rental_unit_id,$todays_month,$todays_year,null);
		// echo $lease_id.' one';
		$total_paid_amount = 0;

					$count++;
		if($lease_id > 0)
		{
			$rent_payable = $this->reports_model->get_rent_payable($lease_id,$rental_unit_id,$todays_month,$todays_year,$return_date);
				
			$lease_query = $this->reports_model->get_lease_detail($lease_id);
			foreach ($lease_query->result() as $key => $value) {

				$lease_id = $value->lease_id;
				$tenant_unit_id = $value->tenant_unit_id;
				$rental_unit_id = $value->rental_unit_id;
				$tenant_name = $value->tenant_name;
				$tenant_email = $value->tenant_email;
				$tenant_phone_number = $value->tenant_phone_number;
				$lease_start_date = $value->lease_start_date;
				$lease_duration = $value->lease_duration;
				$rent_amount = $value->rent_amount;
				$lease_number = $value->lease_number;
				$arreas_bf = $value->arrears_bf;
				$rent_calculation = $value->rent_calculation;
				$deposit = $value->deposit;
				$deposit_ext = $value->deposit_ext;
				$lease_status = $value->lease_status;

				$points = 0;//$value->points;
				$created = $value->created;

					// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
				$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			

			
				$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		     	
		     	// $this_month = $this->session->userdata('month');

				$parent_invoice_date = date('Y-m-d', strtotime($todays_year.'-'.$todays_month.'-'.$return_date));
				$previous_invoice_date = date('Y-m-d', strtotime($parent_invoice_date.'-1 month')); //date('Y-m-d', strtotime('last day of previous month'));
				// $todays_year.'-'.$todays_month.'-'.$return_date;

				
// if($lease_id = 8)
// 					{
						// var_dump($previous_invoice_date); 	die();
// 					}
				

					$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
					$total_pardon_amount = $tenants_response['total_pardon_amount'];


					// $invoice_type = 1;

					$rent_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$previous_invoice_date,null);
					$rent_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$previous_invoice_date,null);


					$total_rent_brought_forward = $rent_invoice_amount - $rent_paid_amount -$total_pardon_amount;

					
					
					$rent_current_invoice =  $this->accounts_model->get_invoice_tenants_current_month_forward($lease_id,$todays_month,$todays_year,null,$previous_invoice_date,$parent_invoice_date);
					

					$rent_current_payment =  $this->accounts_model->get_today_tenants_paid_current_month_forward($lease_id,$todays_month,$todays_year,null,$previous_invoice_date,$parent_invoice_date);
				
					
					$total_variance = $rent_current_invoice - $rent_current_payment;
					
					$total_balance = $total_variance + $total_rent_brought_forward  ;
					
					// if($lease_id = 1)
					// {
					// 	var_dump($total_rent_brought_forward); die();
					// }
					
					$result .= 
								'	<tr>
										<td>'.$count.'</td>
										<td>'.$rental_unit_name.'</td>
										<td>'.$tenant_name.'</td>
										<td>'.number_format($rent_payable,2).'</td>
										<td>'.number_format($total_rent_brought_forward,2).'</td>
										<td>'.number_format($rent_current_payment,2).'</td>
										<td>'.number_format($total_balance,0).'</td>
									</tr> 
								';
					// add all the balances


					$total_invoice_balance_end = $total_invoice_balance_end + $total_balance;
					$total_arreas_end = $total_arreas_end + $total_rent_brought_forward;
					$total_payable_end = $total_payable_end + $rent_current_payment;
					
					$total_rent_end = $total_rent_end + $rent_payable;
					
			}

			

		}
		else{

			$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$rental_unit_name.'</td>
							<td>Vacant House</td>

							<td>0.00</td>
							<td>0.00</td>
							<td></td>
							<td></td>
						</tr> 
					';
		}

			
		
	
		
		
		
		
	}


	$result .='<tr> 
					<td></td>
					<td></td>
					<td><strong>Totals</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_rent_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_arreas_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_payable_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_invoice_balance_end,2).'</strong></td>
				</tr>';
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no items for this month";
}


$property_id = $this->session->userdata('property_id');


//  GET ALL OUT FLOWS OF THE MONTH 
$outflows = $this->reports_model->get_property_month_outflows($property_id,$this_month_item,$this_year_item);
$total_outflows = 0;
$x = 0;


// var_dump($outflows); die();
if($outflows->num_rows() > 0)
{
	$outflow = '';

	foreach ($outflows->result() as $key) {
		# code...

		$account_name = $key->account_name;
		$account_invoice_description = $key->account_invoice_description;
		$invoice_amount = $key->invoice_amount;
		$x++;
		$outflow .='<tr>
						<td>'.$x.'</td>
						<td>'.$account_name.'</td>
						<td>'.$account_invoice_description.'</td>
						<td>KES. '.number_format($invoice_amount,2).'</td>
					</tr>';
		$total_outflows = $total_outflows + $invoice_amount;
	}



	
	// $outflow .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows ,2).' </strong> </li>';
}else
{
	$outflow ='<tr><td colspan=3>No expense added</td></tr>';
}
// var_dump($total_outflows); die();

$percent = $this->reports_model->get_manager_percent($property_id);
$commission = $total_payable_end * ($percent/100);
$x++;
$outflow .='<tr>
						<td>'.$x.'</td>
						<td colspan=2>Agent Commission '.$percent.'% </td>
						<td>KES. '.number_format($commission,2).'</td>
					</tr>';
$total_outflows += $commission;


$outflow .='<tr>
				<td colspan=2>Total<td>
				<td>KES. '.number_format($total_outflows,2).'</td>
		</tr>';



$leases = $this->reports_model->get_property_month_leases($property_id,$this_month_item,$this_year_item);
$total_leases = 0;
$x = 0;
$total_deposit_refund = 0;
if($leases->num_rows() > 0)
{
	$closed_leases = '';

	foreach ($leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		$remarks = $key->remarks;
		$expense_amount = $key->expense_amount;
		$lease_id = $key->lease_id;

		$deposit_amount = $this->reports_model->get_lease_deposit($lease_id);
		$x++;
		$closed_leases .='  <tr>
								<td>'.$x.'</td>
								<td>'.$rental_unit_name.'</td>
								<td>'.$tenant_name.'</td>
								<td>'.$remarks.'</td>
								<td>KES. '.number_format($expense_amount,2).'</td>
								<td>KES. '.number_format($deposit_amount - $expense_amount,2).'</td>
							</tr>';
		
		$total_leases = $total_leases + $expense_amount;
		$total_deposit_refund = $total_deposit_refund + ($deposit_amount - $expense_amount);
	}
	$closed_leases .='<tr>
						<td colspan=3>Total<td>
						<td>KES. '.number_format($total_leases,2).'</td>
						<td>KES. '.number_format($total_deposit_refund,2).'</td>
				</tr>';
	// $closed_leases .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_leases ,2).' </strong> </li>';
}else
{
	$closed_leases ='<tr><td colspan=3>No closed leases</td></tr>';
}



$new_leases = $this->reports_model->get_property_month_new_leases($property_id,$this_month_item,$this_year_item);
$total_new_lease = 0;
$x = 0;
if($new_leases->num_rows() > 0)
{
	$closed_new_leases = '';

	foreach ($new_leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		$remarks = $key->remarks;
		$expense_amount = $key->expense_amount;
		$lease_id = $key->lease_id;

		$deposit_amount = $this->reports_model->get_lease_deposit($lease_id);
		$x++;
		$closed_new_leases .='<tr>
						<td>'.$x.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$tenant_name.'</td>
						
					</tr>';
		// if($expense_amount > 0)
		// {

		// }
		$total_new_lease = $total_new_lease + $deposit_amount;
	}
	
	// $closed_new_leases .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_new_lease ,2).' </strong> </li>';
}else
{
	$closed_new_leases ='<tr><td colspan=3>No expense added</td></tr>';
}
?>  
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
		</style>
    </head>
     <body class="receipt_spacing">
        <div class="row">
            <div class="col-xs-12">
                <div class="receipt_bottom_border">
                    <table class="table table-condensed">
                        <tr>
                            <th><h2><?php echo $contacts['company_name'];?> </h2></th>
                            <th class="align-right">
                                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            	<tr>
                    <th class="align-center"><h3><?php echo $title;?> </h3></th>
                </tr>
                <?php echo $result;?>
            </div>
        </div>
        <div class="row" >
       		<div class="col-xs-12" style="border-top:1px #000 solid;">
       			<h4>Key Areas to Note:</h4>
       			<div class="row">
       				<div class="col-md-6">
	       				<h5>New Leases</h5>
	       				<table class="table table-hover table-bordered ">
						 	<thead>
								<tr>
								  <th>#</th>						  
								  <th>Rental Unit</th>
								  <th>Tenant</th>							  		
								  			
								</tr>
							</thead>
							<tbody>							
	       						<?php echo $closed_new_leases;?>
							</tbody>
						</table>
	       			</div>
	       			<div class="col-md-6">
	       				<h5>Closed Leases</h5>
	       				<table class="table table-hover table-bordered ">
						 	<thead>
								<tr>
								  <th>#</th>						  
								  <th>Rental Unit</th>
								  <th>Tenant</th>							  		
								  <th>Description</th>
								  <th>Expense</th>	
								  <th>Deposit Refund</th>					
								</tr>
							</thead>
							<tbody>							
	       						<?php echo $closed_leases;?>
							</tbody>
						</table>
	       			</div>
       				
       			</div>
       			<div class="row">
	       			<div class="col-md-6">
	       				<h5>Expenses</h5>
	       				<table class="table table-hover table-bordered ">
						 	<thead>
								<tr>
								  <th>#</th>						  
								  <th>Account</th>
								  <th>Description</th>
								  <th>Amount</th>						
								</tr>
							</thead>
							<tbody>							
	       						<?php echo $outflow;?>
							</tbody>
						</table>
	       			</div>
	       			<div class="col-md-6">
	       				<?php echo '<h4>Amount banked : <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows  - $total_deposit_refund ,2).' </strong> </h4>';?>
	       			</div>
	       		</div>
       			
       			
       		</div>
       	</div>
        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
     </body> 
    </html>
		
