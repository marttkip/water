<?php

class Property_model extends CI_Model 
{	
	public function upload_property_image($property_path, $edit = NULL)
	{
		//upload product's gallery images
		$resize['width'] = 500;
		$resize['height'] = 500;
		
		if(!empty($_FILES['property_image']['tmp_name']))
		{
			$image = $this->session->userdata('property_file_name');
			
			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}
				//delete any other uploaded image
				$this->file_model->delete_file($property_path."\\".$image, $property_path);
				
				//delete any other uploaded thumbnail
				$this->file_model->delete_file($property_path."\\thumbnail_".$image, $property_path);
			}
			//Upload image
			$response = $this->file_model->upload_file($property_path, 'property_image', $resize, 'height');
			if($response['check'])
			{
				$file_name = $response['file_name'];
				$thumb_name = $response['thumb_name'];
				
				//crop file to 1920 by 1010
				$response_crop = $this->file_model->crop_file($property_path."\\".$file_name, $resize['width'], $resize['height']);
				
				if(!$response_crop)
				{
					$this->session->set_userdata('property_error_message', $response_crop);
				
					return FALSE;
				}
				
				else
				{
					//Set sessions for the image details
					$this->session->set_userdata('property_file_name', $file_name);
					$this->session->set_userdata('property_thumb_name', $thumb_name);
				
					return TRUE;
				}
			}
		
			else
			{
				$this->session->set_userdata('property_error_message', $response['error']);
				
				return FALSE;
			}
		}
		
		else
		{
			$this->session->set_userdata('property_error_message', '');
			return FALSE;
		}
	}
	
	public function get_all_properties($table, $where, $per_page, $page)
	{
		//retrieve all property
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('property.property_name');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Delete an existing property
	*	@param int $property_id
	*
	*/
	public function delete_property($property_id)
	{
		if($this->db->delete('property', array('property_id' => $property_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_property($property_id)
	{
		$this->db->select('*');
		$this->db->where('property_id', $property_id);
		$query = $this->db->get('property');
		return $query;
	}
	/*
	*	Activate a deactivated property
	*	@param int $property_id
	*
	*/
	public function activate_property($property_id)
	{
		$data = array(
				'property_status' => 1
			);
		$this->db->where('property_id', $property_id);
		
		if($this->db->update('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated property
	*	@param int $property_id
	*
	*/
	public function deactivate_property($property_id)
	{
		$data = array(
				'property_status' => 0
			);
		$this->db->where('property_id', $property_id);
		
		if($this->db->update('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_active_property()
	{
  		$table = "property";
		$where = "property_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_property_name($property_id)
	{
		
		$table = "property";
		$where = "property_id = ".$property_id;
		
		$this->db->where($where);
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$property_name =$key->property_name;
			}
			return $property_name;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_property_billings($table, $where, $per_page, $page)
	{
		//retrieve all property
		$this->db->from($table);
		$this->db->select('invoice_type.invoice_type_name,billing_schedule.billing_schedule_name, property.property_name,property_billing.*');
		$this->db->where($where);
		$this->db->order_by('property_billing.property_billing_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	/*
	*	Retrieve all community_group
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_active_list($table, $where, $order, $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('');
		
		return $query;
	}

	public function add_billing($property_id)
	{
		// check if there is data already added
		$charge_to = $this->input->post('charge_to');
		$invoice_type_id = $this->input->post('invoice_type_id');
		$billing_schedule_id = $this->input->post('billing_schedule_id');
		$amount = $this->input->post('amount');
		$where_array = array(
								'charge_to'=>$charge_to,
								'invoice_type_id' => $invoice_type_id,
								'billing_schedule_id' => $billing_schedule_id,
								'property_id' => $property_id,
							);
		$this->db->where($where_array);
		$query = $this->db->get('property_billing');
		
		if($query->num_rows() > 0)
		{
			$this->db->where($where_array);
			$where_array['billing_amount'] = $amount;
			if($this->db->update('property_billing',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//  do an insert
			
			$where_array['billing_amount'] = $amount;
			if($this->db->insert('property_billing',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		

	}
	public function get_property_invoicing($table, $where,$per_page ,$page, $order='invoice_structure.invoice_structure_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('');
		
		return $query;
	}
	public function add_property_invoicing($property_id)
	{
		// check if there is data already added
		$charge_to = $this->input->post('charge_to');
		$invoice_structure_id = $this->input->post('invoice_structure_id');
		$where_array = array(
								'charge_to'=>$charge_to,
								'invoice_structure_id' => $invoice_structure_id,
								'property_id' => $property_id,
							);
		$this->db->where($where_array);
		$query = $this->db->get('property_invoice_structure');
		
		if($query->num_rows() > 0)
		{
			$this->db->where($where_array);
			if($this->db->update('property_invoice_structure',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//  do an insert
			
			if($this->db->insert('property_invoice_structure',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		

	}
	
	public function get_months()
	{
		return $this->db->get('month');
	}
}
