<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class leases extends admin {
	var $leases_path;
	var $leases_location;
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('admin/users_model');
		$this->load->model('property_model');
		$this->load->model('admin/admin_model');
		$this->load->model('leases_model');	
		$this->load->model('tenants_model');	
		$this->load->model('home_owners_model');	
		$this->load->model('accounts/accounts_model');		
		$this->load->library('image_lib');
		
		
		//path to image directory
		$this->leases_path = realpath(APPPATH . '../assets/leases');
		$this->leases_location = base_url().'assets/leases/';
		$this->csv_path = realpath(APPPATH . '../assets/csv');
	}
    
	/*
	*
	*	Default action is to show all the registered lease
	*
	*/
	public function index() 
	{
		
		$where = 'leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id ';
		$table = 'leases,rental_unit,tenant_unit,tenants,property';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'lease-management/leases';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->leases_model->get_all_leases($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['title'] ='Leases';
		

		$tenant_order = 'tenants.tenant_name';
		$tenant_table = 'tenants';
		$tenant_where = 'tenants.tenant_status = 1';

		$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);
		$rs8 = $tenant_query->result();
		$tenants_list = '';
		foreach ($rs8 as $tenant_rs) :
			$tenant_id = $tenant_rs->tenant_id;
			$tenant_name = $tenant_rs->tenant_name;

			$tenant_national_id = $tenant_rs->tenant_national_id;
			$tenant_phone_number = $tenant_rs->tenant_phone_number;

		    $tenants_list .="<option value='".$tenant_id."'>".$tenant_name." Phone: ".$tenant_phone_number."</option>";

		endforeach;

		$v_data['tenants_list'] = $tenants_list;


		$rental_unit_order = 'rental_unit.property_id = property.property_id';
		$rental_unit_table = 'rental_unit,property';
		$rental_unit_where = 'rental_unit.rental_unit_status = 1 and rental_unit.property_id = property.property_id';

		$rental_unit_query = $this->tenants_model->get_tenant_list($rental_unit_table, $rental_unit_where, $rental_unit_order);
		$rs8 = $rental_unit_query->result();
		$rental_unit_list = '';
		foreach ($rs8 as $rental_unit) :
			$rental_unit_id = $rental_unit->rental_unit_id;
			$rental_unit_name = $rental_unit->rental_unit_name;

			$property_name = $rental_unit->property_name;

		    $rental_unit_list .="<option value='".$rental_unit_id."'>".$rental_unit_name." Property: ".$property_name."</option>";

		endforeach;

		$v_data['rental_unit_list'] = $rental_unit_list;


		$data['content'] = $this->load->view('leases/all_leases', $v_data, true);
		
		$data['title'] = 'All rental unit';
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	function add_lease($tenant_id,$rental_unit_id)
	{
		
		$lease_error = $this->session->userdata('lease_error_message');
		
		$this->form_validation->set_rules('lease_start_date', 'lease name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('lease_duration', 'lease duration', 'required|trim|xss_clean');
		$this->form_validation->set_rules('rent_amount', 'Rent amount', 'required|trim|xss_clean');
		$this->form_validation->set_rules('deposit_amount', 'lease location', 'required|trim|xss_clean');

		if ($this->form_validation->run())
		{	
			if(empty($lease_error))
			{
				if($this->leases_model->add_lease($tenant_id,$rental_unit_id))
				{
					$this->session->unset_userdata('lease_error_message');
					$this->session->set_userdata('success_message', 'Lease has been added successfully');
				}
				else
				{
					$this->session->set_userdata('error_message', 'Sorry please check for errors has been added successfully');
				}				
			
			
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry please check for errors has been added successfully');
				
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please fill in all the fields');
		}
		redirect('tenants/'.$rental_unit_id);		
	}
	
	function edit_lease($lease_id, $page = NULL)
	{
		//get lease data
		$table = "rental_unit, rental_unit_owners";
		$where = "rental_unit_owners.rental_unit_owner_id = rental_unit.rental_unit_owner_id AND rental_unit.rental_unit_id = ".$rental_unit_id;
		
		$this->db->where($where);
		$rental_unit_query = $this->db->get($table);
		$rental_unit_row = $rental_unit_query->row();
		$v_data['rental_unit_row'] = $rental_unit_row;		
		$v_data['rental_unit_owners'] = $this->rental_unit_owners_model->get_all_front_end_rental_unit_owners();
		
		$this->form_validation->set_rules('rental_unit_name', 'rental_unit name', 'trim|xss_clean');
		$this->form_validation->set_rules('rental_unit_location', 'rental_unit location', 'trim|xss_clean');

		if ($this->form_validation->run())
		{	
			if(empty($rental_unit_error))
			{
		
				$data2 = array(
					'rental_unit_name'=>$this->input->post("rental_unit_name"),
					'rental_unit_location'=>$this->input->post("rental_unit_location"),
					'rental_unit_status'=>1,
					'rental_unit_owner_id'=>$this->input->post("rental_unit_owner_id")
				);
				
				$table = "rental_unit";
				$this->db->where('rental_unit_id', $rental_unit_id);
				$this->db->update($table, $data2);
				$this->session->set_userdata('success_message', 'rental_unit has been edited');
				
				redirect('real-estate-administration/rental-units/'.$page);
			}
		}
		
		$rental_unit = $this->session->userdata('rental_unit_file_name');
		
		
		
		$data['content'] = $this->load->view("rental_unit/edit_rental_unit", $v_data, TRUE);
		$data['title'] = 'Edit rental_unit';
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
    public function update_lease_detail($lease_id)
    {
    	$this->form_validation->set_rules('lease_start_date', 'Lease Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('lease_end_date', 'Lease End Date', 'trim|required|xss_clean');

		if ($this->form_validation->run())
		{	

			$this->db->where('lease_status = 0 AND lease_id ='.$lease_id);
			$query= $this->db->get('leases');

			

			if($query->num_rows() > 0)
			{
				$lease_number = $this->leases_model->create_lease_number();
				$data2 = array(
					'lease_start_date'=>$this->input->post("lease_start_date"),
					'lease_end_date'=>$this->input->post("lease_end_date"),
					'lease_status'=>1,
					'lease_number'=>$lease_number
				);
				// var_dump($data2); die();
				$table = "leases";
				$this->db->where('lease_id', $lease_id);
				$this->db->update($table, $data2);
				$this->session->set_userdata('success_message', 'lease has been edited');
			}
			else
			{

				$data2 = array(
					'lease_end_date'=>$this->input->post("lease_end_date")
				);
				
				$table = "leases";
				$this->db->where('lease_id', $lease_id);
				$this->db->update($table, $data2);
				$this->session->set_userdata('success_message', 'lease has been edited');
			}
				
			
		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
    }


	/*
	*
	*	Delete an existing rental_unit
	*	@param int $rental_unit_id
	*
	*/
	function delete_rental_unit($rental_unit_id, $page)
	{
		//get rental_unit data
		$table = "rental_unit";
		$where = "rental_unit_id = ".$rental_unit_id;
		
		$this->db->where($where);
		$rental_unit_query = $this->db->get($table);
		$rental_unit_row = $rental_unit_query->row();
		$rental_unit_path = $this->rental_unit_path;
		
		$image_name = $rental_unit_row->rental_unit_image_name;
		
		//delete any other uploaded image
		$this->file_model->delete_file($rental_unit_path."\\".$image_name);
		
		//delete any other uploaded thumbnail
		$this->file_model->delete_file($rental_unit_path."\\thumbnail_".$image_name);
		
		if($this->rental_unit_model->delete_rental_unit($rental_unit_id))
		{
			$this->session->set_userdata('success_message', 'rental_unit has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'rental_unit could not be deleted');
		}
		redirect('real-estate-administration/rental-units/'.$page);
	}
    
	/*
	*
	*	Activate an existing rental_unit
	*	@param int $rental_unit_id
	*
	*/
	public function activate_rental_unit($rental_unit_id, $page = NULL)
	{
		if($this->rental_unit_model->activate_rental_unit($rental_unit_id))
		{
			$this->session->set_userdata('success_message', 'rental_unit has been activated');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'rental_unit could not be activated');
		}
		redirect('rental-units/'.$page);
	}
    
	/*
	*
	*	Deactivate an existing rental_unit
	*	@param int $rental_unit_id
	*
	*/
	public function deactivate_rental_unit($rental_unit_id, $page = NULL) 
	{
		if($this->rental_unit_model->deactivate_rental_unit($rental_unit_id))
		{
			$this->session->set_userdata('success_message', 'rental_unit has been disabled');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'rental_unit could not be disabled');
		}
		redirect('rental-units/'.$page);
	}
	public function import_tenant_units()
	{
		$v_data['title'] = $data['title'] = 'Import Tenant Units';
		
		$data['content'] = $this->load->view('import/import_tenant_units', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function tenant_units_import_template()
	{
			//export products template in excel 
		$this->leases_model->tenants_units_import_template();
	}

	
	public function tenant_units_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->leases_model->import_csv_payroll($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = 'Import Tenant Units';
		
		$data['content'] = $this->load->view('import/import_tenant_units', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}












	public function import_home_owners()
	{
		$v_data['title'] = $data['title'] = 'Import Home Owners';
		
		$data['content'] = $this->load->view('import/import_home_owners', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function home_owners_import_template()
	{
			//export products template in excel 
		$this->leases_model->home_owners_import_template();
	}

	
	public function home_owners_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->leases_model->import_csv_home_owners($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = 'Import Tenant Units';
		
		$data['content'] = $this->load->view('import/import_home_owners', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function lease_detail($lease_id)
	{

		//get lease data
		$where = 'leases.lease_id > 0 AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id  and leases.lease_id ='.$lease_id;
		$table = 'leases,rental_unit,tenant_unit,tenants,property';
		
		$this->db->where($where);
		$rental_unit_query = $this->db->get($table);
		$rental_unit_row = $rental_unit_query->row();
		$v_data['rental_unit_row'] = $rental_unit_row;		
		
		
		
		$rental_unit = $this->session->userdata('rental_unit_file_name');
		
		$v_data['lease_id'] = $lease_id;

		$invoice_type_order = 'invoice_type.invoice_type_id';
		$invoice_type_table = 'invoice_type';
		$invoice_type_where = 'invoice_type.invoice_type_status = 1';

		$invoice_type_query = $this->tenants_model->get_tenant_list($invoice_type_table, $invoice_type_where, $invoice_type_order);
		$rs8 = $invoice_type_query->result();
		$invoice_type_list = '';
		foreach ($rs8 as $invoice_rs) :
			$invoice_type_id = $invoice_rs->invoice_type_id;
			$invoice_type_name = $invoice_rs->invoice_type_name;


		    $invoice_type_list .="<option value='".$invoice_type_id."'>".$invoice_type_name."</option>";

		endforeach;

		$v_data['invoice_type_list'] = $invoice_type_list;



		$billing_schedule_order = 'billing_schedule.billing_schedule_id';
		$billing_schedule_table = 'billing_schedule';
		$billing_schedule_where = 'billing_schedule.billing_schedule_status = 1';

		$billing_schedule_query = $this->tenants_model->get_tenant_list($billing_schedule_table, $billing_schedule_where, $billing_schedule_order);
		$rs8 = $billing_schedule_query->result();
		$billing_schedule_list = '';
		foreach ($rs8 as $billing_rs) :
			$billing_schedule_id = $billing_rs->billing_schedule_id;
			$billing_schedule_name = $billing_rs->billing_schedule_name;


		    $billing_schedule_list .="<option value='".$billing_schedule_id."'>".$billing_schedule_name."</option>";

		endforeach;

		$v_data['billing_schedule_list'] = $billing_schedule_list;


		$unit_billing_where = 'billing_schedule.billing_schedule_id = property_billing.billing_schedule_id AND property_billing.invoice_type_id = invoice_type.invoice_type_id AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id = '.$lease_id;
		$unit_billing_table = 'billing_schedule,property_billing,invoice_type';
		$unit_billing_order = 'property_billing.property_billing_id';

		$unit_billing_query = $this->tenants_model->get_tenant_list($unit_billing_table, $unit_billing_where, $unit_billing_order);

		$v_data['query_invoice'] = $unit_billing_query;



		
		$data['content'] = $this->load->view("leases/lease_detail", $v_data, TRUE);
		$data['title'] = 'Lease Detail';
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function add_billing($lease_id)
	{
		$this->form_validation->set_rules('charge_to', 'Charge To', 'trim|xss_clean');
		$this->form_validation->set_rules('billing_schedule_id', 'Billing', 'trim|xss_clean');
		$this->form_validation->set_rules('invoice_type_id', 'Invoice Type', 'trim|xss_clean');
		$this->form_validation->set_rules('arrears', 'Arrears', 'trim|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start date', 'trim|xss_clean');
		if ($this->form_validation->run())
		{	

			if($this->leases_model->add_billing($lease_id))
			{
				$this->session->set_userdata('success_message', 'property has been edited');
			}
			else
			{
				$this->session->set_userdata('error_message', 'property billing has not been added');
			}
		
				
			
		}

		redirect('lease-detail/'.$lease_id);
	}
	public function update_property_billing($property_billing_id,$invoice_type_id)
	{

		$this->form_validation->set_rules('billing_schedule_id', 'Billing', 'trim|xss_clean');
		$this->form_validation->set_rules('arrears', 'Arrears', 'trim|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'required|trim|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start date', 'required|trim|xss_clean');
		if ($this->form_validation->run())
		{	

			$data2 = array(
				'billing_schedule_id'=>$this->input->post("billing_schedule_id"),
				'arrears_bf'=>$this->input->post("arrears"),
				'initial_reading'=>$this->input->post("initial_reading"),
				'start_date'=>$this->input->post("start_date"),
				'billing_amount'=>$this->input->post("amount"),
			);
			
			$table = "property_billing";
			$this->db->where('property_billing_id', $property_billing_id);


			if($this->db->update($table, $data2))
			{
				$this->session->set_userdata('success_message', 'property billing has been edited successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'property billing has not been edited');
			}
		
				
			
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);

	}

	public function delete_property_billing($property_billing_id,$lease_id)
	{
		$data2 = array(
			'property_billing_deleted'=>1,
		);
		
		$table = "property_billing";
		$this->db->where('property_billing_id', $property_billing_id);


		if($this->db->update($table, $data2))
		{
			$this->session->set_userdata('success_message', 'property billing has been deleted successfully');
		}
		else
		{
			$this->session->set_userdata('error_message', 'property billing has not been deleted');
		}
	
		redirect('lease-detail/'.$lease_id);

	}
	public function update_billing($lease_id)
	{
		$this->leases_model->update_billing($lease_id);
		redirect('lease-detail/'.$lease_id);
	}


	public function close_lease($lease_id,$tenant_unit_id)
	{
		$this->form_validation->set_rules('expense_amount', 'Expense', 'trim|xss_clean');
		$this->form_validation->set_rules('closing_end_date', 'Closing Date', 'trim|required|xss_clean');
		if ($this->form_validation->run())
		{	

			$data2 = array(
				'expense_amount'=>$this->input->post("expense_amount"),
				'remarks'=>$this->input->post("remarks"),
				'closing_water_reading'=>$this->input->post("closing_water_reading"),
				'closing_end_date'=>$this->input->post("closing_end_date"),
				'lease_status'=>2,
			);
			// var_dump($data2); die();
			$table = "leases";
			$this->db->where('lease_id', $lease_id);


			if($this->db->update($table, $data2))
			{
				$data23 = array(
					'tenant_unit_status'=>0,
				);
				// var_dump($data23); die();
				$table2 = "tenant_unit";
				$this->db->where('tenant_unit_id', $tenant_unit_id);
				$this->db->update($table2, $data23);

				$this->session->set_userdata('success_message', 'Lease successfully terminated');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Leasse has not been terminisated');
			}
		
				
			
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}





	//// BOBS FUNCTIONS
	public function import_lease_details()
	{
		$v_data['title'] = $data['title'] = 'Import Lease Details';
		
		$data['content'] = $this->load->view('import/import_lease_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function lease_details_import_template()
	{
			//export products template in excel 
		$this->leases_model->lease_details_import_template();
	}

	
	public function lease_details_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->leases_model->import_csv_lease_details($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = 'Import Lease Details';
		
		$data['content'] = $this->load->view('import/import_lease_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}





	public function import_invoice_details()
	{
		$v_data['title'] = $data['title'] = 'Import Invoice Details';
		
		$data['content'] = $this->load->view('import/import_invoice_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function invoice_details_import_template()
	{
			//export products template in excel 
		$this->leases_model->invoice_details_import_template();
	}

	
	public function invoice_details_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->leases_model->import_csv_invoice_details($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = 'Import Invoice Details';
		
		$data['content'] = $this->load->view('import/import_invoice_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


	public function import_payment_details()
	{
		$v_data['title'] = $data['title'] = 'Import Payment Details';
		
		$data['content'] = $this->load->view('import/import_payment_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function payment_details_import_template()
	{
			//export products template in excel 
		$this->leases_model->payment_details_import_template();
	}

	
	public function payment_details_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->leases_model->import_csv_payment_details($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = 'Import Payment Details';
		
		$data['content'] = $this->load->view('import/import_payment_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
}
?>