<?php
//if users exist display them
$result ='';
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result .= 
	'
	
		  
	';
	$total_invoices_balances = 0;
	$total_current_bills = 0;
	$total_balance_now = 0;
	$totals_list = '';
	$total_paids = 0;
	
	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_id = $leases_row->property_id;
		$property_name = $leases_row->property_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		// $arreas_bf = $leases_row->arreas_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		// $arrears_bf = $leases_row->arrears_bf;
		$created = $leases_row->created;


	
			
		$lease_start_date = date('jS M Y',strtotime($lease_start_date));

     	$this_month = date('m');
     	$amount_paid = 0;
	     	$payments = $this->accounts_model->get_this_months_payment($lease_id,$this_month);
	     	$current_items = '<td> -</td>
	     			  <td>-</td>'; 
	     	$total_paid_amount = 0;
	     	if($payments->num_rows() > 0)
	     	{
	     		$counter = 0;
	     		
	     		$receipt_counter = '';
	     		foreach ($payments->result() as $value) {
	     			# code...
	     			$receipt_number = $value->receipt_number;
	     			$amount_paid = $value->amount_paid;

	     			if($counter > 0)
	     			{
	     				$addition = '#';
	     				// $receipt_counter .= $receipt_number.$addition;
	     			}
	     			else
	     			{
	     				$addition = ' ';
	     				
	     			}
	     			$receipt_counter .= $receipt_number.$addition;

	     			$total_paid_amount = $total_paid_amount + $amount_paid;
	     			$counter++;
	     		}
	     		$current_items = '<td>'.$receipt_number.'</td>
	     					<td>'.number_format($amount_paid,0).'</td>';
	     	}
	     	else
	     	{
	     		$current_items = '<td> -</td>
	     					<td>-</td>';
	     	}
			
	     	// get billable items
	     	$billable_items = $this->reports_model->get_billable_items($property_id,1);
	     	$list_titles = '';
	     	$mama_bills = '';
	     	if($billable_items->num_rows() > 0)
	     	{
	     		$total_current_bill = 0;
	     		$total_current_payment = 0;
	     		$bills_array['billed_items'] = array();
	     		$x=0;
	     		foreach ($billable_items->result() as $value) {
	     			# code...
	     			$invoice_type_name = $value->invoice_type_name;
	     			$invoice_type = $value->invoice_type_id;
	     			$billing_schedule_name = $value->billing_schedule_name;
	     			$billing_schedule_id = $value->billing_schedule_id;
	     			$product = array();

	     			

	     			if($billing_schedule_name == "Monthly")
	     			{
	     				$this_month = date('m');
	     				$this_year = date('Y');
	     				$parent_invoice_date = $this->accounts_model->get_max_invoice_date($lease_id,$this_month,$this_year);
	     				$parent_date = $parent_invoice_date;
	     			}
	     			else if($billing_schedule_name == "Quaterly")
	     			{	
	     				$current_quater_date = $this->accounts_model->get_current_quarter_dates();
	     				$parent_invoice_date = $this->accounts_model->get_max_quater_invoice_date($lease_id,$current_quater_date);

	     				$parent_date = $parent_invoice_date;

	     			}
	     			else
	     			{
	     				$this_month = date('m');
	     				$this_year = date('Y');
	     				$parent_invoice_date = $this->accounts_model->get_max_invoice_date($lease_id,$this_month,$this_year);


	     			}
	     			if($parent_invoice_date == NULL)
	     			{
	     				$parent_invoice_date = $parent_date;
	     			}

	     			// var_dump($parent_invoice_date); die();

	     			$penalty_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

					$penalty_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

					$total_brought_forward = $penalty_invoice_amount - $penalty_paid_amount;
					
					$penalty_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);

					if($penalty_current_invoice == NULL)
					{
						$penalty_current_invoice = 0;
					}
					$total_current_bill = $total_current_bill + $penalty_current_invoice;

					$penalty_current_payment =  $this->reports_model->get_today_tenants_paid_current_forward_reports($lease_id,$parent_invoice_date,$invoice_type);

					// $total_current_payment = $total_current_payment + $penalty_current_payment;
					$total_current_payment = $total_current_payment ;
					
					$total_penalty = $penalty_current_invoice - $penalty_current_payment;
					$x++;
					$mama_bills .= '<td>'.number_format($penalty_current_invoice,2).'</td>';
	     			$list_titles .='<th>'.$invoice_type_name.'</th>';
	     			$product[$x] = $total_current_payment;
	     			array_push($bills_array['billed_items'], $product);

	     		}

	     		
	     		
	     	}


			$todays_date = date('Y-m-d');
			$todays_month = date('m');
			$todays_year = date('Y');
			$total_payments = $this->accounts_model->get_total_payments_before($lease_id,$todays_year,$todays_month);
			$total_payments_after = $this->accounts_model->get_total_payments_after($lease_id,$todays_year,$todays_month);
			

			$total_invoices = $this->accounts_model->get_total_invoices_before($lease_id,$todays_year,$todays_month);
			// var_dump($total_current_payment); die();
			$current_balance = $total_invoices - $total_payments - $total_payments_after;

			$current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));


			$total_current_invoice = $current_invoice_amount + $current_balance - $total_paid_amount;
			// var_dump($total_current_invoice); die();
			$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
			$total_pardon_amount = $tenants_response['total_pardon_amount'];
			$total_invoice_balance = $tenants_response['total_invoice_balance'];
			$total_arrears = $tenants_response['total_arrears'];
			$total_current_payment = $total_current_payment + $total_payments_after;
			$total_balance = $total_current_invoice   - $total_current_payment;

			// var_dump($total_balance); die();

				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$tenant_name.'</td>
						<td>'.number_format($total_invoice_balance,2).'</td>
							'.$mama_bills.'
						<td>'.number_format($total_current_bill,2).'</td>
						<td>'.number_format($total_current_payment,2).'</td>
						<td>'.number_format($total_arrears,2).'</td>
						
	
						
					</tr> 
				';
				if($lease_id == 150){

					// var_dump($total_current_payment); die();
				}
			$total_invoices_balances = $total_invoices_balances + $total_invoice_balance;
			$total_current_bills = $total_current_bills + $total_current_bill;
			$total_paids = $total_paids + $total_current_payment;
			$total_balance_now = $total_balance_now + $total_arrears;

			
			
	}
	$result .= 
		'
					<tr>
						<td></td>
						<td></td>
						<td><strong>TOTAL ARREARS</strong></td>
						<td><strong>'.number_format($total_invoices_balances,2).'</strong></td>
						<td colspan="'.$billable_items->num_rows().'"></td>
						<td><strong>'.number_format($total_current_bills,2).'</strong></td>
						<td><strong>'.number_format($total_paids,2).'</strong></td>
						<td><strong>'.number_format($total_balance_now,2).'</strong></td>
						
	
						
					</tr> 
		';
		$result .= 
		'
					  </tbody>
					</table>
		';
	}

	else
	{
		$result .= "There are no defaulters";
	}


?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | <?php echo $property_name;?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-12">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	 <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        
		            <div class="col-xs-8 ">
		                <p><strong> Property Name </strong> : <?php echo $property_name;?></p>
		               
		            </div>
		        </div>

		        <div class="row">
			       	<div class="col-xs-12 ">

			       		<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>#</th>
									<th>HSE NO.</th>
									<th>RESIDENT /OWNERS</th>
									<th>Balance B/F</th>
									<?php echo $list_titles;?>
									<th>TOTAL PAYABLE</th>
									<th>PAYMENTS RECEIVED</th>
									<th>BALANCE</th>

								</tr>
							</thead>
							<tbody>
				       			<?php echo $result;?>
				       		</tbody>
				       	</table>
			       	</div>
			   	</div>




		       </div>
		    </div>
		</body>
    </html>