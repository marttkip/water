        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
			$error = $this->session->userdata('error_message');
			if(!empty($error))
			{
				?>
				<div class="alert alert-danger">
				<?php echo $error;?>
                </div>
                <?php
				$this->session->unset_userdata('error_message');
			}
            echo form_open("reports/property-list", array("class" => "form-horizontal", 'target' => '_blank'));
            ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Property</label>
                        
                        <div class="col-lg-8">
                           <select id='property_id' name='property_id' class='form-control'>
                              <option value=''>None - Please Select a property</option>
                              <?php echo $property_list;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Year</label>
                        
                        <div class="col-lg-8">
                        <input type="text" class="form-control" name="year" placeholder="Year">
                        </div>
                    </div>

                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Month</label>
                        
                        <div class="col-lg-8">
                           <select name='month_id' class='form-control'>
                              <option value=''>None - Please Select a month</option>
                              <?php echo $months;?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-8 col-lg-offset-4">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>

        