<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require_once "./application/modules/real_estate_administration/controllers/property.php";

class Reports extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('administration/reports_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('real_estate_administration/leases_model');	
		$this->load->model('real_estate_administration/tenants_model');	
		$this->load->model('administration/personnel_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/email_model');
		$this->load->model('accounts/accounts_model');

	}
	
	public function all_reports($module = '__')
	{
		$this->session->unset_userdata('all_transactions_search');
		$this->session->unset_userdata('all_transactions_tables');
		
		$this->session->set_userdata('debtors', 'false2');
		$this->session->set_userdata('page_title', 'All Transactions');
		
		$this->all_transactions($module);
	}
	

	
	public function all_transactions($module = '__')
	{


		$where = 'payments.lease_id = leases.lease_id AND payment_method.payment_method_id = payments.payment_method_id AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND payments.payment_status = 1 AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0';
		$table = 'payments, leases,rental_unit,property,tenant_unit,tenants,payment_method';
		
		$transaction_search = $this->session->userdata('all_transactions_search');
		$table_search = $this->session->userdata('all_transactions_tables');
		$property_search = $this->session->userdata('property_search');
		
		if(!empty($transaction_search))
		{
			$where .= $transaction_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		if($module == '__')
		{
			$segment = 3;
		}
		else
		{
			$segment = 4;	
		}
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'reports/tenants-payments-report/'.$module;
		$config['total_rows'] = $this->reports_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_transactions($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['branch_name'] = $this->session->userdata('branch_code');
		$v_data['module'] = $module;
		$v_data['query'] = $query;
		$v_data['page'] = $page;



		$properties = $this->property_model->get_active_property();
		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;

		$branches = $this->reports_model->get_all_active_branches();

		$rs9 = $branches->result();
		$branches_list = '';
		foreach($branches->result() as $row):
			$branch_name = $row->branch_name;
			$branch_code = $row->branch_code;

		    $branches_list .="<option value='".$branch_code."'>".$branch_name."</option>";

		endforeach;
		$v_data['branches_list'] = $branches_list;



		$v_data['total_payments'] = $this->reports_model->get_total_cash_collection($where, $table);
		$v_data['normal_payments'] = $this->reports_model->get_normal_payments($where, $table);
		$v_data['payment_methods'] = $this->reports_model->get_payment_methods($where, $table);
		$v_data['total_expenses'] = 0;


		$leases_table= 'leases';
		$leases_where = 'lease_status = 1';
		$v_data['active_leases'] = $this->reports_model->count_items($leases_table, $leases_where);

		$inactive_leases_table= 'leases';
		$inactive_leases_where = 'lease_status <> 1';
		$v_data['inactive_leases'] = $this->reports_model->count_items($inactive_leases_table, $inactive_leases_where);



		// $v_data['properties'] = $this->properties_model->get_all_active_branches();
		$v_data['title'] = "All Transactions";
		$data['content'] = $this->load->view('reports/all_transactions', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	public function all_transactions_owners()
	{


		$where = 'home_owners_payments.rental_unit_id = rental_unit.rental_unit_id AND payment_method.payment_method_id = home_owners_payments.payment_method_id AND  home_owners_payments.payment_status = 1 AND home_owner_unit.home_owner_id = home_owners.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id';
		$table = 'home_owners_payments,rental_unit,property,home_owner_unit,home_owners,payment_method';
		
		$transaction_search = $this->session->userdata('all_owners_transactions_search');
		$table_search = $this->session->userdata('all_owners_transactions_tables');
		$property_search = $this->session->userdata('owners_property_search');
		
		if(!empty($transaction_search))
		{
			$where .= $transaction_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'reports/owners-payments-report';
		$config['total_rows'] = $this->reports_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_transactions_owners($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['branch_name'] = $this->session->userdata('branch_code');
		// $v_data['module'] = $module;
		$v_data['query'] = $query;
		$v_data['page'] = $page;



		$properties = $this->property_model->get_active_property();
		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;

		$branches = $this->reports_model->get_all_active_branches();

		$rs9 = $branches->result();
		$branches_list = '';
		foreach($branches->result() as $row):
			$branch_name = $row->branch_name;
			$branch_code = $row->branch_code;

		    $branches_list .="<option value='".$branch_code."'>".$branch_name."</option>";

		endforeach;
		$v_data['branches_list'] = $branches_list;



		$v_data['total_payments'] = $this->reports_model->get_total_cash_collection_owners($where, $table);
		$v_data['normal_payments'] = $this->reports_model->get_normal_payments_owners($where, $table);
		$v_data['payment_methods'] = $this->reports_model->get_payment_methods($where, $table);
		$v_data['total_expenses'] = 0;


		$leases_table= 'home_owner_unit';
		$leases_where = 'home_owner_unit_status = 1';
		$v_data['active_leases'] = $this->reports_model->count_items($leases_table, $leases_where);

		$inactive_leases_table= 'home_owner_unit';
		$inactive_leases_where = 'home_owner_unit_status <> 1';
		$v_data['inactive_leases'] = $this->reports_model->count_items($inactive_leases_table, $inactive_leases_where);



		// $v_data['properties'] = $this->properties_model->get_all_active_branches();
		$v_data['title'] = "All Transactions Owners";
		$data['content'] = $this->load->view('reports/all_transactions_owners', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	
	public function export_transactions()
	{
		$this->reports_model->export_transactions();
	}
	public function export_transactions_owners()
	{
		$this->reports_model->export_transactions_owners();
	}
	public function export_defaulters_owners()
	{
		$this->reports_model->export_defaulters_owners();
	}
	public function export_defaulters()
	{
		$this->reports_model->export_defaulters();
	}
	
	public function close_search()
	{
		$this->session->unset_userdata('all_transactions_search');
		$this->session->unset_userdata('all_transactions_search');
		$this->session->unset_userdata('all_transactions_tables');
		$this->session->unset_userdata('all_defaulters_search');
		$this->session->unset_userdata('search_title');
		
		$debtors = $this->session->userdata('debtors');
		
		
		redirect('reports/tenants-payments-report');
	}

	public function close_defaulters_search()
	{
		$this->session->unset_userdata('all_defaulters_search');
		$this->session->unset_userdata('search_title');
		
		$debtors = $this->session->userdata('debtors');

		// $this->all_defaulters();
		redirect('reports/owners-accounts');
	}
	public function close_defaulters_owners_search()
	{
		$this->session->unset_userdata('all_defaulters_owners_search');
		$this->session->unset_userdata('search_title');
		
		redirect('reports/owners-accounts');
	}
	public function tenants_export()
	{
		$where = 'tenants.tenant_id > 0 AND property.property_id = rental_unit.property_id AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';

		$defaulters_search = $this->session->userdata('all_defaulters_search');
		$table_search = $this->session->userdata('all_transactions_tables');

		if(!empty($defaulters_search))
		{
			$where .= $defaulters_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		$query = $this->reports_model->get_all_defaulters_items($table, $where);
		$v_data['query'] = $query;
		$v_data['contacts'] = $this->site_model->get_contacts();
		$v_data['title'] = "Defaulters";
		$data['content'] = $this->load->view('reports/tenants_export', $v_data);
	}

	public function owners_export()
	{
	
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id';
		$table = 'rental_unit,property,home_owners,home_owner_unit';

		$defaulters_search = $this->session->userdata('all_defaulters_owners_search');
		$table_search = $this->session->userdata('all_transactions_tables');

		if(!empty($defaulters_search))
		{
			$where .= $defaulters_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		$query = $this->reports_model->get_all_defaulters_items($table, $where);
		$v_data['query'] = $query;
		$v_data['contacts'] = $this->site_model->get_contacts();
		$v_data['title'] = "Defaulters";
		$data['content'] = $this->load->view('reports/owners_export', $v_data);
	}
	
	public function all_defaulters($module=NULL)
	{

		$where = 'tenants.tenant_id > 0 AND property.property_id = rental_unit.property_id AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';

		$defaulters_search = $this->session->userdata('all_defaulters_search');
		$table_search = $this->session->userdata('all_transactions_tables');

		if(!empty($defaulters_search))
		{
			$where .= $defaulters_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		
		$segment = 3;	
		
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'reports/tenants-accounts';
		$config['total_rows'] = $this->reports_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_defaulters($table, $where, $config["per_page"], $page);

		$v_data['branch_name'] = $this->session->userdata('branch_code');
		$v_data['module'] = $module;
		$v_data['query'] = $query;
		$v_data['page'] = $page;



		$properties = $this->property_model->get_active_property();
		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;

		$branches = $this->reports_model->get_all_active_branches();

		$rs9 = $branches->result();
		$branches_list = '';
		foreach($branches->result() as $row):
			$branch_name = $row->branch_name;
			$branch_code = $row->branch_code;

		    $branches_list .="<option value='".$branch_code."'>".$branch_name."</option>";

		endforeach;
		$v_data['branches_list'] = $branches_list;

		$v_data['title'] = "Defaulters";
		$data['content'] = $this->load->view('reports/all_defaulters', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}



	public function all_defaulters_owners($module=NULL)
	{

		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND  home_owner_unit.home_owner_unit_status = 1';
		$table = 'rental_unit,property,home_owners,home_owner_unit';

		$defaulters_search = $this->session->userdata('all_defaulters_owners_search');
		$table_search = $this->session->userdata('all_transactions_tables');

		if(!empty($defaulters_search))
		{
			$where .= $defaulters_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		
		$segment = 3;	
		
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'reports/owners-accounts';
		$config['total_rows'] = $this->reports_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_defaulters($table, $where, $config["per_page"], $page);

		$v_data['branch_name'] = $this->session->userdata('branch_code');
		$v_data['module'] = $module;
		$v_data['query'] = $query;
		$v_data['page'] = $page;



		$properties = $this->property_model->get_active_property();
		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;

		$branches = $this->reports_model->get_all_active_branches();

		$rs9 = $branches->result();
		$branches_list = '';
		foreach($branches->result() as $row):
			$branch_name = $row->branch_name;
			$branch_code = $row->branch_code;

		    $branches_list .="<option value='".$branch_code."'>".$branch_name."</option>";

		endforeach;
		$v_data['branches_list'] = $branches_list;

		$v_data['title'] = "Defaulters";
		$data['content'] = $this->load->view('reports/all_defaulters_owners', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	
	public function debtors_report_invoices($visit_type_id, $order = 'debtor_invoice_created', $order_method = 'DESC')
	{
		//get bill to
		$v_data['visit_type_query'] = $this->reports_model->get_visit_type();
		
		//select first debtor from query
		if($visit_type_id == 0)
		{
			if($v_data['visit_type_query']->num_rows() > 0)
			{
				$res = $v_data['visit_type_query']->result();
				$visit_type_id = $res[0]->visit_type_id;
				$visit_type_name = $res[0]->visit_type_name;
			}
		}
		
		else
		{
			if($v_data['visit_type_query']->num_rows() > 0)
			{
				$res = $v_data['visit_type_query']->result();
				
				foreach($res as $r)
				{
					$visit_type_id2 = $r->visit_type_id;
					
					if($visit_type_id == $visit_type_id2)
					{
						$visit_type_name = $r->visit_type_name;
						break;
					}
				}
			}
		}
		
		if($visit_type_id > 0)
		{
			$where = 'debtor_invoice.visit_type_id = '.$visit_type_id;
			$table = 'debtor_invoice';
			
			$visit_search = $this->session->userdata('debtors_invoice_search');
			$table_search = $this->session->userdata('debtors_invoice_tables');
			
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				if(!empty($table_search))
				{
					$table .= $table_search;
				}
			}
			
			$segment = 7;
			
			//pagination
			$this->load->library('pagination');
			$config['base_url'] = site_url().'administration/reports/debtors_report_data/'.$visit_type_id.'/'.$order.'/'.$order_method;
			$config['total_rows'] = $this->reception_model->count_items($table, $where);
			$config['uri_segment'] = $segment;
			$config['per_page'] = 20;
			$config['num_links'] = 5;
			
			$config['full_tag_open'] = '<ul class="pagination pull-right">';
			$config['full_tag_close'] = '</ul>';
			
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			$config['next_tag_open'] = '<li>';
			$config['next_link'] = 'Next';
			$config['next_tag_close'] = '</span>';
			
			$config['prev_tag_open'] = '<li>';
			$config['prev_link'] = 'Prev';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
			$v_data["links"] = $this->pagination->create_links();
			$query = $this->reports_model->get_all_debtors_invoices($table, $where, $config["per_page"], $page, $order, $order_method);
			
			$where .= ' AND debtor_invoice.debtor_invoice_id = debtor_invoice_item.debtor_invoice_id AND visit.visit_id = debtor_invoice_item.visit_id ';
			$table .= ', visit, debtor_invoice_item';
			$v_data['where'] = $where;
			$v_data['table'] = $table;
			
			if($order_method == 'DESC')
			{
				$order_method = 'ASC';
			}
			else
			{
				$order_method = 'DESC';
			}
			$v_data['total_patients'] = $this->reports_model->get_total_visits($where, $table);
			$v_data['total_services_revenue'] = $this->reports_model->get_total_services_revenue($where, $table);
			$v_data['total_payments'] = $this->reports_model->get_total_cash_collection($where, $table);
			
			$v_data['order'] = $order;
			$v_data['order_method'] = $order_method;
			$v_data['visit_type_name'] = $visit_type_name;
			$v_data['visit_type_id'] = $visit_type_id;
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			$v_data['search'] = $visit_search;
			
			$data['title'] = $this->session->userdata('page_title');
			$v_data['title'] = $this->session->userdata('page_title');
			$v_data['debtors'] = $this->session->userdata('debtors');
			
			$v_data['services_query'] = $this->reports_model->get_all_active_services();
			$v_data['type'] = $this->reception_model->get_types();
			$v_data['doctors'] = $this->reception_model->get_doctor();
			//$v_data['module'] = $module;
			
			$data['content'] = $this->load->view('reports/debtors_report_invoices', $v_data, true);
		}
		
		else
		{
			$data['title'] = $this->session->userdata('page_title');
			$data['content'] = 'Please add debtors first';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function create_new_batch($visit_type_id)
	{
		$this->form_validation->set_rules('invoice_date_from', 'Invoice date from', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_date_to', 'Invoice date to', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->reports_model->add_debtor_invoice($visit_type_id))
			{
				
			}
			
			else
			{
				
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		//echo 'done '.$visit_type_id;
		redirect('accounts/insurance-invoices/'.$visit_type_id);
	}
	
	public function export_debt_transactions($debtor_invoice_id)
	{
		$this->reports_model->export_debt_transactions($debtor_invoice_id);
	}


	
	public function view_invoices($debtor_invoice_id)
	{
		$where = 'debtor_invoice.debtor_invoice_id = '.$debtor_invoice_id.' AND debtor_invoice.visit_type_id = visit_type.visit_type_id';
		$table = 'debtor_invoice, visit_type';
		
		$v_data = array(
			'debtor_invoice_id'=>$debtor_invoice_id,
			'query' => $this->reports_model->get_debtor_invoice($where, $table),
			'debtor_invoice_items' => $this->reports_model->get_debtor_invoice_items($debtor_invoice_id),
			'personnel_query' => $this->personnel_model->get_all_personnel()
		);
			
		$where .= ' AND debtor_invoice.debtor_invoice_id = debtor_invoice_item.debtor_invoice_id AND visit.visit_id = debtor_invoice_item.visit_id ';
		$table .= ', visit, debtor_invoice_item';
		
		$v_data['where'] = $where;
		$v_data['table'] = $table;
			
		$data['title'] = $v_data['title'] = 'Debtors Invoice';
		
		$data['content'] = $this->load->view('reports/view_invoices', $v_data, TRUE);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function activate_debtor_invoice_item($debtor_invoice_item_id, $debtor_invoice_id)
	{
		$visit_data = array('debtor_invoice_item_status'=>0);
		$this->db->where('debtor_invoice_item_id',$debtor_invoice_item_id);
		if($this->db->update('debtor_invoice_item', $visit_data))
		{
			redirect('administration/reports/view_invoices/'.$debtor_invoice_id);
		}
		else
		{
			redirect('administration/reports/view_invoices/'.$debtor_invoice_id);
		}
	}
	
	public function deactivate_debtor_invoice_item($debtor_invoice_item_id, $debtor_invoice_id)
	{
		$visit_data = array('debtor_invoice_item_status'=>1);
		$this->db->where('debtor_invoice_item_id',$debtor_invoice_item_id);
		if($this->db->update('debtor_invoice_item', $visit_data))
		{
			redirect('administration/reports/view_invoices/'.$debtor_invoice_id);
		}
		else
		{
			redirect('administration/reports/view_invoices/'.$debtor_invoice_id);
		}
	}
	
	public function invoice($debtor_invoice_id)
	{
		$where = 'debtor_invoice.debtor_invoice_id = '.$debtor_invoice_id.' AND debtor_invoice.visit_type_id = visit_type.visit_type_id';
		$table = 'debtor_invoice, visit_type';
		
		$data = array(
			'debtor_invoice_id'=>$debtor_invoice_id,
			'query' => $this->reports_model->get_debtor_invoice($where, $table),
			'debtor_invoice_items' => $this->reports_model->get_debtor_invoice_items($debtor_invoice_id),
			'personnel_query' => $this->personnel_model->get_all_personnel()
		);
			
		$where .= ' AND debtor_invoice.debtor_invoice_id = debtor_invoice_item.debtor_invoice_id AND visit.visit_id = debtor_invoice_item.visit_id ';
		$table .= ', visit, debtor_invoice_item';
		
		$data['where'] = $where;
		$data['table'] = $table;
		$data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('reports/invoice', $data);
	}
	
	public function search_debtors($visit_type_id)
	{
		$_SESSION['all_transactions_search'] = NULL;
		$_SESSION['all_transactions_tables'] = NULL;
		
		$this->session->unset_userdata('search_title');
		
		$date_from = $this->input->post('batch_date_from');
		$date_to = $this->input->post('batch_date_to');
		$batch_no = $this->input->post('batch_no');
		
		if(!empty($batch_no) && !empty($date_from) && !empty($date_to))
		{
			$search = ' AND debtor_invoice.batch_no LIKE \'%'.$batch_no.'%\' AND debtor_invoice.debtor_invoice_created >= \''.$date_from.'\' AND debtor_invoice.debtor_invoice_created <= \''.$date_to.'\'';
			$search_title = 'Showing invoices for batch no. '.$batch_no.' created between '.date('jS M Y',strtotime($date_from)).' and '.date('jS M Y',strtotime($date_to));
		}
		
		else if(!empty($batch_no) && !empty($date_from) && empty($date_to))
		{
			$search = ' AND debtor_invoice.batch_no LIKE \'%'.$batch_no.'%\' AND debtor_invoice.debtor_invoice_created LIKE \''.$date_from.'%\'';
			$search_title = 'Showing invoices for batch no. '.$batch_no.' created on '.date('jS M Y',strtotime($date_from));
		}
		
		else if(!empty($batch_no) && empty($date_from) && !empty($date_to))
		{
			$search = ' AND debtor_invoice.batch_no LIKE \'%'.$batch_no.'%\' AND debtor_invoice.debtor_invoice_created LIKE \''.$date_to.'%\'';
			$search_title = 'Showing invoices for batch no. '.$batch_no.' created on '.date('jS M Y',strtotime($date_to));
		}
		
		else if(empty($batch_no) && !empty($date_from) && !empty($date_to))
		{
			$search = ' AND debtor_invoice.debtor_invoice_created >= \''.$date_from.'\' AND debtor_invoice.debtor_invoice_created <= \''.$date_to.'\'';
			$search_title = 'Showing invoices created between '.date('jS M Y',strtotime($date_from)).' and '.date('jS M Y',strtotime($date_to));
		}
		
		else if(empty($batch_no) && !empty($date_from) && empty($date_to))
		{
			$search = ' AND debtor_invoice.debtor_invoice_created LIKE \''.$date_from.'%\'';
			$search_title = 'Showing invoices created created on '.date('jS M Y',strtotime($date_from));
		}
		
		else if(empty($batch_no) && empty($date_from) && !empty($date_to))
		{
			$search = ' AND debtor_invoice.debtor_invoice_created LIKE \''.$date_to.'%\'';
			$search_title = 'Showing invoices created created on '.date('jS M Y',strtotime($date_to));
		}
		else if(!empty($batch_no) && empty($date_from) && empty($date_to))
		{
			$search = ' AND debtor_invoice.batch_no LIKE \'%'.$batch_no.'%\'';
			$search_title = 'Showing invoices for batch no. '.$batch_no;
		}
		
		else
		{
			$search = '';
			$search_title = '';
		}
		
		
		$_SESSION['all_transactions_search'] = $search;
		
		$this->session->set_userdata('search_title', $search_title);
		
		redirect('administration/reports/debtors_report_data/'.$visit_type_id);
	}
	
	public function search_transactions($module = NULL)
	{
		$property_id = $this->input->post('property_id');
		$payment_date_from = $this->input->post('payment_date_from');
		$payment_date_to = $this->input->post('payment_date_to');
		$branch_code = $this->input->post('branch_code');
		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'Showing reports for: ';
		
		if(!empty($property_id))
		{
			$property_id = ' AND property.property_id = '.$property_id.' ';
			
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}
		}
		
		if(!empty($payment_date_from) && !empty($payment_date_to))
		{
			$payment_date = ' AND payments.payment_date BETWEEN \''.$payment_date_from.'\' AND \''.$payment_date_to.'\'';
			$search_title .= 'payment date from '.date('jS M Y', strtotime($payment_date_from)).' to '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else if(!empty($payment_date_from))
		{
			$payment_date = ' AND payments.payment_date = \''.$payment_date_from.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($payment_date_from)).' ';
		}
		
		else if(!empty($payment_date_to))
		{
			$payment_date = ' AND payments.payment_date = \''.$payment_date_to.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else
		{
			$payment_date = '';
		}

		$search = $property_id.$payment_date;

		$property_search = $property_id;

		// $transactions_search = $this->session->userdata('all_transactions_search');
		
		
		$this->session->set_userdata('all_transactions_search', $search);
		$this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('search_title', $search_title);
		
		$this->all_transactions($module);
	}


	public function search_transactions_owners()
	{
		$property_id = $this->input->post('property_id');
		$payment_date_from = $this->input->post('payment_date_from');
		$payment_date_to = $this->input->post('payment_date_to');
		$branch_code = $this->input->post('branch_code');
		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'Showing reports for: ';
		
		if(!empty($property_id))
		{
			$property_id = ' AND property.property_id = '.$property_id.' ';
			
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}
		}
		
		if(!empty($payment_date_from) && !empty($payment_date_to))
		{
			$payment_date = ' AND home_owners_payments.payment_date BETWEEN \''.$payment_date_from.'\' AND \''.$payment_date_to.'\'';
			$search_title .= 'payment date from '.date('jS M Y', strtotime($payment_date_from)).' to '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else if(!empty($payment_date_from))
		{
			$payment_date = ' AND home_owners_payments.payment_date = \''.$payment_date_from.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($payment_date_from)).' ';
		}
		
		else if(!empty($payment_date_to))
		{
			$payment_date = ' AND home_owners_payments.payment_date = \''.$payment_date_to.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else
		{
			$payment_date = '';
		}

		$search = $property_id.$payment_date;

		$property_search = $property_id;

		// $transactions_search = $this->session->userdata('all_transactions_search');
		
		
		$this->session->set_userdata('all_owners_transactions_search', $search);
		$this->session->set_userdata('owners_property_search', $property_search);
		$this->session->set_userdata('owners_search_title', $search_title);
		
		$this->all_transactions_owners();
	}


	public function search_defaulters($module = NULL)
	{
		$property_id = $this->input->post('property_id');
		$payment_date_from = $this->input->post('payment_date_from');
		$payment_date_to = $this->input->post('payment_date_to');
		$branch_code = $this->input->post('branch_code');
		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'Showing reports for: ';
		
		if(!empty($property_id))
		{
			$property_id = ' AND property.property_id = '.$property_id.' ';
			
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}
		}
		
		if(!empty($payment_date_from) && !empty($payment_date_to))
		{
			$payment_date = ' AND payments.payment_date BETWEEN \''.$payment_date_from.'\' AND \''.$payment_date_to.'\'';
			$search_title .= 'payment date from '.date('jS M Y', strtotime($payment_date_from)).' to '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else if(!empty($payment_date_from))
		{
			$payment_date = ' AND payments.payment_date = \''.$payment_date_from.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($payment_date_from)).' ';
		}
		
		else if(!empty($payment_date_to))
		{
			$payment_date = ' AND payments.payment_date = \''.$payment_date_to.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else
		{
			$payment_date = '';
		}

		$search = $property_id.$payment_date;

		$property_search = $property_id;

		$defaulters_search = $this->session->userdata('all_defaulters_search');
		
		if(!empty($defaulters_search))
		{
			$search .= $defaulters_search;
		}
		$this->session->set_userdata('all_defaulters_search', $search);
		$this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('search_title', $search_title);
		
		$this->all_defaulters($module);
	}
	public function search_defaulters_owners()
	{
		$property_id = $this->input->post('property_id');
		$payment_date_from = $this->input->post('payment_date_from');
		$payment_date_to = $this->input->post('payment_date_to');
		$branch_code = $this->input->post('branch_code');
		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title_owners = 'Showing reports for: ';
		
		if(!empty($property_id))
		{
			$property_id = ' AND property.property_id = '.$property_id.' ';
			
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title_owners .= $row->property_name.' ';
			}
		}
		
		if(!empty($payment_date_from) && !empty($payment_date_to))
		{
			$payment_date = ' AND payments.payment_date BETWEEN \''.$payment_date_from.'\' AND \''.$payment_date_to.'\'';
			$search_title_owners .= 'payment date from '.date('jS M Y', strtotime($payment_date_from)).' to '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else if(!empty($payment_date_from))
		{
			$payment_date = ' AND payments.payment_date = \''.$payment_date_from.'\'';
			$search_title_owners .= 'payment date of '.date('jS M Y', strtotime($payment_date_from)).' ';
		}
		
		else if(!empty($payment_date_to))
		{
			$payment_date = ' AND payments.payment_date = \''.$payment_date_to.'\'';
			$search_title_owners .= 'payment date of '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else
		{
			$payment_date = '';
		}

		$search = $property_id.$payment_date;

		$property_search = $property_id;

		$defaulters_search = $this->session->userdata('all_defaulters_owners_search');
		
		if(!empty($defaulters_search))
		{
			$search .= $defaulters_search;
		}
		$this->session->set_userdata('all_defaulters_owners_search', $search);
		$this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('search_title_owners', $search_title_owners);
		
		$this->all_defaulters_owners();

	}
	public function close_debtors_search($visit_type_id)
	{
		$_SESSION['all_transactions_search'] = NULL;
		$_SESSION['all_transactions_tables'] = NULL;
		
		$this->session->unset_userdata('search_title');
		redirect('administration/reports/debtors_report_data/'.$visit_type_id);
	}
	
	public function cash_report()
	{
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}
		
		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		
		$where = 'payments.payment_method_id = payment_method.payment_method_id AND payments.visit_id = visit.visit_id AND payments.payment_type = 1 AND visit.visit_delete = 0 AND visit.branch_code = \''.$branch_code.'\' AND visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND payments.cancel = 0';
		
		$table = 'payments, visit, patients, visit_type, payment_method';
		$visit_search = $this->session->userdata('cash_report_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		$segment = 3;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'hospital-reports/cash-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_payments($table, $where, $config["per_page"], $page, 'ASC');
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['search'] = $visit_search;
		$v_data['total_patients'] = $config['total_rows'];
		$v_data['total_payments'] = $this->reports_model->get_total_cash_collection($where, $table, 'cash');
		
		//all normal payments
		$where2 = $where.' AND payments.payment_type = 1';
		$v_data['normal_payments'] = $this->reports_model->get_normal_payments($where2, $table, 'cash');
		$v_data['payment_methods'] = $this->reports_model->get_payment_methods($where2, $table, 'cash');
		
		//normal payments
		$where2 = $where.' AND payments.payment_type = 1';
		$v_data['total_cash_collection'] = $this->reports_model->get_total_cash_collection($where2, $table, 'cash');
		
		//count outpatient visits
		$where2 = $where.' AND patients.inpatient = 0';
		$v_data['outpatients'] = $this->reception_model->count_items($table, $where2);
		
		//count inpatient visits
		$where2 = $where.' AND patients.inpatient = 1';
		$v_data['inpatients'] = $this->reception_model->count_items($table, $where2);
		
		$page_title = $this->session->userdata('cash_search_title');
		
		if(empty($page_title))
		{
			$page_title = 'Cash report';
		}
		
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['debtors'] = $this->session->userdata('debtors');
		
		$v_data['branches'] = $this->reports_model->get_all_active_branches();
		$v_data['services_query'] = $this->reports_model->get_all_active_services();
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('reports/cash_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	
	public function close_cash_search()
	{
		$this->session->unset_userdata('cash_report_search');
		$this->session->unset_userdata('cash_search_title');
		
		redirect('hospital-reports/cash-report');
	}

	
	
	public function export_cash_report()
	{
		$this->reports_model->export_cash_report();
	}
	
	public function select_debtor()
	{
		$visit_type_id = $this->input->post('visit_type_id');
		
		redirect('accounts/insurance-invoices/'.$visit_type_id);
	}
	public function income_and_expense_old($date_from = NULL, $property_id = NULL)
	{
		$where = 'creditor.creditor_id = creditor_account.creditor_id AND property.property_id = creditor_account.property_id AND creditor_service.creditor_service_id = creditor_account.creditor_service_id AND creditor_account.transaction_type_id = 1 ';
		$table = 'creditor,creditor_account,property,creditor_service';
		if(!empty($date_from) && !empty($property_id))
		{
			$date_explode = explode('-',$date_from);
			$year = $date_explode[0];
			$next_year =$year+1;
			$date_to = date('\''.$next_year.'-03-31');
			$where .= 'AND creditor_account.creditor_account_date >= \''.$date_from.'\' AND creditor_account.creditor_account_date <= '.$date_to.'\'  AND creditor_account.property_id = \''.$property_id.'\'';
		}
		else if(!empty($date_from))
		{
			$date_explode = explode('-',$date_from);
			$year = $date_explode[0];
			$next_year =$year+1;
			$date_to = date('\''.$next_year.'-03-31');
			$where .= 'AND creditor_account.creditor_account_date >= \''.$date_from.'\' AND creditor_account.creditor_account_date <= '.$date_to.'\'';
		}
		else if(!empty($property_id))
		{
			$date_from = date('Y-04-01');
			$year = date('Y');
			$next_year =$year+1;
			$date_to = date('\''.$next_year.'-03-31');
			$where .= 'AND creditor_account.creditor_account_date = \''.$date_from.'\' AND creditor_account.creditor_account_date <= '.$date_to.'\' AND creditor_account.property_id = \''.$property_id.'\'';
		}
		else
		{
			$date_from = date('Y-04-01');
			$year = date('Y');
			$next_year =$year+1;
			$date_to = date('\''.$next_year.'-03-31');
			$where .= 'AND creditor_account.creditor_account_date = \''.$date_from.'\' AND creditor_account.creditor_account_date <= '.$date_to.'\'';
		}
		// var_dump($where);die();
		$v_data['properties'] = $this->property_model->get_active_property();
		$v_data['period_expenses'] = $this->reports_model->get_expenses_for_period($where,$table);
		$v_data['title'] = $data['title'] = 'Income And Expense';
		$data['content'] = $this->load->view('reports/income_and_expense', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_income_expense_statement()
	{
		$property_id = $this->input->post('property_id');
		$financial_year = $this->input->post('financial_year');
		
		$search_title_data = 'Showing reports for: ';
		
		if(!empty($property_id))
		{
			$property_id = ' AND property.property_id = '.$property_id.' ';
			
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$property_id .= $row->property_name.' ';
			}
			$property_search_item = ' AND creditor_account.property_id = '.$this->input->post('property_id').' ';

			$inflow_search_item = ' AND inflow_account.property_id = '.$this->input->post('property_id').' ';

		}
		else
		{
			$property_search_item = '';
			$inflow_search_item = '';
		}
		if(!empty($financial_year))
		{
			$financial_year = $financial_year;
			$search_title_data .= ' Financial Year '.$financial_year.' ';
		}
		
		else
		{
			$financial_year = 'Financial Year '.date('Y').' ';
		}


		$property_search = $property_id;


		// $transactions_search = $this->session->userdata('all_transactions_search');
		
		
		$this->session->set_userdata('financial_year_search', $financial_year);
		$this->session->set_userdata('search_property', $property_search);
		$this->session->set_userdata('search_title_data', $search_title_data);
		$this->session->set_userdata('property_search_id', $property_search_item);
		$this->session->set_userdata('property_inflow_id', $inflow_search_item);
		
		redirect('reports/income-and-expense');




	}

	public function close_income_search()
	{
		$this->session->unset_userdata('search_property');
		$this->session->unset_userdata('financial_year_search');
		$this->session->unset_userdata('search_title_data');
		$this->session->unset_userdata('property_search_id');
		$this->session->unset_userdata('property_inflow_id');
		
		redirect('reports/income-and-expense');
	}
	public function income_and_expense()
	{
		$inflow_income_search = $this->session->userdata('search_property');
		$other_inflow = $this->session->userdata('property_inflow_id');
		if(!empty($inflow_income_search) OR !empty($other_inflow))
		{
			$v_data['all_invoice_types'] = $this->reports_model->get_all_active_invoice_types_per_property($inflow_income_search);
			$v_data['all_creditor_services'] = $this->reports_model->get_all_active_creditor_services_per_property($inflow_income_search);
			$v_data['all_other_inflows'] = $this->reports_model->get_all_active_inflow_services_per_property($other_inflow);
		}
		else
		{
			$v_data['all_invoice_types'] = $this->reports_model->get_all_active_invoice_types();
			$v_data['all_creditor_services'] = $this->reports_model->get_all_active_creditor_services();
			$v_data['all_other_inflows'] = $this->reports_model->get_all_active_other_inflow();
		}
		$v_data['all_creditors'] = $this->reports_model->get_all_active_creditors();
		$v_data['properties'] = $this->property_model->get_active_property();
		$v_data['title'] = $data['title'] = 'Income And Expense';
		$data['content'] = $this->load->view('reports/income_and_expense', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);

	}

	public function income_and_expense_print()
	{
		$inflow_income_search = $this->session->userdata('search_property');
		if(!empty($inflow_income_search))
		{
			$v_data['all_invoice_types'] = $this->reports_model->get_all_active_invoice_types_per_property($inflow_income_search);
			$v_data['all_creditor_services'] = $this->reports_model->get_all_active_creditor_services_per_property($inflow_income_search);
		}
		else
		{
			$v_data['all_invoice_types'] = $this->reports_model->get_all_active_invoice_types();
			$v_data['all_creditor_services'] = $this->reports_model->get_all_active_creditor_services();
		}
		$v_data['all_creditors'] = $this->reports_model->get_all_active_creditors();
		$v_data['properties'] = $this->property_model->get_active_property();

		$v_data['contacts'] = $this->site_model->get_contacts();
		$v_data['title'] = $data['title'] = 'Income And Expense';
		$this->load->view('reports/incomeandexpense_print', $v_data);

	}
	public function income_and_expense_older($date_from = NULL,$property_id = NULL)
	{
		$outflows_where = 'creditor_account.creditor_account_status = 1';
		$tenant_infows_where = 'payments.payment_status = 1 AND payments.cancel = 0';
		$outflows_table = 'creditor_account';
		$tenant_table = 'payments,payment_item';
		$home_owner_table = 'home_owner_payment_item';
		$home_owners_where = 'home_owner_payment_item.payment_item_status = 1';
		// var_dump(expression)
		if(!empty($date_from) && !empty($property_id))
		{
			$date_explode = explode('-',$date_from);
			$year = $date_explode[0];
			$next_year =$year+1;
			$date_to = date('\''.$next_year.'-03-31');
			$outflows_where .= ' AND creditor_account.creditor_account_date >= \''.$date_from.'\' AND creditor_account.creditor_account_date <= '.$date_to.'\'  AND creditor_account.property_id = \''.$property_id.'\'';

			$tenant_infows_where .=' AND payment_item.payment_id = payments.payment_id  AND payments.payment_date >= \''.$date_from.'\' AND payments.payment_date <=  '.$date_to.'\'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND property.property_id = \''.$property_id.'\'';

			// var_dump($tenant_infows_where);die();
			$tenant_table .= ',leases,rental_unit,property';
			$home_owners_where .=' AND home_owner_payment_item.payment_item_created >= \''.$date_from.'\' AND home_owner_payment_item.payment_item_created <= '.$date_to.'\'  AND home_owner_payment_item.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND property.property_id = \''.$property_id.'\'';
			$home_owner_table .= ',rental_unit,property';
		}
		
		else if(!empty($property_id))
		{
			$date_from = date('Y-04-01');
			$year = date('Y');
			$next_year =$year+1;
			$date_to = date('\''.$next_year.'-03-31');
			$outflows_where .= 'AND creditor_account.creditor_account_date = \''.$date_from.'\' AND creditor_account.creditor_account_date <= '.$date_to.'\' AND creditor_account.property_id = \''.$property_id.'\'';
			$tenant_infows_where .=' AND payment_item.payment_item_created >= \''.$date_from.'\' AND payment_item.payment_item_created <= '.$date_to.'\'  AND payment_item.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND property.property_id = \''.$property_id.'\'';
			
			$tenant_table .= 'leases,rental_unit,property';
			$home_owners_where .=' AND home_owner_paymnent_item.payment_item_created >= \''.$date_from.'\' AND home_owner_paymnent_item.payment_item_created <= '.$date_to.'\'  AND home_owner_paymnent_item.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND property.property_id = \''.$property_id.'\'';
			$home_owner_table .= ',rental_unit,property';
		}
		else
		{
			$date_from = date('Y-04-01');
			$year = date('Y');
			$next_year =$year+1;
			$date_to = date('\''.$next_year.'-03-31');
			$outflows_where .= ' AND creditor_account.creditor_account_date >= \''.$date_from.'\' AND creditor_account.creditor_account_date <= '.$date_to.'\'';
			$tenant_infows_where .=' AND payment_item.payment_item_created >= \''.$date_from.'\' AND payment_item.payment_item_created <= '.$date_to.'\'
AND payments.lease_id = leases.lease_id 
AND leases.rental_unit_id = rental_unit.rental_unit_id 
AND rental_unit.property_id = property.property_id 
AND payments.payment_id = payment_item.payment_id';
			$tenant_table .= ',leases,rental_unit,property,payments';
			$home_owners_where .=' AND home_owner_payment_item.payment_item_created >= \''.$date_from.'\' AND home_owner_payment_item.payment_item_created <= '.$date_to.'\'  AND home_owner_payment_item.rental_unit_id = rental_unit.rental_unit_id 
AND rental_unit.property_id = property.property_id 
AND home_owners_payments.payment_id = home_owner_payment_item.payment_id';
			$home_owner_table .= ',rental_unit,property, home_owners_payments';
		}
		//var_dump($tenant_infows_where);die();
		//select all creditors
		$v_data['all_creditors'] = $this->reports_model->get_all_active_creditors();
		$v_data['all_invoice_types'] = $this->reports_model->get_all_active_invoice_types();
		$v_data['all_creditor_services'] = $this->reports_model->get_all_active_creditor_services();
		$v_data['all_creditor_accounts'] = $this->reports_model->get_all_active_creditor_accounts($outflows_where);
		$v_data['all_home_owner_accounts'] = $this->reports_model->get_all_home_owner_payments($home_owners_where,$home_owner_table);
		$v_data['all_tenants_account'] = $this->reports_model->get_all_tenant_payments($tenant_infows_where,$tenant_table);
		$v_data['properties'] = $this->property_model->get_active_property();
		$v_data['title'] = $data['title'] = 'Income And Expense';
		$data['content'] = $this->load->view('reports/income_and_expense', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_property_report()
	{
		$properties = $this->property_model->get_active_property();
		$months = $this->property_model->get_months();
		$rs8 = $properties->result();
		
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."_".$property_name."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		
		$month_list = '';
		if($months->num_rows() > 0)
		{
			foreach ($months->result() as $month_rs) :
				$month_id = $month_rs->month_id;
				$month_name = $month_rs->month_name;
				$month_list .="<option value='".$month_id."_".$month_name."'>".$month_name."</option>";
			endforeach;
		}
		$v_data['months'] = $month_list;
		$v_data['property_list'] = $property_list;
		$v_data['title'] = $data['title'] = 'Search Property';
		$data['content'] = $this->load->view('reports/search_property', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function property($property_id=NULL,$month_id=NULL,$year=NULL)
	{
		//$payment_month = $this->reports_model->get_payment_item_month();
		$table = 'rental_unit,property';
		
		$property = $this->input->post('property_id');
		$month = $this->input->post('month_id');	
		$year = $this->input->post('year');
		
		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');
		$this->form_validation->set_rules('month_id', 'Month', 'xss_clean');
		//we can't have month without year
		if(!empty($month_id))
		{
			$this->form_validation->set_rules('year', 'Year', 'required|xss_clean');
		}
		
		else
		{
			$this->form_validation->set_rules('year', 'Year', 'xss_clean');
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			//get property data
			$property_array = explode('_', $property);
			$property_id = $property_array[0];
			$property_name = $property_array[1];

			$this->db->where('property_id',$property_id);
			$query_result = $this->db->get('property');
			$return_date = '01';
			if($query_result->num_rows() > 0)
			{
				foreach ($query_result->result() as $key => $value) {
					# code...
					$return_date = $value->return_date;
				}
			}
			if(empty($return_date))
			{
				$return_date = '01';
			}

			
			//get month data
			$month_array = explode('_', $month);
			$month_id = $month_array[0];
			$month_name = $month_array[1];

			$this->session->set_userdata('month',$month_id);
			$this->session->set_userdata('year',$year);
			$this->session->set_userdata('return_date',$return_date);
			$this->session->set_userdata('property_id',$property_id);
			
			if((!empty($property_id))&&(!empty($month_id)) &&(!empty($year)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
				
				$title = $property_name.' Report for '.$month_name.' '.$year;
			}
			//Shouldn't apply. Month needs year
			elseif((!empty($property_id))&&(!empty($month_id)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
				
				$title = $property_name.' Report for '.$month_name;
			}
			elseif((!empty($month_id)) &&(!empty($year)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND payments.lease_id = leases.lease_id  AND property.property_id = rental_unit.property_id AND invoice.lease_id = leases.lease_id AND invoice.invoice_month = '.$month_id;
				
				$title = $property_name.' Report for '.$month_name.' '.$year;
			}
			elseif((!empty($property_id))&&(!empty($year)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
				
				$title = $property_name.' Report for '.$year;
			}
			else
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id';
				
				$title = $property_name.' Report';
			}
			// var_dump($where);die();

			$where = 'rental_unit.property_id = property.property_id AND rental_unit.property_id = '.$property_id;
			$segment = 3;
			//pagination

			// var_dump($where); die();
			
			$this->load->library('pagination');
			$config['base_url'] = base_url().'reports/property';
			$config['total_rows'] = $this->tenants_model->count_items($table, $where);
			$config['uri_segment'] = $segment;
			$config['per_page'] = 200;
			$config['num_links'] = 5;
			
			$config['full_tag_open'] = '<ul class="pagination pull-right">';
			$config['full_tag_close'] = '</ul>';
			
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			$config['next_tag_open'] = '<li>';
			$config['next_link'] = 'Next';
			$config['next_tag_close'] = '</span>';
			
			$config['prev_tag_open'] = '<li>';
			$config['prev_link'] = 'Prev';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active">';
			$config['cur_tag_close'] = '</li>';
			
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
			$v_data["links"] = $this->pagination->create_links();
			$query = $this->reports_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');
			//var_dump($query->num_rows());
			// $this->accounts_model->update_invoices();
			$data['contacts'] = $this->site_model->get_contacts();
			$data['title'] = $v_data['title'] = $title;
			$v_data['query'] = $data['query'] =  $query;
			$v_data['page'] = $data['page'] = $page;
			echo $this->load->view('real_estate_administration/property/property_statement', $data, true);
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
			
			redirect('reports/property');
		}
	}
	public function property_old($property_id=NULL,$month_id=NULL,$year=NULL)
	{
		//$payment_month = $this->reports_model->get_payment_item_month();
		$table = 'tenants,tenant_unit,rental_unit,leases,property';
		
		$property = $this->input->post('property_id');
		$month = $this->input->post('month_id');	
		$year = $this->input->post('year');
		
		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');
		$this->form_validation->set_rules('month_id', 'Month', 'xss_clean');
		//we can't have month without year
		if(!empty($month_id))
		{
			$this->form_validation->set_rules('year', 'Year', 'required|xss_clean');
		}
		
		else
		{
			$this->form_validation->set_rules('year', 'Year', 'xss_clean');
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			//get property data
			$property_array = explode('_', $property);
			$property_id = $property_array[0];
			$property_name = $property_array[1];
			
			//get month data
			$month_array = explode('_', $month);
			$month_id = $month_array[0];
			$month_name = $month_array[1];

			$this->session->set_userdata('month',$month_id);
			$this->session->set_userdata('year',$year);
			$this->session->set_userdata('property_id',$property_id);
			
			if((!empty($property_id))&&(!empty($month_id)) &&(!empty($year)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
				
				$title = $property_name.' Report for '.$month_name.' '.$year;
			}
			//Shouldn't apply. Month needs year
			elseif((!empty($property_id))&&(!empty($month_id)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
				
				$title = $property_name.' Report for '.$month_name;
			}
			elseif((!empty($month_id)) &&(!empty($year)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND payments.lease_id = leases.lease_id  AND property.property_id = rental_unit.property_id AND invoice.lease_id = leases.lease_id AND invoice.invoice_month = '.$month_id;
				
				$title = $property_name.' Report for '.$month_name.' '.$year;
			}
			elseif((!empty($property_id))&&(!empty($year)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
				
				$title = $property_name.' Report for '.$year;
			}
			else
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id';
				
				$title = $property_name.' Report';
			}
			// var_dump($where);die();

			// $where .=' AND CASE WHEN leases.closing_end_date IS NOT NULL THEN MONTH(leases.closing_end_date) <= 3 AND YEAR(leases.closing_end_date) = 2017 END';
			$segment = 3;
			//pagination
			
			$this->load->library('pagination');
			$config['base_url'] = base_url().'reports/property';
			$config['total_rows'] = $this->tenants_model->count_items($table, $where);
			$config['uri_segment'] = $segment;
			$config['per_page'] = 200;
			$config['num_links'] = 5;
			
			$config['full_tag_open'] = '<ul class="pagination pull-right">';
			$config['full_tag_close'] = '</ul>';
			
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			$config['next_tag_open'] = '<li>';
			$config['next_link'] = 'Next';
			$config['next_tag_close'] = '</span>';
			
			$config['prev_tag_open'] = '<li>';
			$config['prev_link'] = 'Prev';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active">';
			$config['cur_tag_close'] = '</li>';
			
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
			$v_data["links"] = $this->pagination->create_links();
			$query = $this->reports_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');
			
			// $this->accounts_model->update_invoices();
			$data['contacts'] = $this->site_model->get_contacts();
			$data['title'] = $v_data['title'] = $title;
			$v_data['query'] = $data['query'] =  $query;
			$v_data['page'] = $data['page'] = $page;
			echo $this->load->view('real_estate_administration/property/property_statement', $data, true);
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
			
			redirect('reports/property');
		}
	}
	public function owner_return()
	{
		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';		


		$return_search = $this->session->userdata('all_return_search');
		
		if(!empty($return_search))
		{
			$where .= $return_search;	
			
		}
		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'cash-office/accounts';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		
		// $this->accounts_model->update_invoices();
		

		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Tenants Accounts';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/tenants_list', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function print_tenant_statement($lease_id)
	{
		$data = array('lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);

		
		$this->load->view('tenantstatement', $data);
	}
	public function print_owner_statement($rental_unit_id,$home_owner_id)
	{
		$v_data = array('rental_unit_id'=>$rental_unit_id,'home_owner_id'=>$home_owner_id);
		$v_data['contacts'] = $this->site_model->get_contacts();

		$this->load->view('ownerstatement', $v_data);
		
		
	}
	public function send_tenant_statements($lease_id)
	{
		$data = array('lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);

		
		
		$all_leases = $this->leases_model->get_lease_detail($lease_id);


		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$rental_unit_id = $leases_row->rental_unit_id;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;
			$message_prefix = $leases_row->message_prefix;
			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];

		$data['rental_unit_id'] = $lease_id;

		
		$html = $this->load->view('tenantstatement', $data, true );

		$this->load->library('mpdf');
	     //var_dump($html);die();
		// $document_number = date("ymdhis");
		$document_number = 'Statement'.$lease_id;


		$title = $document_number.'.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;
		// echo $invoice;die();
		
		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}
		// var_dump($tenant_phone_number);die();

		//$tenant_email="robert.gichohi@gmail.com"; 

		if(!empty($tenant_email))
		{
			$message['subject'] =  $document_number.' Invoice';
			$message['text'] = ' <p>Dear '.$tenant_name.',<br>
									Please find attached Statement
									</p>
								';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email']; 
			
			$button = '';
			$sender['email'] = $contacts['email']; 
			$sender['name'] = $contacts['company_name'];
			// $receiver['email'] = $tenant_email;
			$receiver['name'] = $tenant_name;
			$payslip = $title;
			// $tenant_email ='marttkip@gmail.com';
			$tenant_email .= '/homesandabroad@gmail.com';
			$email_array = explode('/', $tenant_email);
			$total_rows_email = count($email_array);


			//

			for($x = 0; $x < $total_rows_email; $x++)
			{
				$receiver['email'] = $email_array[$x];
				//var_dump($receiver);die();
				$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
				//$insert_array = array('email'=>$receiver['email'],'client_name'=>$tenant_name,'email_body'=>$message['text'],'type_of_account'=>1,'email_attachment'=>$title,'date_created'=>date('Y-m-d'), 'rental_unit_id'=>$rental_unit_id, 'invoice_year'=>$_year, 'invoice_month'=>$_month, 'email_sent'=>1);
				//$this->db->insert('email',$insert_array);
				

			}
		}
		redirect('reports/tenants-accounts');
		// var_dump($tenant_phone_number);die();
		// send SMS to the person
		// $tenant_phone_number = 704808007;
	}
	public function send_owner_statements($rental_unit_id,$home_owner_id)
	{
		$v_data = array('rental_unit_id'=>$rental_unit_id,'home_owner_id'=>$home_owner_id);
		$v_data['contacts'] = $contacts = $this->site_model->get_contacts();

		
		$all_leases = $this->leases_model->get_owner_unit_detail($rental_unit_id);

		foreach ($all_leases->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$property_id = $leases_row->property_id;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_email = $leases_row->home_owner_email;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$home_owner_unit_id = $leases_row->home_owner_unit_id;

		}

		$starting_arreas = $arrears_amount =$this->accounts_model->get_all_owners_arrears($rental_unit_id);
		$owners_response = $this->reports_model->get_owners_billings($rental_unit_id,$home_owner_id);

		
		
		
		$html = $this->load->view('ownerstatement', $v_data, true);

		$this->load->library('mpdf');
	     //var_dump($html);die();
		// $document_number = date("ymdhis");
		$document_number = 'Statement'.$home_owner_id;


		$title = $document_number.'.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;
		// echo $invoice;die();
		
		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}
		// var_dump($tenant_phone_number);die();

		// $home_owner_email="marttkip@gmail.com"; 

		// var_dump($home_owner_email); die();

		if(!empty($home_owner_email))
		{
			$message['subject'] =  $document_number.' Invoice';
			$message['text'] = ' <p>Dear '.$home_owner_name.',<br>
									Please find attached Statement
									</p>
								';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email']; 
			// var_dump($from); die();
			$button = '';
			$sender['email'] = $contacts['email']; 
			$sender['name'] = $contacts['company_name'];
			// $receiver['email'] = $tenant_email;
			$receiver['name'] = $home_owner_name;
			$payslip = $title;
			// $home_owner_email = 'marttkip@gmail.com';
			$home_owner_email .= '/homesandabroad@gmail.com';
			$email_array = explode('/', $home_owner_email);
			$total_rows_email = count($email_array);
			// var_dump($home_owner_email); die();

			//

			for($x = 0; $x < $total_rows_email; $x++)
			{
				$receiver['email'] = $email_array[$x];
				$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
				

			}
		}
		redirect('reports/owners-accounts');
	}

	public function search_property_report_total_payments()
	{
		$properties = $this->property_model->get_active_property();
		$months = $this->property_model->get_months();
		$rs8 = $properties->result();
		
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."_".$property_name."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		
		$month_list = '';
		if($months->num_rows() > 0)
		{
			foreach ($months->result() as $month_rs) :
				$month_id = $month_rs->month_id;
				$month_name = $month_rs->month_name;
				$month_name = "One month till the 12th of ".$month_name;
				$month_list .="<option value='".$month_id."_".$month_name."'>".$month_name."</option>";
			endforeach;
		}
		$v_data['months'] = $month_list;
		$v_data['property_list'] = $property_list;
		$v_data['title'] = $data['title'] = 'Search Property';
		$data['content'] = $this->load->view('reports/search_total_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_agency_property_report()
	{
		$properties = $this->property_model->get_active_property();
		$months = $this->property_model->get_months();
		$rs8 = $properties->result();
		
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."_".$property_name."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		
		$month_list = '';
		if($months->num_rows() > 0)
		{
			foreach ($months->result() as $month_rs) :
				$month_id = $month_rs->month_id;
				$month_name = $month_rs->month_name;
				$month_list .="<option value='".$month_id."_".$month_name."'>".$month_name."</option>";
			endforeach;
		}
		$v_data['months'] = $month_list;
		$v_data['property_list'] = $property_list;
		$v_data['title'] = $data['title'] = 'Search Agency Property';
		$data['content'] = $this->load->view('reports/search_agency_property', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function agency_property($property_id=NULL,$month_id=NULL,$year=NULL)
	{
		//$payment_month = $this->reports_model->get_payment_item_month();
		$table = 'rental_unit,property';
		
		$property = $this->input->post('property_id');
		$month = $this->input->post('month_id');	
		$year = $this->input->post('year');
		
		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');
		$this->form_validation->set_rules('month_id', 'Month', 'xss_clean');
		//we can't have month without year
		if(!empty($month_id))
		{
			$this->form_validation->set_rules('year', 'Year', 'required|xss_clean');
		}
		
		else
		{
			$this->form_validation->set_rules('year', 'Year', 'xss_clean');
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			//get property data
			$property_array = explode('_', $property);
			$property_id = $property_array[0];
			$property_name = $property_array[1];

			$this->db->where('property_id',$property_id);
			$query_result = $this->db->get('property');
			$return_date = '01';
			if($query_result->num_rows() > 0)
			{
				foreach ($query_result->result() as $key => $value) {
					# code...
					$return_date = $value->return_date;
				}
			}
			if(empty($return_date))
			{
				$return_date = '01';
			}

			
			//get month data
			$month_array = explode('_', $month);
			$month_id = $month_array[0];
			$month_name = $month_array[1];

			$this->session->set_userdata('month',$month_id);
			$this->session->set_userdata('year',$year);
			$this->session->set_userdata('return_date',$return_date);
			$this->session->set_userdata('property_id',$property_id);
			
			if((!empty($property_id))&&(!empty($month_id)) &&(!empty($year)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
				
				$title = $property_name.' Report for '.$month_name.' '.$year;
			}
			//Shouldn't apply. Month needs year
			elseif((!empty($property_id))&&(!empty($month_id)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
				
				$title = $property_name.' Report for '.$month_name;
			}
			elseif((!empty($month_id)) &&(!empty($year)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND payments.lease_id = leases.lease_id  AND property.property_id = rental_unit.property_id AND invoice.lease_id = leases.lease_id AND invoice.invoice_month = '.$month_id;
				
				$title = $property_name.' Report for '.$month_name.' '.$year;
			}
			elseif((!empty($property_id))&&(!empty($year)))
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
				
				$title = $property_name.' Report for '.$year;
			}
			else
			{
				$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id';
				
				$title = $property_name.' Report';
			}
			// var_dump($where);die();

			$where = 'rental_unit.property_id = property.property_id AND rental_unit.property_id = '.$property_id;
			$segment = 3;
			//pagination

			// var_dump($where); die();
			
			$this->load->library('pagination');
			$config['base_url'] = base_url().'reports/property';
			$config['total_rows'] = $this->tenants_model->count_items($table, $where);
			$config['uri_segment'] = $segment;
			$config['per_page'] = 200;
			$config['num_links'] = 5;
			
			$config['full_tag_open'] = '<ul class="pagination pull-right">';
			$config['full_tag_close'] = '</ul>';
			
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			$config['next_tag_open'] = '<li>';
			$config['next_link'] = 'Next';
			$config['next_tag_close'] = '</span>';
			
			$config['prev_tag_open'] = '<li>';
			$config['prev_link'] = 'Prev';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active">';
			$config['cur_tag_close'] = '</li>';
			
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
			$v_data["links"] = $this->pagination->create_links();
			$query = $this->reports_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');
			//var_dump($query->num_rows());
			// $this->accounts_model->update_invoices();
			$data['contacts'] = $this->site_model->get_contacts();
			$data['title'] = $v_data['title'] = $title;
			$v_data['query'] = $data['query'] =  $query;
			$v_data['page'] = $data['page'] = $page;
			echo $this->load->view('real_estate_administration/property/agency_property_statement', $data, true);
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
			
			redirect('reports/agency_property');
		}
	}


}
?>