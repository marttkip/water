<?php

class Reports_model extends CI_Model 
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	
	public function get_queue_total($date = NULL, $where = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		if($where == NULL)
		{
			$where = 'visit.visit_id = visit_department.visit_id AND visit.close_card = 0 AND visit.visit_date = \''.$date.'\'';
		}
		
		else
		{
			$where .= ' AND visit.visit_id = visit_department.visit_id AND visit.close_card = 0 AND visit.visit_date = \''.$date.'\' ';
		}
		
		$this->db->select('COUNT(visit.visit_id) AS queue_total');
		$this->db->where($where);
		$query = $this->db->get('visit, visit_department');
		
		$result = $query->row();
		
		return $result->queue_total;
	}
	
	public function get_daily_balance($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		//select the user by email from the database
		$this->db->select('SUM(amount_paid) AS total_amount');
		$this->db->where('payment_type = 1 AND payment_method_id = 2 AND payment_created = \''.$date.'\'');
		$this->db->from('payments');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_amount;
	}
	
	public function get_patients_total($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$this->db->select('COUNT(visit_id) AS patients_total');
		$this->db->where('visit_date = \''.$date.'\'');
		$query = $this->db->get('visit');
		
		$result = $query->row();
		
		return $result->patients_total;
	}
	
	public function get_all_payment_methods()
	{
		$this->db->select('*');
		$query = $this->db->get('payment_method');
		
		return $query;
	}
	
	public function get_payment_method_total($payment_method_id, $date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where('payments.visit_id = visit.visit_id AND payment_method_id = '.$payment_method_id.' AND visit_date = \''.$date.'\'');
		$query = $this->db->get('payments, visit');
		
		$result = $query->row();
		
		return $result->total_paid;
	}
	
	public function get_all_visit_types()
	{
		$this->db->select('*');
		$query = $this->db->get('visit_type');
		
		return $query;
	}
	
	public function get_visit_type_total($visit_type_id, $date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$where = 'visit_date = \''.$date.'\' AND visit_type = '.$visit_type_id;
		
		$this->db->select('COUNT(visit_id) AS visit_total');
		$this->db->where($where);
		$query = $this->db->get('visit');
		
		$result = $query->row();
		
		return $result->visit_total;
	}
	
	public function get_patient_type_total($patient_type_id, $date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$where = 'visit_date = \''.$date.'\' AND visit.inpatient = '.$patient_type_id;
		
		$this->db->select('COUNT(visit_id) AS visit_total');
		$this->db->where($where);
		$query = $this->db->get('visit');
		
		$result = $query->row();
		
		return $result->visit_total;
	}
	public function get_all_defaulters($table, $where, $per_page, $page, $order = 'rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method = 'ASC')
	{
		//retrieve all leases
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_defaulters_items($table, $where, $order = 'rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method = 'ASC')
	{
		//retrieve all leases
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('');
		
		return $query;
	}
	
	public function get_all_service_types()
	{
		$this->db->select('*');
		$this->db->where('service_delete', 0);
		$query = $this->db->get('service');
		
		return $query;
	}
	
	public function get_service_total($service_id, $date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		
		$table = 'visit_charge, service_charge';
		
		$where = 'visit_charge_timestamp LIKE \''.$date.'%\' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service_charge.service_id = '.$service_id;
		
		$visit_search = $this->session->userdata('all_departments_search');
		if(!empty($visit_search))
		{
			$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND service_charge.service_id = '.$service_id.' AND visit.visit_id = visit_charge.visit_id'. $visit_search;
			$table .= ', visit';
		}
		
		$this->db->select('SUM(visit_charge_units*visit_charge_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		$result = $query->row();
		$total = $result->service_total;;
		
		if($total == NULL)
		{
			$total = 0;
		}
		
		return $total;
	}
	
	public function get_all_appointments($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$where = 'visit.visit_delete = 0 AND patients.patient_delete = 0 AND visit.visit_type = visit_type.visit_type_id AND visit.patient_id = patients.patient_id AND visit.appointment_id = 1 AND visit.close_card = 2 AND visit.visit_date >= \''.$date.'\' AND visit.personnel_id = personnel.personnel_id';
		
		$this->db->select('visit.*, visit_type.visit_type_name, patients.*, personnel.*');
		$this->db->where($where);
		$query = $this->db->get('visit, visit_type, patients, personnel');
		
		return $query;
	}
	
	public function get_doctor_appointments($personnel_id, $date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$where = 'visit.visit_delete = 0 AND patients.patient_delete = 0 AND visit.visit_type = visit_type.visit_type_id AND visit.patient_id = patients.patient_id AND visit.appointment_id = 1 AND visit.close_card = 2 AND visit.visit_date >= \''.$date.'\' AND visit.personnel_id = '.$personnel_id;
		
		$this->db->select('visit.*, visit_type.visit_type_name, patients.*');
		$this->db->where($where);
		$query = $this->db->get('visit, visit_type, patients');
		
		return $query;
	}
	
	public function get_all_sessions($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$where = 'personnel.personnel_id = session.personnel_id AND session.session_name_id = session_name.session_name_id AND session_time LIKE \''.$date.'%\'';
		
		$this->db->select('session_name_name, session_time, personnel_fname, personnel_onames');
		$this->db->where($where);
		$this->db->order_by('session_time', 'DESC');
		$query = $this->db->get('session, session_name, personnel');
		
		return $query;
	}
	
	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_transactions($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('payments.*,payment_method.payment_method AS payment_method, tenants.*, rental_unit.rental_unit_name,property.property_name, leases.*');
		$this->db->where($where);
		$this->db->order_by('payments.payment_date','ASC');
		// $this->db->group_by('visit.visit_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_transactions_owners($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('home_owners_payments.*,payment_method.payment_method AS payment_method, home_owners.*, rental_unit.rental_unit_name,property.property_name');
		$this->db->where($where);
		$this->db->order_by('home_owners_payments.payment_date','ASC');
		// $this->db->group_by('visit.visit_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Retrieve all active services
	*
	*/
	public function get_all_active_services()
	{
		//retrieve all users
		$this->db->from('service');
		$this->db->where('service_delete = 0');
		$this->db->order_by('service_name','ASC');
		$query = $this->db->get();
		
		return $query;
	}
	

	/*
	*	Retrieve all active services
	*
	*/
	public function get_all_active_payment_method()
	{
		//retrieve all users
		$this->db->from('payment_method');
		$this->db->where('payment_method_id > 0');
		$this->db->order_by('payment_method_id','ASC');
		$query = $this->db->get();
		
		return $query;
	}
	
	
	/*
	*	Retrieve all visit payments
	*
	*/
	public function get_all_visit_payments($visit_id)
	{
		//retrieve all users
		$this->db->from('payments');
		$this->db->select('SUM(payments.amount_paid) AS total_paid');
		$this->db->where('visit_id', $visit_id);
		// $this->db->group_by('visit_id');
		$query = $this->db->get();
		
		$cash = $query->row();
		
		if($cash->total_paid > 0)
		{
			return $cash->total_paid;
		}
		
		else
		{
			return 0;
		}
	}
	
	/*
	*	Retrieve all service charges
	*
	*/
	public function get_all_visit_charges($visit_id, $service_id)
	{
		//retrieve all users
		$this->db->from('visit_charge, service_charge');
		$this->db->select('SUM(visit_charge.visit_charge_amount * visit_charge.visit_charge_units) AS total_invoiced');
		$this->db->where('visit_charge.visit_id = '.$visit_id.' AND service_charge.service_id = '.$service_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_charge_delete = 0');
		$query = $this->db->get();
		
		$cash = $query->row();
		
		if($cash->total_invoiced > 0)
		{
			return $cash->total_invoiced;
		}
		
		else
		{
			return 0;
		}
	}
	
	public function get_service_notes($visit_id, $service_id, $payment_type)
	{
		//retrieve all users
		$this->db->from('payments');
		$this->db->select('SUM(amount_paid) AS total_invoiced');
		$this->db->where('payments.visit_id = '.$visit_id.' AND payments.payment_service_id = '.$service_id.' AND payments.payment_type = '.$payment_type);
		$query = $this->db->get();
		
		$cash = $query->row();
		
		if($cash->total_invoiced > 0)
		{
			return $cash->total_invoiced;
		}
		
		else
		{
			return 0;
		}
	}
	
	public function get_all_payment_values($visit_id,$payment_method_id)
	{
		# code...
		//retrieve all users
		$this->db->from('payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where('payments.cancel = 0 AND visit_id = '.$visit_id.' AND payment_method_id = '.$payment_method_id.' AND payment_type = 1');
		$query = $this->db->get();
		
		$cash = $query->row();
		
		if($cash->total_paid > 0)
		{
			return $cash->total_paid;
		}
		
		else
		{
			return 0;
		}
	}
	/*
	*	Retrieve total revenue
	*
	*/
	public function get_total_services_revenue($where, $table)
	{
		//invoiced
		$this->db->from($table.', visit_charge, service_charge');
		$this->db->select('SUM(visit_charge.visit_charge_amount * visit_charge.visit_charge_units) AS total_invoiced');
		$this->db->where($where.' AND visit.visit_id = visit_charge.visit_id AND visit_charge.service_charge_id = service_charge.service_charge_id');
		$query = $this->db->get();
		
		$cash = $query->row();
		$total_invoiced = $cash->total_invoiced;
		
		if($total_invoiced > 0)
		{
			
		}
		
		else
		{
			$total_invoiced = 0;
		}
		
		return $total_invoiced;
	}
	
	/*
	*	Retrieve total revenue
	*
	*/
	public function get_total_cash_collection($where, $table, $page = NULL)
	{
		//payments
		$table_search = $this->session->userdata('all_transactions_tables');
		
		if($page != 'cash')
		{
			$where .= ' AND payments.cancel = 0';
		}
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}
		
		else
		{
			$this->db->from($table);
		}
		$this->db->select('SUM(payments.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		
		$cash = $query->row();
		$total_paid = $cash->total_paid;
		if($total_paid > 0)
		{
		}
		
		else
		{
			$total_paid = 0;
		}
		
		return $total_paid;
	}


	/*
	*	Retrieve total revenue
	*
	*/
	public function get_total_cash_collection_owners($where, $table, $page = NULL)
	{
		//payments
		$table_search = $this->session->userdata('all_owners_transactions_tables');
		
		if($page != 'cash')
		{
			$where .= ' AND home_owners_payments.cancel = 0';
		}
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}
		
		else
		{
			$this->db->from($table);
		}
		$this->db->select('SUM(home_owners_payments.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		
		$cash = $query->row();
		$total_paid = $cash->total_paid;
		if($total_paid > 0)
		{
		}
		
		else
		{
			$total_paid = 0;
		}
		
		return $total_paid;
	}
	
	/*
	*	Retrieve a single lease
	*	@param int $lease_id
	*
	*/
	public function get_all_active_lease($lease_id)
	{
		//retrieve all leases
		$this->db->from('leases');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id);
		$query = $this->db->get();
		
		return $query;
	}
	/*
	*	Retrieve total revenue
	*
	*/
	public function get_normal_payments($where, $table, $page = NULL)
	{
		if($page != 'cash')
		{
			$where .= ' AND payments.cancel = 0';
		}
		//payments
		$table_search = $this->session->userdata('all_transactions_tables');
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}
		
		else
		{
			$this->db->from($table);
		}
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get();
		
		return $query;
	}
	public function get_normal_payments_owners($where, $table, $page = NULL)
	{
		if($page != 'cash')
		{
			$where .= ' AND home_owners_payments.cancel = 0';
		}
		//payments
		$table_search = $this->session->userdata('all_transactions_tables');
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}
		
		else
		{
			$this->db->from($table);
		}
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_payment_methods()
	{
		$this->db->select('*');
		$query = $this->db->get('payment_method');
		
		return $query;
	}
	
	/*
	*	Export Transactions
	*
	*/
	function export_transactions()
	{
		$this->load->library('excel');
		
		//get all transactions
		$where = 'payments.lease_id = leases.lease_id AND payment_method.payment_method_id = payments.payment_method_id AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND payments.payment_status = 1 AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id';

		$table = 'payments, leases,rental_unit,property,tenant_unit,tenants,payment_method';

		
		$transaction_search = $this->session->userdata('all_transactions_search');
		$table_search = $this->session->userdata('all_transactions_tables');
		
		if(!empty($transaction_search))
		{
			$where .= $transaction_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		
		$this->db->where($where);
		$this->db->order_by('payments.payment_date', 'ASC');
		$this->db->select('*');
		$transactions_query = $this->db->get($table);
		
		$title = 'Transaction Report';
		
		if($transactions_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Property Name';
			$report[$row_count][2] = 'Flat No.';
			$report[$row_count][3] = 'Tenant Name';
			$report[$row_count][4] = 'Payment Method';
			$report[$row_count][5] = 'Amount Paid';
			$report[$row_count][6] = 'Receipt Number';
			$report[$row_count][7] = 'Payment Date';
			$report[$row_count][8] = 'Paid By';
			$report[$row_count][9] = 'Receipted Date';
			$report[$row_count][10] = 'Receipt By';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($transactions_query->result() as $leases_row)
			{
				
				$payment_date = date('jS M Y',strtotime($leases_row->payment_date));
				$payment_created = date('jS M Y',strtotime($leases_row->payment_created));
							
				$property_name = $leases_row->property_name;
				$rental_unit_name = $leases_row->rental_unit_name;
				$personnel_id = $leases_row->personnel_id;
				$tenant_name = $leases_row->tenant_name;
				$lease_number = $leases_row->lease_number;
				$receipt_number = $leases_row->receipt_number;
				$amount_paid = $leases_row->amount_paid;
				$payment_method = $leases_row->payment_method;
				$paid_by = $leases_row->paid_by;
				$created_by = $leases_row->created_by;


				//creators and editors
				$personnel_query = $this->personnel_model->get_all_personnel();
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($created_by == $personnel_id2)
						{
							$personnel = $adm->personnel_onames.' '.$adm->personnel_fname;
							break;
						}
						
						else
						{
							$personnel = '-';
						}
					}
				}
				
				else
				{
					$personnel = '-';
				}
				$row_count++;
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $property_name;
				$report[$row_count][2] = $rental_unit_name;
				$report[$row_count][3] = $tenant_name;
				$report[$row_count][4] = $payment_method;
				$report[$row_count][5] = number_format($amount_paid,0);
				$report[$row_count][6] = $receipt_number;
				$report[$row_count][7] = $payment_date;
				$report[$row_count][8] = $paid_by;
				$report[$row_count][9] = $payment_created;
				$report[$row_count][10] = $personnel;
				
				$count++;
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	/*
	*	Export Transactions
	*
	*/
	function export_transactions_owners()
	{
		$this->load->library('excel');
		
		//get all transactions
		$where = 'home_owners_payments.rental_unit_id = rental_unit.rental_unit_id AND payment_method.payment_method_id = home_owners_payments.payment_method_id AND  home_owners_payments.payment_status = 1 AND home_owner_unit.home_owner_id = home_owners.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id';
		$table = 'home_owners_payments,rental_unit,property,home_owner_unit,home_owners,payment_method';
		
		$transaction_search = $this->session->userdata('all_owners_transactions_search');
		$table_search = $this->session->userdata('all_owners_transactions_tables');
		$property_search = $this->session->userdata('owners_property_search');
		
		if(!empty($transaction_search))
		{
			$where .= $transaction_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		
		$this->db->where($where);
		$this->db->order_by('home_owners_payments.payment_date', 'ASC');
		$this->db->select('*');
		$transactions_query = $this->db->get($table);
		$transaction_date = date('jS M Y',strtotime(date('Y-m-d')));
		$title = 'Transaction Report '.$transaction_date;
		
		if($transactions_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Property Name';
			$report[$row_count][2] = 'Flat No.';
			$report[$row_count][3] = 'Owner Name';
			$report[$row_count][4] = 'Payment Method';
			$report[$row_count][5] = 'Amount Paid';
			$report[$row_count][6] = 'Receipt Number';
			$report[$row_count][7] = 'Payment Date';
			$report[$row_count][8] = 'Paid By';
			$report[$row_count][9] = 'Receipted Date';
			$report[$row_count][10] = 'Receipt By';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($transactions_query->result() as $leases_row)
			{
				
				$payment_date = date('jS M Y',strtotime($leases_row->payment_date));
				$payment_created = date('jS M Y',strtotime($leases_row->payment_created));
							
				$property_name = $leases_row->property_name;
				$rental_unit_name = $leases_row->rental_unit_name;
				$personnel_id = $leases_row->personnel_id;
				$home_owner_name = $leases_row->home_owner_name;
				$receipt_number = $leases_row->receipt_number;
				$amount_paid = $leases_row->amount_paid;
				$payment_method = $leases_row->payment_method;
				$paid_by = $leases_row->paid_by;
				$created_by = $leases_row->created_by;


				//creators and editors
				$personnel_query = $this->personnel_model->get_all_personnel();
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($created_by == $personnel_id2)
						{
							$personnel = $adm->personnel_onames.' '.$adm->personnel_fname;
							break;
						}
						
						else
						{
							$personnel = '-';
						}
					}
				}
				
				else
				{
					$personnel = '-';
				}
				$row_count++;
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $property_name;
				$report[$row_count][2] = $rental_unit_name;
				$report[$row_count][3] = $home_owner_name;
				$report[$row_count][4] = $payment_method;
				$report[$row_count][5] = number_format($amount_paid,0);
				$report[$row_count][6] = $receipt_number;
				$report[$row_count][7] = $payment_date;
				$report[$row_count][8] = $paid_by;
				$report[$row_count][9] = $payment_created;
				$report[$row_count][10] = $personnel;
				
				$count++;
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	
	/*
	*	Export Time report
	*
	*/
	function export_defaulters()
	{
		$this->load->library('excel');
		
		//get all transactions
		$where = 'tenants.tenant_id > 0 AND property.property_id = rental_unit.property_id AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';

		$defaulters_search = $this->session->userdata('all_defaulters_search');
		$table_search = $this->session->userdata('all_transactions_tables');

		if(!empty($defaulters_search))
		{
			$where .= $defaulters_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		
		$this->db->where($where);
		$this->db->order_by('rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', 'ASC');
		$this->db->select('*');
		$defaulters_query = $this->db->get($table);
		
		$title = 'List of Defaulters';
		
		if($defaulters_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Property Name';
			$report[$row_count][2] = 'Flat No.';
			$report[$row_count][3] = 'Tenant Name';
			$report[$row_count][4] = 'Arrears B/F';
			$report[$row_count][5] = 'Receipt/Date';
			$report[$row_count][6] = 'Amount Paid';
			$report[$row_count][7] = 'Arrears C/F';
			$report[$row_count][8] = 'Phone Number';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($defaulters_query->result() as $leases_row)
			{
				
				$lease_id = $leases_row->lease_id;
				$tenant_unit_id = $leases_row->tenant_unit_id;
				$property_name = $leases_row->property_name;
				$rental_unit_name = $leases_row->rental_unit_name;
				$tenant_name = $leases_row->tenant_name;
				$lease_start_date = $leases_row->lease_start_date;
				$lease_duration = $leases_row->lease_duration;
				$rent_amount = $leases_row->rent_amount;
				$lease_number = $leases_row->lease_number;
				// $arreas_bf = $leases_row->arreas_bf;
				$rent_calculation = $leases_row->rent_calculation;
				$deposit = $leases_row->deposit;
				$deposit_ext = $leases_row->deposit_ext;
				$lease_status = $leases_row->lease_status;
				$tenant_phone_number = $leases_row->tenant_phone_number;
				$arrears_bf = $leases_row->arrears_bf;
				$created = $leases_row->created;

				// $lease_start_date = date('jS M Y',strtotime($lease_start_date));
				
				// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
				// $expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
				
				$todays_date = date('Y-m-d');
				$todays_month = date('m');
				$todays_year = date('Y');
				$total_payments = $this->accounts_model->get_total_payments_before($lease_id,$todays_year,$todays_month);
				
				$total_invoices = $this->accounts_model->get_total_invoices_before($lease_id,$todays_year,$todays_month);
				// var_dump($total_invoices); die();
				$current_balance = $total_invoices - $total_payments;

				$current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
				
				
			     	
		     	$this_month = date('m');
		     	$payments = $this->accounts_model->get_this_months_payment($lease_id,$this_month);
		     	$current_items = '';
		     	$total_amount_paid = 0;
		     	if($payments->num_rows() > 0)
		     	{
		     		$counter = 0;
		     		$total_amount_paid = 0;
		     		$receipt_counter = '';
		     		foreach ($payments->result() as $value) {
		     			# code...
		     			$receipt_number = $value->receipt_number;
		     			$amount_paid = $value->amount_paid;

		     			if($counter > 0)
		     			{
		     				$addition = '#';
		     				// $receipt_counter .= $receipt_number.$addition;
		     			}
		     			else
		     			{
		     				$addition = ' ';
		     				
		     			}
		     			$receipt_counter .= $receipt_number.$addition;
		     			$total_amount_paid += $total_amount_paid + $amount_paid;
		     			$counter++;
		     		}
		     		$current_items = $receipt_number;
		     		$total_amount_paid = $total_amount_paid;
		     	}
		     	else
		     	{
		     		$current_items = '-';
		     		$total_paid_amount = '-';
		     	}
		     	$total_current_invoice = $current_invoice_amount + $current_balance - $total_amount_paid;

			    if($total_current_invoice > 0)
				{				
						$row_count++;
						$count++;
						//display the patient data
						$report[$row_count][0] = $row_count;
						$report[$row_count][1] = $property_name;
						$report[$row_count][2] = $rental_unit_name;
						$report[$row_count][3] = $tenant_name;
						$report[$row_count][4] = number_format($current_balance,2);
						$report[$row_count][5] = $current_items;
						$report[$row_count][6] = $total_amount_paid;
						$report[$row_count][7] = number_format($total_current_invoice,2);
						$report[$row_count][8] = $tenant_phone_number;
				}
				$count++;
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function export_defaulters_owners()
	{
		$this->load->library('excel');
		
		//get all transactions
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id';
		$table = 'rental_unit,property,home_owners,home_owner_unit';

		$defaulters_search = $this->session->userdata('all_defaulters_owners_search');
		$table_search = $this->session->userdata('all_transactions_tables');

		if(!empty($defaulters_search))
		{
			$where .= $defaulters_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}

		$this->db->where($where);
		$this->db->order_by('property.property_name,rental_unit.rental_unit_name', 'ASC');
		$this->db->select('*');
		$defaulters_query = $this->db->get($table);
		
		$title = 'List of Owners Defaulters';
		
		if($defaulters_query->num_rows() > 0)
		{
			$count = 0;
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Property Name';
			$report[$row_count][2] = 'Flat No.';
			$report[$row_count][3] = 'Owner Name';
			$report[$row_count][4] = 'Arrears B/F';
			$report[$row_count][5] = 'Receipt/Date';
			$report[$row_count][6] = 'Amount Paid';
			$report[$row_count][7] = 'Arrears C/F';
			$report[$row_count][8] = 'Phone Number';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($defaulters_query->result() as $row)
			{
				
				$rental_unit_id = $row->rental_unit_id;
				$home_owner_id = $row->home_owner_id;
				$rental_unit_name = $row->rental_unit_name;
				$rental_unit_price = $row->rental_unit_price;
				$home_owner_name = $row->home_owner_name;
				$home_owner_phone_number = $row->home_owner_phone_number;
				$home_owner_email = $row->home_owner_email;
				$home_owner_national_id = $row->home_owner_national_id;
				$lease_start_date = $row->lease_start_date;
				$property_name = $row->property_name;

				$created = $row->created;
				$rental_unit_status = $row->rental_unit_status;
					
				
				$lease_start_date = date('jS M Y',strtotime($lease_start_date));
	     	
		     	$this_month = date('m');
		     	$amount_paid = 0;
		     	$payments = $this->accounts_model->get_this_months_payment_owners($rental_unit_id,$this_month);
		     	$current_items = ''; 
		     	$total_paid_amount = 0;

		     	if($payments->num_rows() > 0)
		     	{
		     		$counter = 0;
		     		$receipt_counter = '';
		     		foreach ($payments->result() as $value) {
		     			# code...
		     			$receipt_number = $value->receipt_number;
		     			$amount_paid = $value->amount_paid;

		     			if($counter > 0)
		     			{
		     				$addition = '#';
		     				// $receipt_counter .= $receipt_number.$addition;
		     			}
		     			else
		     			{
		     				$addition = ' ';
		     				
		     			}
		     			$receipt_counter .= $receipt_number.$addition;
		     			$total_paid_amount = $total_paid_amount + $amount_paid;
		     			$counter++;
		     		}
		     		$current_items = $receipt_number;
		     	}
		     	else
		     	{
		     		$current_items = '-';
		     	}


				$bills = $this->accounts_model->get_all_owners_invoice_month($rental_unit_id);
				$payments = $this->accounts_model->get_all_owners_payments_lease($rental_unit_id);
				$starting_arreas = $arrears_amount =$this->accounts_model->get_all_owners_arrears($rental_unit_id);
				
				$bills_result = '';
				$last_date = '';
				$current_year = date('Y');
				$total_invoices = $bills->num_rows();
				$invoices_count = 0;
				$total_invoice_balance = 0;
				$total_arrears = 0;
				$total_payment_amount = 0;
				$arrears_amount = $starting_arreas;
				if($bills->num_rows() > 0)
				{
					foreach ($bills->result() as $key_bills) {
						# code...
						$invoice_month = $key_bills->invoice_month;
					    $invoice_year = $key_bills->invoice_year;
						$invoice_date = $key_bills->invoice_date;
						$invoice_amount = $key_bills->invoice_amount;
						$balance_bf_id = $key_bills->balance_bf;
						$invoices_count++;
						
						if($payments->num_rows() > 0)
						{
							foreach ($payments->result() as $payments_key) {
								# code...
								$payment_date = $payments_key->payment_date;
								$payment_year = $payments_key->year;
								$payment_month = $payments_key->month;
								$payment_amount = $payments_key->amount_paid;

								if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0) && ($balance_bf_id !=1))
								{
									$arrears_amount -= $payment_amount;
									if($payment_year >= $current_year)
									{
										
									}
									
									$total_payment_amount += $payment_amount;

								}
							}
						}
						//display disbursment if cheque amount > 0
						if($invoice_amount != 0)
						{
							$arrears_amount += $invoice_amount;
							$total_invoice_balance += $invoice_amount;
							
								
							if($invoice_year >= $current_year)
							{
								
							}
						}
								
						//check if there are any more payments
						if($total_invoices == $invoices_count)
						{
							//get all loan deductions before date
							if($payments->num_rows() > 0)
							{
								foreach ($payments->result() as $payments_key) {
									# code...
									$payment_date = $payments_key->payment_date;
									$payment_year = $payments_key->year;
									$payment_month = $payments_key->month;
									$payment_amount = $payments_key->amount_paid;

									if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
									{
										$arrears_amount -= $payment_amount;
										
										
										$total_payment_amount += $payment_amount;

									}
								}
							}
						}
								$last_date = $invoice_date;
					}
				}	
				else
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;



							if(($payment_amount > 0))
							{
								$arrears_amount -= $payment_amount;
								
								$total_payment_amount += $payment_amount;

							}
						}
					}
				}

				//  get the amount changed
				//  get the amount changed
				$invoice_amount = $this->accounts_model->get_invoice_brought_forward($rental_unit_id,$invoice_date);
				$paid_amount = $this->accounts_model->get_paid_brought_forward($rental_unit_id,$invoice_date);
				$todays_payment =  $this->accounts_model->get_today_paid_current_forward($rental_unit_id,$invoice_date);
				$total_brought_forward = $invoice_amount - $paid_amount - $todays_payment;
				// end of amount changed
				// var_dump($paid_amount); die();

				// get current invoice
				$current_invoice =  $this->accounts_model->get_invoice_current_forward($rental_unit_id,$invoice_date);
				$current_payment =  $this->accounts_model->get_paid_current_forward($rental_unit_id,$invoice_date);
			   				
				$row_count++;
				$count++;
				//display the patient data
				$report[$row_count][0] = $row_count;
				$report[$row_count][1] = $property_name;
				$report[$row_count][2] = $rental_unit_name;
				$report[$row_count][3] = $home_owner_name;
				$report[$row_count][4] = number_format($total_brought_forward,2);
				$report[$row_count][5] = $current_items;
				$report[$row_count][6] = $total_paid_amount;
				$report[$row_count][7] = number_format($arrears_amount,2);
				$report[$row_count][8] = $home_owner_phone_number;
				
				$count++;
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	
	/*
	*	Retrieve total revenue
	*
	*/
	public function get_visit_departments($where, $table)
	{
		//invoiced
		$this->db->from($table.', visit_department');
		$this->db->select('visit_department.*');
		$this->db->where($where.' AND visit.visit_id = visit_department.visit_id');
		$query = $this->db->get();
		
		return $query;
	}


	public function get_insurance_company()
	{
		//invoiced
		$this->db->from('insurance_company');
		$this->db->select('*');
		$this->db->order_by('insurance_company_name');
		$query = $this->db->get();
		
		return $query;
	}
	
	public function calculate_debt_total($debtor_invoice_id, $where, $table)
	{
		$where .= ' AND debtor_invoice.debtor_invoice_id = '.$debtor_invoice_id;
		
		$total_services_revenue = $this->reports_model->get_total_services_revenue($where, $table);
		
		$where2 = $where.' AND payments.payment_type = 1';
		$total_cash_collection = $this->reports_model->get_total_cash_collection($where2, $table);
		
		return $total_services_revenue - $total_cash_collection;
	}
	
	public function get_debtor_invoice($where, $table)
	{
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}


	public function get_all_doctors()
	{
		$this->db->select('personnel.*');
		$this->db->where('personnel.personnel_type_id != 1');
		$this->db->order_by('personnel_fname');
		$query = $this->db->get('personnel');
		
		return $query;
	}

	public function get_total_collected($doctor_id, $date_from = NULL, $date_to = NULL)
	{
		$table = 'visit_charge, visit';
		
		$where = 'visit_charge.visit_id = visit.visit_id AND visit.personnel_id = '.$doctor_id;
		
		$visit_search = $this->session->userdata('all_doctors_search');
		if(!empty($visit_search))
		{
			$where = 'visit_charge.visit_id = visit.visit_id AND visit.personnel_id = '.$doctor_id.' '. $visit_search;
		}
		
		if(!empty($date_from) && !empty($date_to))
		{
			$where .= ' AND (date >= \''.$date_from.'\' AND date <= \''.$date_to.'\') ';
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$where .= ' AND date LIKE \''.$date_to.'\'';
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$where .= ' AND date LIKE \''.$date_from.'\'';
		}
		
		$this->db->select('SUM(visit_charge_units*visit_charge_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		// $result = $query->row();
		// $total = $result[0]->service_total;
		
		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key):
				# code...
				$total = $key->service_total;

				if(!is_numeric($total))
				{
					return 0;
				}
				else
				{
					return $total;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
		
	}
	/*
	*	Retrieve all active branches
	*
	*/
	public function get_all_active_branches()
	{
		//retrieve all users
		$this->db->from('branch');
		$this->db->where('branch_status = 1');
		$this->db->order_by('branch_name','ASC');
		$query = $this->db->get();
		
		return $query;
	}
	public function get_total_patients($doctor_id, $date_from = NULL, $date_to = NULL)
	{
		$table = 'visit';
		
		$where = 'visit.personnel_id = '.$doctor_id;
		
		if(!empty($date_from) && !empty($date_to))
		{
			$where .= ' AND (visit_date >= \''.$date_from.'\' AND visit_date <= \''.$date_to.'\') ';
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$where .= ' AND visit_date = \''.$date_to.'\'';
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$where .= ' AND visit_date = \''.$date_from.'\'';
		}
		
		$this->db->where($where);
		$total = $this->db->count_all_results('visit');
		
		return $total;
	}

	/*
	*	Export Time report
	*
	*/
	function doctor_reports_export($date_from = NULL, $date_to = NULL)
	{
		$this->load->library('excel');
		$report = array();
		
		//export title
		if(!empty($date_from) && !empty($date_to))
		{
			$title = 'Doctors report from '.date('jS M Y',strtotime($date_from)).' to '.date('jS M Y',strtotime($date_to));
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$title = 'Doctors report for '.date('jS M Y',strtotime($date_to));
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$title = 'Doctors report for '.date('jS M Y',strtotime($date_from));
		}
		
		else
		{
			$date_from = date('Y-m-d');
			$title = 'Doctors report for '.date('jS M Y',strtotime($date_from));
		}
		
		//document ehader
		$row_count = 0;
		$report[$row_count][0] = '#';
		$report[$row_count][1] = 'Doctor\'s name';
		$report[$row_count][2] = 'Total collection';
		$report[$row_count][3] = 'Patients seen';
		
		//get all doctors
		$doctor_results = $this->reports_model->get_all_doctors();
		$result = $doctor_results->result();
		$grand_total = 0;
		$patients_total = 0;
		$count = 0;
		
		foreach($result as $res)
		{
			$personnel_id = $res->personnel_id;
			$personnel_onames = $res->personnel_onames;
			$personnel_fname = $res->personnel_fname;
			$count++;
			$row_count++;
			
			//get service total
			$total = $this->reports_model->get_total_collected($personnel_id, $date_from, $date_to);
			$patients = $this->reports_model->get_total_patients($personnel_id, $date_from, $date_to);
			$grand_total += $total;
			$patients_total += $patients;
			
			$report[$row_count][0] = $count;
			$report[$row_count][1] = $personnel_fname.' '.$personnel_onames;
			$report[$row_count][2] = number_format($total, 0);
			$report[$row_count][3] = $patients;
		}
		$row_count++;
		
		$report[$row_count][0] = '';
		$report[$row_count][1] = '';
		$report[$row_count][2] = number_format($grand_total, 0);
		$report[$row_count][3] = $patients_total;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	
	function doctor_patients_export($personnel_id, $date_from = NULL, $date_to = NULL)
	{
		$where = ' AND visit.personnel_id = '.$personnel_id;
		
		if(!empty($date_from) && !empty($date_to))
		{
			$where .= ' AND (visit_date >= \''.$date_from.'\' AND visit_date <= \''.$date_to.'\') ';
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$where .= ' AND visit_date = \''.$date_to.'\'';
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$where .= ' AND visit_date = \''.$date_from.'\'';
		}
		$_SESSION['all_transactions_search'] = $where;
		
		$this->export_transactions();
	}
	
	public function calculate_hours_worked($personnel_id, $date_from, $date_to)
	{
		$where = 'personnel_id = '.$personnel_id;
		
		if(!empty($date_from) && !empty($date_to))
		{
			$where .= ' AND (schedule_date >= \''.$date_from.'\' AND schedule_date <= \''.$date_to.'\') ';
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$where .= ' AND schedule_date = \''.$date_to.'\'';
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$where .= ' AND schedule_date = \''.$date_from.'\'';
		}
		
		$this->db->where($where);
		$query = $this->db->get('schedule_item');
		$total_hours = 0;
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$schedule_start_time = $res->schedule_start_time;
				$schedule_end_time = $res->schedule_end_time;
				
				$hours_difference = (strtotime($schedule_end_time) - strtotime($schedule_start_time)) / 3600;
				$total_hours += $hours_difference;
			}
		}
		
		return $total_hours;
	}
	
	public function calculate_days_worked($personnel_id, $date_from, $date_to)
	{
		$where = 'personnel_id = '.$personnel_id;
		
		if(!empty($date_from) && !empty($date_to))
		{
			$where .= ' AND (schedule_date >= \''.$date_from.'\' AND schedule_date <= \''.$date_to.'\') ';
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$where .= ' AND schedule_date = \''.$date_to.'\'';
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$where .= ' AND schedule_date = \''.$date_from.'\'';
		}
		
		$this->db->where($where);
		$query = $this->db->get('schedule_item');
		$total_days = $query->num_rows();
		
		return $total_days;
	}
	
	public function get_visit_type()
	{
		//invoiced
		$this->db->select('*');
		$this->db->from('visit_type');
		$this->db->where('visit_type_id > 1');
		$this->db->order_by('visit_type_name');
		$query = $this->db->get();
		
		return $query;
	}
	/*
	*	Retrieve total visits
	*
	*/
	public function get_total_visits($where, $table)
	{
		$this->db->from($table);
		$this->db->where($where);
		$total = $this->db->count_all_results();
		
		return $total;
	}
	
	/*
	*	Retrieve debtors_invoices
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_debtors_invoices($table, $where, $per_page, $page, $order, $order_method)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	public function add_debtor_invoice($visit_type_id)
	{
		$data = array(
			'debtor_invoice_created'=>date('Y-m-d H:i:s'),
			'debtor_invoice_created_by'=>$this->session->userdata('personnel_id'),
			'batch_no'=>$this->create_batch_number(),
			'visit_type_id'=>$visit_type_id,
			'debtor_invoice_modified_by'=>$this->session->userdata('personnel_id'),
			'date_from' => $this->input->post('invoice_date_from'),
			'date_to' => $this->input->post('invoice_date_to')
		);
		
		if($this->db->insert('debtor_invoice', $data))
		{
			$debtor_invoice_id = $this->db->insert_id();
			
			if($debtor_invoice_id > 0)
			{
				//get all invoices within the selected dates
				$this->db->where(
					array(
						'close_card' => 1,
						'visit_delete' => 0,
						'visit_type' => $visit_type_id,
						'visit_date >= ' => $this->input->post('invoice_date_from'),
						'visit_date <= ' => $this->input->post('invoice_date_to')
					)
				);
				$this->db->select('visit_id');
				$query = $this->db->get('visit');
				
				if($query->num_rows() > 0)
				{
					$invoice_data['debtor_invoice_id'] = $debtor_invoice_id;
					
					foreach($query->result() as $res)
					{
						$visit_id = $res->visit_id;
						
						$invoice_data['visit_id'] = $visit_id;
						
						if($this->db->insert('debtor_invoice_item', $invoice_data))
						{
						}
						
						else
						{
							$this->session->set_userdata('error_message', 'Unable to add details for visit ID '.$visit_id);
						}
					}
					$this->session->set_userdata('success_message', 'Batch added successfully');
					return TRUE;
				}
				
				else
				{
					$this->session->set_userdata('error_message', 'The selected date range does not contain any invoices');
					return FALSE;
				}
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'The selected date range does not contain any invoices');
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Create batch number
	*
	*/
	public function create_batch_number()
	{
		//select product code
		$this->db->from('debtor_invoice');
		$this->db->where("batch_no LIKE '".$this->session->userdata('branch_code').'-'.date('y')."-%'");
		$this->db->select('MAX(batch_no) AS number');
		$query = $this->db->get();
		$preffix = $this->session->userdata('branch_code').'-'.date('y').'-';
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%06d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%06d', 1);
		}
		
		return $number;
	}
	

	public function check_lease_has_balance($lease_id,$rent_amount,$arreas_bf,$lease_start_date,$tenant_unit_id)
	{
		// get all leases for this tenant and for that unit

		$this->db->from('leases');
		$this->db->where('tenant_unit_id = '.$tenant_unit_id);
		$this->db->select('lease_id');
		$query = $this->db->get();

		$total_amount_paid = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$payment_lease_id = $key->lease_id;

				$total_paid = $this->accounts_model->get_months_amount($payment_lease_id);

				$total_amount_paid = $total_amount_paid + $total_paid;

			}
		}
		

		$date1 = $lease_start_date;

		// $date2 = date('Y-m-d');
		$date2 = '2016-03-01';

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y',$ts1);
		$year2 = date('Y',$ts2);

		$month1 = date('m',$ts1);
		$month2 = date('m',$ts2);
		$total_months_comb = $month2 - $month2;
		$total_months = ($year2-$year1) * 12 + $total_months_comb;


			
		if($total_months == 0)
		{
			$total_months = 1;
		}
		$total_months = $total_months+1;
		

		$lease_balance = ($rent_amount * $total_months) + $arreas_bf;

		// var_dump("close".$lease_balance); die();

		
		if($lease_balance < 0)
		{
			
			$current_balance = $total_amount_paid - $lease_balance;
		}
		else
		{
			$current_balance = $lease_balance - $total_amount_paid;
		}
		
		
		return $current_balance;
		
		
		// get total_

	}
	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_payments($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name, payments.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames, service.service_name');
		$this->db->join('personnel', 'payments.payment_created_by = personnel.personnel_id', 'left');
		$this->db->join('service', 'payments.payment_service_id = service.service_id', 'left');
		$this->db->where($where);
		$this->db->order_by('payments.time','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Export Transactions
	*
	*/
	function export_cash_report()
	{
		$this->load->library('excel');
		
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}
		
		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		
		$where = 'payments.payment_method_id = payment_method.payment_method_id AND payments.visit_id = visit.visit_id AND payments.payment_type = 1 AND visit.visit_delete = 0 AND visit.branch_code = \''.$branch_code.'\' AND visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND payments.cancel = 0';
		
		$table = 'payments, visit, patients, visit_type, payment_method';
		$visit_search = $this->session->userdata('cash_report_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name, payments.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames, service.service_name');
		$this->db->join('personnel', 'payments.payment_created_by = personnel.personnel_id', 'left');
		$this->db->join('service', 'payments.payment_service_id = service.service_id', 'left');
		$this->db->where($where);
		$this->db->order_by('payments.time','DESC');
		$query = $this->db->get($table);
		
		$title = 'Cash report '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;
		
		if($query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Payment Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Time recorded';
			$col_count++;
			$report[$row_count][$col_count] = 'Patient';
			$col_count++;
			$report[$row_count][$col_count] = 'Category';
			$col_count++;
			$report[$row_count][$col_count] = 'Service';
			$col_count++;
			$report[$row_count][$col_count] = 'Amount';
			$col_count++;
			$report[$row_count][$col_count] = 'Method';
			$col_count++;
			$report[$row_count][$col_count] = 'Description';
			$col_count++;
			$report[$row_count][$col_count] = 'Recorded by';
			$col_count++;
			$current_column = $col_count ;
			
			foreach ($query->result() as $row)
			{
				$count++;
				$row_count++;
				$col_count = 0;
				
				$total_invoiced = 0;
				$payment_created = date('jS M Y',strtotime($row->payment_created));
				$time = date('H:i a',strtotime($row->time));
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$payment_method = $row->payment_method;
				$amount_paid = $row->amount_paid;
				$service_name = $row->service_name;
				$transaction_code = $row->transaction_code;
				$created_by = $row->personnel_fname.' '.$row->personnel_onames;
				
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $payment_created;
				$col_count++;
				$report[$row_count][$col_count] = $time;
				$col_count++;
				$report[$row_count][$col_count] = $patient_surname.' '.$patient_othernames;
				$col_count++;
				$report[$row_count][$col_count] = $visit_type_name;
				$col_count++;
				$report[$row_count][$col_count] = $service_name;
				$col_count++;
				$report[$row_count][$col_count] = number_format($amount_paid, 2);
				$col_count++;
				$report[$row_count][$col_count] = $payment_method;
				$col_count++;
				$report[$row_count][$col_count] = $transaction_code;
				$col_count++;
				$report[$row_count][$col_count] = $created_by;
				$col_count++;
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	
	public function get_debtor_invoice_items($debtor_invoice_id)
	{
		$this->db->select('SUM(visit_charge.visit_charge_units * visit_charge.visit_charge_amount) AS invoice_amount, patients.patient_surname, patients.patient_othernames, patients.patient_number, patients.current_patient_number, visit.visit_id, visit.visit_date, visit.patient_insurance_number, debtor_invoice_item.debtor_invoice_item_status, debtor_invoice_item.debtor_invoice_item_id');
		$this->db->where('visit.visit_delete = 0 AND visit.visit_id = debtor_invoice_item.visit_id AND visit.patient_id = patients.patient_id AND visit.visit_id = visit_charge.visit_id AND debtor_invoice_item.debtor_invoice_id = '.$debtor_invoice_id);
		
		$this->db->group_by('visit_id');
		$this->db->order_by('visit_date');
		$query = $this->db->get('debtor_invoice_item, visit, visit_charge, patients');
		
		return $query;
	}
	public function get_expenses_for_period($where,$table)
	{
		$this->db->select('property.property_name,creditor_service.creditor_service_name,creditor.creditor_name,creditor_account.*,SUM(creditor_account.creditor_account_amount) AS total_paid_expenses');
		$this->db->where($where);
		$this->db->group_by('creditor_account.creditor_service_id');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_all_active_creditors()
	{
		$this->db->select('*');
		$this->db->where('creditor_status = 0');
		$this->db->group_by('creditor_name');
		$query = $this->db->get('creditor');
		
		return $query;
	}
	public function get_all_active_creditor_accounts($outflows_where)
	{
		$this->db->select('*');
		$this->db->where($outflows_where);
		$this->db->group_by('creditor_account_id');
		$query = $this->db->get('creditor_account');
		
		return $query;
	}
	public function get_all_active_creditor_services()
	{
		$this->db->select('*');
		$this->db->where('creditor_service_status = 1');
		$this->db->group_by('creditor_service_name');
		$query = $this->db->get('creditor_service');
		
		return $query;
	}
	public function get_all_active_other_inflow()
	{
		$this->db->select('*');
		$this->db->where('inflow_service_status = 1');
		$this->db->group_by('inflow_service_name');
		$query = $this->db->get('inflow_service');
		
		return $query;
	}
	public function get_all_active_invoice_types()
	{
		$this->db->select('*');
		$this->db->group_by('invoice_type_name');
		$query = $this->db->get('invoice_type');
		
		return $query;
	}
	public function get_all_active_invoice_types_per_property($inflow_income_search)
	{//echo $inflow_income_search;die();
		$this->db->select('*');
		$this->db->where('invoice_type_id IN (SELECT property_billing.invoice_type_id from property_billing,property where property_billing.property_id = property.property_id '.$inflow_income_search.')');
		$this->db->group_by('invoice_type_name');
		$query = $this->db->get('invoice_type');
		
		return $query;
	}
	public function get_all_active_creditor_services_per_property($inflow_income_search)
	{
		$this->db->select('*');
		$this->db->where('creditor_service_status = 1 AND creditor_service_id IN (SELECT creditor_service_id from creditor_account,property where creditor_account.property_id = property.property_id '.$inflow_income_search.')');
		$this->db->group_by('creditor_service_name');
		$query = $this->db->get('creditor_service');
		
		return $query;
	}
	public function get_all_active_inflow_services_per_property($inflow_income_search)
	{
		$this->db->select('*');
		$this->db->where('inflow_service_status = 1 AND inflow_service_id IN (SELECT inflow_service_id from inflow_account,property where inflow_account.property_id = property.property_id '.$inflow_income_search.')');
		$this->db->order_by('inflow_service_name');
		$query = $this->db->get('inflow_service');
		
		return $query;
	}

	public function get_all_home_owner_payments($home_owners_where,$home_owner_table)
	{
		$this->db->select('*');
		$this->db->where($home_owners_where);
		$this->db->group_by('home_owner_payment_item.payment_item_id');
		$query = $this->db->get($home_owner_table);
		
		return $query;
	}
	public function get_all_tenant_payments($tenant_infows_where,$tenant_table)
	{
		$this->db->select('*');
		$this->db->where($tenant_infows_where);
		$this->db->group_by('payment_item_id');
		$query = $this->db->get($tenant_table);
		
		return $query;
	}
	public function generate_financial_year()
	{
		$year =array();
		for($m=4;$m<=15;$m++)
		{
			$month_id = $m;
			if($m > 12)
			{
				$month_id = $m - 12;
			}
			array_push($year,$month_id);
		}
		return $year;
	}
	public function get_month_name($month_id)
	{
		$this->db->select('month_name');
		$this->db->where('month_id = '.$month_id);
		$query = $this->db->get('month');
		$month_name = '';
		$months = $query->row();
		$month_name = $months->month_name;
		return $month_name;
	}
	public function get_all_tenants($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		// $this->db->group_by('rental_unit.rental_unit_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_active_lease($property_id,$rental_unit_id,$todays_month,$todays_year,$return_date=null)
	{
		if(!empty($return_date))
		{
			$addition = ' AND invoice_date >= "'.$todays_year.'-'.$todays_month.'-'.$return_date.'" ';
		}
		else
		{
			$addition = '';
		}
		$where = 'invoice.invoice_month = "'.$todays_month.'" AND invoice.invoice_year = "'.$todays_year.'" AND tenant_unit.rental_unit_id ='.$rental_unit_id.' AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND invoice.lease_id = leases.lease_id  '.$addition;

		$this->db->from('invoice,tenant_unit,leases');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->group_by('leases.lease_id');
		$query = $this->db->get('');
		$lease_id = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$lease_id = $value->lease_id;
		}

		return $lease_id;
	}
	public function get_rent_payable($lease_id,$rental_unit_id,$todays_month,$todays_year,$return_date=null)
	{


		if(!empty($return_date))
		{
			$addition = ' AND invoice_date >= "'.$todays_year.'-'.$todays_month.'-'.$return_date.'" ';
		}
		else
		{
			$addition = '';
		}
		$where = 'invoice.invoice_month = "'.$todays_month.'" AND invoice.invoice_year = "'.$todays_year.'" AND tenant_unit.rental_unit_id ='.$rental_unit_id.' AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND invoice.lease_id = leases.lease_id AND invoice.invoice_type = 1  AND invoice.lease_id ='.$lease_id;


		$this->db->from('invoice,tenant_unit,leases');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->group_by('leases.lease_id');
		$query = $this->db->get('');

		$invoice_amount = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$invoice_amount = $value->invoice_amount;
		}

		return $invoice_amount;


	}

	public function get_lease_detail($lease_id)
	{
		$where = 'tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND leases.lease_id = '.$lease_id;

		$this->db->from('leases,tenants,tenant_unit,property,rental_unit');
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get('');

		return $query;

	}


	public function get_month_outflows($property_id,$this_month,$this_year)
	{

		$this->db->select('*');
		$this->db->where('creditor_account.creditor_service_id = creditor_service.creditor_service_id AND creditor.creditor_id = creditor_account.creditor_id AND month(creditor_account.created) = "'.$this_month.'" AND year(creditor_account.created) = "'.$this_year.'" AND creditor_account.property_id = '.$property_id.' AND creditor_account.transaction_type_id = 1 ');
		$query = $this->db->get('creditor,creditor_account,creditor_service');
		
		return $query;

	}

	public function get_property_month_outflows($property_id,$this_month,$this_year)
	{

		$this->db->select('*');
		$this->db->where('month(account_invoices.invoice_date) = "'.$this_month.'" AND year(account_invoices.invoice_date) = "'.$this_year.'" AND account_invoices.invoice_id = '.$property_id.' AND account_invoices.account_to_type = 1 AND account_invoices.account_to_id = account.account_id ');
		$query = $this->db->get('account_invoices,account');
		
		return $query;

	}
	public function get_property_month_leases($property_id,$this_month,$this_year)
	{

		$this->db->select('*');
		$this->db->where('month(leases.closing_end_date) = "'.$this_month.'" AND year(leases.closing_end_date) = "'.$this_year.'"  AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND rental_unit.property_id = '.$property_id.' AND leases.lease_status = 2 AND tenant_unit.tenant_id = tenants.tenant_id');
		$query = $this->db->get('leases,tenant_unit,rental_unit,tenants');
		
		return $query;

	}
	public function get_property_month_new_leases($property_id,$this_month,$this_year)
	{

		$this->db->select('*');
		$this->db->where('month(leases.lease_start_date) = "'.$this_month.'" AND year(leases.lease_start_date) = "'.$this_year.'"  AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND rental_unit.property_id = '.$property_id.' AND leases.lease_status = 1 AND tenant_unit.tenant_id = tenants.tenant_id');
		$query = $this->db->get('leases,tenant_unit,rental_unit,tenants');
		
		return $query;

	}
	
	public function get_total_month_deposits($property_id,$this_month,$this_year)
	{

		$this->db->select('*');
		$this->db->where('rental_unit.rental_unit_id = leases.rental_unit_id AND leases.lease_status = 1 AND month(leases.lease_start_date) = "'.$this_month.'" AND year(leases.lease_start_date) = "'.$this_year.'" AND rental_unit.property_id = '.$property_id.' ');
		$query = $this->db->get('leases,rental_unit');
		
		return $query;

	}

	public function get_amount_collected_invoice_type($invoice_type_id,$month_id,$year)
	{
        $prop = $this->session->userdata('search_property');
		if(!empty($prop))
		{
			$add = $this->session->userdata('search_property');
		}
		else
		{
			$add = '';
		}

		if($invoice_type_id == NULL)
		{
			$where = 'payments.payment_status = 1 AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id  AND MONTH(payments.payment_date) = \''.$month_id.'\' AND YEAR(payments.payment_date) =  '.$year.'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0  '.$add;

		}
		else
		{
			$where = 'payments.payment_status = 1 AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id  AND MONTH(payments.payment_date) = \''.$month_id.'\' AND YEAR(payments.payment_date) =  '.$year.'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0  AND payment_item.invoice_type_id = '.$invoice_type_id.' '.$add;
		}
		
		// var_dump($where); die();
		$this->db->select('SUM(payment_item.amount_paid) AS total_amount');
		$this->db->where($where);
		$query = $this->db->get('payments,payment_item,leases,rental_unit,property');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}
		
		return $total_amount;
	}
	public function get_tenants_previous_inflow_amounts($year)
	{
        $prop = $this->session->userdata('search_property');
		if(!empty($prop))
		{
			$add = $this->session->userdata('search_property');
		}
		else
		{
			$add = '';
		}
		$where = 'payments.payment_status = 1 AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id AND YEAR(payments.payment_date) < '.$year.'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0 '.$add;
		// var_dump($where); die();
		$this->db->select('SUM(payment_item.amount_paid) AS total_amount');
		$this->db->where($where);
		$query = $this->db->get('payments,payment_item,leases,rental_unit,property');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}
		
		return $total_amount;
	}

	public function get_amount_owners_collected_invoice_type($invoice_type_id,$month_id,$year)
	{
		 $prop = $this->session->userdata('search_property');
		if(!empty($prop))
		{
			$add = $this->session->userdata('search_property');
		}
		else
		{
			$add = '';
		}

		if($invoice_type_id == NULL)
		{
			$where = 'home_owners_payments.payment_status = 1 AND home_owners_payments.cancel = 0 AND home_owner_payment_item.payment_id = home_owners_payments.payment_id  AND MONTH(home_owners_payments.payment_date) = \''.$month_id.'\' AND YEAR(home_owners_payments.payment_date) =  '.$year.'  AND home_owners_payments.rental_unit_id = rental_unit.rental_unit_id AND home_owners_payments.cancel = 0 AND rental_unit.property_id = property.property_id '.$add;

		}
		else
		{
			$where = 'home_owners_payments.payment_status = 1 AND home_owners_payments.cancel = 0 AND home_owner_payment_item.payment_id = home_owners_payments.payment_id  AND MONTH(home_owners_payments.payment_date) = \''.$month_id.'\' AND YEAR(home_owners_payments.payment_date) =  '.$year.'  AND home_owners_payments.rental_unit_id = rental_unit.rental_unit_id AND home_owners_payments.cancel = 0 AND rental_unit.property_id = property.property_id  AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id.' '.$add;
		}
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_amount');
		
		// var_dump($where); die();
		$this->db->where($where);


		$query = $this->db->get('home_owners_payments,home_owner_payment_item,rental_unit,property');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}
		
		return $total_amount;
	}

	public function get_owners_previous_inflow_amounts($year)
	{
		 $prop = $this->session->userdata('search_property');
		if(!empty($prop))
		{
			$add = $this->session->userdata('search_property');
		}
		else
		{
			$add = '';
		}
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_amount');
		$where = 'home_owners_payments.payment_status = 1 AND home_owners_payments.cancel = 0 AND home_owner_payment_item.payment_id = home_owners_payments.payment_id  AND YEAR(home_owners_payments.payment_date) <  '.$year.'  AND home_owners_payments.rental_unit_id = rental_unit.rental_unit_id AND home_owners_payments.cancel = 0 AND rental_unit.property_id = property.property_id '.$add;
		// var_dump($where); die();
		$this->db->where($where);


		$query = $this->db->get('home_owners_payments,home_owner_payment_item,rental_unit,property');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}
		
		return $total_amount;
	}

	public function get_creditor_service_amounts($creditor_service_id,$month_id,$year)
	{
        $prop = $this->session->userdata('property_search_id');
		if(!empty($prop))
		{
			$add = $this->session->userdata('property_search_id');
		}
		else
		{
			$add = '';
		}


		if($creditor_service_id == NULL)
		{
			$this->db->select('SUM(creditor_account_amount) AS total_amount');
			$this->db->where('creditor_account.creditor_account_status = 1 AND creditor_account.transaction_type_id = 1 AND YEAR(creditor_account.creditor_account_date) = '.$year.' AND MONTH(creditor_account.creditor_account_date) = "'.$month_id.'"'.$add);
		}
		else
		{
			$this->db->select('SUM(creditor_account_amount) AS total_amount');
			$this->db->where('creditor_account.creditor_account_status = 1 AND creditor_account.transaction_type_id = 1 AND YEAR(creditor_account.creditor_account_date) = '.$year.' AND MONTH(creditor_account.creditor_account_date) = "'.$month_id.'" AND creditor_service_id ='.$creditor_service_id.' '.$add);

		}

		
		$query = $this->db->get('creditor_account');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}

		return $total_amount;

	}

	public function get_inflow_service_amounts($inflow_service_id,$month_id,$year)
	{
        $prop = $this->session->userdata('property_inflow_id');
		if(!empty($prop))
		{
			$add = $this->session->userdata('property_inflow_id');
		}
		else
		{
			$add = '';
		}


		$this->db->select('SUM(inflow_account_amount) AS total_amount');
		$this->db->where('inflow_account.inflow_account_status = 1 AND inflow_account.transaction_type_id = 1 AND YEAR(inflow_account.inflow_account_date) = '.$year.' AND MONTH(inflow_account.inflow_account_date) = "'.$month_id.'" AND inflow_service_id ='.$inflow_service_id.' '.$add);
		$query = $this->db->get('inflow_account');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}

		return $total_amount;

	}

	public function get_creditor_previous_service_amounts($year)
	{
        $prop = $this->session->userdata('property_search_id');
		if(!empty($prop))
		{
			$add = $this->session->userdata('property_search_id');
		}
		else
		{
			$add = '';
		}


		$this->db->select('SUM(creditor_account_amount) AS total_amount');
		$this->db->where('creditor_account.creditor_account_status = 1 AND creditor_account.transaction_type_id = 1 AND YEAR(creditor_account.creditor_account_date) < '.$year.' '.$add);
		$query = $this->db->get('creditor_account');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}

		return $total_amount;

	}

	public function get_tenants_billings($lease_id)
	{
		$bills = $this->accounts_model->get_all_invoice_month($lease_id);
		$payments = $this->accounts_model->get_all_payments_lease($lease_id);
		$pardons = $this->accounts_model->get_all_pardons_lease($lease_id);

		// var_dump($payments); die();
		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$total_pardon_amount=0; 
		$result = '';
		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month = $key_bills->invoice_month;
			    $invoice_year = $key_bills->invoice_year;
				$invoice_date = $key_bills->invoice_date;
				$invoice_amount = $key_bills->total_invoice;
				$invoices_count++;
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_year = $payments_key->year;
						$payment_month = $payments_key->month;
						$payment_amount = $payments_key->amount_paid;

						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
						{
							$total_arrears -= $payment_amount;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										
									</tr> 
								';
							// }
							
							$total_payment_amount += $payment_amount;

						}
					}
				}
				if($pardons->num_rows() > 0)
				{
					foreach ($pardons->result() as $pardons_key) {
						# code...
						$pardon_date = $pardons_key->pardon_date;
						$pardon_explode = explode('-', $pardon_date);
						$pardon_year = $pardon_explode[0];
						$pardon_month = $pardon_explode[1];
						$pardon_amount = $pardons_key->pardon_amount;

						if(($pardon_date <= $invoice_date) && ($pardon_date > $last_date) && ($pardon_amount > 0))
						{
							$total_arrears -= $pardon_amount;
							// if($pardon_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($pardon_date)).' </td>
										<td>Pardon</td>
										<td></td>
										<td>'.number_format($pardon_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										
									</tr> 
								';
							// }
							
							$total_pardon_amount += $pardon_amount;

						}
					}
				}
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$total_arrears += $invoice_amount;
					$total_invoice_balance += $invoice_amount;
						
					// if($invoice_year >= $current_year)
					// {
						$result .= 
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$invoice_month.' '.$invoice_year.' Invoice</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td>'.number_format($total_arrears, 2).'</td>
							</tr> 
						';
					// }
				}
						
				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											
										</tr> 
									';
								// }
								
								$total_payment_amount += $payment_amount;

							}
						}
					}

					if($pardons->num_rows() > 0)
					{
						foreach ($pardons->result() as $pardons_key) {
							# code...
							$pardon_date = $pardons_key->pardon_date;
							$pardon_explode = explode('-', $pardon_date);
							$pardon_year = $pardon_explode[0];
							$pardon_month = $pardon_explode[1];
							$pardon_amount = $pardons_key->pardon_amount;

							if(($pardon_date > $invoice_date) &&  ($pardon_amount > 0))
							{
								$total_arrears -= $pardon_amount;
								// if($pardon_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($pardon_date)).' </td>
											<td>Pardon</td>
											<td></td>
											<td>'.number_format($pardon_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											
										</tr> 
									';
								// }
								
								$total_pardon_amount += $pardon_amount;

							}
						}
					}
				}
						$last_date = $invoice_date;
			}
		}	
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;

					if(($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>Payment</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									
								</tr> 
							';
						// }
						
						$total_payment_amount += $payment_amount;

					}
				}
			}
			if($pardons->num_rows() > 0)
			{
				foreach ($pardons->result() as $pardons_key) {
					# code...
					$pardon_date = $pardons_key->pardon_date;
					$pardon_explode = explode('-', $pardon_date);
					$pardon_year = $pardon_explode[0];
					$pardon_month = $pardon_explode[1];
					$pardon_amount = $pardons_key->pardon_amount;

					if(($payment_amount > 0))
					{
						$total_arrears -= $pardon_amount;
						// if($pardon_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($pardon_date)).' </td>
									<td>Pardon</td>
									<td></td>
									<td>'.number_format($pardon_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									
								</tr> 
							';
						// }
						
						$total_pardon_amount += $pardon_amount;

					}
				}
			}

		}
						
		//display loan
		$result .= 
		'
			<tr>
				<th colspan="2">Total</th>
				<th>'.number_format($total_invoice_balance, 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<th>'.number_format($total_arrears, 2).'</th>
				
			</tr> 
		';

		$invoice_date = $this->accounts_model->get_max_invoice_date($lease_id);

		$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$invoice_date);
		$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$invoice_date);
		$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$invoice_date);
		$total_brought_forward_account = $account_invoice_amount - $account_paid_amount;
		

		if($lease_id == 30)
		{
			// var_dump($account_invoice_amount); die();

		}
		


		$response['total_arrears'] = $total_arrears;
		$response['invoice_date'] = $invoice_date;
		$response['result'] = $result;
		$response['total_invoice_balance'] = $total_brought_forward_account;
		$response['total_payment_amount'] = $total_payment_amount;
		$response['total_pardon_amount'] = $total_pardon_amount;

		return $response;
	}
	public function get_owners_billings($rental_unit_id,$home_owner_id)
	{


		$bills = $this->accounts_model->get_all_owners_invoice_month($rental_unit_id);
		$payments = $this->accounts_model->get_all_owners_payments_lease($rental_unit_id);
		$starting_arreas = $arrears_amount =$this->accounts_model->get_all_owners_arrears($rental_unit_id);
		$pardons = $this->accounts_model->get_all_owner_pardons_lease($rental_unit_id);
		// var_dump($bills); die();
		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0 + $starting_arreas;
		$total_arrears = 0 ;
		$total_payment_amount = 0;
		$total_pardon_amount =0;
		$result = '';
		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month = $key_bills->invoice_month;
			    $invoice_year = $key_bills->invoice_year;
				$invoice_date = $key_bills->invoice_date;
				$invoice_amount = $key_bills->invoice_amount;
				$balance_bf_id = $key_bills->balance_bf;
				$invoices_count++;
				
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_year = $payments_key->year;
						$payment_month = $payments_key->month;
						$payment_amount = $payments_key->amount_paid;

						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0) && ($balance_bf_id !=1))
						{
							$arrears_amount -= $payment_amount;
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td>'.number_format($arrears_amount, 2).'</td>
										
									</tr> 
								';
							// }
							
							$total_payment_amount += $payment_amount;

						}
					}
				}
				if($pardons->num_rows() > 0)
				{
					foreach ($pardons->result() as $pardons_key) {
						# code...
						$pardon_date = $pardons_key->pardon_date;
						$pardon_explode = explode('-', $pardon_date);
						$pardon_year = $pardon_explode[0];
						$pardon_month = $pardon_explode[1];
						$pardon_amount = $pardons_key->pardon_amount;

						if(($pardon_date <= $invoice_date) && ($pardon_date > $last_date) && ($pardon_amount > 0))
						{
							$total_arrears -= $pardon_amount;
							// if($pardon_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($pardon_date)).' </td>
										<td>Pardon</td>
										<td></td>
										<td>'.number_format($pardon_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr> 
								';
							// }
							
							$total_pardon_amount += $pardon_amount;

						}
					}
				}
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$arrears_amount += $invoice_amount;
					$total_invoice_balance += $invoice_amount;
					
						
					// if($invoice_year >= $current_year)
					// {
						$result .= 
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$invoice_month.' '.$invoice_year.' Invoice</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td>'.number_format($arrears_amount, 2).'</td>
							</tr> 
						';
					// }
				}
						
				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$arrears_amount -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($arrears_amount, 2).'</td>
								
										</tr> 
									';
								// }
								
								$total_payment_amount += $payment_amount;

							}
						}
					}
					if($pardons->num_rows() > 0)
					{
						foreach ($pardons->result() as $pardons_key) {
							# code...
							$pardon_date = $pardons_key->pardon_date;
							$pardon_explode = explode('-', $pardon_date);
							$pardon_year = $pardon_explode[0];
							$pardon_month = $pardon_explode[1];
							$pardon_amount = $pardons_key->pardon_amount;

							if(($pardon_date > $invoice_date) &&  ($pardon_amount > 0))
							{
								$total_arrears -= $pardon_amount;
								// if($pardon_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($pardon_date)).' </td>
											<td>Pardon</td>
											<td></td>
											<td>'.number_format($pardon_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr> 
									';
								// }
								
								$total_pardon_amount += $pardon_amount;

							}
						}
					}
				}
						$last_date = $invoice_date;
			}
		}	
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;



					if(($payment_amount > 0))
					{
						$arrears_amount -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>Payment</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($arrears_amount, 2).'</td>
									
								</tr> 
							';
						// }
						
						$total_payment_amount += $payment_amount;

					}
				}
				if($pardons->num_rows() > 0)
				{
					foreach ($pardons->result() as $pardons_key) {
						# code...
						$pardon_date = $pardons_key->pardon_date;
						$pardon_explode = explode('-', $pardon_date);
						$pardon_year = $pardon_explode[0];
						$pardon_month = $pardon_explode[1];
						$pardon_amount = $pardons_key->pardon_amount;

						if(($pardon_date > $invoice_date) &&  ($pardon_amount > 0))
						{
							$total_arrears -= $pardon_amount;
							// if($pardon_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($pardon_date)).' </td>
										<td>Pardon</td>
										<td></td>
										<td>'.number_format($pardon_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr> 
								';
							// }
							
							$total_pardon_amount += $pardon_amount;

						}
					}
				}
			}
		}

		$parent_invoice_date = $this->accounts_model->get_max_owners_invoice_date($rental_unit_id);
		// var_dump($parent_invoice_date); die();
		$account_invoice_amount = $this->accounts_model->get_invoice_owners_brought_forward($rental_unit_id,$parent_invoice_date);
		$account_paid_amount = $this->accounts_model->get_paid_owners_brought_forward($rental_unit_id,$parent_invoice_date);
		$account_todays_payment =  $this->accounts_model->get_today_owners_paid_current_forward($rental_unit_id,$parent_invoice_date);



		$total_brought_forward_account = $account_invoice_amount - $account_paid_amount + $starting_arreas;
		
		if($rental_unit_id == 2026)
		{
			// var_dump($account_paid_amount); die();

		}
		//display loan
		$result .= 
		'
			<tr>
				<th colspan="2">Total</th>
				<th>'.number_format($total_invoice_balance , 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<th>'.number_format($arrears_amount, 2).'</th>
				
			</tr> 
		';

		$response['total_arrears'] = $arrears_amount;
		$response['invoice_date'] = $invoice_date;
		$response['total_pardon_amount'] = $total_pardon_amount;
		$response['result'] = $result;
		$response['total_invoice_balance'] = $total_brought_forward_account;
		$response['total_payment_amount'] = $total_payment_amount;
		$response['parent_invoice_date'] = $parent_invoice_date;

		return $response;
	}

	public function get_billable_items($property_id,$charge_to)
	{
		$this->db->from('invoice_type,property_billing,billing_schedule');
		$this->db->select('*');
		$this->db->where('property_billing.billing_schedule_id = billing_schedule.billing_schedule_id AND invoice_type.invoice_type_id = property_billing.invoice_type_id AND property_billing.charge_to = '.$charge_to.' AND property_billing.property_billing_status = 1 AND property_billing.property_id = '.$property_id);

		$query = $this->db->get('');

		return $query;
	}

	public function get_today_tenants_paid_current_forward_reports($lease_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND payments.payment_status = 1 AND cancel = 0 AND (payments.payment_date >= "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_today_owners_paid_current_forward_reports($rental_unit_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND (home_owners_payments.payment_date >= "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_lease_deposit($lease_id)
	{
		$where = 'payments.lease_id = '.$lease_id.' AND  payments.payment_id = payment_item.payment_id AND payment_item.invoice_type_id = 13 ';
		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;

	}
	public function get_manager_percent($property_id)
	{
		$where = 'property_id ='.$property_id;
		$this->db->from('property');
		$this->db->select('manager_percent');
		$this->db->where($where);
		$query = $this->db->get();
		$manager_percent = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$manager_percent = $row->manager_percent;
		}
		return $manager_percent;

	}

}